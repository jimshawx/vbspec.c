﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmPoke : Form
	{
		// /*******************************************************************************
		// frmPoke.frm within vbSpec.vbp
		// 
		// Author: Miklos Muhi <miklos.muhi@bakonyi.de>
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		// *******************************************************************************
		// Cancel operation
		// *******************************************************************************
		private void cmdCancel_Click(object eventSender, EventArgs eventArgs)
		{

			// Unload the form
			Close();
		}

		private void cmdPeek_Click(object eventSender, EventArgs eventArgs)
		{
			int localpeekb() { int argaddr = Conversions.ToInteger(txtAddress.Text); var ret = modZ80.peekb(argaddr); return ret; }

			int localpeekb1() { int argaddr = Conversions.ToInteger(txtAddress.Text); var ret = modZ80.peekb(argaddr); return ret; }

			txtValue.Text = Strings.Trim(localpeekb1().ToString());
		}

		// *******************************************************************************
		// Poke memory
		// *******************************************************************************
		private void cmdPoke_Click(object eventSender, EventArgs eventArgs)
		{

			// Variables
			int lAddress, lValue;

			// Validation -- there must be a value for the Address
			if (string.IsNullOrEmpty(Strings.Trim(txtAddress.Text)))
			{
				// Report the problem
				Interaction.MsgBox("You must give an address to poke to!", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				// Set the focus
				txtAddress.Focus();
				// Leave this function
				return;
			}

			// Validation -- there must be a value to poke
			if (string.IsNullOrEmpty(Strings.Trim(txtValue.Text)))
			{
				// Report the problem
				Interaction.MsgBox("Missing a value to poke!", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				// Set the focus
				txtValue.Focus();
				// Leave this function
				return;
			}

			// Initialisation
			lAddress = -1;

			// Error handling
			lValue = -1;
			;

			// Convert the values
			lAddress = Conversions.ToInteger(txtAddress.Text);
			lValue = Conversions.ToInteger(txtValue.Text);

			// Poke value
			modZ80.pokeb(lAddress, lValue);

			// Reset controls
			txtAddress.Text = Constants.vbNullString;
			txtValue.Text = Constants.vbNullString;
			// Reset focus
			txtAddress.Focus();

			// There wasn't any problem
			return;
		POKE_ERROR:
			;


			// There can be only one problem: the conversions of the strings in long or byte failed
			// If the address-conversion failed
			if (lAddress == -1)
			{
				// Report the problem
				Interaction.MsgBox("Invalid address format!", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
			}
			// If the address-conversion was a success
			// If the address-conversion failed
			else if (lValue == -1)
			{
				// Report the problem
				Interaction.MsgBox("The value has an invalid format!", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
			}
			// I don't know this...
			else
			{
				// Report the problem
				Interaction.MsgBox("Unknown error!", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
			}
		}
	}
}