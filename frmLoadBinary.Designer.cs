﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmLoadBinary
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmLoadBinary() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdCancel.Name = "cmdCancel";
			_cmdOK.Name = "cmdOK";
			_txtAddr.Name = "txtAddr";
			_cmdOpen.Name = "cmdOpen";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _cmdCancel;

		public Button cmdCancel
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdCancel;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdCancel != null)
				{
					_cmdCancel.Click -= cmdCancel_Click;
				}

				_cmdCancel = value;
				if (_cmdCancel != null)
				{
					_cmdCancel.Click += cmdCancel_Click;
				}
			}
		}

		private Button _cmdOK;

		public Button cmdOK
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOK;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOK != null)
				{
					_cmdOK.Click -= cmdOK_Click;
				}

				_cmdOK = value;
				if (_cmdOK != null)
				{
					_cmdOK.Click += cmdOK_Click;
				}
			}
		}

		public ComboBox cboBase;
		private TextBox _txtAddr;

		public TextBox txtAddr
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtAddr;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtAddr != null)
				{
					_txtAddr.KeyPress -= txtAddr_KeyPress;
				}

				_txtAddr = value;
				if (_txtAddr != null)
				{
					_txtAddr.KeyPress += txtAddr_KeyPress;
				}
			}
		}

		private Button _cmdOpen;

		public Button cmdOpen
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOpen;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOpen != null)
				{
					_cmdOpen.Click -= cmdOpen_Click;
				}

				_cmdOpen = value;
				if (_cmdOpen != null)
				{
					_cmdOpen.Click += cmdOpen_Click;
				}
			}
		}

		public TextBox txtFile;
		public OpenFileDialog dlgCommonOpen;
		public Label Label2;
		public Label Label1;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmLoadBinary));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_cmdCancel = new Button();
			_cmdCancel.Click += new EventHandler(cmdCancel_Click);
			_cmdOK = new Button();
			_cmdOK.Click += new EventHandler(cmdOK_Click);
			cboBase = new ComboBox();
			_txtAddr = new TextBox();
			_txtAddr.KeyPress += new KeyPressEventHandler(txtAddr_KeyPress);
			_cmdOpen = new Button();
			_cmdOpen.Click += new EventHandler(cmdOpen_Click);
			txtFile = new TextBox();
			dlgCommonOpen = new OpenFileDialog();
			Label2 = new Label();
			Label1 = new Label();
			SuspendLayout();
			ToolTip1.Active = true;
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "Load Binary Data";
			ClientSize = new Size(375, 94);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmLoadBinary";
			_cmdCancel.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _cmdCancel;
			_cmdCancel.Text = "Cancel";
			_cmdCancel.Size = new Size(81, 25);
			_cmdCancel.Location = new Point(288, 64);
			_cmdCancel.TabIndex = 1;
			_cmdCancel.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdCancel.BackColor = SystemColors.Control;
			_cmdCancel.CausesValidation = true;
			_cmdCancel.Enabled = true;
			_cmdCancel.ForeColor = SystemColors.ControlText;
			_cmdCancel.Cursor = Cursors.Default;
			_cmdCancel.RightToLeft = RightToLeft.No;
			_cmdCancel.TabStop = true;
			_cmdCancel.Name = "_cmdCancel";
			_cmdOK.TextAlign = ContentAlignment.MiddleCenter;
			_cmdOK.Text = "OK";
			AcceptButton = _cmdOK;
			_cmdOK.Size = new Size(81, 25);
			_cmdOK.Location = new Point(200, 64);
			_cmdOK.TabIndex = 0;
			_cmdOK.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOK.BackColor = SystemColors.Control;
			_cmdOK.CausesValidation = true;
			_cmdOK.Enabled = true;
			_cmdOK.ForeColor = SystemColors.ControlText;
			_cmdOK.Cursor = Cursors.Default;
			_cmdOK.RightToLeft = RightToLeft.No;
			_cmdOK.TabStop = true;
			_cmdOK.Name = "_cmdOK";
			cboBase.Size = new Size(65, 21);
			cboBase.Location = new Point(168, 32);
			cboBase.DropDownStyle = ComboBoxStyle.DropDownList;
			cboBase.TabIndex = 7;
			cboBase.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			cboBase.BackColor = SystemColors.Window;
			cboBase.CausesValidation = true;
			cboBase.Enabled = true;
			cboBase.ForeColor = SystemColors.WindowText;
			cboBase.IntegralHeight = true;
			cboBase.Cursor = Cursors.Default;
			cboBase.RightToLeft = RightToLeft.No;
			cboBase.Sorted = false;
			cboBase.TabStop = true;
			cboBase.Visible = true;
			cboBase.Name = "cboBase";
			_txtAddr.AutoSize = false;
			_txtAddr.Size = new Size(73, 21);
			_txtAddr.Location = new Point(92, 32);
			_txtAddr.TabIndex = 6;
			_txtAddr.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtAddr.AcceptsReturn = true;
			_txtAddr.TextAlign = HorizontalAlignment.Left;
			_txtAddr.BackColor = SystemColors.Window;
			_txtAddr.CausesValidation = true;
			_txtAddr.Enabled = true;
			_txtAddr.ForeColor = SystemColors.WindowText;
			_txtAddr.HideSelection = true;
			_txtAddr.ReadOnly = false;
			_txtAddr.MaxLength = 0;
			_txtAddr.Cursor = Cursors.IBeam;
			_txtAddr.Multiline = false;
			_txtAddr.RightToLeft = RightToLeft.No;
			_txtAddr.ScrollBars = ScrollBars.None;
			_txtAddr.TabStop = true;
			_txtAddr.Visible = true;
			_txtAddr.BorderStyle = BorderStyle.Fixed3D;
			_txtAddr.Name = "_txtAddr";
			_cmdOpen.TextAlign = ContentAlignment.MiddleCenter;
			_cmdOpen.Text = "...";
			_cmdOpen.Size = new Size(25, 21);
			_cmdOpen.Location = new Point(344, 4);
			_cmdOpen.TabIndex = 4;
			_cmdOpen.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOpen.BackColor = SystemColors.Control;
			_cmdOpen.CausesValidation = true;
			_cmdOpen.Enabled = true;
			_cmdOpen.ForeColor = SystemColors.ControlText;
			_cmdOpen.Cursor = Cursors.Default;
			_cmdOpen.RightToLeft = RightToLeft.No;
			_cmdOpen.TabStop = true;
			_cmdOpen.Name = "_cmdOpen";
			txtFile.AutoSize = false;
			txtFile.Size = new Size(249, 21);
			txtFile.Location = new Point(92, 4);
			txtFile.TabIndex = 3;
			txtFile.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			txtFile.AcceptsReturn = true;
			txtFile.TextAlign = HorizontalAlignment.Left;
			txtFile.BackColor = SystemColors.Window;
			txtFile.CausesValidation = true;
			txtFile.Enabled = true;
			txtFile.ForeColor = SystemColors.WindowText;
			txtFile.HideSelection = true;
			txtFile.ReadOnly = false;
			txtFile.MaxLength = 0;
			txtFile.Cursor = Cursors.IBeam;
			txtFile.Multiline = false;
			txtFile.RightToLeft = RightToLeft.No;
			txtFile.ScrollBars = ScrollBars.None;
			txtFile.TabStop = true;
			txtFile.Visible = true;
			txtFile.BorderStyle = BorderStyle.Fixed3D;
			txtFile.Name = "txtFile";
			Label2.TextAlign = ContentAlignment.TopRight;
			Label2.Text = "Memory &Address:";
			Label2.Size = new Size(81, 13);
			Label2.Location = new Point(8, 36);
			Label2.TabIndex = 5;
			Label2.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Label2.BackColor = SystemColors.Control;
			Label2.Enabled = true;
			Label2.ForeColor = SystemColors.ControlText;
			Label2.Cursor = Cursors.Default;
			Label2.RightToLeft = RightToLeft.No;
			Label2.UseMnemonic = true;
			Label2.Visible = true;
			Label2.AutoSize = false;
			Label2.BorderStyle = BorderStyle.None;
			Label2.Name = "Label2";
			Label1.TextAlign = ContentAlignment.TopRight;
			Label1.Text = "&Filename:";
			Label1.Size = new Size(53, 17);
			Label1.Location = new Point(36, 8);
			Label1.TabIndex = 2;
			Label1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Label1.BackColor = SystemColors.Control;
			Label1.Enabled = true;
			Label1.ForeColor = SystemColors.ControlText;
			Label1.Cursor = Cursors.Default;
			Label1.RightToLeft = RightToLeft.No;
			Label1.UseMnemonic = true;
			Label1.Visible = true;
			Label1.AutoSize = false;
			Label1.BorderStyle = BorderStyle.None;
			Label1.Name = "Label1";
			Controls.Add(_cmdCancel);
			Controls.Add(_cmdOK);
			Controls.Add(cboBase);
			Controls.Add(_txtAddr);
			Controls.Add(_cmdOpen);
			Controls.Add(txtFile);
			Controls.Add(Label2);
			Controls.Add(Label1);
			Load += new EventHandler(frmLoadBinary_Load);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}