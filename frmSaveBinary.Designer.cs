﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmSaveBinary
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmSaveBinary() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_txtLength.Name = "txtLength";
			_cmdOpen.Name = "cmdOpen";
			_txtAddr.Name = "txtAddr";
			_cmdOK.Name = "cmdOK";
			_cmdCancel.Name = "cmdCancel";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		public ComboBox _cboBase_1;
		private TextBox _txtLength;

		public TextBox txtLength
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtLength;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtLength != null)
				{
					_txtLength.KeyPress -= txtLength_KeyPress;
				}

				_txtLength = value;
				if (_txtLength != null)
				{
					_txtLength.KeyPress += txtLength_KeyPress;
				}
			}
		}

		public TextBox txtFile;
		private Button _cmdOpen;

		public Button cmdOpen
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOpen;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOpen != null)
				{
					_cmdOpen.Click -= cmdOpen_Click;
				}

				_cmdOpen = value;
				if (_cmdOpen != null)
				{
					_cmdOpen.Click += cmdOpen_Click;
				}
			}
		}

		private TextBox _txtAddr;

		public TextBox txtAddr
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtAddr;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtAddr != null)
				{
					_txtAddr.KeyPress -= txtAddr_KeyPress;
				}

				_txtAddr = value;
				if (_txtAddr != null)
				{
					_txtAddr.KeyPress += txtAddr_KeyPress;
				}
			}
		}

		public ComboBox _cboBase_0;
		private Button _cmdOK;

		public Button cmdOK
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOK;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOK != null)
				{
					_cmdOK.Click -= cmdOK_Click;
				}

				_cmdOK = value;
				if (_cmdOK != null)
				{
					_cmdOK.Click += cmdOK_Click;
				}
			}
		}

		private Button _cmdCancel;

		public Button cmdCancel
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdCancel;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdCancel != null)
				{
					_cmdCancel.Click -= cmdCancel_Click;
				}

				_cmdCancel = value;
				if (_cmdCancel != null)
				{
					_cmdCancel.Click += cmdCancel_Click;
				}
			}
		}

		public OpenFileDialog dlgCommonOpen;
		public SaveFileDialog dlgCommonSave;
		public Label Label3;
		public Label Label1;
		public Label Label2;
		public Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray cboBase;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmSaveBinary));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_cboBase_1 = new ComboBox();
			_txtLength = new TextBox();
			_txtLength.KeyPress += new KeyPressEventHandler(txtLength_KeyPress);
			txtFile = new TextBox();
			_cmdOpen = new Button();
			_cmdOpen.Click += new EventHandler(cmdOpen_Click);
			_txtAddr = new TextBox();
			_txtAddr.KeyPress += new KeyPressEventHandler(txtAddr_KeyPress);
			_cboBase_0 = new ComboBox();
			_cmdOK = new Button();
			_cmdOK.Click += new EventHandler(cmdOK_Click);
			_cmdCancel = new Button();
			_cmdCancel.Click += new EventHandler(cmdCancel_Click);
			dlgCommonOpen = new OpenFileDialog();
			dlgCommonSave = new SaveFileDialog();
			Label3 = new Label();
			Label1 = new Label();
			Label2 = new Label();
			cboBase = new Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(components);
			SuspendLayout();
			ToolTip1.Active = true;
			((System.ComponentModel.ISupportInitialize)cboBase).BeginInit();
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "Save Binary Data";
			ClientSize = new Size(421, 92);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmSaveBinary";
			_cboBase_1.Size = new Size(65, 21);
			_cboBase_1.Location = new Point(352, 32);
			_cboBase_1.DropDownStyle = ComboBoxStyle.DropDownList;
			_cboBase_1.TabIndex = 10;
			_cboBase_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cboBase_1.BackColor = SystemColors.Window;
			_cboBase_1.CausesValidation = true;
			_cboBase_1.Enabled = true;
			_cboBase_1.ForeColor = SystemColors.WindowText;
			_cboBase_1.IntegralHeight = true;
			_cboBase_1.Cursor = Cursors.Default;
			_cboBase_1.RightToLeft = RightToLeft.No;
			_cboBase_1.Sorted = false;
			_cboBase_1.TabStop = true;
			_cboBase_1.Visible = true;
			_cboBase_1.Name = "_cboBase_1";
			_txtLength.AutoSize = false;
			_txtLength.Size = new Size(73, 21);
			_txtLength.Location = new Point(276, 32);
			_txtLength.TabIndex = 9;
			_txtLength.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtLength.AcceptsReturn = true;
			_txtLength.TextAlign = HorizontalAlignment.Left;
			_txtLength.BackColor = SystemColors.Window;
			_txtLength.CausesValidation = true;
			_txtLength.Enabled = true;
			_txtLength.ForeColor = SystemColors.WindowText;
			_txtLength.HideSelection = true;
			_txtLength.ReadOnly = false;
			_txtLength.MaxLength = 0;
			_txtLength.Cursor = Cursors.IBeam;
			_txtLength.Multiline = false;
			_txtLength.RightToLeft = RightToLeft.No;
			_txtLength.ScrollBars = ScrollBars.None;
			_txtLength.TabStop = true;
			_txtLength.Visible = true;
			_txtLength.BorderStyle = BorderStyle.Fixed3D;
			_txtLength.Name = "_txtLength";
			txtFile.AutoSize = false;
			txtFile.Size = new Size(301, 21);
			txtFile.Location = new Point(88, 4);
			txtFile.TabIndex = 3;
			txtFile.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			txtFile.AcceptsReturn = true;
			txtFile.TextAlign = HorizontalAlignment.Left;
			txtFile.BackColor = SystemColors.Window;
			txtFile.CausesValidation = true;
			txtFile.Enabled = true;
			txtFile.ForeColor = SystemColors.WindowText;
			txtFile.HideSelection = true;
			txtFile.ReadOnly = false;
			txtFile.MaxLength = 0;
			txtFile.Cursor = Cursors.IBeam;
			txtFile.Multiline = false;
			txtFile.RightToLeft = RightToLeft.No;
			txtFile.ScrollBars = ScrollBars.None;
			txtFile.TabStop = true;
			txtFile.Visible = true;
			txtFile.BorderStyle = BorderStyle.Fixed3D;
			txtFile.Name = "txtFile";
			_cmdOpen.TextAlign = ContentAlignment.MiddleCenter;
			_cmdOpen.Text = "...";
			_cmdOpen.Size = new Size(25, 21);
			_cmdOpen.Location = new Point(392, 4);
			_cmdOpen.TabIndex = 4;
			_cmdOpen.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOpen.BackColor = SystemColors.Control;
			_cmdOpen.CausesValidation = true;
			_cmdOpen.Enabled = true;
			_cmdOpen.ForeColor = SystemColors.ControlText;
			_cmdOpen.Cursor = Cursors.Default;
			_cmdOpen.RightToLeft = RightToLeft.No;
			_cmdOpen.TabStop = true;
			_cmdOpen.Name = "_cmdOpen";
			_txtAddr.AutoSize = false;
			_txtAddr.Size = new Size(73, 21);
			_txtAddr.Location = new Point(88, 32);
			_txtAddr.TabIndex = 6;
			_txtAddr.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtAddr.AcceptsReturn = true;
			_txtAddr.TextAlign = HorizontalAlignment.Left;
			_txtAddr.BackColor = SystemColors.Window;
			_txtAddr.CausesValidation = true;
			_txtAddr.Enabled = true;
			_txtAddr.ForeColor = SystemColors.WindowText;
			_txtAddr.HideSelection = true;
			_txtAddr.ReadOnly = false;
			_txtAddr.MaxLength = 0;
			_txtAddr.Cursor = Cursors.IBeam;
			_txtAddr.Multiline = false;
			_txtAddr.RightToLeft = RightToLeft.No;
			_txtAddr.ScrollBars = ScrollBars.None;
			_txtAddr.TabStop = true;
			_txtAddr.Visible = true;
			_txtAddr.BorderStyle = BorderStyle.Fixed3D;
			_txtAddr.Name = "_txtAddr";
			_cboBase_0.Size = new Size(65, 21);
			_cboBase_0.Location = new Point(164, 32);
			_cboBase_0.DropDownStyle = ComboBoxStyle.DropDownList;
			_cboBase_0.TabIndex = 7;
			_cboBase_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cboBase_0.BackColor = SystemColors.Window;
			_cboBase_0.CausesValidation = true;
			_cboBase_0.Enabled = true;
			_cboBase_0.ForeColor = SystemColors.WindowText;
			_cboBase_0.IntegralHeight = true;
			_cboBase_0.Cursor = Cursors.Default;
			_cboBase_0.RightToLeft = RightToLeft.No;
			_cboBase_0.Sorted = false;
			_cboBase_0.TabStop = true;
			_cboBase_0.Visible = true;
			_cboBase_0.Name = "_cboBase_0";
			_cmdOK.TextAlign = ContentAlignment.MiddleCenter;
			_cmdOK.Text = "OK";
			AcceptButton = _cmdOK;
			_cmdOK.Size = new Size(81, 25);
			_cmdOK.Location = new Point(248, 60);
			_cmdOK.TabIndex = 0;
			_cmdOK.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOK.BackColor = SystemColors.Control;
			_cmdOK.CausesValidation = true;
			_cmdOK.Enabled = true;
			_cmdOK.ForeColor = SystemColors.ControlText;
			_cmdOK.Cursor = Cursors.Default;
			_cmdOK.RightToLeft = RightToLeft.No;
			_cmdOK.TabStop = true;
			_cmdOK.Name = "_cmdOK";
			_cmdCancel.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _cmdCancel;
			_cmdCancel.Text = "Cancel";
			_cmdCancel.Size = new Size(81, 25);
			_cmdCancel.Location = new Point(336, 60);
			_cmdCancel.TabIndex = 1;
			_cmdCancel.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdCancel.BackColor = SystemColors.Control;
			_cmdCancel.CausesValidation = true;
			_cmdCancel.Enabled = true;
			_cmdCancel.ForeColor = SystemColors.ControlText;
			_cmdCancel.Cursor = Cursors.Default;
			_cmdCancel.RightToLeft = RightToLeft.No;
			_cmdCancel.TabStop = true;
			_cmdCancel.Name = "_cmdCancel";
			Label3.TextAlign = ContentAlignment.TopRight;
			Label3.Text = "&Length:";
			Label3.Size = new Size(41, 13);
			Label3.Location = new Point(232, 36);
			Label3.TabIndex = 8;
			Label3.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Label3.BackColor = SystemColors.Control;
			Label3.Enabled = true;
			Label3.ForeColor = SystemColors.ControlText;
			Label3.Cursor = Cursors.Default;
			Label3.RightToLeft = RightToLeft.No;
			Label3.UseMnemonic = true;
			Label3.Visible = true;
			Label3.AutoSize = false;
			Label3.BorderStyle = BorderStyle.None;
			Label3.Name = "Label3";
			Label1.TextAlign = ContentAlignment.TopRight;
			Label1.Text = "&Filename:";
			Label1.Size = new Size(53, 17);
			Label1.Location = new Point(32, 8);
			Label1.TabIndex = 2;
			Label1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Label1.BackColor = SystemColors.Control;
			Label1.Enabled = true;
			Label1.ForeColor = SystemColors.ControlText;
			Label1.Cursor = Cursors.Default;
			Label1.RightToLeft = RightToLeft.No;
			Label1.UseMnemonic = true;
			Label1.Visible = true;
			Label1.AutoSize = false;
			Label1.BorderStyle = BorderStyle.None;
			Label1.Name = "Label1";
			Label2.TextAlign = ContentAlignment.TopRight;
			Label2.Text = "Memory &Address:";
			Label2.Size = new Size(81, 13);
			Label2.Location = new Point(4, 36);
			Label2.TabIndex = 5;
			Label2.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Label2.BackColor = SystemColors.Control;
			Label2.Enabled = true;
			Label2.ForeColor = SystemColors.ControlText;
			Label2.Cursor = Cursors.Default;
			Label2.RightToLeft = RightToLeft.No;
			Label2.UseMnemonic = true;
			Label2.Visible = true;
			Label2.AutoSize = false;
			Label2.BorderStyle = BorderStyle.None;
			Label2.Name = "Label2";
			Controls.Add(_cboBase_1);
			Controls.Add(_txtLength);
			Controls.Add(txtFile);
			Controls.Add(_cmdOpen);
			Controls.Add(_txtAddr);
			Controls.Add(_cboBase_0);
			Controls.Add(_cmdOK);
			Controls.Add(_cmdCancel);
			Controls.Add(Label3);
			Controls.Add(Label1);
			Controls.Add(Label2);
			cboBase.SetIndex(_cboBase_1, Conversions.ToShort(1));
			cboBase.SetIndex(_cboBase_0, Conversions.ToShort(0));
			((System.ComponentModel.ISupportInitialize)cboBase).EndInit();
			Load += new EventHandler(frmSaveBinary_Load);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}