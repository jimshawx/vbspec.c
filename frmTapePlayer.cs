﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmTapePlayer : Form
	{
		// /*******************************************************************************
		// frmTapePlayer.frm within vbSpec.vbp
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2000 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		public void UpdateCurBlock()
		{
			if (lstBlocks.Items.Count > modTZX.TZXCurBlock)
				lstBlocks.SelectedIndex = modTZX.TZXCurBlock;
		}

		public void UpdateTapeList()
		{
			int l;
			int lType = default, lLen = default;
			var sText = default(string);
			lstBlocks.Items.Clear();
			var loopTo = modTZX.TZXNumBlocks - 1;
			for (l = 0; l <= loopTo; l++)
			{
				modTZX.GetTZXBlockInfo(ref l, ref lType, ref sText, ref lLen);
				lstBlocks.Items.Add(Conversion.Hex(lType) + " - " + sText);
			}

			if (lstBlocks.Items.Count > modTZX.TZXCurBlock)
				lstBlocks.SelectedIndex = modTZX.TZXCurBlock;
		}

		private void cmdNext_Click(object eventSender, EventArgs eventArgs)
		{
			if (Conversions.ToBoolean(modTZX.gbTZXInserted & Conversions.ToInteger(modTZX.TZXCurBlock < modTZX.TZXNumBlocks - 1)))
			{
				int argBlockNum = modTZX.TZXCurBlock + 1;
				modTZX.SetCurTZXBlock(ref argBlockNum);
			}

			My.MyProject.Forms.frmMainWnd.Activate();
		}

		private void cmdPlay_Click(object eventSender, EventArgs eventArgs)
		{
			if (Conversions.ToBoolean(modTZX.gbTZXInserted))
				modTZX.StartTape();
			My.MyProject.Forms.frmMainWnd.Activate();
		}

		private void cmdPrev_Click(object eventSender, EventArgs eventArgs)
		{
			if (Conversions.ToBoolean(modTZX.gbTZXInserted & Conversions.ToInteger(modTZX.TZXCurBlock > 0)))
			{
				int argBlockNum = modTZX.TZXCurBlock - 1;
				modTZX.SetCurTZXBlock(ref argBlockNum);
			}

			My.MyProject.Forms.frmMainWnd.Activate();
		}

		private void cmdRewind_Click(object eventSender, EventArgs eventArgs)
		{
			if (Conversions.ToBoolean(modTZX.gbTZXInserted))
			{
				int argBlockNum = 0;
				modTZX.SetCurTZXBlock(ref argBlockNum);
			}

			My.MyProject.Forms.frmMainWnd.Activate();
		}

		private void cmdStop_Click(object eventSender, EventArgs eventArgs)
		{
			if (Conversions.ToBoolean(modTZX.gbTZXInserted))
				modTZX.StopTape();
			My.MyProject.Forms.frmMainWnd.Activate();
		}


		// UPGRADE_WARNING: Form event frmTapePlayer.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		private void frmTapePlayer_Activated(object eventSender, EventArgs eventArgs)
		{
			UpdateCurBlock();
		}

		private void frmTapePlayer_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (Conversions.ToBoolean(Shift & 4))
				return;
			bool argdown = true;
			modSpectrum.doKey(ref argdown, ref KeyCode, ref Shift);
		}

		private void frmTapePlayer_KeyUp(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			bool argdown = false;
			modSpectrum.doKey(ref argdown, ref KeyCode, ref Shift);
		}

		private void frmTapePlayer_Load(object eventSender, EventArgs eventArgs)
		{
			int X, y;
			X = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "TapeWndX", "-1"));
			y = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "TapeWndY", "-1"));
			if (X >= 0 & X <= Screen.PrimaryScreen.Bounds.Width - 16)
			{
				Left = X;
			}

			if (y >= 0 & y <= Screen.PrimaryScreen.Bounds.Height - 16)
			{
				Top = y;
			}
		}

		private void frmTapePlayer_FormClosed(object eventSender, FormClosedEventArgs eventArgs)
		{
			Interaction.SaveSetting("Grok", "vbSpec", "TapeWndX", Left.ToString());
			Interaction.SaveSetting("Grok", "vbSpec", "TapeWndY", Top.ToString());
			My.MyProject.Forms.frmMainWnd.mnuOptions[4].Checked = false;
		}

		// UPGRADE_WARNING: Event lstBlocks.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void lstBlocks_SelectedIndexChanged(object eventSender, EventArgs eventArgs)
		{
			UpdateCurBlock();
		}
	}
}