﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmOptions
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmOptions() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdOK.Name = "cmdOK";
			_cmdCancel.Name = "cmdCancel";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		public Button _cmdDefine_0;
		public Button _cmdDefine_1;
		public ComboBox _cmbZXJoystick_1;
		public ComboBox _cmbFire_1;
		public ComboBox _cmbZXJoystick_0;
		public ComboBox _cmbFire_0;
		public Label _lblZXJoystick_1;
		public Label _lblFire_1;
		public Label _lblZXJoystick_0;
		public Label _lblFire_0;
		public Panel _picFrame_3;
		public ComboBox cboMouseType;
		public RadioButton optMouseGlobal;
		public RadioButton optMouseVB;
		public GroupBox Frame1;
		public Label Label1;
		public Panel _picFrame_2;
		public CheckBox chkSEBasic;
		public RadioButton optSpeed200;
		public RadioButton optSpeed50;
		public RadioButton optSpeed100;
		public RadioButton optSpeedFastest;
		public GroupBox fraStd;
		public CheckBox chkSound;
		public ComboBox cboModel;
		public Label lblStatic;
		public Panel _picFrame_1;
		private Button _cmdOK;

		public Button cmdOK
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOK;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOK != null)
				{
					_cmdOK.Click -= cmdOK_Click;
				}

				_cmdOK = value;
				if (_cmdOK != null)
				{
					_cmdOK.Click += cmdOK_Click;
				}
			}
		}

		private Button _cmdCancel;

		public Button cmdCancel
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdCancel;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdCancel != null)
				{
					_cmdCancel.Click -= cmdCancel_Click;
				}

				_cmdCancel = value;
				if (_cmdCancel != null)
				{
					_cmdCancel.Click += cmdCancel_Click;
				}
			}
		}
		// Public WithEvents ts1 As AxMSComctlLib.AxTabStrip
		public Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray cmbFire;
		private Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray _cmbZXJoystick;

		public Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray cmbZXJoystick
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmbZXJoystick;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmbZXJoystick != null)
				{
					_cmbZXJoystick.SelectedIndexChanged -= cmbZXJoystick_SelectedIndexChanged;
				}

				_cmbZXJoystick = value;
				if (_cmbZXJoystick != null)
				{
					_cmbZXJoystick.SelectedIndexChanged += cmbZXJoystick_SelectedIndexChanged;
				}
			}
		}

		private Microsoft.VisualBasic.Compatibility.VB6.ButtonArray _cmdDefine;

		public Microsoft.VisualBasic.Compatibility.VB6.ButtonArray cmdDefine
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdDefine;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdDefine != null)
				{
					_cmdDefine.Click -= cmdDefine_Click;
				}

				_cmdDefine = value;
				if (_cmdDefine != null)
				{
					_cmdDefine.Click += cmdDefine_Click;
				}
			}
		}

		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblFire;
		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblZXJoystick;
		public Microsoft.VisualBasic.Compatibility.VB6.PanelArray picFrame;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmOptions));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_picFrame_3 = new Panel();
			_cmdDefine_0 = new Button();
			_cmdDefine_1 = new Button();
			_cmbZXJoystick_1 = new ComboBox();
			_cmbFire_1 = new ComboBox();
			_cmbZXJoystick_0 = new ComboBox();
			_cmbFire_0 = new ComboBox();
			_lblZXJoystick_1 = new Label();
			_lblFire_1 = new Label();
			_lblZXJoystick_0 = new Label();
			_lblFire_0 = new Label();
			_picFrame_2 = new Panel();
			cboMouseType = new ComboBox();
			Frame1 = new GroupBox();
			optMouseGlobal = new RadioButton();
			optMouseVB = new RadioButton();
			Label1 = new Label();
			_picFrame_1 = new Panel();
			chkSEBasic = new CheckBox();
			fraStd = new GroupBox();
			optSpeed200 = new RadioButton();
			optSpeed50 = new RadioButton();
			optSpeed100 = new RadioButton();
			optSpeedFastest = new RadioButton();
			chkSound = new CheckBox();
			cboModel = new ComboBox();
			lblStatic = new Label();
			_cmdOK = new Button();
			_cmdOK.Click += new EventHandler(cmdOK_Click);
			_cmdCancel = new Button();
			_cmdCancel.Click += new EventHandler(cmdCancel_Click);
			// Me.ts1 = New AxMSComctlLib.AxTabStrip
			cmbFire = new Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(components);
			_cmbZXJoystick = new Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(components);
			_cmbZXJoystick.SelectedIndexChanged += new EventHandler(cmbZXJoystick_SelectedIndexChanged);
			_cmdDefine = new Microsoft.VisualBasic.Compatibility.VB6.ButtonArray(components);
			_cmdDefine.Click += new EventHandler(cmdDefine_Click);
			lblFire = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			lblZXJoystick = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			picFrame = new Microsoft.VisualBasic.Compatibility.VB6.PanelArray(components);
			_picFrame_3.SuspendLayout();
			_picFrame_2.SuspendLayout();
			Frame1.SuspendLayout();
			_picFrame_1.SuspendLayout();
			fraStd.SuspendLayout();
			SuspendLayout();
			ToolTip1.Active = true;
			// CType(Me.ts1, System.ComponentModel.ISupportInitialize).BeginInit()
			((System.ComponentModel.ISupportInitialize)cmbFire).BeginInit();
			((System.ComponentModel.ISupportInitialize)_cmbZXJoystick).BeginInit();
			((System.ComponentModel.ISupportInitialize)_cmdDefine).BeginInit();
			((System.ComponentModel.ISupportInitialize)lblFire).BeginInit();
			((System.ComponentModel.ISupportInitialize)lblZXJoystick).BeginInit();
			((System.ComponentModel.ISupportInitialize)picFrame).BeginInit();
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "General Settings";
			ClientSize = new Size(355, 263);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmOptions";
			_picFrame_3.Size = new Size(337, 185);
			_picFrame_3.Location = new Point(8, 32);
			_picFrame_3.TabIndex = 19;
			_picFrame_3.TabStop = false;
			_picFrame_3.Visible = false;
			_picFrame_3.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_picFrame_3.Dock = DockStyle.None;
			_picFrame_3.BackColor = SystemColors.Control;
			_picFrame_3.CausesValidation = true;
			_picFrame_3.Enabled = true;
			_picFrame_3.ForeColor = SystemColors.ControlText;
			_picFrame_3.Cursor = Cursors.Default;
			_picFrame_3.RightToLeft = RightToLeft.No;
			_picFrame_3.BorderStyle = BorderStyle.None;
			_picFrame_3.Name = "_picFrame_3";
			_cmdDefine_0.TextAlign = ContentAlignment.MiddleCenter;
			_cmdDefine_0.Text = "Define...";
			_cmdDefine_0.Size = new Size(73, 25);
			_cmdDefine_0.Location = new Point(256, 52);
			_cmdDefine_0.TabIndex = 24;
			_cmdDefine_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdDefine_0.BackColor = SystemColors.Control;
			_cmdDefine_0.CausesValidation = true;
			_cmdDefine_0.Enabled = true;
			_cmdDefine_0.ForeColor = SystemColors.ControlText;
			_cmdDefine_0.Cursor = Cursors.Default;
			_cmdDefine_0.RightToLeft = RightToLeft.No;
			_cmdDefine_0.TabStop = true;
			_cmdDefine_0.Name = "_cmdDefine_0";
			_cmdDefine_1.TextAlign = ContentAlignment.MiddleCenter;
			_cmdDefine_1.Text = "Define...";
			_cmdDefine_1.Size = new Size(73, 25);
			_cmdDefine_1.Location = new Point(256, 135);
			_cmdDefine_1.TabIndex = 29;
			_cmdDefine_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdDefine_1.BackColor = SystemColors.Control;
			_cmdDefine_1.CausesValidation = true;
			_cmdDefine_1.Enabled = true;
			_cmdDefine_1.ForeColor = SystemColors.ControlText;
			_cmdDefine_1.Cursor = Cursors.Default;
			_cmdDefine_1.RightToLeft = RightToLeft.No;
			_cmdDefine_1.TabStop = true;
			_cmdDefine_1.Name = "_cmdDefine_1";
			_cmbZXJoystick_1.Size = new Size(189, 21);
			_cmbZXJoystick_1.Location = new Point(140, 87);
			_cmbZXJoystick_1.DropDownStyle = ComboBoxStyle.DropDownList;
			_cmbZXJoystick_1.TabIndex = 26;
			_cmbZXJoystick_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmbZXJoystick_1.BackColor = SystemColors.Window;
			_cmbZXJoystick_1.CausesValidation = true;
			_cmbZXJoystick_1.Enabled = true;
			_cmbZXJoystick_1.ForeColor = SystemColors.WindowText;
			_cmbZXJoystick_1.IntegralHeight = true;
			_cmbZXJoystick_1.Cursor = Cursors.Default;
			_cmbZXJoystick_1.RightToLeft = RightToLeft.No;
			_cmbZXJoystick_1.Sorted = false;
			_cmbZXJoystick_1.TabStop = true;
			_cmbZXJoystick_1.Visible = true;
			_cmbZXJoystick_1.Name = "_cmbZXJoystick_1";
			_cmbFire_1.Size = new Size(189, 21);
			_cmbFire_1.Location = new Point(140, 111);
			_cmbFire_1.DropDownStyle = ComboBoxStyle.DropDownList;
			_cmbFire_1.TabIndex = 28;
			_cmbFire_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmbFire_1.BackColor = SystemColors.Window;
			_cmbFire_1.CausesValidation = true;
			_cmbFire_1.Enabled = true;
			_cmbFire_1.ForeColor = SystemColors.WindowText;
			_cmbFire_1.IntegralHeight = true;
			_cmbFire_1.Cursor = Cursors.Default;
			_cmbFire_1.RightToLeft = RightToLeft.No;
			_cmbFire_1.Sorted = false;
			_cmbFire_1.TabStop = true;
			_cmbFire_1.Visible = true;
			_cmbFire_1.Name = "_cmbFire_1";
			_cmbZXJoystick_0.Size = new Size(189, 21);
			_cmbZXJoystick_0.Location = new Point(140, 4);
			_cmbZXJoystick_0.DropDownStyle = ComboBoxStyle.DropDownList;
			_cmbZXJoystick_0.TabIndex = 21;
			_cmbZXJoystick_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmbZXJoystick_0.BackColor = SystemColors.Window;
			_cmbZXJoystick_0.CausesValidation = true;
			_cmbZXJoystick_0.Enabled = true;
			_cmbZXJoystick_0.ForeColor = SystemColors.WindowText;
			_cmbZXJoystick_0.IntegralHeight = true;
			_cmbZXJoystick_0.Cursor = Cursors.Default;
			_cmbZXJoystick_0.RightToLeft = RightToLeft.No;
			_cmbZXJoystick_0.Sorted = false;
			_cmbZXJoystick_0.TabStop = true;
			_cmbZXJoystick_0.Visible = true;
			_cmbZXJoystick_0.Name = "_cmbZXJoystick_0";
			_cmbFire_0.Size = new Size(189, 21);
			_cmbFire_0.Location = new Point(140, 28);
			_cmbFire_0.DropDownStyle = ComboBoxStyle.DropDownList;
			_cmbFire_0.TabIndex = 23;
			_cmbFire_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmbFire_0.BackColor = SystemColors.Window;
			_cmbFire_0.CausesValidation = true;
			_cmbFire_0.Enabled = true;
			_cmbFire_0.ForeColor = SystemColors.WindowText;
			_cmbFire_0.IntegralHeight = true;
			_cmbFire_0.Cursor = Cursors.Default;
			_cmbFire_0.RightToLeft = RightToLeft.No;
			_cmbFire_0.Sorted = false;
			_cmbFire_0.TabStop = true;
			_cmbFire_0.Visible = true;
			_cmbFire_0.Name = "_cmbFire_0";
			_lblZXJoystick_1.TextAlign = ContentAlignment.TopRight;
			_lblZXJoystick_1.Text = "Joystick 2 Emulates:";
			_lblZXJoystick_1.Size = new Size(137, 14);
			_lblZXJoystick_1.Location = new Point(0, 91);
			_lblZXJoystick_1.TabIndex = 25;
			_lblZXJoystick_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblZXJoystick_1.BackColor = SystemColors.Control;
			_lblZXJoystick_1.Enabled = true;
			_lblZXJoystick_1.ForeColor = SystemColors.ControlText;
			_lblZXJoystick_1.Cursor = Cursors.Default;
			_lblZXJoystick_1.RightToLeft = RightToLeft.No;
			_lblZXJoystick_1.UseMnemonic = true;
			_lblZXJoystick_1.Visible = true;
			_lblZXJoystick_1.AutoSize = false;
			_lblZXJoystick_1.BorderStyle = BorderStyle.None;
			_lblZXJoystick_1.Name = "_lblZXJoystick_1";
			_lblFire_1.TextAlign = ContentAlignment.TopRight;
			_lblFire_1.Text = "Joystick 2 Fire Button:";
			_lblFire_1.Size = new Size(137, 14);
			_lblFire_1.Location = new Point(0, 115);
			_lblFire_1.TabIndex = 27;
			_lblFire_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblFire_1.BackColor = SystemColors.Control;
			_lblFire_1.Enabled = true;
			_lblFire_1.ForeColor = SystemColors.ControlText;
			_lblFire_1.Cursor = Cursors.Default;
			_lblFire_1.RightToLeft = RightToLeft.No;
			_lblFire_1.UseMnemonic = true;
			_lblFire_1.Visible = true;
			_lblFire_1.AutoSize = false;
			_lblFire_1.BorderStyle = BorderStyle.None;
			_lblFire_1.Name = "_lblFire_1";
			_lblZXJoystick_0.TextAlign = ContentAlignment.TopRight;
			_lblZXJoystick_0.Text = "Joystick 1 Emulates:";
			_lblZXJoystick_0.Size = new Size(137, 14);
			_lblZXJoystick_0.Location = new Point(0, 8);
			_lblZXJoystick_0.TabIndex = 20;
			_lblZXJoystick_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblZXJoystick_0.BackColor = SystemColors.Control;
			_lblZXJoystick_0.Enabled = true;
			_lblZXJoystick_0.ForeColor = SystemColors.ControlText;
			_lblZXJoystick_0.Cursor = Cursors.Default;
			_lblZXJoystick_0.RightToLeft = RightToLeft.No;
			_lblZXJoystick_0.UseMnemonic = true;
			_lblZXJoystick_0.Visible = true;
			_lblZXJoystick_0.AutoSize = false;
			_lblZXJoystick_0.BorderStyle = BorderStyle.None;
			_lblZXJoystick_0.Name = "_lblZXJoystick_0";
			_lblFire_0.TextAlign = ContentAlignment.TopRight;
			_lblFire_0.Text = "Joystick 1 Fire Button:";
			_lblFire_0.Size = new Size(137, 14);
			_lblFire_0.Location = new Point(0, 32);
			_lblFire_0.TabIndex = 22;
			_lblFire_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblFire_0.BackColor = SystemColors.Control;
			_lblFire_0.Enabled = true;
			_lblFire_0.ForeColor = SystemColors.ControlText;
			_lblFire_0.Cursor = Cursors.Default;
			_lblFire_0.RightToLeft = RightToLeft.No;
			_lblFire_0.UseMnemonic = true;
			_lblFire_0.Visible = true;
			_lblFire_0.AutoSize = false;
			_lblFire_0.BorderStyle = BorderStyle.None;
			_lblFire_0.Name = "_lblFire_0";
			_picFrame_2.Size = new Size(337, 185);
			_picFrame_2.Location = new Point(8, 32);
			_picFrame_2.TabIndex = 18;
			_picFrame_2.TabStop = false;
			_picFrame_2.Visible = false;
			_picFrame_2.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_picFrame_2.Dock = DockStyle.None;
			_picFrame_2.BackColor = SystemColors.Control;
			_picFrame_2.CausesValidation = true;
			_picFrame_2.Enabled = true;
			_picFrame_2.ForeColor = SystemColors.ControlText;
			_picFrame_2.Cursor = Cursors.Default;
			_picFrame_2.RightToLeft = RightToLeft.No;
			_picFrame_2.BorderStyle = BorderStyle.None;
			_picFrame_2.Name = "_picFrame_2";
			cboMouseType.Size = new Size(189, 21);
			cboMouseType.Location = new Point(140, 4);
			cboMouseType.Items.AddRange(new object[] { "Nothing", "Kempston Mouse Interface", "Amiga Mouse in Kempston Joyport" });
			cboMouseType.DropDownStyle = ComboBoxStyle.DropDownList;
			cboMouseType.TabIndex = 13;
			cboMouseType.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			cboMouseType.BackColor = SystemColors.Window;
			cboMouseType.CausesValidation = true;
			cboMouseType.Enabled = true;
			cboMouseType.ForeColor = SystemColors.WindowText;
			cboMouseType.IntegralHeight = true;
			cboMouseType.Cursor = Cursors.Default;
			cboMouseType.RightToLeft = RightToLeft.No;
			cboMouseType.Sorted = false;
			cboMouseType.TabStop = true;
			cboMouseType.Visible = true;
			cboMouseType.Name = "cboMouseType";
			Frame1.Text = "Respond to Mouse Buttons";
			Frame1.Size = new Size(321, 65);
			Frame1.Location = new Point(8, 32);
			Frame1.TabIndex = 14;
			Frame1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Frame1.BackColor = SystemColors.Control;
			Frame1.Enabled = true;
			Frame1.ForeColor = SystemColors.ControlText;
			Frame1.RightToLeft = RightToLeft.No;
			Frame1.Visible = true;
			Frame1.Padding = new Padding(0);
			Frame1.Name = "Frame1";
			optMouseGlobal.TextAlign = ContentAlignment.MiddleLeft;
			optMouseGlobal.Text = "&Always";
			optMouseGlobal.Size = new Size(145, 13);
			optMouseGlobal.Location = new Point(12, 40);
			optMouseGlobal.TabIndex = 16;
			optMouseGlobal.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optMouseGlobal.CheckAlign = ContentAlignment.MiddleLeft;
			optMouseGlobal.BackColor = SystemColors.Control;
			optMouseGlobal.CausesValidation = true;
			optMouseGlobal.Enabled = true;
			optMouseGlobal.ForeColor = SystemColors.ControlText;
			optMouseGlobal.Cursor = Cursors.Default;
			optMouseGlobal.RightToLeft = RightToLeft.No;
			optMouseGlobal.Appearance = Appearance.Normal;
			optMouseGlobal.TabStop = true;
			optMouseGlobal.Checked = false;
			optMouseGlobal.Visible = true;
			optMouseGlobal.Name = "optMouseGlobal";
			optMouseVB.TextAlign = ContentAlignment.MiddleLeft;
			optMouseVB.Text = "&Only when Windows pointer is over vbSpec window";
			optMouseVB.Size = new Size(277, 13);
			optMouseVB.Location = new Point(12, 20);
			optMouseVB.TabIndex = 15;
			optMouseVB.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optMouseVB.CheckAlign = ContentAlignment.MiddleLeft;
			optMouseVB.BackColor = SystemColors.Control;
			optMouseVB.CausesValidation = true;
			optMouseVB.Enabled = true;
			optMouseVB.ForeColor = SystemColors.ControlText;
			optMouseVB.Cursor = Cursors.Default;
			optMouseVB.RightToLeft = RightToLeft.No;
			optMouseVB.Appearance = Appearance.Normal;
			optMouseVB.TabStop = true;
			optMouseVB.Checked = false;
			optMouseVB.Visible = true;
			optMouseVB.Name = "optMouseVB";
			Label1.TextAlign = ContentAlignment.TopRight;
			Label1.Text = "Windows &Mouse Emulates:";
			Label1.Size = new Size(137, 17);
			Label1.Location = new Point(0, 8);
			Label1.TabIndex = 12;
			Label1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			Label1.BackColor = SystemColors.Control;
			Label1.Enabled = true;
			Label1.ForeColor = SystemColors.ControlText;
			Label1.Cursor = Cursors.Default;
			Label1.RightToLeft = RightToLeft.No;
			Label1.UseMnemonic = true;
			Label1.Visible = true;
			Label1.AutoSize = false;
			Label1.BorderStyle = BorderStyle.None;
			Label1.Name = "Label1";
			_picFrame_1.Size = new Size(337, 185);
			_picFrame_1.Location = new Point(8, 32);
			_picFrame_1.TabIndex = 17;
			_picFrame_1.TabStop = false;
			_picFrame_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_picFrame_1.Dock = DockStyle.None;
			_picFrame_1.BackColor = SystemColors.Control;
			_picFrame_1.CausesValidation = true;
			_picFrame_1.Enabled = true;
			_picFrame_1.ForeColor = SystemColors.ControlText;
			_picFrame_1.Cursor = Cursors.Default;
			_picFrame_1.RightToLeft = RightToLeft.No;
			_picFrame_1.Visible = true;
			_picFrame_1.BorderStyle = BorderStyle.None;
			_picFrame_1.Name = "_picFrame_1";
			chkSEBasic.Text = "Use SE Basic ROM";
			chkSEBasic.Size = new Size(129, 17);
			chkSEBasic.Location = new Point(12, 32);
			chkSEBasic.TabIndex = 5;
			chkSEBasic.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			chkSEBasic.CheckAlign = ContentAlignment.MiddleLeft;
			chkSEBasic.FlatStyle = FlatStyle.Standard;
			chkSEBasic.BackColor = SystemColors.Control;
			chkSEBasic.CausesValidation = true;
			chkSEBasic.Enabled = true;
			chkSEBasic.ForeColor = SystemColors.ControlText;
			chkSEBasic.Cursor = Cursors.Default;
			chkSEBasic.RightToLeft = RightToLeft.No;
			chkSEBasic.Appearance = Appearance.Normal;
			chkSEBasic.TabStop = true;
			chkSEBasic.CheckState = CheckState.Unchecked;
			chkSEBasic.Visible = true;
			chkSEBasic.Name = "chkSEBasic";
			fraStd.Text = "Emulation Speed";
			fraStd.Size = new Size(325, 105);
			fraStd.Location = new Point(8, 72);
			fraStd.TabIndex = 7;
			fraStd.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			fraStd.BackColor = SystemColors.Control;
			fraStd.Enabled = true;
			fraStd.ForeColor = SystemColors.ControlText;
			fraStd.RightToLeft = RightToLeft.No;
			fraStd.Visible = true;
			fraStd.Padding = new Padding(0);
			fraStd.Name = "fraStd";
			optSpeed200.TextAlign = ContentAlignment.MiddleLeft;
			optSpeed200.Text = "&Double - Limit emulation to 200% real Spectrum speed";
			optSpeed200.Size = new Size(289, 17);
			optSpeed200.Location = new Point(12, 60);
			optSpeed200.TabIndex = 10;
			optSpeed200.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optSpeed200.CheckAlign = ContentAlignment.MiddleLeft;
			optSpeed200.BackColor = SystemColors.Control;
			optSpeed200.CausesValidation = true;
			optSpeed200.Enabled = true;
			optSpeed200.ForeColor = SystemColors.ControlText;
			optSpeed200.Cursor = Cursors.Default;
			optSpeed200.RightToLeft = RightToLeft.No;
			optSpeed200.Appearance = Appearance.Normal;
			optSpeed200.TabStop = true;
			optSpeed200.Checked = false;
			optSpeed200.Visible = true;
			optSpeed200.Name = "optSpeed200";
			optSpeed50.TextAlign = ContentAlignment.MiddleLeft;
			optSpeed50.Text = "&Slow - Limit emulation to 50% of real Spectrum speed";
			optSpeed50.Size = new Size(293, 17);
			optSpeed50.Location = new Point(12, 20);
			optSpeed50.TabIndex = 8;
			optSpeed50.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optSpeed50.CheckAlign = ContentAlignment.MiddleLeft;
			optSpeed50.BackColor = SystemColors.Control;
			optSpeed50.CausesValidation = true;
			optSpeed50.Enabled = true;
			optSpeed50.ForeColor = SystemColors.ControlText;
			optSpeed50.Cursor = Cursors.Default;
			optSpeed50.RightToLeft = RightToLeft.No;
			optSpeed50.Appearance = Appearance.Normal;
			optSpeed50.TabStop = true;
			optSpeed50.Checked = false;
			optSpeed50.Visible = true;
			optSpeed50.Name = "optSpeed50";
			optSpeed100.TextAlign = ContentAlignment.MiddleLeft;
			optSpeed100.Text = "&Real - Limit emulation to 100% real Spectrum speed";
			optSpeed100.Size = new Size(289, 17);
			optSpeed100.Location = new Point(12, 40);
			optSpeed100.TabIndex = 9;
			optSpeed100.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optSpeed100.CheckAlign = ContentAlignment.MiddleLeft;
			optSpeed100.BackColor = SystemColors.Control;
			optSpeed100.CausesValidation = true;
			optSpeed100.Enabled = true;
			optSpeed100.ForeColor = SystemColors.ControlText;
			optSpeed100.Cursor = Cursors.Default;
			optSpeed100.RightToLeft = RightToLeft.No;
			optSpeed100.Appearance = Appearance.Normal;
			optSpeed100.TabStop = true;
			optSpeed100.Checked = false;
			optSpeed100.Visible = true;
			optSpeed100.Name = "optSpeed100";
			optSpeedFastest.TextAlign = ContentAlignment.MiddleLeft;
			optSpeedFastest.Text = "&Fastest - Do not limit emulation speed";
			optSpeedFastest.Size = new Size(285, 17);
			optSpeedFastest.Location = new Point(12, 80);
			optSpeedFastest.TabIndex = 11;
			optSpeedFastest.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optSpeedFastest.CheckAlign = ContentAlignment.MiddleLeft;
			optSpeedFastest.BackColor = SystemColors.Control;
			optSpeedFastest.CausesValidation = true;
			optSpeedFastest.Enabled = true;
			optSpeedFastest.ForeColor = SystemColors.ControlText;
			optSpeedFastest.Cursor = Cursors.Default;
			optSpeedFastest.RightToLeft = RightToLeft.No;
			optSpeedFastest.Appearance = Appearance.Normal;
			optSpeedFastest.TabStop = true;
			optSpeedFastest.Checked = false;
			optSpeedFastest.Visible = true;
			optSpeedFastest.Name = "optSpeedFastest";
			chkSound.Text = "Enable sound output";
			chkSound.Size = new Size(141, 17);
			chkSound.Location = new Point(12, 52);
			chkSound.TabIndex = 6;
			chkSound.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			chkSound.CheckAlign = ContentAlignment.MiddleLeft;
			chkSound.FlatStyle = FlatStyle.Standard;
			chkSound.BackColor = SystemColors.Control;
			chkSound.CausesValidation = true;
			chkSound.Enabled = true;
			chkSound.ForeColor = SystemColors.ControlText;
			chkSound.Cursor = Cursors.Default;
			chkSound.RightToLeft = RightToLeft.No;
			chkSound.Appearance = Appearance.Normal;
			chkSound.TabStop = true;
			chkSound.CheckState = CheckState.Unchecked;
			chkSound.Visible = true;
			chkSound.Name = "chkSound";
			cboModel.Size = new Size(129, 21);
			cboModel.Location = new Point(92, 4);
			cboModel.DropDownStyle = ComboBoxStyle.DropDownList;
			cboModel.TabIndex = 4;
			cboModel.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			cboModel.BackColor = SystemColors.Window;
			cboModel.CausesValidation = true;
			cboModel.Enabled = true;
			cboModel.ForeColor = SystemColors.WindowText;
			cboModel.IntegralHeight = true;
			cboModel.Cursor = Cursors.Default;
			cboModel.RightToLeft = RightToLeft.No;
			cboModel.Sorted = false;
			cboModel.TabStop = true;
			cboModel.Visible = true;
			cboModel.Name = "cboModel";
			lblStatic.TextAlign = ContentAlignment.TopRight;
			lblStatic.Text = "Emulated model:";
			lblStatic.Size = new Size(85, 17);
			lblStatic.Location = new Point(4, 8);
			lblStatic.TabIndex = 3;
			lblStatic.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblStatic.BackColor = SystemColors.Control;
			lblStatic.Enabled = true;
			lblStatic.ForeColor = SystemColors.ControlText;
			lblStatic.Cursor = Cursors.Default;
			lblStatic.RightToLeft = RightToLeft.No;
			lblStatic.UseMnemonic = true;
			lblStatic.Visible = true;
			lblStatic.AutoSize = false;
			lblStatic.BorderStyle = BorderStyle.None;
			lblStatic.Name = "lblStatic";
			_cmdOK.TextAlign = ContentAlignment.MiddleCenter;
			_cmdOK.Text = "OK";
			AcceptButton = _cmdOK;
			_cmdOK.Size = new Size(73, 25);
			_cmdOK.Location = new Point(196, 228);
			_cmdOK.TabIndex = 0;
			_cmdOK.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOK.BackColor = SystemColors.Control;
			_cmdOK.CausesValidation = true;
			_cmdOK.Enabled = true;
			_cmdOK.ForeColor = SystemColors.ControlText;
			_cmdOK.Cursor = Cursors.Default;
			_cmdOK.RightToLeft = RightToLeft.No;
			_cmdOK.TabStop = true;
			_cmdOK.Name = "_cmdOK";
			_cmdCancel.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _cmdCancel;
			_cmdCancel.Text = "Cancel";
			_cmdCancel.Size = new Size(73, 25);
			_cmdCancel.Location = new Point(276, 228);
			_cmdCancel.TabIndex = 1;
			_cmdCancel.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdCancel.BackColor = SystemColors.Control;
			_cmdCancel.CausesValidation = true;
			_cmdCancel.Enabled = true;
			_cmdCancel.ForeColor = SystemColors.ControlText;
			_cmdCancel.Cursor = Cursors.Default;
			_cmdCancel.RightToLeft = RightToLeft.No;
			_cmdCancel.TabStop = true;
			_cmdCancel.Name = "_cmdCancel";
			// ts1.OcxState = CType(resources.GetObject("ts1.OcxState"), System.Windows.Forms.AxHost.State)
			// Me.ts1.Size = New System.Drawing.Size(345, 217)
			// Me.ts1.Location = New System.Drawing.Point(4, 4)
			// Me.ts1.TabIndex = 2
			// Me.ts1.Name = "ts1"
			Controls.Add(_picFrame_3);
			Controls.Add(_picFrame_2);
			Controls.Add(_picFrame_1);
			Controls.Add(_cmdOK);
			Controls.Add(_cmdCancel);
			// Me.Controls.Add(ts1)
			_picFrame_3.Controls.Add(_cmdDefine_0);
			_picFrame_3.Controls.Add(_cmdDefine_1);
			_picFrame_3.Controls.Add(_cmbZXJoystick_1);
			_picFrame_3.Controls.Add(_cmbFire_1);
			_picFrame_3.Controls.Add(_cmbZXJoystick_0);
			_picFrame_3.Controls.Add(_cmbFire_0);
			_picFrame_3.Controls.Add(_lblZXJoystick_1);
			_picFrame_3.Controls.Add(_lblFire_1);
			_picFrame_3.Controls.Add(_lblZXJoystick_0);
			_picFrame_3.Controls.Add(_lblFire_0);
			_picFrame_2.Controls.Add(cboMouseType);
			_picFrame_2.Controls.Add(Frame1);
			_picFrame_2.Controls.Add(Label1);
			Frame1.Controls.Add(optMouseGlobal);
			Frame1.Controls.Add(optMouseVB);
			_picFrame_1.Controls.Add(chkSEBasic);
			_picFrame_1.Controls.Add(fraStd);
			_picFrame_1.Controls.Add(chkSound);
			_picFrame_1.Controls.Add(cboModel);
			_picFrame_1.Controls.Add(lblStatic);
			fraStd.Controls.Add(optSpeed200);
			fraStd.Controls.Add(optSpeed50);
			fraStd.Controls.Add(optSpeed100);
			fraStd.Controls.Add(optSpeedFastest);
			cmbFire.SetIndex(_cmbFire_1, Conversions.ToShort(1));
			cmbFire.SetIndex(_cmbFire_0, Conversions.ToShort(0));
			_cmbZXJoystick.SetIndex(_cmbZXJoystick_1, Conversions.ToShort(1));
			_cmbZXJoystick.SetIndex(_cmbZXJoystick_0, Conversions.ToShort(0));
			_cmdDefine.SetIndex(_cmdDefine_0, Conversions.ToShort(0));
			_cmdDefine.SetIndex(_cmdDefine_1, Conversions.ToShort(1));
			lblFire.SetIndex(_lblFire_1, Conversions.ToShort(1));
			lblFire.SetIndex(_lblFire_0, Conversions.ToShort(0));
			lblZXJoystick.SetIndex(_lblZXJoystick_1, Conversions.ToShort(1));
			lblZXJoystick.SetIndex(_lblZXJoystick_0, Conversions.ToShort(0));
			picFrame.SetIndex(_picFrame_3, Conversions.ToShort(3));
			picFrame.SetIndex(_picFrame_2, Conversions.ToShort(2));
			picFrame.SetIndex(_picFrame_1, Conversions.ToShort(1));
			((System.ComponentModel.ISupportInitialize)picFrame).EndInit();
			((System.ComponentModel.ISupportInitialize)lblZXJoystick).EndInit();
			((System.ComponentModel.ISupportInitialize)lblFire).EndInit();
			((System.ComponentModel.ISupportInitialize)_cmdDefine).EndInit();
			((System.ComponentModel.ISupportInitialize)_cmbZXJoystick).EndInit();
			((System.ComponentModel.ISupportInitialize)cmbFire).EndInit();
			// CType(Me.ts1, System.ComponentModel.ISupportInitialize).EndInit()
			_picFrame_3.ResumeLayout(false);
			_picFrame_2.ResumeLayout(false);
			Frame1.ResumeLayout(false);
			_picFrame_1.ResumeLayout(false);
			fraStd.ResumeLayout(false);
			Load += new EventHandler(frmOptions_Load);
			Activated += new EventHandler(frmOptions_Activated);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}