﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmMainWnd
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmMainWnd() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_mnuFullScreenFile.Name = "mnuFullScreenFile";
			_mnuFullScreenOptions.Name = "mnuFullScreenOptions";
			_mnuFullScreenHelp.Name = "mnuFullScreenHelp";
			_mnuFullNormalView.Name = "mnuFullNormalView";
			_picDisplay.Name = "picDisplay";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		public ToolStripMenuItem _mnuFile_1;
		public ToolStripMenuItem _mnuFile_2;
		public ToolStripMenuItem _mnuFile_3;
		public ToolStripMenuItem _mnuFile_4;
		public ToolStripMenuItem _mnuFile_5;
		public ToolStripMenuItem _mnuFile_6;
		public ToolStripMenuItem _mnuFile_7;
		public ToolStripMenuItem _mnuFile_8;
		public ToolStripMenuItem _mnuFile_9;
		public ToolStripMenuItem _mnuFile_10;
		public ToolStripMenuItem _mnuFile_11;
		public ToolStripMenuItem _mnuFile_12;
		public ToolStripMenuItem _mnuFile_13;
		public ToolStripMenuItem _mnuFile_14;
		public ToolStripMenuItem _mnuFile_15;
		public ToolStripMenuItem _mnuFile_16;
		public ToolStripMenuItem _mnuFile_17;
		public ToolStripMenuItem mnuFileMain;
		public ToolStripMenuItem _mnuOptions_1;
		public ToolStripMenuItem _mnuOptions_2;
		public ToolStripMenuItem _mnuOptions_3;
		public ToolStripMenuItem _mnuOptions_4;
		public ToolStripMenuItem _mnuOptions_5;
		public ToolStripMenuItem _mnuOptions_6;
		public ToolStripMenuItem _mnuOptions_7;
		public ToolStripMenuItem _mnuOptions_8;
		public ToolStripMenuItem mnuOptionsMain;
		public ToolStripMenuItem _mnuHelp_1;
		public ToolStripMenuItem _mnuHelp_2;
		public ToolStripMenuItem _mnuHelp_3;
		public ToolStripMenuItem mnuHelpMain;
		private ToolStripMenuItem _mnuFullScreenFile;

		public ToolStripMenuItem mnuFullScreenFile
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuFullScreenFile;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuFullScreenFile != null)
				{
					_mnuFullScreenFile.Click -= mnuFullScreenFile_Click;
				}

				_mnuFullScreenFile = value;
				if (_mnuFullScreenFile != null)
				{
					_mnuFullScreenFile.Click += mnuFullScreenFile_Click;
				}
			}
		}

		private ToolStripMenuItem _mnuFullScreenOptions;

		public ToolStripMenuItem mnuFullScreenOptions
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuFullScreenOptions;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuFullScreenOptions != null)
				{
					_mnuFullScreenOptions.Click -= mnuFullScreenOptions_Click;
				}

				_mnuFullScreenOptions = value;
				if (_mnuFullScreenOptions != null)
				{
					_mnuFullScreenOptions.Click += mnuFullScreenOptions_Click;
				}
			}
		}

		private ToolStripMenuItem _mnuFullScreenHelp;

		public ToolStripMenuItem mnuFullScreenHelp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuFullScreenHelp;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuFullScreenHelp != null)
				{
					_mnuFullScreenHelp.Click -= mnuFullScreenHelp_Click;
				}

				_mnuFullScreenHelp = value;
				if (_mnuFullScreenHelp != null)
				{
					_mnuFullScreenHelp.Click += mnuFullScreenHelp_Click;
				}
			}
		}

		private ToolStripMenuItem _mnuFullNormalView;

		public ToolStripMenuItem mnuFullNormalView
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuFullNormalView;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuFullNormalView != null)
				{
					_mnuFullNormalView.Click -= mnuFullNormalView_Click;
				}

				_mnuFullNormalView = value;
				if (_mnuFullNormalView != null)
				{
					_mnuFullNormalView.Click += mnuFullNormalView_Click;
				}
			}
		}

		public ToolStripMenuItem mnuFullScreenMenu;
		public MenuStrip MainMenu1;
		public Microsoft.VisualBasic.PowerPacks.LineShape _Line1_3;
		public Microsoft.VisualBasic.PowerPacks.LineShape _Line1_2;
		public Label lblStatusMsg;
		public Panel picStatus;
		public OpenFileDialog dlgCommonOpen;
		public SaveFileDialog dlgCommonSave;
		private PictureBox _picDisplay;

		public PictureBox picDisplay
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _picDisplay;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_picDisplay != null)
				{
					_picDisplay.MouseDown -= picDisplay_MouseDown;
					_picDisplay.MouseUp -= picDisplay_MouseUp;
				}

				_picDisplay = value;
				if (_picDisplay != null)
				{
					_picDisplay.MouseDown += picDisplay_MouseDown;
					_picDisplay.MouseUp += picDisplay_MouseUp;
				}
			}
		}

		public Microsoft.VisualBasic.PowerPacks.LineShape _Line1_1;
		public Microsoft.VisualBasic.PowerPacks.LineShape _Line1_0;
		// Public WithEvents Line1 As LineShapeArray
		private Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray _mnuFile;

		public Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray mnuFile
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuFile;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuFile != null)
				{
					_mnuFile.Click -= mnuFile_Click;
				}

				_mnuFile = value;
				if (_mnuFile != null)
				{
					_mnuFile.Click += mnuFile_Click;
				}
			}
		}

		private Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray _mnuHelp;

		public Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray mnuHelp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuHelp;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuHelp != null)
				{
					_mnuHelp.Click -= mnuHelp_Click;
				}

				_mnuHelp = value;
				if (_mnuHelp != null)
				{
					_mnuHelp.Click += mnuHelp_Click;
				}
			}
		}

		private Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray _mnuOptions;

		public Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray mnuOptions
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _mnuOptions;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_mnuOptions != null)
				{
					_mnuOptions.Click -= mnuOptions_Click;
				}

				_mnuOptions = value;
				if (_mnuOptions != null)
				{
					_mnuOptions.Click += mnuOptions_Click;
				}
			}
		}

		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmMainWnd));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			MainMenu1 = new MenuStrip();
			mnuFileMain = new ToolStripMenuItem();
			_mnuFile_1 = new ToolStripMenuItem();
			_mnuFile_2 = new ToolStripMenuItem();
			_mnuFile_3 = new ToolStripMenuItem();
			_mnuFile_4 = new ToolStripMenuItem();
			_mnuFile_5 = new ToolStripMenuItem();
			_mnuFile_6 = new ToolStripMenuItem();
			_mnuFile_7 = new ToolStripMenuItem();
			_mnuFile_8 = new ToolStripMenuItem();
			_mnuFile_9 = new ToolStripMenuItem();
			_mnuFile_10 = new ToolStripMenuItem();
			_mnuFile_11 = new ToolStripMenuItem();
			_mnuFile_12 = new ToolStripMenuItem();
			_mnuFile_13 = new ToolStripMenuItem();
			_mnuFile_14 = new ToolStripMenuItem();
			_mnuFile_15 = new ToolStripMenuItem();
			_mnuFile_16 = new ToolStripMenuItem();
			_mnuFile_17 = new ToolStripMenuItem();
			mnuOptionsMain = new ToolStripMenuItem();
			_mnuOptions_1 = new ToolStripMenuItem();
			_mnuOptions_2 = new ToolStripMenuItem();
			_mnuOptions_3 = new ToolStripMenuItem();
			_mnuOptions_4 = new ToolStripMenuItem();
			_mnuOptions_5 = new ToolStripMenuItem();
			_mnuOptions_6 = new ToolStripMenuItem();
			_mnuOptions_7 = new ToolStripMenuItem();
			_mnuOptions_8 = new ToolStripMenuItem();
			mnuHelpMain = new ToolStripMenuItem();
			_mnuHelp_1 = new ToolStripMenuItem();
			_mnuHelp_2 = new ToolStripMenuItem();
			_mnuHelp_3 = new ToolStripMenuItem();
			mnuFullScreenMenu = new ToolStripMenuItem();
			_mnuFullScreenFile = new ToolStripMenuItem();
			_mnuFullScreenFile.Click += new EventHandler(mnuFullScreenFile_Click);
			_mnuFullScreenOptions = new ToolStripMenuItem();
			_mnuFullScreenOptions.Click += new EventHandler(mnuFullScreenOptions_Click);
			_mnuFullScreenHelp = new ToolStripMenuItem();
			_mnuFullScreenHelp.Click += new EventHandler(mnuFullScreenHelp_Click);
			_mnuFullNormalView = new ToolStripMenuItem();
			_mnuFullNormalView.Click += new EventHandler(mnuFullNormalView_Click);
			picStatus = new Panel();
			_Line1_3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			_Line1_2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			lblStatusMsg = new Label();
			dlgCommonOpen = new OpenFileDialog();
			dlgCommonSave = new SaveFileDialog();
			_picDisplay = new PictureBox();
			_picDisplay.MouseDown += new MouseEventHandler(picDisplay_MouseDown);
			_picDisplay.MouseUp += new MouseEventHandler(picDisplay_MouseUp);
			_Line1_1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			_Line1_0 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			// Me.Line1 = New LineShapeArray(components)
			_mnuFile = new Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components);
			_mnuFile.Click += new EventHandler(mnuFile_Click);
			_mnuHelp = new Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components);
			_mnuHelp.Click += new EventHandler(mnuHelp_Click);
			_mnuOptions = new Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components);
			_mnuOptions.Click += new EventHandler(mnuOptions_Click);
			MainMenu1.SuspendLayout();
			picStatus.SuspendLayout();
			SuspendLayout();
			ToolTip1.Active = true;
			// CType(Me.Line1, System.ComponentModel.ISupportInitialize).BeginInit()
			((System.ComponentModel.ISupportInitialize)_mnuFile).BeginInit();
			((System.ComponentModel.ISupportInitialize)_mnuHelp).BeginInit();
			((System.ComponentModel.ISupportInitialize)_mnuOptions).BeginInit();
			FormBorderStyle = FormBorderStyle.FixedSingle;
			Text = "vbSpec";
			ClientSize = new Size(281, 266);
			Location = new Point(10, 48);
			ControlBox = false;
			Icon = (Icon)resources.GetObject("frmMainWnd.Icon");
			KeyPreview = true;
			MaximizeBox = false;
			MinimizeBox = false;
			StartPosition = FormStartPosition.WindowsDefaultLocation;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			Enabled = true;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			ShowInTaskbar = true;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmMainWnd";
			mnuFileMain.Name = "mnuFileMain";
			mnuFileMain.Text = "&File";
			mnuFileMain.Checked = false;
			mnuFileMain.Enabled = true;
			mnuFileMain.Visible = true;
			_mnuFile_1.Name = "_mnuFile_1";
			_mnuFile_1.Text = "&Open...";
			_mnuFile_1.Checked = false;
			_mnuFile_1.Enabled = true;
			_mnuFile_1.Visible = true;
			_mnuFile_2.Name = "_mnuFile_2";
			_mnuFile_2.Text = "&Save As...";
			_mnuFile_2.Checked = false;
			_mnuFile_2.Enabled = true;
			_mnuFile_2.Visible = true;
			_mnuFile_3.Enabled = true;
			_mnuFile_3.Visible = true;
			_mnuFile_3.Name = "_mnuFile_3";
			_mnuFile_4.Name = "_mnuFile_4";
			_mnuFile_4.Text = "&Create blank tape file for saving...";
			_mnuFile_4.Checked = false;
			_mnuFile_4.Enabled = true;
			_mnuFile_4.Visible = true;
			_mnuFile_5.Enabled = true;
			_mnuFile_5.Visible = true;
			_mnuFile_5.Name = "_mnuFile_5";
			_mnuFile_6.Name = "_mnuFile_6";
			_mnuFile_6.Text = "Load &Binary...";
			_mnuFile_6.Checked = false;
			_mnuFile_6.Enabled = true;
			_mnuFile_6.Visible = true;
			_mnuFile_7.Name = "_mnuFile_7";
			_mnuFile_7.Text = "Save B&inary...";
			_mnuFile_7.Checked = false;
			_mnuFile_7.Enabled = true;
			_mnuFile_7.Visible = true;
			_mnuFile_8.Enabled = true;
			_mnuFile_8.Visible = true;
			_mnuFile_8.Name = "_mnuFile_8";
			_mnuFile_9.Name = "_mnuFile_9";
			_mnuFile_9.Text = "&Reset Spectrum";
			_mnuFile_9.Checked = false;
			_mnuFile_9.Enabled = true;
			_mnuFile_9.Visible = true;
			_mnuFile_10.Visible = false;
			_mnuFile_10.Enabled = true;
			_mnuFile_10.Name = "_mnuFile_10";
			_mnuFile_11.Name = "_mnuFile_11";
			_mnuFile_11.Text = "&1";
			_mnuFile_11.Visible = false;
			_mnuFile_11.Checked = false;
			_mnuFile_11.Enabled = true;
			_mnuFile_12.Name = "_mnuFile_12";
			_mnuFile_12.Text = "&2";
			_mnuFile_12.Visible = false;
			_mnuFile_12.Checked = false;
			_mnuFile_12.Enabled = true;
			_mnuFile_13.Name = "_mnuFile_13";
			_mnuFile_13.Text = "&3";
			_mnuFile_13.Visible = false;
			_mnuFile_13.Checked = false;
			_mnuFile_13.Enabled = true;
			_mnuFile_14.Name = "_mnuFile_14";
			_mnuFile_14.Text = "&4";
			_mnuFile_14.Visible = false;
			_mnuFile_14.Checked = false;
			_mnuFile_14.Enabled = true;
			_mnuFile_15.Name = "_mnuFile_15";
			_mnuFile_15.Text = "&5";
			_mnuFile_15.Visible = false;
			_mnuFile_15.Checked = false;
			_mnuFile_15.Enabled = true;
			_mnuFile_16.Enabled = true;
			_mnuFile_16.Visible = true;
			_mnuFile_16.Name = "_mnuFile_16";
			_mnuFile_17.Name = "_mnuFile_17";
			_mnuFile_17.Text = "E&xit";
			_mnuFile_17.Checked = false;
			_mnuFile_17.Enabled = true;
			_mnuFile_17.Visible = true;
			mnuOptionsMain.Name = "mnuOptionsMain";
			mnuOptionsMain.Text = "&Options";
			mnuOptionsMain.Checked = false;
			mnuOptionsMain.Enabled = true;
			mnuOptionsMain.Visible = true;
			_mnuOptions_1.Name = "_mnuOptions_1";
			_mnuOptions_1.Text = "&General Settings...";
			_mnuOptions_1.Checked = false;
			_mnuOptions_1.Enabled = true;
			_mnuOptions_1.Visible = true;
			_mnuOptions_2.Name = "_mnuOptions_2";
			_mnuOptions_2.Text = "&Display...";
			_mnuOptions_2.Checked = false;
			_mnuOptions_2.Enabled = true;
			_mnuOptions_2.Visible = true;
			_mnuOptions_3.Enabled = true;
			_mnuOptions_3.Visible = true;
			_mnuOptions_3.Name = "_mnuOptions_3";
			_mnuOptions_4.Name = "_mnuOptions_4";
			_mnuOptions_4.Text = "&Tape Controls...";
			_mnuOptions_4.Checked = false;
			_mnuOptions_4.Enabled = true;
			_mnuOptions_4.Visible = true;
			_mnuOptions_5.Name = "_mnuOptions_5";
			_mnuOptions_5.Text = "ZX &Printer...";
			_mnuOptions_5.Checked = false;
			_mnuOptions_5.Enabled = true;
			_mnuOptions_5.Visible = true;
			_mnuOptions_6.Enabled = true;
			_mnuOptions_6.Visible = true;
			_mnuOptions_6.Name = "_mnuOptions_6";
			_mnuOptions_7.Name = "_mnuOptions_7";
			_mnuOptions_7.Text = "&YZ Switch";
			_mnuOptions_7.Checked = false;
			_mnuOptions_7.Enabled = true;
			_mnuOptions_7.Visible = true;
			_mnuOptions_8.Name = "_mnuOptions_8";
			_mnuOptions_8.Text = "&Poke memory";
			_mnuOptions_8.Checked = false;
			_mnuOptions_8.Enabled = true;
			_mnuOptions_8.Visible = true;
			mnuHelpMain.Name = "mnuHelpMain";
			mnuHelpMain.Text = "&Help";
			mnuHelpMain.Checked = false;
			mnuHelpMain.Enabled = true;
			mnuHelpMain.Visible = true;
			_mnuHelp_1.Name = "_mnuHelp_1";
			_mnuHelp_1.Text = "Spectrum &Keyboard...";
			_mnuHelp_1.Checked = false;
			_mnuHelp_1.Enabled = true;
			_mnuHelp_1.Visible = true;
			_mnuHelp_2.Enabled = true;
			_mnuHelp_2.Visible = true;
			_mnuHelp_2.Name = "_mnuHelp_2";
			_mnuHelp_3.Name = "_mnuHelp_3";
			_mnuHelp_3.Text = "&About vbSpec...";
			_mnuHelp_3.Checked = false;
			_mnuHelp_3.Enabled = true;
			_mnuHelp_3.Visible = true;
			mnuFullScreenMenu.Name = "mnuFullScreenMenu";
			mnuFullScreenMenu.Text = "Full screen mode menu";
			mnuFullScreenMenu.Visible = false;
			mnuFullScreenMenu.Checked = false;
			mnuFullScreenMenu.Enabled = true;
			_mnuFullScreenFile.Name = "_mnuFullScreenFile";
			_mnuFullScreenFile.Text = "File...";
			_mnuFullScreenFile.Checked = false;
			_mnuFullScreenFile.Enabled = true;
			_mnuFullScreenFile.Visible = true;
			_mnuFullScreenOptions.Name = "_mnuFullScreenOptions";
			_mnuFullScreenOptions.Text = "Options...";
			_mnuFullScreenOptions.Checked = false;
			_mnuFullScreenOptions.Enabled = true;
			_mnuFullScreenOptions.Visible = true;
			_mnuFullScreenHelp.Name = "_mnuFullScreenHelp";
			_mnuFullScreenHelp.Text = "Help...";
			_mnuFullScreenHelp.Checked = false;
			_mnuFullScreenHelp.Enabled = true;
			_mnuFullScreenHelp.Visible = true;
			_mnuFullNormalView.Name = "_mnuFullNormalView";
			_mnuFullNormalView.Text = "Normal view!";
			_mnuFullNormalView.Checked = false;
			_mnuFullNormalView.Enabled = true;
			_mnuFullNormalView.Visible = true;
			picStatus.Dock = DockStyle.Bottom;
			picStatus.Size = new Size(281, 22);
			picStatus.Location = new Point(0, 244);
			picStatus.TabIndex = 1;
			picStatus.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			picStatus.BackColor = SystemColors.Control;
			picStatus.CausesValidation = true;
			picStatus.Enabled = true;
			picStatus.ForeColor = SystemColors.ControlText;
			picStatus.Cursor = Cursors.Default;
			picStatus.RightToLeft = RightToLeft.No;
			picStatus.TabStop = true;
			picStatus.Visible = true;
			picStatus.BorderStyle = BorderStyle.None;
			picStatus.Name = "picStatus";
			_Line1_3.BorderColor = SystemColors.ControlDark;
			_Line1_3.X1 = 0;
			_Line1_3.X2 = 11884;
			_Line1_3.Y1 = 0;
			_Line1_3.Y2 = 0;
			_Line1_3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			_Line1_3.BorderWidth = 1;
			_Line1_3.Visible = true;
			_Line1_3.Name = "_Line1_3";
			_Line1_2.BorderColor = SystemColors.ControlLight;
			_Line1_2.X1 = 0;
			_Line1_2.X2 = 11884;
			_Line1_2.Y1 = 1;
			_Line1_2.Y2 = 1;
			_Line1_2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			_Line1_2.BorderWidth = 1;
			_Line1_2.Visible = true;
			_Line1_2.Name = "_Line1_2";
			lblStatusMsg.Text = "vbSpec";
			lblStatusMsg.Size = new Size(269, 17);
			lblStatusMsg.Location = new Point(4, 6);
			lblStatusMsg.TabIndex = 2;
			lblStatusMsg.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblStatusMsg.TextAlign = ContentAlignment.TopLeft;
			lblStatusMsg.BackColor = SystemColors.Control;
			lblStatusMsg.Enabled = true;
			lblStatusMsg.ForeColor = SystemColors.ControlText;
			lblStatusMsg.Cursor = Cursors.Default;
			lblStatusMsg.RightToLeft = RightToLeft.No;
			lblStatusMsg.UseMnemonic = true;
			lblStatusMsg.Visible = true;
			lblStatusMsg.AutoSize = false;
			lblStatusMsg.BorderStyle = BorderStyle.None;
			lblStatusMsg.Name = "lblStatusMsg";
			_picDisplay.BackColor = SystemColors.Window;
			_picDisplay.ForeColor = SystemColors.WindowText;
			_picDisplay.Size = new Size(256, 192);
			_picDisplay.Location = new Point(12, 40);
			_picDisplay.TabIndex = 0;
			_picDisplay.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_picDisplay.Dock = DockStyle.None;
			_picDisplay.CausesValidation = true;
			_picDisplay.Enabled = true;
			_picDisplay.RightToLeft = RightToLeft.No;
			_picDisplay.TabStop = true;
			_picDisplay.Visible = true;
			_picDisplay.SizeMode = PictureBoxSizeMode.Normal;
			_picDisplay.BorderStyle = BorderStyle.None;
			_picDisplay.Name = "_picDisplay";
			_Line1_1.BorderColor = SystemColors.ControlLight;
			_Line1_1.X1 = 0;
			_Line1_1.X2 = 792;
			_Line1_1.Y1 = 1;
			_Line1_1.Y2 = 1;
			_Line1_1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			_Line1_1.BorderWidth = 1;
			_Line1_1.Visible = true;
			_Line1_1.Name = "_Line1_1";
			_Line1_0.BorderColor = SystemColors.ControlDark;
			_Line1_0.X1 = 0;
			_Line1_0.X2 = 792;
			_Line1_0.Y1 = 0;
			_Line1_0.Y2 = 0;
			_Line1_0.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			_Line1_0.BorderWidth = 1;
			_Line1_0.Visible = true;
			_Line1_0.Name = "_Line1_0";
			Controls.Add(picStatus);
			Controls.Add(_picDisplay);
			ShapeContainer1.Shapes.Add(_Line1_1);
			ShapeContainer1.Shapes.Add(_Line1_0);
			Controls.Add(ShapeContainer1);
			ShapeContainer2.Shapes.Add(_Line1_3);
			ShapeContainer2.Shapes.Add(_Line1_2);
			picStatus.Controls.Add(lblStatusMsg);
			picStatus.Controls.Add(ShapeContainer2);
			// Me.Line1.SetIndex(_Line1_3, CType(3, Short))
			// Me.Line1.SetIndex(_Line1_2, CType(2, Short))
			// Me.Line1.SetIndex(_Line1_1, CType(1, Short))
			// Me.Line1.SetIndex(_Line1_0, CType(0, Short))
			_mnuFile.SetIndex(_mnuFile_1, Conversions.ToShort(1));
			_mnuFile.SetIndex(_mnuFile_2, Conversions.ToShort(2));
			_mnuFile.SetIndex(_mnuFile_3, Conversions.ToShort(3));
			_mnuFile.SetIndex(_mnuFile_4, Conversions.ToShort(4));
			_mnuFile.SetIndex(_mnuFile_5, Conversions.ToShort(5));
			_mnuFile.SetIndex(_mnuFile_6, Conversions.ToShort(6));
			_mnuFile.SetIndex(_mnuFile_7, Conversions.ToShort(7));
			_mnuFile.SetIndex(_mnuFile_8, Conversions.ToShort(8));
			_mnuFile.SetIndex(_mnuFile_9, Conversions.ToShort(9));
			_mnuFile.SetIndex(_mnuFile_10, Conversions.ToShort(10));
			_mnuFile.SetIndex(_mnuFile_11, Conversions.ToShort(11));
			_mnuFile.SetIndex(_mnuFile_12, Conversions.ToShort(12));
			_mnuFile.SetIndex(_mnuFile_13, Conversions.ToShort(13));
			_mnuFile.SetIndex(_mnuFile_14, Conversions.ToShort(14));
			_mnuFile.SetIndex(_mnuFile_15, Conversions.ToShort(15));
			_mnuFile.SetIndex(_mnuFile_16, Conversions.ToShort(16));
			_mnuFile.SetIndex(_mnuFile_17, Conversions.ToShort(17));
			_mnuHelp.SetIndex(_mnuHelp_1, Conversions.ToShort(1));
			_mnuHelp.SetIndex(_mnuHelp_2, Conversions.ToShort(2));
			_mnuHelp.SetIndex(_mnuHelp_3, Conversions.ToShort(3));
			_mnuOptions.SetIndex(_mnuOptions_1, Conversions.ToShort(1));
			_mnuOptions.SetIndex(_mnuOptions_2, Conversions.ToShort(2));
			_mnuOptions.SetIndex(_mnuOptions_3, Conversions.ToShort(3));
			_mnuOptions.SetIndex(_mnuOptions_4, Conversions.ToShort(4));
			_mnuOptions.SetIndex(_mnuOptions_5, Conversions.ToShort(5));
			_mnuOptions.SetIndex(_mnuOptions_6, Conversions.ToShort(6));
			_mnuOptions.SetIndex(_mnuOptions_7, Conversions.ToShort(7));
			_mnuOptions.SetIndex(_mnuOptions_8, Conversions.ToShort(8));
			((System.ComponentModel.ISupportInitialize)_mnuOptions).EndInit();
			((System.ComponentModel.ISupportInitialize)_mnuHelp).EndInit();
			((System.ComponentModel.ISupportInitialize)_mnuFile).EndInit();
			// CType(Me.Line1, System.ComponentModel.ISupportInitialize).EndInit()
			MainMenu1.Items.AddRange(new ToolStripItem[] { mnuFileMain, mnuOptionsMain, mnuHelpMain, mnuFullScreenMenu });
			mnuFileMain.DropDownItems.AddRange(new ToolStripItem[] { _mnuFile_1, _mnuFile_2, _mnuFile_3, _mnuFile_4, _mnuFile_5, _mnuFile_6, _mnuFile_7, _mnuFile_8, _mnuFile_9, _mnuFile_10, _mnuFile_11, _mnuFile_12, _mnuFile_13, _mnuFile_14, _mnuFile_15, _mnuFile_16, _mnuFile_17 });
			mnuOptionsMain.DropDownItems.AddRange(new ToolStripItem[] { _mnuOptions_1, _mnuOptions_2, _mnuOptions_3, _mnuOptions_4, _mnuOptions_5, _mnuOptions_6, _mnuOptions_7, _mnuOptions_8 });
			mnuHelpMain.DropDownItems.AddRange(new ToolStripItem[] { _mnuHelp_1, _mnuHelp_2, _mnuHelp_3 });
			mnuFullScreenMenu.DropDownItems.AddRange(new ToolStripItem[] { _mnuFullScreenFile, _mnuFullScreenOptions, _mnuFullScreenHelp, _mnuFullNormalView });
			Controls.Add(MainMenu1);
			MainMenu1.ResumeLayout(false);
			picStatus.ResumeLayout(false);
			KeyDown += new KeyEventHandler(frmMainWnd_KeyDown);
			KeyUp += new KeyEventHandler(frmMainWnd_KeyUp);
			Load += new EventHandler(frmMainWnd_Load);
			FormClosing += new FormClosingEventHandler(frmMainWnd_FormClosing);
			Resize += new EventHandler(frmMainWnd_Resize);
			Activated += new EventHandler(frmMainWnd_Activated);
			FormClosed += new FormClosedEventHandler(frmMainWnd_FormClosed);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}