﻿using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	static class modZ80
	{
		// /*******************************************************************************
		// modZ80.bas within vbSpec.vbp
		// 
		// Complete Z80 emulation, including (as far as I know) the
		// correct emulation of bits 3 and 5 of F, and undocumented ops.
		// Please mail me if you find any bugs in the emulation!
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2002 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/

		private static void adc_a(int b)
		{
			int ans, c;
			c = -modMain.fC;
			ans = modMain.regA + b + c & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((modMain.regA + b + c & 0x100) != 0);
			modMain.fPV = Conversions.ToInteger(((modMain.regA ^ ~b & 0xFFFF) & (modMain.regA ^ ans) & 0x80) != 0);
			modMain.fH = Conversions.ToInteger(((short)((short)(modMain.regA & 0xF) + (short)(b & 0xF)) + c & modMain.F_H) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.regA = ans;
		}

		private static int adc16(int a, int b)
		{
			int adc16Ret = default;
			int c, ans;
			c = -modMain.fC;
			ans = a + b + c & 0xFFFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S * 256) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3 * 256) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5 * 256) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((a + b + c & 0x10000) != 0);
			modMain.fPV = Conversions.ToInteger(((a ^ ~b & 0xFFFF) & (a ^ ans) & 0x8000) != 0);
			modMain.fH = Conversions.ToInteger(((short)((short)(a & 0xFFF) + (short)(b & 0xFFF)) + c & 0x1000) != 0);
			modMain.fN = Conversions.ToInteger(false);
			adc16Ret = ans;
			return adc16Ret;
		}

		private static void add_a(int b)
		{
			int ans;
			ans = modMain.regA + b & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((modMain.regA + b & 0x100) != 0);
			modMain.fPV = Conversions.ToInteger(((modMain.regA ^ ~b & 0xFFFF) & (modMain.regA ^ ans) & 0x80) != 0);
			modMain.fH = Conversions.ToInteger(((short)(modMain.regA & 0xF) + (short)(b & 0xF) & modMain.F_H) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.regA = ans;
		}

		private static int add16(int a, int b)
		{
			int add16Ret = default;
			int ans;
			ans = a + b & 0xFFFF;
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3 * 256) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5 * 256) != 0);
			modMain.fC = Conversions.ToInteger((a + b & 0x10000) != 0);
			modMain.fH = Conversions.ToInteger(((short)(a & 0xFFF) + (short)(b & 0xFFF) & 0x1000) != 0);
			modMain.fN = Conversions.ToInteger(false);
			add16Ret = ans;
			return add16Ret;
		}

		private static void and_a(int b)
		{
			modMain.regA = modMain.regA & b;
			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fH = Conversions.ToInteger(true);
			modMain.fPV = modMain.Parity[modMain.regA];
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fC = Conversions.ToInteger(false);
		}

		private static void bit(int b, int r)
		{
			int IsbitSet;
			IsbitSet = Conversions.ToInteger((r & b) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(true);
			// f3 = (r And F_3) <> 0&
			// f5 = (r And F_5) <> 0&

			modMain.fS = IsbitSet & Conversions.ToInteger(b == modMain.F_S);
			modMain.fZ = ~IsbitSet;
			modMain.fPV = modMain.fZ;
		}

		// UPGRADE_NOTE: val was upgraded to val_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		public static int bitRes(int bit, int val_Renamed)
		{
			int bitResRet = default;
			bitResRet = val_Renamed & (bit ^ 0xFFFF);
			return bitResRet;
		}

		// UPGRADE_NOTE: val was upgraded to val_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		public static int bitSet(int bit, int val_Renamed)
		{
			int bitSetRet = default;
			bitSetRet = val_Renamed | bit;
			return bitSetRet;
		}

		private static void ccf()
		{
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fH = modMain.fC;
			modMain.fN = Conversions.ToInteger(false);
			modMain.fC = ~modMain.fC;
		}

		public static void cp_a(int b)
		{
			int ans;
			ans = modMain.regA - b & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((b & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((b & modMain.F_5) != 0);
			modMain.fN = Conversions.ToInteger(true);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((modMain.regA - b & 0x100) != 0);
			modMain.fH = Conversions.ToInteger(((short)(modMain.regA & 0xF) - (short)(b & 0xF) & modMain.F_H) != 0);
			modMain.fPV = Conversions.ToInteger(((modMain.regA ^ b) & (modMain.regA ^ ans) & 0x80) != 0);
		}

		private static void cpl_a()
		{
			modMain.regA = (modMain.regA ^ 0xFF) & 0xFF;
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fH = Conversions.ToInteger(true);
			modMain.fN = Conversions.ToInteger(true);
		}

		private static void daa_a()
		{
			var incr = default(int);
			if (modMain.fH == Conversions.ToInteger(true) | (modMain.regA & 0xF) > 0x9)
			{
				incr = incr | 0x6;
			}

			if (modMain.fC == Conversions.ToInteger(true) | modMain.regA > 0x9F)
			{
				incr = incr | 0x60;
			}

			if (modMain.regA > 0x8F & (modMain.regA & 0xF) > 9)
			{
				incr = incr | 0x60;
			}

			if (modMain.regA > 0x99)
				modMain.fC = Conversions.ToInteger(true);
			if (modMain.fN == Conversions.ToInteger(true))
			{
				// sub_a incr
				modMain.fH = Conversions.ToInteger(((short)(modMain.regA & 0xF) - (short)(incr & 0xF) & modMain.F_H) != 0);
				modMain.regA = modMain.regA - incr & 0xFF;
			}
			else
			{
				// // add_a incr
				modMain.fH = Conversions.ToInteger(((short)(modMain.regA & 0xF) + (short)(incr & 0xF) & modMain.F_H) != 0);
				modMain.regA = modMain.regA + incr & 0xFF;
			}

			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fPV = modMain.Parity[modMain.regA];
		}

		private static int dec16(int a)
		{
			int dec16Ret = default;
			dec16Ret = a - 1 & 0xFFFF;
			return dec16Ret;
		}

		private static void ex_af_af()
		{
			int t;
			t = modMain.regA * 256 | getF();
			setAF(modMain.regAF_);
			modMain.regAF_ = t;
		}

		private static int execute_cb()
		{
			int execute_cbRet = default;
			int xxx;

			// // Yes, I appreciate that GOTO's and labels are a hideous blashphemy!
			// // However, this code is the fastest possible way of fetching and handling
			// // Z80 instructions I could come up with. There are only 8 compares per
			// // instruction fetch rather than between 1 and 255 as required in
			// // the previous version of vbSpec with it's huge Case statement.
			// //
			// // I know it's slightly harder to follow the new code, but I think the
			// // speed increase justifies it. <CC>



			// // REFRESH 1
			modMain.intRTemp = modMain.intRTemp + 1;

			// // Inlined version of "xxx = nxtpcb()" suggested by Gonchuki and Woody
			if ((modMain.regPC & 49152) == 16384)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			xxx = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[modMain.regPC]], modMain.regPC & 16383];
			modMain.regPC = modMain.regPC + 1;
			if (Conversions.ToBoolean(xxx & 128))
				goto ex_cb128_255;
			else
				goto ex_cb0_127;
			ex_cb0_127:
			;
			if (Conversions.ToBoolean(xxx & 64))
				goto ex_cb64_127;
			else
				goto ex_cb0_63;
			ex_cb0_63:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_cb32_63;
			else
				goto ex_cb0_31;
			ex_cb0_31:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb16_31;
			else
				goto ex_cb0_15;
			ex_cb0_15:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb8_15;
			else
				goto ex_cb0_7;
			ex_cb0_7:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb4_7;
			else
				goto ex_cb0_3;
			ex_cb0_3:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb2_3;
			else
				goto ex_cb0_1;
			ex_cb0_1:
			;
			if (xxx == 0)
			{
				// 000 RLC B
				modMain.regB = rlc(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 001 RLC C
				modMain.regC = rlc(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb2_3:
			;
			if (xxx == 2)
			{
				// 002 RLC D
				int argl = rlc(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl);
				execute_cbRet = 8;
			}
			else
			{
				// 003 RLC E
				int argl1 = rlc(getE());
				setE(argl1);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb4_7:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb6_7;
			else
				goto ex_cb4_5;
			ex_cb4_5:
			;
			if (xxx == 4)
			{
				// 004 RLC H
				int argl2 = rlc(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl2);
				execute_cbRet = 8;
			}
			else
			{
				// 005 RLC L
				int localrlc() { int argans = modMain.regHL & 0xFF; var ret = rlc(argans); return ret; }

				int argl3 = localrlc();
				setL(argl3);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb6_7:
			;
			if (xxx == 6)
			{
				// 006 RLC (HL)
				int localrlc1() { int argans = peekb(modMain.regHL); var ret = rlc(argans); return ret; }

				int argnewByte = localrlc1();
				pokeb(modMain.regHL, argnewByte);
				execute_cbRet = 15;
			}
			else
			{
				// 007 RLC A
				modMain.regA = rlc(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb8_15:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb12_15;
			else
				goto ex_cb8_11;
			ex_cb8_11:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb10_11;
			else
				goto ex_cb8_9;
			ex_cb8_9:
			;
			if (xxx == 8)
			{
				// 008 RRC B
				modMain.regB = rrc(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 009 RRC C
				modMain.regC = rrc(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb10_11:
			;
			if (xxx == 10)
			{
				// 010 RRC D
				int argl4 = rrc(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl4);
				execute_cbRet = 8;
			}
			else
			{
				// 011 RRC E
				int argl5 = rrc(getE());
				setE(argl5);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb12_15:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb14_15;
			else
				goto ex_cb12_13;
			ex_cb12_13:
			;
			if (xxx == 12)
			{
				// 012 RRC H
				int argl6 = rrc(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl6);
				execute_cbRet = 8;
			}
			else
			{
				// 013 RRC L
				int localrrc() { int argans = modMain.regHL & 0xFF; var ret = rrc(argans); return ret; }

				int argl7 = localrrc();
				setL(argl7);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb14_15:
			;
			if (xxx == 14)
			{
				// 014 RRC (HL)
				int localrrc1() { int argans = peekb(modMain.regHL); var ret = rrc(argans); return ret; }

				int argnewByte1 = localrrc1();
				pokeb(modMain.regHL, argnewByte1);
				execute_cbRet = 15;
			}
			else
			{
				// 015 RRC A
				modMain.regA = rrc(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb16_31:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb24_31;
			else
				goto ex_cb16_23;
			ex_cb16_23:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb20_23;
			else
				goto ex_cb16_19;
			ex_cb16_19:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb18_19;
			else
				goto ex_cb16_17;
			ex_cb16_17:
			;
			if (xxx == 16)
			{
				// 016 RL B
				modMain.regB = rl(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 017 RL C
				modMain.regC = rl(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb18_19:
			;
			if (xxx == 18)
			{
				// 018 RL D
				int argl8 = rl(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl8);
				execute_cbRet = 8;
			}
			else
			{
				// 019 RL E
				int argl9 = rl(getE());
				setE(argl9);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb20_23:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb22_23;
			else
				goto ex_cb20_21;
			ex_cb20_21:
			;
			if (xxx == 20)
			{
				// 020 RL H
				int argl10 = rl(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl10);
				execute_cbRet = 8;
			}
			else
			{
				// 021 RL L
				int localrl() { int argans = modMain.regHL & 0xFF; var ret = rl(argans); return ret; }

				int argl11 = localrl();
				setL(argl11);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb22_23:
			;
			if (xxx == 22)
			{
				// 022 RL (HL)
				int localrl1() { int argans = peekb(modMain.regHL); var ret = rl(argans); return ret; }

				int argnewByte2 = localrl1();
				pokeb(modMain.regHL, argnewByte2);
				execute_cbRet = 15;
			}
			else
			{
				// 023 RL A
				modMain.regA = rl(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb24_31:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb28_31;
			else
				goto ex_cb24_27;
			ex_cb24_27:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb26_27;
			else
				goto ex_cb24_25;
			ex_cb24_25:
			;
			if (xxx == 24)
			{
				// 024 RR B
				modMain.regB = rr(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 025 RR C
				modMain.regC = rr(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb26_27:
			;
			if (xxx == 26)
			{
				// 026 RR D
				int argl12 = rr(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl12);
				execute_cbRet = 8;
			}
			else
			{
				// 027 RR E
				int argl13 = rr(getE());
				setE(argl13);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb28_31:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb30_31;
			else
				goto ex_cb28_29;
			ex_cb28_29:
			;
			if (xxx == 28)
			{
				// 028 RR H
				int argl14 = rr(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl14);
				execute_cbRet = 8;
			}
			else
			{
				// 029 RR L
				int localrr() { int argans = modMain.regHL & 0xFF; var ret = rr(argans); return ret; }

				int argl15 = localrr();
				setL(argl15);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb30_31:
			;
			if (xxx == 30)
			{
				// 030 RR (HL)
				int localrr1() { int argans = peekb(modMain.regHL); var ret = rr(argans); return ret; }

				int argnewByte3 = localrr1();
				pokeb(modMain.regHL, argnewByte3);
				execute_cbRet = 15;
			}
			else
			{
				// 031 RR A
				modMain.regA = rr(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb32_63:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb48_63;
			else
				goto ex_cb32_47;
			ex_cb32_47:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb40_47;
			else
				goto ex_cb32_39;
			ex_cb32_39:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb36_39;
			else
				goto ex_cb32_35;
			ex_cb32_35:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb34_35;
			else
				goto ex_cb32_33;
			ex_cb32_33:
			;
			if (xxx == 32)
			{
				// 32 ' SLA B
				modMain.regB = sla(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 33 ' SLA C
				modMain.regC = sla(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb34_35:
			;
			if (xxx == 34)
			{
				// 34 ' SLA D
				int argl16 = sla(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl16);
				execute_cbRet = 8;
			}
			else
			{
				// 35 ' SLA E
				int argl17 = sla(getE());
				setE(argl17);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb36_39:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb38_39;
			else
				goto ex_cb36_37;
			ex_cb36_37:
			;
			if (xxx == 36)
			{
				// 36 ' SLA H
				int argl18 = sla(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl18);
				execute_cbRet = 8;
			}
			else
			{
				// 37 ' SLA L
				int argl19 = sla(modMain.regHL & 0xFF);
				setL(argl19);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb38_39:
			;
			if (xxx == 38)
			{
				// 38 ' SLA (HL)
				int argnewByte4 = sla(peekb(modMain.regHL));
				pokeb(modMain.regHL, argnewByte4);
				execute_cbRet = 15;
			}
			else
			{
				// 39 ' SLA A
				modMain.regA = sla(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb40_47:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb44_47;
			else
				goto ex_cb40_43;
			ex_cb40_43:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb42_43;
			else
				goto ex_cb40_41;
			ex_cb40_41:
			;
			if (xxx == 40)
			{
				// 40 ' SRA B
				modMain.regB = sra(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 41 ' SRA C
				modMain.regC = sra(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb42_43:
			;
			if (xxx == 42)
			{
				// 42 ' SRA D
				int argl20 = sra(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl20);
				execute_cbRet = 8;
			}
			else
			{
				// 43 ' SRA E
				int argl21 = sra(getE());
				setE(argl21);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb44_47:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb46_47;
			else
				goto ex_cb44_45;
			ex_cb44_45:
			;
			if (xxx == 44)
			{
				// 44 ' SRA H
				int argl22 = sra(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl22);
				execute_cbRet = 8;
			}
			else
			{
				// 45  ' SRA L
				int argl23 = sra(modMain.regHL & 0xFF);
				setL(argl23);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb46_47:
			;
			if (xxx == 46)
			{
				// 46 ' SRA (HL)
				int argnewByte5 = sra(peekb(modMain.regHL));
				pokeb(modMain.regHL, argnewByte5);
				execute_cbRet = 15;
			}
			else
			{
				// 47 ' SRA A
				modMain.regA = sra(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb48_63:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb56_63;
			else
				goto ex_cb48_55;
			ex_cb48_55:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb52_55;
			else
				goto ex_cb48_51;
			ex_cb48_51:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb50_51;
			else
				goto ex_cb48_49;
			ex_cb48_49:
			;
			if (xxx == 48)
			{
				// 48 ' SLS B
				modMain.regB = sls(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 49 ' SLS C
				modMain.regC = sls(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb50_51:
			;
			if (xxx == 50)
			{
				// 50 ' SLS D
				int argl24 = sls(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl24);
				execute_cbRet = 8;
			}
			else
			{
				// 51 ' SLS E
				int argl25 = sls(getE());
				setE(argl25);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb52_55:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb54_55;
			else
				goto ex_cb52_53;
			ex_cb52_53:
			;
			if (xxx == 52)
			{
				// 52 ' SLS H
				int argl26 = sls(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl26);
				execute_cbRet = 8;
			}
			else
			{
				// 53 ' SLS L
				int argl27 = sls(modMain.regHL & 0xFF);
				setL(argl27);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb54_55:
			;
			if (xxx == 54)
			{
				// 54 ' SLS (HL)
				int argnewByte6 = sls(peekb(modMain.regHL));
				pokeb(modMain.regHL, argnewByte6);
				execute_cbRet = 15;
			}
			else
			{
				// 55 ' SLS A
				modMain.regA = sls(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb56_63:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb60_63;
			else
				goto ex_cb56_59;
			ex_cb56_59:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb58_59;
			else
				goto ex_cb56_57;
			ex_cb56_57:
			;
			if (xxx == 56)
			{
				// 56 ' SRL B
				modMain.regB = srl(modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 57 ' SRL C
				modMain.regC = srl(modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb58_59:
			;
			if (xxx == 58)
			{
				// 58 ' SRL D
				int argl28 = srl(modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl28);
				execute_cbRet = 8;
			}
			else
			{
				// 59 ' SRL E
				int argl29 = srl(getE());
				setE(argl29);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb60_63:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb62_63;
			else
				goto ex_cb60_61;
			ex_cb60_61:
			;
			if (xxx == 60)
			{
				// 60 ' SRL H
				int argl30 = srl(modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl30);
				execute_cbRet = 8;
			}
			else
			{
				// 61 ' SRL L
				int argl31 = srl(modMain.regHL & 0xFF);
				setL(argl31);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb62_63:
			;
			if (xxx == 62)
			{
				// 62 ' SRL (HL)
				int argnewByte7 = srl(peekb(modMain.regHL));
				pokeb(modMain.regHL, argnewByte7);
				execute_cbRet = 15;
			}
			else
			{
				// 63 ' SRL A
				modMain.regA = srl(modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb64_127:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_cb96_127;
			else
				goto ex_cb64_95;
			ex_cb64_95:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb80_95;
			else
				goto ex_cb64_79;
			ex_cb64_79:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb72_79;
			else
				goto ex_cb64_71;
			ex_cb64_71:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb68_71;
			else
				goto ex_cb64_67;
			ex_cb64_67:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb66_67;
			else
				goto ex_cb64_65;
			ex_cb64_65:
			;
			if (xxx == 64)
			{
				// 064 BIT 0,B
				int argb = 0x1;
				bit(argb, modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 065 ' BIT 0,C
				int argb1 = 1;
				bit(argb1, modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb66_67:
			;
			if (xxx == 66)
			{
				// 066 BIT 0,D
				int argb2 = 1;
				bit(argb2, modMain.glMemAddrDiv256[modMain.regDE]);
				execute_cbRet = 8;
			}
			else
			{
				// 067 BIT 0,E
				int argb3 = 1;
				bit(argb3, getE());
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb68_71:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb70_71;
			else
				goto ex_cb68_69;
			ex_cb68_69:
			;
			if (xxx == 68)
			{
				// 068 BIT 0,H
				int argb4 = 1;
				bit(argb4, modMain.glMemAddrDiv256[modMain.regHL]);
				execute_cbRet = 8;
			}
			else
			{
				// 069 BIT 0,L
				int argb5 = 1;
				int argr = modMain.regHL & 0xFF;
				bit(argb5, argr);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb70_71:
			;
			if (xxx == 70)
			{
				// 070 BIT 0,(HL)
				int argb6 = 1;
				int argr1 = peekb(modMain.regHL);
				bit(argb6, argr1);
				execute_cbRet = 12;
			}
			else
			{
				// 071 BIT 0,A
				int argb7 = 1;
				bit(argb7, modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb72_79:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb76_79;
			else
				goto ex_cb72_75;
			ex_cb72_75:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb74_75;
			else
				goto ex_cb72_73;
			ex_cb72_73:
			;
			if (xxx == 72)
			{
				// 72 ' BIT 1,B
				int argb8 = 2;
				bit(argb8, modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 73 ' BIT 1,C
				int argb9 = 2;
				bit(argb9, modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb74_75:
			;
			if (xxx == 74)
			{
				// 74 ' BIT 1,D
				int argb10 = 2;
				bit(argb10, modMain.glMemAddrDiv256[modMain.regDE]);
				execute_cbRet = 8;
			}
			else
			{
				// 75 ' BIT 1,E
				int argb11 = 2;
				bit(argb11, getE());
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb76_79:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb78_79;
			else
				goto ex_cb76_77;
			ex_cb76_77:
			;
			if (xxx == 76)
			{
				// 76 ' BIT 1,H
				int argb12 = 2;
				bit(argb12, modMain.glMemAddrDiv256[modMain.regHL]);
				execute_cbRet = 8;
			}
			else
			{
				// 77 ' BIT 1,L
				int argb13 = 2;
				int argr2 = modMain.regHL & 0xFF;
				bit(argb13, argr2);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb78_79:
			;
			if (xxx == 78)
			{
				// 78 ' BIT 1,(HL)
				int argb14 = 2;
				int argr3 = peekb(modMain.regHL);
				bit(argb14, argr3);
				execute_cbRet = 12;
			}
			else
			{
				// 79 ' BIT 1,A
				int argb15 = 2;
				bit(argb15, modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb80_95:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb88_95;
			else
				goto ex_cb80_87;
			ex_cb80_87:
			;
			switch (xxx)
			{
				case 80: // BIT 2,B
					{
						int argb16 = 4;
						bit(argb16, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 81: // BIT 2,C
					{
						int argb17 = 4;
						bit(argb17, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 82: // BIT 2,D
					{
						int argb18 = 4;
						bit(argb18, modMain.glMemAddrDiv256[modMain.regDE]);
						execute_cbRet = 8;
						break;
					}

				case 83: // BIT 2,E
					{
						int argb19 = 4;
						bit(argb19, getE());
						execute_cbRet = 8;
						break;
					}

				case 84: // BIT 2,H
					{
						int argb20 = 4;
						bit(argb20, modMain.glMemAddrDiv256[modMain.regHL]);
						execute_cbRet = 8;
						break;
					}

				case 85: // BIT 2,L
					{
						int argb21 = 4;
						int argr4 = modMain.regHL & 0xFF;
						bit(argb21, argr4);
						execute_cbRet = 8;
						break;
					}

				case 86: // BIT 2,(HL)
					{
						int argb22 = 4;
						int argr5 = peekb(modMain.regHL);
						bit(argb22, argr5);
						execute_cbRet = 12;
						break;
					}

				case 87: // BIT 2,A
					{
						int argb23 = 4;
						bit(argb23, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb88_95:
			;
			switch (xxx)
			{
				case 88: // BIT 3,B
					{
						int argb24 = 8;
						bit(argb24, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 89: // BIT 3,C
					{
						int argb25 = 8;
						bit(argb25, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 90: // BIT 3,D
					{
						int argb26 = 8;
						bit(argb26, modMain.glMemAddrDiv256[modMain.regDE]);
						execute_cbRet = 8;
						break;
					}

				case 91: // BIT 3,E
					{
						int argb27 = 8;
						bit(argb27, getE());
						execute_cbRet = 8;
						break;
					}

				case 92: // BIT 3,H
					{
						int argb28 = 8;
						bit(argb28, modMain.glMemAddrDiv256[modMain.regHL]);
						execute_cbRet = 8;
						break;
					}

				case 93: // BIT 3,L
					{
						int argb29 = 8;
						int argr6 = modMain.regHL & 0xFF;
						bit(argb29, argr6);
						execute_cbRet = 8;
						break;
					}

				case 94: // BIT 3,(HL)
					{
						int argb30 = 8;
						int argr7 = peekb(modMain.regHL);
						bit(argb30, argr7);
						execute_cbRet = 12;
						break;
					}

				case 95: // BIT 3,A
					{
						int argb31 = 8;
						bit(argb31, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb96_127:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb112_127;
			else
				goto ex_cb96_111;
			ex_cb96_111:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb104_111;
			else
				goto ex_cb96_103;
			ex_cb96_103:
			;
			switch (xxx)
			{
				case 96: // BIT 4,B
					{
						int argb32 = 0x10;
						bit(argb32, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 97: // BIT 4,C
					{
						int argb33 = 0x10;
						bit(argb33, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 98: // BIT 4,D
					{
						int argb34 = 0x10;
						bit(argb34, modMain.glMemAddrDiv256[modMain.regDE]);
						execute_cbRet = 8;
						break;
					}

				case 99: // BIT 4,E
					{
						int argb35 = 0x10;
						bit(argb35, getE());
						execute_cbRet = 8;
						break;
					}

				case 100: // BIT 4,H
					{
						int argb36 = 0x10;
						bit(argb36, modMain.glMemAddrDiv256[modMain.regHL]);
						execute_cbRet = 8;
						break;
					}

				case 101: // BIT 4,L
					{
						int argb37 = 0x10;
						int argr8 = modMain.regHL & 0xFF;
						bit(argb37, argr8);
						execute_cbRet = 8;
						break;
					}

				case 102: // BIT 4,(HL)
					{
						int argb38 = 0x10;
						int argr9 = peekb(modMain.regHL);
						bit(argb38, argr9);
						execute_cbRet = 12;
						break;
					}

				case 103: // BIT 4,A
					{
						int argb39 = 0x10;
						bit(argb39, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb104_111:
			;
			switch (xxx)
			{
				case 104: // BIT 5,B
					{
						int argb40 = 0x20;
						bit(argb40, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 105: // BIT 5,C
					{
						int argb41 = 0x20;
						bit(argb41, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 106: // BIT 5,D
					{
						int argb42 = 0x20;
						bit(argb42, modMain.glMemAddrDiv256[modMain.regDE]);
						execute_cbRet = 8;
						break;
					}

				case 107: // BIT 5,E
					{
						int argb43 = 0x20;
						bit(argb43, getE());
						execute_cbRet = 8;
						break;
					}

				case 108: // BIT 5,H
					{
						int argb44 = 0x20;
						bit(argb44, modMain.glMemAddrDiv256[modMain.regHL]);
						execute_cbRet = 8;
						break;
					}

				case 109: // BIT 5,L
					{
						int argb45 = 0x20;
						int argr10 = modMain.regHL & 0xFF;
						bit(argb45, argr10);
						execute_cbRet = 8;
						break;
					}

				case 110: // BIT 5,(HL)
					{
						int argb46 = 0x20;
						int argr11 = peekb(modMain.regHL);
						bit(argb46, argr11);
						execute_cbRet = 12;
						break;
					}

				case 111: // BIT 5,A
					{
						int argb47 = 0x20;
						bit(argb47, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb112_127:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb120_127;
			else
				goto ex_cb112_119;
			ex_cb112_119:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb116_119;
			else
				goto ex_cb112_115;
			ex_cb112_115:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb114_115;
			else
				goto ex_cb112_113;
			ex_cb112_113:
			;
			if (xxx == 112)
			{
				// 112 BIT 6,B
				int argb48 = 0x40;
				bit(argb48, modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 113 BIT 6,C
				int argb49 = 0x40;
				bit(argb49, modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb114_115:
			;
			if (xxx == 114)
			{
				// 114 BIT 6,D
				int argb50 = 0x40;
				bit(argb50, modMain.glMemAddrDiv256[modMain.regDE]);
				execute_cbRet = 8;
			}
			else
			{
				// 115 BIT 6,E
				int argb51 = 0x40;
				bit(argb51, getE());
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb116_119:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb118_119;
			else
				goto ex_cb116_117;
			ex_cb116_117:
			;
			if (xxx == 116)
			{
				// 116 BIT 6,H
				int argb52 = 0x40;
				bit(argb52, modMain.glMemAddrDiv256[modMain.regHL]);
				execute_cbRet = 8;
			}
			else
			{
				// 117 BIT 6,L
				int argb53 = 0x40;
				int argr12 = modMain.regHL & 0xFF;
				bit(argb53, argr12);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb118_119:
			;
			if (xxx == 118)
			{
				// 118 BIT 6,(HL)
				int argb54 = 0x40;
				int argr13 = peekb(modMain.regHL);
				bit(argb54, argr13);
				execute_cbRet = 12;
			}
			else
			{
				// 119 ' BIT 6,A
				int argb55 = 0x40;
				bit(argb55, modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb120_127:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb124_127;
			else
				goto ex_cb120_123;
			ex_cb120_123:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb122_123;
			else
				goto ex_cb120_121;
			ex_cb120_121:
			;
			if (xxx == 120)
			{
				// 120 BIT 7,B
				int argb56 = 0x80;
				bit(argb56, modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 121 BIT 7,C
				int argb57 = 0x80;
				bit(argb57, modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb122_123:
			;
			if (xxx == 122)
			{
				// 122 BIT 7,D
				int argb58 = 0x80;
				bit(argb58, modMain.glMemAddrDiv256[modMain.regDE]);
				execute_cbRet = 8;
			}
			else
			{
				// 123 BIT 7,E
				int argb59 = 0x80;
				bit(argb59, getE());
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb124_127:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb126_127;
			else
				goto ex_cb124_125;
			ex_cb124_125:
			;
			if (xxx == 124)
			{
				// 124 BIT 7,H
				int argb60 = 0x80;
				bit(argb60, modMain.glMemAddrDiv256[modMain.regHL]);
				execute_cbRet = 8;
			}
			else
			{
				// 125 BIT 7,L
				int argb61 = 0x80;
				int argr14 = modMain.regHL & 0xFF;
				bit(argb61, argr14);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb126_127:
			;
			if (xxx == 126)
			{
				// 126 BIT 7,(HL)
				int argb62 = 0x80;
				int argr15 = peekb(modMain.regHL);
				bit(argb62, argr15);
				execute_cbRet = 12;
			}
			else
			{
				// 127 BIT 7,A
				int argb63 = 0x80;
				bit(argb63, modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb128_255:
			;
			if (Conversions.ToBoolean(xxx & 64))
				goto ex_cb192_255;
			else
				goto ex_cb128_191;
			ex_cb128_191:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_cb160_191;
			else
				goto ex_cb128_159;
			ex_cb128_159:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb144_159;
			else
				goto ex_cb128_143;
			ex_cb128_143:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb136_143;
			else
				goto ex_cb128_135;
			ex_cb128_135:
			;
			switch (xxx)
			{
				case 128: // RES 0,B
					{
						int argbit = 1;
						modMain.regB = bitRes(argbit, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 129: // RES 0,C
					{
						int argbit1 = 1;
						modMain.regC = bitRes(argbit1, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 130: // RES 0,D
					{
						int argbit2 = 1;
						int argl32 = bitRes(argbit2, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl32);
						execute_cbRet = 8;
						break;
					}

				case 131: // RES 0,E
					{
						int argbit3 = 1;
						int argl33 = bitRes(argbit3, getE());
						setE(argl33);
						execute_cbRet = 8;
						break;
					}

				case 132: // RES 0,H
					{
						int argbit4 = 1;
						int argl34 = bitRes(argbit4, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl34);
						execute_cbRet = 8;
						break;
					}

				case 133: // RES 0,L
					{
						int localbitRes() { int argbit = 1; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl35 = localbitRes();
						setL(argl35);
						execute_cbRet = 8;
						break;
					}

				case 134: // RES 0,(HL)
					{
						int localbitRes1() { int argbit = 0x1; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte8 = localbitRes1();
						pokeb(modMain.regHL, argnewByte8);
						execute_cbRet = 15;
						break;
					}

				case 135: // RES 0,A
					{
						int argbit5 = 1;
						modMain.regA = bitRes(argbit5, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb136_143:
			;
			switch (xxx)
			{
				case 136: // RES 1,B
					{
						int argbit6 = 2;
						modMain.regB = bitRes(argbit6, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 137: // RES 1,C
					{
						int argbit7 = 2;
						modMain.regC = bitRes(argbit7, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 138: // RES 1,D
					{
						int argbit8 = 2;
						int argl36 = bitRes(argbit8, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl36);
						execute_cbRet = 8;
						break;
					}

				case 139: // RES 1,E
					{
						int argbit9 = 2;
						int argl37 = bitRes(argbit9, getE());
						setE(argl37);
						execute_cbRet = 8;
						break;
					}

				case 140: // RES 1,H
					{
						int argbit10 = 2;
						int argl38 = bitRes(argbit10, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl38);
						execute_cbRet = 8;
						break;
					}

				case 141: // RES 1,L
					{
						int localbitRes2() { int argbit = 2; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl39 = localbitRes2();
						setL(argl39);
						execute_cbRet = 8;
						break;
					}

				case 142: // RES 1,(HL)
					{
						int localbitRes3() { int argbit = 2; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte9 = localbitRes3();
						pokeb(modMain.regHL, argnewByte9);
						execute_cbRet = 15;
						break;
					}

				case 143: // RES 1,A
					{
						int argbit11 = 2;
						modMain.regA = bitRes(argbit11, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb144_159:
			;
			switch (xxx)
			{
				case 144: // RES 2,B
					{
						int argbit12 = 4;
						modMain.regB = bitRes(argbit12, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 145: // RES 2,C
					{
						int argbit13 = 4;
						modMain.regC = bitRes(argbit13, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 146: // RES 2,D
					{
						int argbit14 = 4;
						int argl40 = bitRes(argbit14, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl40);
						execute_cbRet = 8;
						break;
					}

				case 147: // RES 2,E
					{
						int argbit15 = 4;
						int argl41 = bitRes(argbit15, getE());
						setE(argl41);
						execute_cbRet = 8;
						break;
					}

				case 148: // RES 2,H
					{
						int argbit16 = 4;
						int argl42 = bitRes(argbit16, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl42);
						execute_cbRet = 8;
						break;
					}

				case 149: // RES 2,L
					{
						int localbitRes4() { int argbit = 4; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl43 = localbitRes4();
						setL(argl43);
						execute_cbRet = 8;
						break;
					}

				case 150: // RES 2,(HL)
					{
						int localbitRes5() { int argbit = 4; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte10 = localbitRes5();
						pokeb(modMain.regHL, argnewByte10);
						execute_cbRet = 15;
						break;
					}

				case 151: // RES 2,A
					{
						int argbit17 = 4;
						modMain.regA = bitRes(argbit17, modMain.regA);
						execute_cbRet = 8;
						break;
					}

				case 152: // RES 3,B
					{
						int argbit18 = 8;
						modMain.regB = bitRes(argbit18, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 153: // RES 3,C
					{
						int argbit19 = 8;
						modMain.regC = bitRes(argbit19, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 154: // RES 3,D
					{
						int argbit20 = 8;
						int argl44 = bitRes(argbit20, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl44);
						execute_cbRet = 8;
						break;
					}

				case 155: // RES 3,E
					{
						int argbit21 = 8;
						int argl45 = bitRes(argbit21, getE());
						setE(argl45);
						execute_cbRet = 8;
						break;
					}

				case 156: // RES 3,H
					{
						int argbit22 = 8;
						int argl46 = bitRes(argbit22, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl46);
						execute_cbRet = 8;
						break;
					}

				case 157: // RES 3,L
					{
						int localbitRes6() { int argbit = 8; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl47 = localbitRes6();
						setL(argl47);
						execute_cbRet = 8;
						break;
					}

				case 158: // RES 3,(HL)
					{
						int localbitRes7() { int argbit = 8; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte11 = localbitRes7();
						pokeb(modMain.regHL, argnewByte11);
						execute_cbRet = 15;
						break;
					}

				case 159: // RES 3,A
					{
						int argbit23 = 8;
						modMain.regA = bitRes(argbit23, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb160_191:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb176_191;
			else
				goto ex_cb160_175;
			ex_cb160_175:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb168_175;
			else
				goto ex_cb160_167;
			ex_cb160_167:
			;
			switch (xxx)
			{
				case 160: // RES 4,B
					{
						int argbit24 = 0x10;
						modMain.regB = bitRes(argbit24, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 161: // RES 4,C
					{
						int argbit25 = 0x10;
						modMain.regC = bitRes(argbit25, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 162: // RES 4,D
					{
						int argbit26 = 0x10;
						int argl48 = bitRes(argbit26, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl48);
						execute_cbRet = 8;
						break;
					}

				case 163: // RES 4,E
					{
						int argbit27 = 0x10;
						int argl49 = bitRes(argbit27, getE());
						setE(argl49);
						execute_cbRet = 8;
						break;
					}

				case 164: // RES 4,H
					{
						int argbit28 = 0x10;
						int argl50 = bitRes(argbit28, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl50);
						execute_cbRet = 8;
						break;
					}

				case 165: // RES 4,L
					{
						int localbitRes8() { int argbit = 0x10; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl51 = localbitRes8();
						setL(argl51);
						execute_cbRet = 8;
						break;
					}

				case 166: // RES 4,(HL)
					{
						int localbitRes9() { int argbit = 0x10; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte12 = localbitRes9();
						pokeb(modMain.regHL, argnewByte12);
						execute_cbRet = 15;
						break;
					}

				case 167: // RES 4,A
					{
						int argbit29 = 0x10;
						modMain.regA = bitRes(argbit29, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb168_175:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb172_175;
			else
				goto ex_cb168_171;
			ex_cb168_171:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb170_171;
			else
				goto ex_cb168_169;
			ex_cb168_169:
			;
			if (xxx == 168)
			{
				// 168 RES 5,B
				int argbit30 = 0x20;
				modMain.regB = bitRes(argbit30, modMain.regB);
				execute_cbRet = 8;
			}
			else
			{
				// 169 RES 5,C
				int argbit31 = 0x20;
				modMain.regC = bitRes(argbit31, modMain.regC);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb170_171:
			;
			if (xxx == 170)
			{
				// 170 RES 5,D
				int argbit32 = 0x20;
				int argl52 = bitRes(argbit32, modMain.glMemAddrDiv256[modMain.regDE]);
				setD(argl52);
				execute_cbRet = 8;
			}
			else
			{
				// 171 RES 5,E
				int argbit33 = 0x20;
				int argl53 = bitRes(argbit33, getE());
				setE(argl53);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb172_175:
			;
			switch (xxx)
			{
				case 172: // RES 5,H
					{
						int argbit34 = 0x20;
						int argl54 = bitRes(argbit34, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl54);
						execute_cbRet = 8;
						break;
					}

				case 173: // RES 5,L
					{
						int localbitRes10() { int argbit = 0x20; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl55 = localbitRes10();
						setL(argl55);
						execute_cbRet = 8;
						break;
					}

				case 174: // RES 5,(HL)
					{
						int localbitRes11() { int argbit = 0x20; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte13 = localbitRes11();
						pokeb(modMain.regHL, argnewByte13);
						execute_cbRet = 15;
						break;
					}

				case 175: // RES 5,A
					{
						int argbit35 = 0x20;
						modMain.regA = bitRes(argbit35, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb176_191:
			;
			switch (xxx)
			{
				case 176: // RES 6,B
					{
						int argbit36 = 0x40;
						modMain.regB = bitRes(argbit36, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 177: // RES 6,C
					{
						int argbit37 = 0x40;
						modMain.regC = bitRes(argbit37, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 178: // RES 6,D
					{
						int argbit38 = 0x40;
						int argl56 = bitRes(argbit38, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl56);
						execute_cbRet = 8;
						break;
					}

				case 179: // RES 6,E
					{
						int argbit39 = 0x40;
						int argl57 = bitRes(argbit39, getE());
						setE(argl57);
						execute_cbRet = 8;
						break;
					}

				case 180: // RES 6,H
					{
						int argbit40 = 0x40;
						int argl58 = bitRes(argbit40, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl58);
						execute_cbRet = 8;
						break;
					}

				case 181: // RES 6,L
					{
						int localbitRes12() { int argbit = 0x40; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl59 = localbitRes12();
						setL(argl59);
						execute_cbRet = 8;
						break;
					}

				case 182: // RES 6,(HL)
					{
						int localbitRes13() { int argbit = 0x40; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte14 = localbitRes13();
						pokeb(modMain.regHL, argnewByte14);
						execute_cbRet = 15;
						break;
					}

				case 183: // RES 6,A
					{
						int argbit41 = 0x40;
						modMain.regA = bitRes(argbit41, modMain.regA);
						execute_cbRet = 8;
						break;
					}

				case 184: // RES 7,B
					{
						int argbit42 = 0x80;
						modMain.regB = bitRes(argbit42, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 185: // RES 7,C
					{
						int argbit43 = 0x80;
						modMain.regC = bitRes(argbit43, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 186: // RES 7,D
					{
						int argbit44 = 0x80;
						int argl60 = bitRes(argbit44, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl60);
						execute_cbRet = 8;
						break;
					}

				case 187: // RES 7,E
					{
						int argbit45 = 0x80;
						int argl61 = bitRes(argbit45, getE());
						setE(argl61);
						execute_cbRet = 8;
						break;
					}

				case 188: // RES 7,H
					{
						int argbit46 = 0x80;
						int argl62 = bitRes(argbit46, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl62);
						execute_cbRet = 8;
						break;
					}

				case 189: // RES 7,L
					{
						int localbitRes14() { int argbit = 0x80; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl63 = localbitRes14();
						setL(argl63);
						execute_cbRet = 8;
						break;
					}

				case 190: // RES 7,(HL)
					{
						int localbitRes15() { int argbit = 0x80; int argval_Renamed = peekb(modMain.regHL); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte15 = localbitRes15();
						pokeb(modMain.regHL, argnewByte15);
						execute_cbRet = 15;
						break;
					}

				case 191: // RES 7,A
					{
						int argbit47 = 0x80;
						modMain.regA = bitRes(argbit47, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb192_255:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_cb224_255;
			else
				goto ex_cb192_223;
			ex_cb192_223:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb208_223;
			else
				goto ex_cb192_207;
			ex_cb192_207:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb200_207;
			else
				goto ex_cb192_199;
			ex_cb192_199:
			;
			switch (xxx)
			{
				case 192: // SET 0,B
					{
						int argbit48 = 1;
						modMain.regB = bitSet(argbit48, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 193: // SET 0,C
					{
						int argbit49 = 1;
						modMain.regC = bitSet(argbit49, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 194: // SET 0,D
					{
						int argbit50 = 1;
						int argl64 = bitSet(argbit50, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl64);
						execute_cbRet = 8;
						break;
					}

				case 195: // SET 0,E
					{
						int argbit51 = 1;
						int argl65 = bitSet(argbit51, getE());
						setE(argl65);
						execute_cbRet = 8;
						break;
					}

				case 196: // SET 0,H
					{
						int argbit52 = 1;
						int argl66 = bitSet(argbit52, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl66);
						execute_cbRet = 8;
						break;
					}

				case 197: // SET 0,L
					{
						int localbitSet() { int argbit = 1; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl67 = localbitSet();
						setL(argl67);
						execute_cbRet = 8;
						break;
					}

				case 198: // SET 0,(HL)
					{
						int localbitSet1() { int argbit = 1; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte16 = localbitSet1();
						pokeb(modMain.regHL, argnewByte16);
						execute_cbRet = 15;
						break;
					}

				case 199: // SET 0,A
					{
						int argbit53 = 1;
						modMain.regA = bitSet(argbit53, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb200_207:
			;
			switch (xxx)
			{
				case 200: // SET 1,B
					{
						int argbit54 = 2;
						modMain.regB = bitSet(argbit54, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 201: // SET 1,C
					{
						int argbit55 = 2;
						modMain.regC = bitSet(argbit55, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 202: // SET 1,D
					{
						int argbit56 = 2;
						int argl68 = bitSet(argbit56, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl68);
						execute_cbRet = 8;
						break;
					}

				case 203: // SET 1,E
					{
						int argbit57 = 2;
						int argl69 = bitSet(argbit57, getE());
						setE(argl69);
						execute_cbRet = 8;
						break;
					}

				case 204: // SET 1,H
					{
						int argbit58 = 2;
						int argl70 = bitSet(argbit58, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl70);
						execute_cbRet = 8;
						break;
					}

				case 205: // SET 1,L
					{
						int localbitSet2() { int argbit = 2; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl71 = localbitSet2();
						setL(argl71);
						execute_cbRet = 8;
						break;
					}

				case 206: // SET 1,(HL)
					{
						int localbitSet3() { int argbit = 2; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte17 = localbitSet3();
						pokeb(modMain.regHL, argnewByte17);
						execute_cbRet = 15;
						break;
					}

				case 207: // SET 1,A
					{
						int argbit59 = 2;
						modMain.regA = bitSet(argbit59, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb208_223:
			;
			switch (xxx)
			{
				case 208: // SET 2,B
					{
						int argbit60 = 4;
						modMain.regB = bitSet(argbit60, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 209: // SET 2,C
					{
						int argbit61 = 4;
						modMain.regC = bitSet(argbit61, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 210: // SET 2,D
					{
						int argbit62 = 4;
						int argl72 = bitSet(argbit62, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl72);
						execute_cbRet = 8;
						break;
					}

				case 211: // SET 2,E
					{
						int argbit63 = 4;
						int argl73 = bitSet(argbit63, getE());
						setE(argl73);
						execute_cbRet = 8;
						break;
					}

				case 212: // SET 2,H
					{
						int argbit64 = 4;
						int argl74 = bitSet(argbit64, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl74);
						execute_cbRet = 8;
						break;
					}

				case 213: // SET 2,L
					{
						int localbitSet4() { int argbit = 4; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl75 = localbitSet4();
						setL(argl75);
						execute_cbRet = 8;
						break;
					}

				case 214: // SET 2,(HL)
					{
						int localbitSet5() { int argbit = 0x4; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte18 = localbitSet5();
						pokeb(modMain.regHL, argnewByte18);
						execute_cbRet = 15;
						break;
					}

				case 215: // SET 2,A
					{
						int argbit65 = 4;
						modMain.regA = bitSet(argbit65, modMain.regA);
						execute_cbRet = 8;
						break;
					}

				case 216: // SET 3,B
					{
						int argbit66 = 8;
						modMain.regB = bitSet(argbit66, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 217: // SET 3,C
					{
						int argbit67 = 8;
						modMain.regC = bitSet(argbit67, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 218: // SET 3,D
					{
						int argbit68 = 8;
						int argl76 = bitSet(argbit68, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl76);
						execute_cbRet = 8;
						break;
					}

				case 219: // SET 3,E
					{
						int argbit69 = 8;
						int argl77 = bitSet(argbit69, getE());
						setE(argl77);
						execute_cbRet = 8;
						break;
					}

				case 220: // SET 3,H
					{
						int argbit70 = 8;
						int argl78 = bitSet(argbit70, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl78);
						execute_cbRet = 8;
						break;
					}

				case 221: // SET 3,L
					{
						int localbitSet6() { int argbit = 8; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl79 = localbitSet6();
						setL(argl79);
						execute_cbRet = 8;
						break;
					}

				case 222: // SET 3,(HL)
					{
						int localbitSet7() { int argbit = 0x8; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte19 = localbitSet7();
						pokeb(modMain.regHL, argnewByte19);
						execute_cbRet = 15;
						break;
					}

				case 223: // SET 3,A
					{
						int argbit71 = 8;
						modMain.regA = bitSet(argbit71, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb224_255:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_cb240_255;
			else
				goto ex_cb224_239;
			ex_cb224_239:
			;
			switch (xxx)
			{
				case 224: // SET 4,B
					{
						int argbit72 = 0x10;
						modMain.regB = bitSet(argbit72, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 225: // SET 4,C
					{
						int argbit73 = 0x10;
						modMain.regC = bitSet(argbit73, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 226: // SET 4,D
					{
						int argbit74 = 0x10;
						int argl80 = bitSet(argbit74, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl80);
						execute_cbRet = 8;
						break;
					}

				case 227: // SET 4,E
					{
						int argbit75 = 0x10;
						int argl81 = bitSet(argbit75, getE());
						setE(argl81);
						execute_cbRet = 8;
						break;
					}

				case 228: // SET 4,H
					{
						int argbit76 = 0x10;
						int argl82 = bitSet(argbit76, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl82);
						execute_cbRet = 8;
						break;
					}

				case 229: // SET 4,L
					{
						int localbitSet8() { int argbit = 0x10; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl83 = localbitSet8();
						setL(argl83);
						execute_cbRet = 8;
						break;
					}

				case 230: // SET 4,(HL)
					{
						int localbitSet9() { int argbit = 0x10; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte20 = localbitSet9();
						pokeb(modMain.regHL, argnewByte20);
						execute_cbRet = 15;
						break;
					}

				case 231: // SET 4,A
					{
						int argbit77 = 0x10;
						modMain.regA = bitSet(argbit77, modMain.regA);
						execute_cbRet = 8;
						break;
					}

				case 232: // SET 5,B
					{
						int argbit78 = 0x20;
						modMain.regB = bitSet(argbit78, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 233: // SET 5,C
					{
						int argbit79 = 0x20;
						modMain.regC = bitSet(argbit79, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 234: // SET 5,D
					{
						int argbit80 = 0x20;
						int argl84 = bitSet(argbit80, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl84);
						execute_cbRet = 8;
						break;
					}

				case 235: // SET 5,E
					{
						int argbit81 = 0x20;
						int argl85 = bitSet(argbit81, getE());
						setE(argl85);
						execute_cbRet = 8;
						break;
					}

				case 236: // SET 5,H
					{
						int argbit82 = 0x20;
						int argl86 = bitSet(argbit82, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl86);
						execute_cbRet = 8;
						break;
					}

				case 237: // SET 5,L
					{
						int localbitSet10() { int argbit = 0x20; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl87 = localbitSet10();
						setL(argl87);
						execute_cbRet = 8;
						break;
					}

				case 238: // SET 5,(HL)
					{
						int localbitSet11() { int argbit = 0x20; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte21 = localbitSet11();
						pokeb(modMain.regHL, argnewByte21);
						execute_cbRet = 15;
						break;
					}

				case 239: // SET 5,A
					{
						int argbit83 = 0x20;
						modMain.regA = bitSet(argbit83, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb240_255:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_cb248_255;
			else
				goto ex_cb240_247;
			ex_cb240_247:
			;
			switch (xxx)
			{
				case 240: // SET 6,B
					{
						int argbit84 = 0x40;
						modMain.regB = bitSet(argbit84, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 241: // SET 6,C
					{
						int argbit85 = 0x40;
						modMain.regC = bitSet(argbit85, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 242: // SET 6,D
					{
						int argbit86 = 0x40;
						int argl88 = bitSet(argbit86, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl88);
						execute_cbRet = 8;
						break;
					}

				case 243: // SET 6,E
					{
						int argbit87 = 0x40;
						int argl89 = bitSet(argbit87, getE());
						setE(argl89);
						execute_cbRet = 8;
						break;
					}

				case 244: // SET 6,H
					{
						int argbit88 = 0x40;
						int argl90 = bitSet(argbit88, modMain.glMemAddrDiv256[modMain.regHL]);
						setH(argl90);
						execute_cbRet = 8;
						break;
					}

				case 245: // SET 6,L
					{
						int localbitSet12() { int argbit = 0x40; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl91 = localbitSet12();
						setL(argl91);
						execute_cbRet = 8;
						break;
					}

				case 246: // SET 6,(HL)
					{
						int localbitSet13() { int argbit = 0x40; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte22 = localbitSet13();
						pokeb(modMain.regHL, argnewByte22);
						execute_cbRet = 15;
						break;
					}

				case 247: // SET 6,A
					{
						int argbit89 = 0x40;
						modMain.regA = bitSet(argbit89, modMain.regA);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb248_255:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_cb252_255;
			else
				goto ex_cb248_251;
			ex_cb248_251:
			;
			switch (xxx)
			{
				case 248: // SET 7,B
					{
						int argbit90 = 0x80;
						modMain.regB = bitSet(argbit90, modMain.regB);
						execute_cbRet = 8;
						break;
					}

				case 249: // SET 7,C
					{
						int argbit91 = 0x80;
						modMain.regC = bitSet(argbit91, modMain.regC);
						execute_cbRet = 8;
						break;
					}

				case 250: // SET 7,D
					{
						int argbit92 = 0x80;
						int argl92 = bitSet(argbit92, modMain.glMemAddrDiv256[modMain.regDE]);
						setD(argl92);
						execute_cbRet = 8;
						break;
					}

				case 251: // SET 7,E
					{
						int argbit93 = 0x80;
						int argl93 = bitSet(argbit93, getE());
						setE(argl93);
						execute_cbRet = 8;
						break;
					}
			}

			return execute_cbRet;
		ex_cb252_255:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_cb254_255;
			else
				goto ex_cb252_253;
			ex_cb252_253:
			;
			if (xxx == 252)
			{
				// 252 SET 7,H
				int argbit94 = 0x80;
				int argl94 = bitSet(argbit94, modMain.glMemAddrDiv256[modMain.regHL]);
				setH(argl94);
				execute_cbRet = 8;
			}
			else
			{
				// 253 SET 7,L
				int localbitSet14() { int argbit = 0x80; int argval_Renamed = modMain.regHL & 0xFF; var ret = bitSet(argbit, argval_Renamed); return ret; }

				int argl95 = localbitSet14();
				setL(argl95);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		ex_cb254_255:
			;
			if (xxx == 254)
			{
				// 254 SET 7,(HL)
				int localbitSet15() { int argbit = 0x80; int argval_Renamed = peekb(modMain.regHL); var ret = bitSet(argbit, argval_Renamed); return ret; }

				int argnewByte23 = localbitSet15();
				pokeb(modMain.regHL, argnewByte23);
				execute_cbRet = 15;
			}
			else
			{
				// 255 SET 7,A
				int argbit95 = 0x80;
				modMain.regA = bitSet(argbit95, modMain.regA);
				execute_cbRet = 8;
			}

			return execute_cbRet;
		}

		private static int execute_ed()
		{
			int execute_edRet = default;
			int dest, xxx, count, from;
			int c, b;

			// // Yes, I appreciate that GOTO's and labels are a hideous blashphemy!
			// // However, this code is the fastest possible way of fetching and handling
			// // Z80 instructions I could come up with. There are only 8 compares per
			// // instruction fetch rather than between 1 and 255 as required in
			// // the previous version of vbSpec with it's huge Case statement.
			// //
			// // I know it's slightly harder to follow the new code, but I think the
			// // speed increase justifies it. <CC>


			// // REFRESH 1
			modMain.intRTemp = modMain.intRTemp + 1;

			// // Inlined version of "xxx = nxtpcb()" suggested by Gonchuki and Woody
			if ((modMain.regPC & 49152) == 16384)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			xxx = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[modMain.regPC]], modMain.regPC & 16383];
			modMain.regPC = modMain.regPC + 1;
			if (Conversions.ToBoolean(xxx & 128))
				goto ex_ed128_255;
			else
				goto ex_ed0_127;
			ex_ed0_127:
			;
			if (Conversions.ToBoolean(xxx & 64))
			{
				goto ex_ed64_127;
			}
			else
			{
				// 000 to 063 = NOP * 2
				execute_edRet = 8;
				return execute_edRet;
			}

		ex_ed64_127:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_ed96_127;
			else
				goto ex_ed64_95;
			ex_ed64_95:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_ed80_95;
			else
				goto ex_ed64_79;
			ex_ed64_79:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_ed72_79;
			else
				goto ex_ed64_71;
			ex_ed64_71:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_ed68_71;
			else
				goto ex_ed64_67;
			ex_ed64_67:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_ed66_67;
			else
				goto ex_ed64_65;
			ex_ed64_65:
			;
			if (xxx == 64)
			{
				// 064 IN B,(c)
				modMain.regB = in_bc();
				execute_edRet = 12;
			}
			else
			{
				// 065 OUT (c),B
				int argport = modMain.regB * 256 | modMain.regC;
				modSpectrum.outb(argport, modMain.regB);
				execute_edRet = 12;
			}

			return execute_edRet;
		ex_ed66_67:
			;
			if (xxx == 66)
			{
				// 066 SBC HL,BC
				int argb = modMain.regB * 256 | modMain.regC;
				modMain.regHL = sbc16(modMain.regHL, argb);
				execute_edRet = 15;
			}
			else
			{
				// 067 LD (nn),BC
				int argaddr = nxtpcw();
				int argword = modMain.regB * 256 | modMain.regC;
				pokew(argaddr, argword);
				execute_edRet = 20;
			}

			return execute_edRet;
		ex_ed68_71:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_ed70_71;
			else
				goto ex_ed68_69;
			ex_ed68_69:
			;
			if (xxx == 68)
			{
				// 068 NEG
				neg_a();
				execute_edRet = 8;
			}
			else
			{
				// 069 RETn
				modMain.intIFF1 = modMain.intIFF2;
				poppc();
				execute_edRet = 14;
			}

			return execute_edRet;
		ex_ed70_71:
			;
			if (xxx == 70)
			{
				// 070 IM 0
				modMain.intIM = 0;
				execute_edRet = 8;
			}
			else
			{
				// 071 LD I,A
				modMain.intI = modMain.regA;
				execute_edRet = 9;
			}

			return execute_edRet;
		ex_ed72_79:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_ed76_79;
			else
				goto ex_ed72_75;
			ex_ed72_75:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_ed74_75;
			else
				goto ex_ed72_73;
			ex_ed72_73:
			;
			if (xxx == 72)
			{
				// 072 IN C,(c)
				modMain.regC = in_bc();
				execute_edRet = 12;
			}
			else
			{
				// 073 OUT (c),C
				int argport1 = modMain.regB * 256 | modMain.regC;
				modSpectrum.outb(argport1, modMain.regC);
				execute_edRet = 12;
			}

			return execute_edRet;
		ex_ed74_75:
			;
			if (xxx == 74)
			{
				// 074 ADC HL,BC
				int argb1 = modMain.regB * 256 | modMain.regC;
				modMain.regHL = adc16(modMain.regHL, argb1);
				execute_edRet = 15;
			}
			else
			{
				// 075 LD BC,(nn)
				int localpeekw() { int argaddr = nxtpcw(); var ret = peekw(argaddr); return ret; }

				int argnn = localpeekw();
				setBC(argnn);
				execute_edRet = 20;
			}

			return execute_edRet;
		ex_ed76_79:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_ed78_79;
			else
				goto ex_ed76_77;
			ex_ed76_77:
			;
			if (xxx == 76)
			{
				// 076 NEG
				neg_a();
				execute_edRet = 8;
			}
			else
			{
				// 077 RETI
				// // TOCHECK: according to the official Z80 docs, IFF2 does not get
				// //          copied to IFF1 for RETI - but in a real Z80 it is
				modMain.intIFF1 = modMain.intIFF2;
				poppc();
				execute_edRet = 14;
			}

			return execute_edRet;
		ex_ed78_79:
			;
			if (xxx == 78)
			{
				// 078 IM 0
				modMain.intIM = 0;
				execute_edRet = 8;
			}
			else
			{
				// 079 LD R,A
				modMain.intR = modMain.regA;
				modMain.intRTemp = modMain.intR;
				execute_edRet = 9;
			}

			return execute_edRet;
		ex_ed80_95:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_ed88_95;
			else
				goto ex_ed80_87;
			ex_ed80_87:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_ed84_87;
			else
				goto ex_ed80_83;
			ex_ed80_83:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_ed82_83;
			else
				goto ex_ed80_81;
			ex_ed80_81:
			;
			if (xxx == 80)
			{
				// 080 IN D,(c)
				int argl = in_bc();
				setD(argl);
				execute_edRet = 12;
			}
			else
			{
				// 081 OUT (c),D
				int argport2 = modMain.regB * 256 | modMain.regC;
				modSpectrum.outb(argport2, modMain.glMemAddrDiv256[modMain.regDE]);
				execute_edRet = 12;
			}

			return execute_edRet;
		ex_ed82_83:
			;
			if (xxx == 82)
			{
				// 082 SBC HL,DE
				modMain.regHL = sbc16(modMain.regHL, modMain.regDE);
				execute_edRet = 15;
			}
			else
			{
				// 083 LD (nn),DE
				int argaddr1 = nxtpcw();
				pokew(argaddr1, modMain.regDE);
				execute_edRet = 20;
			}

			return execute_edRet;
		ex_ed84_87:
			;
			switch (xxx)
			{
				case 84: // NEG
					{
						neg_a();
						execute_edRet = 8;
						break;
					}

				case 85: // RETn
					{
						modMain.intIFF1 = modMain.intIFF2;
						poppc();
						execute_edRet = 14;
						break;
					}

				case 86: // IM 1
					{
						modMain.intIM = 1;
						execute_edRet = 8;
						break;
					}

				case 87: // LD A,I
					{
						ld_a_i();
						execute_edRet = 9;
						break;
					}
			}

			return execute_edRet;
		ex_ed88_95:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_ed92_95;
			else
				goto ex_ed88_91;
			ex_ed88_91:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_ed90_91;
			else
				goto ex_ed88_89;
			ex_ed88_89:
			;
			if (xxx == 88)
			{
				// 088 IN E,(c)
				int argl1 = in_bc();
				setE(argl1);
				execute_edRet = 12;
			}
			else
			{
				// 089 OUT (c),E
				int argport3 = modMain.regB * 256 | modMain.regC;
				modSpectrum.outb(argport3, getE());
				execute_edRet = 12;
			}

			return execute_edRet;
		ex_ed90_91:
			;
			if (xxx == 90)
			{
				// 090 ADC HL,DE
				modMain.regHL = adc16(modMain.regHL, modMain.regDE);
				execute_edRet = 15;
			}
			else
			{
				// 091 LD DE,(nn)
				int argaddr2 = nxtpcw();
				modMain.regDE = peekw(argaddr2);
				execute_edRet = 20;
			}

			return execute_edRet;
		ex_ed92_95:
			;
			switch (xxx)
			{
				case 92: // NEG
					{
						neg_a();
						execute_edRet = 8;
						break;
					}

				case 93: // RETI
					{
						// // TOCHECK: according to the official Z80 docs, IFF2 does not get
						// //          copied to IFF1 for RETI - but in a real Z80 it is
						modMain.intIFF1 = modMain.intIFF2;
						poppc();
						execute_edRet = 14;
						break;
					}

				case 94: // IM 2
					{
						modMain.intIM = 2;
						execute_edRet = 8;
						break;
					}

				case 95: // LD A,R
					{
						ld_a_r();
						execute_edRet = 9;
						break;
					}
			}

			return execute_edRet;
		ex_ed96_127:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_ed112_127;
			else
				goto ex_ed96_111;
			ex_ed96_111:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_ed104_111;
			else
				goto ex_ed96_103;
			ex_ed96_103:
			;
			switch (xxx)
			{
				case 96: // IN H,(c)
					{
						int argl2 = in_bc();
						setH(argl2);
						execute_edRet = 12;
						break;
					}

				case 97: // OUT (c),H
					{
						int argport4 = modMain.regB * 256 | modMain.regC;
						modSpectrum.outb(argport4, modMain.glMemAddrDiv256[modMain.regHL]);
						execute_edRet = 12;
						break;
					}

				case 98: // SBC HL,HL
					{
						modMain.regHL = sbc16(modMain.regHL, modMain.regHL);
						execute_edRet = 15;
						break;
					}

				case 99: // LD (nn),HL
					{
						int argaddr3 = nxtpcw();
						pokew(argaddr3, modMain.regHL);
						execute_edRet = 20;
						break;
					}

				case 100: // NEG
					{
						neg_a();
						execute_edRet = 8;
						break;
					}

				case 101: // RETn
					{
						modMain.intIFF1 = modMain.intIFF2;
						poppc();
						execute_edRet = 14;
						break;
					}

				case 102: // IM 0
					{
						modMain.intIM = 0;
						execute_edRet = 8;
						break;
					}

				case 103: // RRD
					{
						rrd_a();
						execute_edRet = 18;
						break;
					}
			}

			return execute_edRet;
		ex_ed104_111:
			;
			switch (xxx)
			{
				case 104: // IN L,(c)
					{
						int argl3 = in_bc();
						setL(argl3);
						execute_edRet = 12;
						break;
					}

				case 105: // OUT (c),L
					{
						int argport5 = modMain.regB * 256 | modMain.regC;
						int argoutbyte = modMain.regHL & 0xFF;
						modSpectrum.outb(argport5, argoutbyte);
						execute_edRet = 12;
						break;
					}

				case 106: // ADC HL,HL
					{
						modMain.regHL = adc16(modMain.regHL, modMain.regHL);
						execute_edRet = 15;
						break;
					}

				case 107: // LD HL,(nn)
					{
						int argaddr4 = nxtpcw();
						modMain.regHL = peekw(argaddr4);
						execute_edRet = 20;
						break;
					}

				case 108: // NEG
					{
						neg_a();
						execute_edRet = 8;
						break;
					}

				case 109: // RETI
					{
						// // TOCHECK: according to the official Z80 docs, IFF2 does not get
						// //          copied to IFF1 for RETI - but in a real Z80 it is
						modMain.intIFF1 = modMain.intIFF2;
						poppc();
						execute_edRet = 14;
						break;
					}

				case 110: // IM 0
					{
						modMain.intIM = 0;
						execute_edRet = 8;
						break;
					}

				case 111: // RLD
					{
						rld_a();
						execute_edRet = 18;
						break;
					}
			}

			return execute_edRet;
		ex_ed112_127:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_ed120_127;
			else
				goto ex_ed112_119;
			ex_ed112_119:
			;
			switch (xxx)
			{
				case 112: // IN (c)
					{
						in_bc();
						execute_edRet = 12;
						break;
					}

				case 113: // OUT (c),0
					{
						int argport6 = modMain.regB * 256 | modMain.regC;
						int argoutbyte1 = 0;
						modSpectrum.outb(argport6, argoutbyte1);
						execute_edRet = 12;
						break;
					}

				case 114: // SBC HL,SP
					{
						modMain.regHL = sbc16(modMain.regHL, modMain.regSP);
						execute_edRet = 15;
						break;
					}

				case 115: // LD (nn),SP
					{
						int argaddr5 = nxtpcw();
						pokew(argaddr5, modMain.regSP);
						execute_edRet = 20;
						break;
					}

				case 116: // NEG
					{
						neg_a();
						execute_edRet = 8;
						break;
					}

				case 117: // RETn
					{
						modMain.intIFF1 = modMain.intIFF2;
						poppc();
						execute_edRet = 14;
						break;
					}

				case 118: // IM 1
					{
						modMain.intIM = 1;
						execute_edRet = 8;
						break;
					}

				case 119:
					{
						// // Undocumented NOP * 2
						execute_edRet = 8;
						break;
					}
			}

			return execute_edRet;
		ex_ed120_127:
			;
			switch (xxx)
			{
				case 120: // IN A,(c)
					{
						modMain.regA = in_bc();
						execute_edRet = 12;
						break;
					}

				case 121: // OUT (c),A
					{
						int argport7 = modMain.regB * 256 | modMain.regC;
						modSpectrum.outb(argport7, modMain.regA);
						execute_edRet = 12;
						break;
					}

				case 122: // ADC HL,SP
					{
						modMain.regHL = adc16(modMain.regHL, modMain.regSP);
						execute_edRet = 15;
						break;
					}

				case 123: // LD SP,(nn)
					{
						int argaddr6 = nxtpcw();
						modMain.regSP = peekw(argaddr6);
						execute_edRet = 20;
						break;
					}

				case 124: // NEG
					{
						neg_a();
						execute_edRet = 8;
						break;
					}

				case 125: // RETI
					{
						// // TOCHECK: according to the official Z80 docs, IFF2 does not get
						// //          copied to IFF1 for RETI - but in a real Z80 it is
						modMain.intIFF1 = modMain.intIFF2;
						poppc();
						execute_edRet = 14;
						break;
					}

				case 126: // IM 2
					{
						modMain.intIM = 2;
						execute_edRet = 8;
						break;
					}

				case 127: // NOP
					{
						execute_edRet = 8;
						break;
					}
			}

			return execute_edRet;
		ex_ed128_255:
			;
			if (Conversions.ToBoolean(xxx & 64))
				goto ex_ed192_255;
			else
				goto ex_ed128_191;
			ex_ed128_191:
			;
			if (Conversions.ToBoolean(xxx & 32))
			{
				goto ex_ed160_191;
			}
			else
			{
				// NOP * 2 (128 to 159)
				execute_edRet = 8;
				return execute_edRet;
			}

		ex_ed160_191:
			;
			switch (xxx)
			{
				// // xxI
				case 160: // LDI
					{
						int argnewByte = peekb(modMain.regHL);
						pokeb(modMain.regDE, argnewByte);
						if ((modMain.regDE & 49152) == 16384)
						{
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						modMain.regDE = modMain.regDE + 1 & 0xFFFF;
						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						int localdec16() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

						int argnn1 = localdec16();
						setBC(argnn1);
						modMain.fPV = Conversions.ToInteger((modMain.regB * 256 | modMain.regC) != 0);
						modMain.fH = Conversions.ToInteger(false);
						modMain.fN = Conversions.ToInteger(false);
						execute_edRet = 16;
						break;
					}

				case 161: // CPI
					{
						c = modMain.fC;
						int argb2 = peekb(modMain.regHL);
						cp_a(argb2);
						if ((modMain.regHL & 49152) == 16384)
						{
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						int localdec161() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

						int argnn2 = localdec161();
						setBC(argnn2);
						modMain.fPV = Conversions.ToInteger((modMain.regB * 256 | modMain.regC) != 0);
						modMain.fC = c;
						execute_edRet = 16;
						break;
					}

				case 162: // INI
					{
						int localinb() { int argport = modMain.regB * 256 | modMain.regC; var ret = modSpectrum.inb(argport); return ret; }

						int argnewByte1 = localinb();
						pokeb(modMain.regHL, argnewByte1);
						b = qdec8(modMain.regB);
						modMain.regB = b;
						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						modMain.fZ = Conversions.ToInteger(b == 0);
						modMain.fN = Conversions.ToInteger(true);
						execute_edRet = 16;
						break;
					}

				case 163: // OUTI
					{
						b = qdec8(modMain.regB);
						modMain.regB = b;
						int argport8 = modMain.regB * 256 | modMain.regC;
						int argoutbyte2 = peekb(modMain.regHL);
						modSpectrum.outb(argport8, argoutbyte2);
						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						modMain.fZ = Conversions.ToInteger(b == 0);
						modMain.fN = Conversions.ToInteger(true);
						execute_edRet = 16;
						break;
					}

				// /* xxD */
				case 168: // LDD
					{
						int argnewByte2 = peekb(modMain.regHL);
						pokeb(modMain.regDE, argnewByte2);
						modMain.regDE = dec16(modMain.regDE);
						modMain.regHL = dec16(modMain.regHL);
						int localdec162() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

						int argnn3 = localdec162();
						setBC(argnn3);
						modMain.fPV = Conversions.ToInteger((modMain.regB * 256 | modMain.regC) != 0);
						modMain.fH = Conversions.ToInteger(false);
						modMain.fN = Conversions.ToInteger(false);
						execute_edRet = 16;
						break;
					}

				case 169: // CPD
					{
						c = modMain.fC;
						int argb3 = peekb(modMain.regHL);
						cp_a(argb3);
						modMain.regHL = dec16(modMain.regHL);
						int localdec163() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

						int argnn4 = localdec163();
						setBC(argnn4);
						modMain.fPV = Conversions.ToInteger((modMain.regB * 256 | modMain.regC) != 0);
						modMain.fC = c;
						execute_edRet = 16;
						break;
					}

				case 170: // IND
					{
						int localinb1() { int argport = modMain.regB * 256 | modMain.regC; var ret = modSpectrum.inb(argport); return ret; }

						int argnewByte3 = localinb1();
						pokeb(modMain.regHL, argnewByte3);
						b = qdec8(modMain.regB);
						modMain.regB = b;
						modMain.regHL = dec16(modMain.regHL);
						modMain.fZ = Conversions.ToInteger(b == 0);
						modMain.fN = Conversions.ToInteger(true);
						execute_edRet = 16;
						break;
					}

				case 171: // OUTD
					{
						count = qdec8(modMain.regB);
						modMain.regB = count;
						int argport9 = modMain.regB * 256 | modMain.regC;
						int argoutbyte3 = peekb(modMain.regHL);
						modSpectrum.outb(argport9, argoutbyte3);
						modMain.regHL = dec16(modMain.regHL);
						modMain.fZ = Conversions.ToInteger(count == 0);
						modMain.fN = Conversions.ToInteger(true);
						execute_edRet = 16;
						break;
					}

				// // xxIR
				case 176: // LDIR
					{
						b = peekb(modMain.regHL);
						pokeb(modMain.regDE, b);
						if ((modMain.regHL & 49152) == 16384)
						{
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						if ((modMain.regDE & 49152) == 16384)
						{
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						modMain.regDE = modMain.regDE + 1 & 0xFFFF;
						int argnn5 = (ushort)(modMain.regB * 256 | modMain.regC) - 1;
						setBC(argnn5);
						modMain.fH = Conversions.ToInteger(false);
						modMain.fN = Conversions.ToInteger(false);
						modMain.f3 = Conversions.ToInteger((b & modMain.F_3) != 0);
						modMain.f5 = Conversions.ToInteger((b & modMain.F_5) != 0);
						if ((modMain.regB * 256 | modMain.regC) != 0)
						{
							modMain.regPC = modMain.regPC - 2;
							if ((modMain.regDE & 49152) == 16384)
							{
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
							}

							modMain.fPV = Conversions.ToInteger(true);
							execute_edRet = 21;
						}
						else
						{
							modMain.fPV = Conversions.ToInteger(false);
							execute_edRet = 16;
						}

						break;
					}

				// TempLocal_tstates = 0
				// count = getBC
				// dest = regDE
				// from = regHL
				// 
				// ' // REFRESH -2
				// intRTemp = intRTemp - 2
				// Do
				// pokeb dest, peekb(from)
				// 
				// If ((dest And 49152) = 16384) Then
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// End If
				// 
				// from = (from + 1) And 65535
				// dest = (dest + 1) And 65535
				// count = count - 1
				// TempLocal_tstates = TempLocal_tstates + 21
				// ' // REFRESH (2)
				// intRTemp = intRTemp + 2
				// If (TempLocal_tstates >= 0) Then
				// ' // interruptTriggered
				// Exit Do
				// End If
				// Loop While count <> 0
				// 
				// regPC = regPC - 2
				// fH = False
				// fN = False
				// fPV = True
				// 
				// If count = 0 Then
				// regPC = regPC + 2
				// TempLocal_tstates = TempLocal_tstates - 5
				// fPV = False
				// End If
				// regDE = dest
				// regHL = from
				// setBC count
				// 
				// execute_ed = TempLocal_tstates
				case 177: // CPIR
					{
						c = modMain.fC;
						b = peekb(modMain.regHL);
						cp_a(b);
						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						int localdec164() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

						int argnn6 = localdec164();
						setBC(argnn6);
						modMain.fC = c;
						modMain.f3 = Conversions.ToInteger((b & modMain.F_3) != 0);
						modMain.f5 = Conversions.ToInteger((b & modMain.F_5) != 0);
						c = Conversions.ToInteger((modMain.regB * 256 | modMain.regC) != 0);
						modMain.fPV = c;
						if (Conversions.ToBoolean(modMain.fPV & Conversions.ToInteger(modMain.fZ == Conversions.ToInteger(false))))
						{
							modMain.regPC = modMain.regPC - 2;
							execute_edRet = 21;
						}
						else
						{
							execute_edRet = 16;
						}

						break;
					}

				case 178: // INIR
					{
						int localinb2() { int argport = modMain.regB * 256 | modMain.regC; var ret = modSpectrum.inb(argport); return ret; }

						int argnewByte4 = localinb2();
						pokeb(modMain.regHL, argnewByte4);
						b = qdec8(modMain.regB);
						modMain.regB = b;
						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						modMain.fZ = Conversions.ToInteger(true);
						modMain.fN = Conversions.ToInteger(true);
						if (b != 0)
						{
							modMain.regPC = modMain.regPC - 2;
							execute_edRet = 21;
						}
						else
						{
							execute_edRet = 16;
						}

						break;
					}

				case 179: // OTIR
					{
						b = qdec8(modMain.regB);
						modMain.regB = b;
						int argport10 = modMain.regB * 256 | modMain.regC;
						int argoutbyte4 = peekb(modMain.regHL);
						modSpectrum.outb(argport10, argoutbyte4);
						modMain.regHL = modMain.regHL + 1 & 0xFFFF;
						modMain.fZ = Conversions.ToInteger(true);
						modMain.fN = Conversions.ToInteger(true);
						if (b != 0)
						{
							modMain.regPC = modMain.regPC - 2;
							execute_edRet = 21;
						}
						else
						{
							execute_edRet = 16;
						}

						break;
					}

				// // xxDR
				case 184: // LDDR
					{
						b = peekb(modMain.regHL);
						pokeb(modMain.regDE, b);
						if ((modMain.regHL & 49152) == 16384)
						{
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						if ((modMain.regDE & 49152) == 16384)
						{
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						modMain.regHL = dec16(modMain.regHL);
						modMain.regDE = dec16(modMain.regDE);
						int argnn7 = (ushort)(modMain.regB * 256 | modMain.regC) - 1;
						setBC(argnn7);
						modMain.fH = Conversions.ToInteger(false);
						modMain.fN = Conversions.ToInteger(false);
						modMain.f3 = Conversions.ToInteger((b & modMain.F_3) != 0);
						modMain.f5 = Conversions.ToInteger((b & modMain.F_5) != 0);
						if ((modMain.regB * 256 | modMain.regC) != 0)
						{
							modMain.regPC = modMain.regPC - 2;
							if ((modMain.regDE & 49152) == 16384)
							{
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
								modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
							}

							modMain.fPV = Conversions.ToInteger(true);
							execute_edRet = 21;
						}
						else
						{
							modMain.fPV = Conversions.ToInteger(false);
							execute_edRet = 16;
						}

						break;
					}

				// TempLocal_tstates = 0
				// count = getBC
				// dest = regDE
				// from = regHL
				// 
				// ' // REFRESH -2
				// intRTemp = intRTemp - 2
				// Do
				// pokeb dest, peekb(from)
				// 
				// If ((dest And 49152) = 16384) Then
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// glTStates = glTStates + glContentionTable(-glTStates+30)
				// End If
				// 
				// from = (from - 1) And 65535
				// dest = (dest - 1) And 65535
				// count = count - 1
				// 
				// TempLocal_tstates = TempLocal_tstates + 21
				// 
				// ' // REFRESH (2)
				// intRTemp = intRTemp + 2
				// 
				// If (TempLocal_tstates >= 0) Then
				// ' // interruptTriggered
				// Exit Do
				// End If
				// Loop While count <> 0
				// regPC = regPC - 2
				// fH = False
				// fN = False
				// fPV = True
				// If count = 0 Then
				// regPC = regPC + 2
				// TempLocal_tstates = TempLocal_tstates - 5
				// fPV = False
				// End If
				// 
				// regDE = dest
				// regHL = from
				// setBC count
				// 
				// execute_ed = TempLocal_tstates
				case 185: // CPDR
					{
						c = modMain.fC;
						b = peekb(modMain.regHL);
						cp_a(b);
						modMain.regHL = dec16(modMain.regHL);
						int localdec165() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

						int argnn8 = localdec165();
						setBC(argnn8);
						modMain.fPV = Conversions.ToInteger((modMain.regB * 256 | modMain.regC) != 0);
						modMain.fC = c;
						modMain.f3 = Conversions.ToInteger((b & modMain.F_3) != 0);
						modMain.f5 = Conversions.ToInteger((b & modMain.F_5) != 0);
						if (Conversions.ToBoolean(modMain.fPV & Conversions.ToInteger(modMain.fZ == Conversions.ToInteger(false))))
						{
							modMain.regPC = modMain.regPC - 2;
							execute_edRet = 21;
						}
						else
						{
							execute_edRet = 16;
						}

						break;
					}

				case 186: // INDR
					{
						int localinb3() { int argport = modMain.regB * 256 | modMain.regC; var ret = modSpectrum.inb(argport); return ret; }

						int argnewByte5 = localinb3();
						pokeb(modMain.regHL, argnewByte5);
						b = qdec8(modMain.regB);
						modMain.regB = b;
						modMain.regHL = dec16(modMain.regHL);
						modMain.fZ = Conversions.ToInteger(true);
						modMain.fN = Conversions.ToInteger(true);
						if (b != 0)
						{
							modMain.regPC = modMain.regPC - 2;
							execute_edRet = 21;
						}
						else
						{
							execute_edRet = 16;
						}

						break;
					}

				case 187: // OTDR
					{
						b = qdec8(modMain.regB);
						modMain.regB = b;
						int argport11 = modMain.regB * 256 | modMain.regC;
						int argoutbyte5 = peekb(modMain.regHL);
						modSpectrum.outb(argport11, argoutbyte5);
						modMain.regHL = dec16(modMain.regHL);
						modMain.fZ = Conversions.ToInteger(true);
						modMain.fN = Conversions.ToInteger(true);
						if (b != 0)
						{
							modMain.regPC = modMain.regPC - 2;
							execute_edRet = 21;
						}
						else
						{
							execute_edRet = 16;
						}

						break;
					}

				case var @case when 187 <= @case && @case <= 191:
					{
						Interaction.MsgBox("Unknown ED instruction " + xxx + " at " + modMain.regPC);
						execute_edRet = 8; // (164 To 167, 172 To 175, 180 To 183)
						break;
					}

				default:
					{
						// NOP * 2
						execute_edRet = 8;
						break;
					}
			}

			return execute_edRet;
		ex_ed192_255:
			;

			// // MsgBox "Unknown ED instruction " & xxx & " at " & regPC
			// NOP * 2
			execute_edRet = 8;
			return execute_edRet;
		}

		private static void execute_id_cb(int op, int z)
		{
			// // Yes, I appreciate that GOTO's and labels are a hideous blashphemy!
			// // However, this code is the fastest possible way of fetching and handling
			// // Z80 instructions I could come up with. There are only 8 compares per
			// // instruction fetch rather than between 1 and 255 as required in
			// // the previous version of vbSpec with it's huge Case statement.
			// //
			// // I know it's slightly harder to follow the new code, but I think the
			// // speed increase justifies it. <CC>


			if (Conversions.ToBoolean(op & 128))
				goto ex_id_cb128_255;
			else
				goto ex_id_cb0_127;
			ex_id_cb0_127:
			;
			if (Conversions.ToBoolean(op & 64))
				goto ex_id_cb64_127;
			else
				goto ex_id_cb0_63;
			ex_id_cb0_63:
			;
			if (Conversions.ToBoolean(op & 32))
				goto ex_id_cb32_63;
			else
				goto ex_id_cb0_31;
			ex_id_cb0_31:
			;
			if (Conversions.ToBoolean(op & 16))
				goto ex_id_cb16_31;
			else
				goto ex_id_cb0_15;
			ex_id_cb0_15:
			;
			if (Conversions.ToBoolean(op & 8))
				goto ex_id_cb8_15;
			else
				goto ex_id_cb0_7;
			ex_id_cb0_7:
			;
			if (Conversions.ToBoolean(op & 4))
				goto ex_id_cb4_7;
			else
				goto ex_id_cb0_3;
			ex_id_cb0_3:
			;
			if (Conversions.ToBoolean(op & 2))
				goto ex_id_cb2_3;
			else
				goto ex_id_cb0_1;
			ex_id_cb0_1:
			;
			if (op == 0)
			{
				// 000 RLC B
				int argans = peekb(z);
				op = rlc(argans);
				modMain.regB = op;
				pokeb(z, op);
			}
			else
			{
				// 001 RLC C
				int argans1 = peekb(z);
				op = rlc(argans1);
				modMain.regC = op;
				pokeb(z, op);
			}

			return;
		ex_id_cb2_3:
			;
			if (op == 2)
			{
				// 002 RLC D
				int argans2 = peekb(z);
				op = rlc(argans2);
				setD(op);
				pokeb(z, op);
			}
			else
			{
				// 003 RLC E
				int argans3 = peekb(z);
				op = rlc(argans3);
				setE(op);
				pokeb(z, op);
			}

			return;
		ex_id_cb4_7:
			;
			if (Conversions.ToBoolean(op & 2))
				goto ex_id_cb6_7;
			else
				goto ex_id_cb4_5;
			ex_id_cb4_5:
			;
			if (op == 4)
			{
				// 004 RLC H
				int argans4 = peekb(z);
				op = rlc(argans4);
				setH(op);
				pokeb(z, op);
			}
			else
			{
				// 005 RLC L
				int argans5 = peekb(z);
				op = rlc(argans5);
				setL(op);
				pokeb(z, op);
			}

			return;
		ex_id_cb6_7:
			;
			if (op == 6)
			{
				// 006 RLC (HL)
				int localrlc() { int argans = peekb(z); var ret = rlc(argans); return ret; }

				int argnewByte = localrlc();
				pokeb(z, argnewByte);
			}
			else
			{
				// 007 RLC A
				int argans6 = peekb(z);
				op = rlc(argans6);
				modMain.regA = op;
				pokeb(z, op);
			}

			return;
		ex_id_cb8_15:
			;
			if (Conversions.ToBoolean(op & 4))
				goto ex_id_cb12_15;
			else
				goto ex_id_cb8_11;
			ex_id_cb8_11:
			;
			if (Conversions.ToBoolean(op & 2))
				goto ex_id_cb10_11;
			else
				goto ex_id_cb8_9;
			ex_id_cb8_9:
			;
			if (op == 8)
			{
				// 008 RRC B
				int argans7 = peekb(z);
				op = rrc(argans7);
				modMain.regB = op;
				pokeb(z, op);
			}
			else
			{
				// 009 RRC C
				int argans8 = peekb(z);
				op = rrc(argans8);
				modMain.regC = op;
				pokeb(z, op);
			}

			return;
		ex_id_cb10_11:
			;
			if (op == 10)
			{
				// 010 RRC D
				int argans9 = peekb(z);
				op = rrc(argans9);
				setD(op);
				pokeb(z, op);
			}
			else
			{
				// 011 RRC E
				int argans10 = peekb(z);
				op = rrc(argans10);
				setE(op);
				pokeb(z, op);
			}

			return;
		ex_id_cb12_15:
			;
			if (Conversions.ToBoolean(op & 2))
				goto ex_id_cb14_15;
			else
				goto ex_id_cb12_13;
			ex_id_cb12_13:
			;
			if (op == 12)
			{
				// 012 RRC H
				int argans11 = peekb(z);
				op = rrc(argans11);
				setH(op);
				pokeb(z, op);
			}
			else
			{
				// 013 RRC L
				int argans12 = peekb(z);
				op = rrc(argans12);
				setL(op);
				pokeb(z, op);
			}

			return;
		ex_id_cb14_15:
			;
			if (op == 14)
			{
				// 014 RRC (HL)
				int localrrc() { int argans = peekb(z); var ret = rrc(argans); return ret; }

				int argnewByte1 = localrrc();
				pokeb(z, argnewByte1);
			}
			else
			{
				// 015 RRC A
				int argans13 = peekb(z);
				op = rrc(argans13);
				modMain.regA = op;
				pokeb(z, op);
			}

			return;
		ex_id_cb16_31:
			;
			switch (op)
			{
				case 16: // RL B
					{
						int argans14 = peekb(z);
						op = rl(argans14);
						modMain.regB = op;
						pokeb(z, op);
						break;
					}

				case 17: // RL C
					{
						int argans15 = peekb(z);
						op = rl(argans15);
						modMain.regC = op;
						pokeb(z, op);
						break;
					}

				case 18: // RL D
					{
						int argans16 = peekb(z);
						op = rl(argans16);
						setD(op);
						pokeb(z, op);
						break;
					}

				case 19: // RL E
					{
						int argans17 = peekb(z);
						op = rl(argans17);
						setE(op);
						pokeb(z, op);
						break;
					}

				case 20: // RL H
					{
						int argans18 = peekb(z);
						op = rl(argans18);
						setH(op);
						pokeb(z, op);
						break;
					}

				case 21: // RL L
					{
						int argans19 = peekb(z);
						op = rl(argans19);
						setL(op);
						pokeb(z, op);
						break;
					}

				case 22: // RL (HL)
					{
						int localrl() { int argans = peekb(z); var ret = rl(argans); return ret; }

						int argnewByte2 = localrl();
						pokeb(z, argnewByte2);
						break;
					}

				case 23: // RL A
					{
						int argans20 = peekb(z);
						op = rl(argans20);
						modMain.regA = op;
						pokeb(z, op);
						break;
					}

				case 24: // RR B
					{
						int argans21 = peekb(z);
						op = rr(argans21);
						modMain.regB = op;
						pokeb(z, op);
						break;
					}

				case 25: // RR C
					{
						int argans22 = peekb(z);
						op = rr(argans22);
						modMain.regC = op;
						pokeb(z, op);
						break;
					}

				case 26: // RR D
					{
						int argans23 = peekb(z);
						op = rr(argans23);
						setD(op);
						pokeb(z, op);
						break;
					}

				case 27: // RR E
					{
						int argans24 = peekb(z);
						op = rr(argans24);
						setE(op);
						pokeb(z, op);
						break;
					}

				case 28: // RR H
					{
						int argans25 = peekb(z);
						op = rr(argans25);
						setH(op);
						pokeb(z, op);
						break;
					}

				case 29: // RR L
					{
						int argans26 = peekb(z);
						op = rr(argans26);
						setL(op);
						pokeb(z, op);
						break;
					}

				case 30: // RR (HL)
					{
						int localrr() { int argans = peekb(z); var ret = rr(argans); return ret; }

						int argnewByte3 = localrr();
						pokeb(z, argnewByte3);
						break;
					}

				case 31: // RR A
					{
						int argans27 = peekb(z);
						op = rr(argans27);
						modMain.regA = op;
						pokeb(z, op);
						break;
					}
			}

			return;
		ex_id_cb32_63:
			;
			switch (op)
			{
				case 32: // SLA B
					{
						op = sla(peekb(z));
						modMain.regB = op;
						pokeb(z, op);
						break;
					}

				case 33: // SLA C
					{
						op = sla(peekb(z));
						modMain.regC = op;
						pokeb(z, op);
						break;
					}

				case 34: // SLA D
					{
						op = sla(peekb(z));
						setD(op);
						pokeb(z, op);
						break;
					}

				case 35: // SLA E
					{
						op = sla(peekb(z));
						setE(op);
						pokeb(z, op);
						break;
					}

				case 36: // SLA H
					{
						op = sla(peekb(z));
						setH(op);
						pokeb(z, op);
						break;
					}

				case 37: // SLA L
					{
						op = sla(peekb(z));
						setL(op);
						pokeb(z, op);
						break;
					}

				case 38: // SLA (HL)
					{
						int argnewByte4 = sla(peekb(z));
						pokeb(z, argnewByte4);
						break;
					}

				case 39: // SLA A
					{
						op = sla(peekb(z));
						modMain.regA = op;
						pokeb(z, op);
						break;
					}

				case 40: // SRA B
					{
						op = sra(peekb(z));
						modMain.regB = op;
						pokeb(z, op);
						break;
					}

				case 41: // SRA C
					{
						op = sra(peekb(z));
						modMain.regC = op;
						pokeb(z, op);
						break;
					}

				case 42: // SRA D
					{
						op = sra(peekb(z));
						setD(op);
						pokeb(z, op);
						break;
					}

				case 43: // SRA E
					{
						op = sra(peekb(z));
						setE(op);
						pokeb(z, op);
						break;
					}

				case 44: // SRA H
					{
						op = sra(peekb(z));
						setH(op);
						pokeb(z, op);
						break;
					}

				case 45: // SRA L
					{
						op = sra(peekb(z));
						setL(op);
						pokeb(z, op);
						break;
					}

				case 46: // SRA (HL)
					{
						int argnewByte5 = sra(peekb(z));
						pokeb(z, argnewByte5);
						break;
					}

				case 47: // SRA A
					{
						op = sra(peekb(z));
						modMain.regA = op;
						pokeb(z, op);
						break;
					}

				case 48: // SLS B
					{
						op = sls(peekb(z));
						modMain.regB = op;
						pokeb(z, op);
						break;
					}

				case 49: // SLS C
					{
						op = sls(peekb(z));
						modMain.regC = op;
						pokeb(z, op);
						break;
					}

				case 50: // SLS D
					{
						op = sls(peekb(z));
						setD(op);
						pokeb(z, op);
						break;
					}

				case 51: // SLS E
					{
						op = sls(peekb(z));
						setE(op);
						pokeb(z, op);
						break;
					}

				case 52: // SLS H
					{
						op = sls(peekb(z));
						setH(op);
						pokeb(z, op);
						break;
					}

				case 53: // SLS L
					{
						op = sls(peekb(z));
						setL(op);
						pokeb(z, op);
						break;
					}

				case 54: // SLS (HL)
					{
						int argnewByte6 = sls(peekb(z));
						pokeb(z, argnewByte6);
						break;
					}

				case 55: // SLS A
					{
						op = sls(peekb(z));
						modMain.regA = op;
						pokeb(z, op);
						break;
					}

				case 56: // SRL B
					{
						op = srl(peekb(z));
						modMain.regB = op;
						pokeb(z, op);
						break;
					}

				case 57: // SRL C
					{
						op = srl(peekb(z));
						modMain.regC = op;
						pokeb(z, op);
						break;
					}

				case 58: // SRL D
					{
						op = srl(peekb(z));
						setD(op);
						pokeb(z, op);
						break;
					}

				case 59: // SRL E
					{
						op = srl(peekb(z));
						setE(op);
						pokeb(z, op);
						break;
					}

				case 60: // SRL H
					{
						op = srl(peekb(z));
						setH(op);
						pokeb(z, op);
						break;
					}

				case 61: // SRL L
					{
						op = srl(peekb(z));
						setL(op);
						pokeb(z, op);
						break;
					}

				case 62: // SRL (ID)
					{
						int argnewByte7 = srl(peekb(z));
						pokeb(z, argnewByte7);
						break;
					}

				case 63: // SRL A
					{
						op = srl(peekb(z));
						modMain.regA = op;
						pokeb(z, op);
						break;
					}
			}

			return;
		ex_id_cb64_127:
			;
			if (Conversions.ToBoolean(op & 32))
				goto ex_id_cb96_127;
			else
				goto ex_id_cb64_95;
			ex_id_cb64_95:
			;
			if (Conversions.ToBoolean(op & 16))
				goto ex_id_cb80_95;
			else
				goto ex_id_cb64_79;
			ex_id_cb64_79:
			;
			if (Conversions.ToBoolean(op & 8))
			{
				// 072 to 079 BIT 1,B
				int argb = 0x2;
				int argr = peekb(z);
				bit(argb, argr);
			}
			else
			{
				// 064 to 071 BIT 0,B
				int argb1 = 0x1;
				int argr1 = peekb(z);
				bit(argb1, argr1);
			}

			return;
		ex_id_cb80_95:
			;
			if (Conversions.ToBoolean(op & 8))
			{
				// 088 to 095 BIT 3,B
				int argb2 = 0x8;
				int argr2 = peekb(z);
				bit(argb2, argr2);
			}
			else
			{
				// 080 To 087 BIT 2,B
				int argb3 = 0x4;
				int argr3 = peekb(z);
				bit(argb3, argr3);
			}

			return;
		ex_id_cb96_127:
			;
			if (Conversions.ToBoolean(op & 16))
				goto ex_id_cb112_127;
			else
				goto ex_id_cb96_111;
			ex_id_cb96_111:
			;
			if (Conversions.ToBoolean(op & 8))
			{
				// 104 to 111 BIT 5,B
				int argb4 = 0x20;
				int argr4 = peekb(z);
				bit(argb4, argr4);
			}
			else
			{
				// 096 to 103 BIT 4,B
				int argb5 = 0x10;
				int argr5 = peekb(z);
				bit(argb5, argr5);
			}

			return;
		ex_id_cb112_127:
			;
			if (Conversions.ToBoolean(op & 8))
			{
				// 120 to 127 BIT 7,B
				int argb6 = 0x80;
				int argr6 = peekb(z);
				bit(argb6, argr6);
			}
			else
			{
				// 112 To 119 BIT 6,B
				int argb7 = 0x40;
				int argr7 = peekb(z);
				bit(argb7, argr7);
			}

			return;
		ex_id_cb128_255:
			;
			if (Conversions.ToBoolean(op & 64))
				goto ex_id_cb192_255;
			else
				goto ex_id_cb128_191;
			ex_id_cb128_191:
			;
			switch (op)
			{
				case 128: // RES 0,(ID+y)->B
					{
						int argbit = 1;
						int argval_Renamed = peekb(z);
						modMain.regB = bitRes(argbit, argval_Renamed);
						pokeb(z, modMain.regB);
						break;
					}

				case 129: // RES 0,(ID+y)->C
					{
						int argbit1 = 1;
						int argval_Renamed1 = peekb(z);
						modMain.regC = bitRes(argbit1, argval_Renamed1);
						pokeb(z, modMain.regC);
						break;
					}

				case 130: // RES 0,(ID+y)->D
					{
						int localbitRes() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl = localbitRes();
						setD(argl);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 131: // RES 0,(ID+y)->E
					{
						int localbitRes1() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl1 = localbitRes1();
						setE(argl1);
						pokeb(z, getE());
						break;
					}

				case 132: // RES 0,(ID+y)->H
					{
						int localbitRes2() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl2 = localbitRes2();
						setH(argl2);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 133: // RES 0,(ID+y)->L
					{
						int localbitRes3() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl3 = localbitRes3();
						setL(argl3);
						int argnewByte8 = modMain.regHL & 0xFF;
						pokeb(z, argnewByte8);
						break;
					}

				case 134: // RES 0,(HL)
					{
						int localbitRes4() { int argbit = 0x1; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte9 = localbitRes4();
						pokeb(z, argnewByte9);
						break;
					}

				case 135: // RES 0,(ID+y)->A
					{
						int argbit2 = 1;
						int argval_Renamed2 = peekb(z);
						modMain.regA = bitRes(argbit2, argval_Renamed2);
						pokeb(z, modMain.regA);
						break;
					}

				case 136: // RES 1,(ID+y)->B
					{
						int argbit3 = 2;
						int argval_Renamed3 = peekb(z);
						modMain.regB = bitRes(argbit3, argval_Renamed3);
						pokeb(z, modMain.regB);
						break;
					}

				case 137: // RES 1,(ID+y)->C
					{
						int argbit4 = 2;
						int argval_Renamed4 = peekb(z);
						modMain.regC = bitRes(argbit4, argval_Renamed4);
						pokeb(z, modMain.regC);
						break;
					}

				case 138: // RES 1,(ID+y)->D
					{
						int localbitRes5() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl4 = localbitRes5();
						setD(argl4);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 139: // RES 1,(ID+y)->E
					{
						int localbitRes6() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl5 = localbitRes6();
						setE(argl5);
						pokeb(z, getE());
						break;
					}

				case 140: // RES 1,(ID+y)->H
					{
						int localbitRes7() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl6 = localbitRes7();
						setH(argl6);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 141: // RES 1,(ID+y)->L
					{
						int localbitRes8() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl7 = localbitRes8();
						setL(argl7);
						pokeb(z, getL());
						break;
					}

				case 142: // RES 1,(HL)
					{
						int localbitRes9() { int argbit = 0x2; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte10 = localbitRes9();
						pokeb(z, argnewByte10);
						break;
					}

				case 143: // RES 1,(ID+y)->A
					{
						int argbit5 = 2;
						int argval_Renamed5 = peekb(z);
						modMain.regA = bitRes(argbit5, argval_Renamed5);
						pokeb(z, modMain.regA);
						break;
					}

				case 144: // RES 2,(ID+y)->B
					{
						int argbit6 = 4;
						int argval_Renamed6 = peekb(z);
						modMain.regB = bitRes(argbit6, argval_Renamed6);
						pokeb(z, modMain.regB);
						break;
					}

				case 145: // RES 2,(ID+y)->C
					{
						int argbit7 = 4;
						int argval_Renamed7 = peekb(z);
						modMain.regC = bitRes(argbit7, argval_Renamed7);
						pokeb(z, modMain.regC);
						break;
					}

				case 146: // RES 2,(ID+y)->D
					{
						int localbitRes10() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl8 = localbitRes10();
						setD(argl8);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 147: // RES 2,(ID+y)->E
					{
						int localbitRes11() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl9 = localbitRes11();
						setE(argl9);
						pokeb(z, getE());
						break;
					}

				case 148: // RES 2,(ID+y)->H
					{
						int localbitRes12() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl10 = localbitRes12();
						setH(argl10);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 149: // RES 2,(ID+y)->L
					{
						int localbitRes13() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl11 = localbitRes13();
						setL(argl11);
						pokeb(z, getL());
						break;
					}

				case 150: // RES 2,(HL)
					{
						int localbitRes14() { int argbit = 0x4; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte11 = localbitRes14();
						pokeb(z, argnewByte11);
						break;
					}

				case 151: // RES 2,(ID+y)->A
					{
						int argbit8 = 4;
						int argval_Renamed8 = peekb(z);
						modMain.regA = bitRes(argbit8, argval_Renamed8);
						pokeb(z, modMain.regA);
						break;
					}

				case 152: // RES 3,(ID+y)->B
					{
						int argbit9 = 8;
						int argval_Renamed9 = peekb(z);
						modMain.regB = bitRes(argbit9, argval_Renamed9);
						pokeb(z, modMain.regB);
						break;
					}

				case 153: // RES 3,(ID+y)->C
					{
						int argbit10 = 8;
						int argval_Renamed10 = peekb(z);
						modMain.regC = bitRes(argbit10, argval_Renamed10);
						pokeb(z, modMain.regC);
						break;
					}

				case 154: // RES 3,(ID+y)->D
					{
						int localbitRes15() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl12 = localbitRes15();
						setD(argl12);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 155: // RES 3,(ID+y)->E
					{
						int localbitRes16() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl13 = localbitRes16();
						setE(argl13);
						pokeb(z, getE());
						break;
					}

				case 156: // RES 3,(ID+y)->H
					{
						int localbitRes17() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl14 = localbitRes17();
						setH(argl14);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 157: // RES 3,(ID+y)->L
					{
						int localbitRes18() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl15 = localbitRes18();
						setL(argl15);
						pokeb(z, getL());
						break;
					}

				case 158: // RES 3,(HL)
					{
						int localbitRes19() { int argbit = 0x8; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte12 = localbitRes19();
						pokeb(z, argnewByte12);
						break;
					}

				case 159: // RES 3,(ID+y)->A
					{
						int argbit11 = 8;
						int argval_Renamed11 = peekb(z);
						modMain.regA = bitRes(argbit11, argval_Renamed11);
						pokeb(z, modMain.regA);
						break;
					}

				case 160: // RES 4,(ID+y)->B
					{
						int argbit12 = 0x10;
						int argval_Renamed12 = peekb(z);
						modMain.regB = bitRes(argbit12, argval_Renamed12);
						pokeb(z, modMain.regB);
						break;
					}

				case 161: // RES 4,(ID+y)->C
					{
						int argbit13 = 0x10;
						int argval_Renamed13 = peekb(z);
						modMain.regC = bitRes(argbit13, argval_Renamed13);
						pokeb(z, modMain.regC);
						break;
					}

				case 162: // RES 4,(ID+y)->D
					{
						int localbitRes20() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl16 = localbitRes20();
						setD(argl16);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 163: // RES 4,(ID+y)->E
					{
						int localbitRes21() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl17 = localbitRes21();
						setE(argl17);
						pokeb(z, getE());
						break;
					}

				case 164: // RES 4,(ID+y)->H
					{
						int localbitRes22() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl18 = localbitRes22();
						setH(argl18);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 165: // RES 4,(ID+y)->L
					{
						int localbitRes23() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl19 = localbitRes23();
						setL(argl19);
						pokeb(z, getL());
						break;
					}

				case 166: // RES 4,(HL)
					{
						int localbitRes24() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte13 = localbitRes24();
						pokeb(z, argnewByte13);
						break;
					}

				case 167: // RES 4,(ID+y)->A
					{
						int argbit14 = 0x10;
						int argval_Renamed14 = peekb(z);
						modMain.regA = bitRes(argbit14, argval_Renamed14);
						pokeb(z, modMain.regA);
						break;
					}

				case 168: // RES 5,(ID+y)->B
					{
						int argbit15 = 0x20;
						int argval_Renamed15 = peekb(z);
						modMain.regB = bitRes(argbit15, argval_Renamed15);
						pokeb(z, modMain.regB);
						break;
					}

				case 169: // RES 5,(ID+y)->C
					{
						int argbit16 = 0x20;
						int argval_Renamed16 = peekb(z);
						modMain.regC = bitRes(argbit16, argval_Renamed16);
						pokeb(z, modMain.regC);
						break;
					}

				case 170: // RES 5,(ID+y)->D
					{
						int localbitRes25() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl20 = localbitRes25();
						setD(argl20);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 171: // RES 5,(ID+y)->E
					{
						int localbitRes26() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl21 = localbitRes26();
						setE(argl21);
						pokeb(z, getE());
						break;
					}

				case 172: // RES 5,(ID+y)->H
					{
						int localbitRes27() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl22 = localbitRes27();
						setH(argl22);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 173: // RES 5,(ID+y)->L
					{
						int localbitRes28() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl23 = localbitRes28();
						setL(argl23);
						pokeb(z, getL());
						break;
					}

				case 174: // RES 5,(HL)
					{
						int localbitRes29() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte14 = localbitRes29();
						pokeb(z, argnewByte14);
						break;
					}

				case 175: // RES 5,(ID+y)->A
					{
						int argbit17 = 0x20;
						int argval_Renamed17 = peekb(z);
						modMain.regA = bitRes(argbit17, argval_Renamed17);
						pokeb(z, modMain.regA);
						break;
					}

				case 176: // RES 6,(ID+y)->B
					{
						int argbit18 = 0x40;
						int argval_Renamed18 = peekb(z);
						modMain.regB = bitRes(argbit18, argval_Renamed18);
						pokeb(z, modMain.regB);
						break;
					}

				case 177: // RES 6,(ID+y)->C
					{
						int argbit19 = 0x40;
						int argval_Renamed19 = peekb(z);
						modMain.regC = bitRes(argbit19, argval_Renamed19);
						pokeb(z, modMain.regC);
						break;
					}

				case 178: // RES 6,(ID+y)->D
					{
						int localbitRes30() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl24 = localbitRes30();
						setD(argl24);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 179: // RES 6,(ID+y)->E
					{
						int localbitRes31() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl25 = localbitRes31();
						setE(argl25);
						pokeb(z, getE());
						break;
					}

				case 180: // RES 6,(ID+y)->H
					{
						int localbitRes32() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl26 = localbitRes32();
						setH(argl26);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 181: // RES 6,(ID+y)->L
					{
						int localbitRes33() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl27 = localbitRes33();
						setL(argl27);
						pokeb(z, getL());
						break;
					}

				case 182: // RES 6,(HL)
					{
						int localbitRes34() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte15 = localbitRes34();
						pokeb(z, argnewByte15);
						break;
					}

				case 183: // RES 6,(ID+y)->A
					{
						int argbit20 = 0x40;
						int argval_Renamed20 = peekb(z);
						modMain.regA = bitRes(argbit20, argval_Renamed20);
						pokeb(z, modMain.regA);
						break;
					}

				case 184: // RES 6,(ID+y)->B
					{
						int argbit21 = 0x80;
						int argval_Renamed21 = peekb(z);
						modMain.regB = bitRes(argbit21, argval_Renamed21);
						pokeb(z, modMain.regB);
						break;
					}

				case 185: // RES 6,(ID+y)->C
					{
						int argbit22 = 0x80;
						int argval_Renamed22 = peekb(z);
						modMain.regC = bitRes(argbit22, argval_Renamed22);
						pokeb(z, modMain.regC);
						break;
					}

				case 186: // RES 6,(ID+y)->D
					{
						int localbitRes35() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl28 = localbitRes35();
						setD(argl28);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 187: // RES 6,(ID+y)->E
					{
						int localbitRes36() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl29 = localbitRes36();
						setE(argl29);
						pokeb(z, getE());
						break;
					}

				case 188: // RES 6,(ID+y)->H
					{
						int localbitRes37() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl30 = localbitRes37();
						setH(argl30);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 189: // RES 6,(ID+y)->L
					{
						int localbitRes38() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argl31 = localbitRes38();
						setL(argl31);
						pokeb(z, getL());
						break;
					}

				case 190: // RES 7,(HL)
					{
						int localbitRes39() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitRes(argbit, argval_Renamed); return ret; }

						int argnewByte16 = localbitRes39();
						pokeb(z, argnewByte16);
						break;
					}

				case 191: // RES 7,(ID+y)->A
					{
						int argbit23 = 0x80;
						int argval_Renamed23 = peekb(z);
						modMain.regA = bitRes(argbit23, argval_Renamed23);
						pokeb(z, modMain.regA);
						break;
					}
			}

			return;
		ex_id_cb192_255:
			;
			switch (op)
			{
				case 192: // SET 0,(ID+y)->B
					{
						int argbit24 = 1;
						int argval_Renamed24 = peekb(z);
						modMain.regB = bitSet(argbit24, argval_Renamed24);
						pokeb(z, modMain.regB);
						break;
					}

				case 193: // SET 0,(ID+y)->C
					{
						int argbit25 = 1;
						int argval_Renamed25 = peekb(z);
						modMain.regC = bitSet(argbit25, argval_Renamed25);
						pokeb(z, modMain.regC);
						break;
					}

				case 194: // SET 0,(ID+y)->D
					{
						int localbitSet() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl32 = localbitSet();
						setD(argl32);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 195: // SET 0,(ID+y)->E
					{
						int localbitSet1() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl33 = localbitSet1();
						setE(argl33);
						pokeb(z, getE());
						break;
					}

				case 196: // SET 0,(ID+y)->H
					{
						int localbitSet2() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl34 = localbitSet2();
						setH(argl34);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 197: // SET 0,(ID+y)->L
					{
						int localbitSet3() { int argbit = 1; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl35 = localbitSet3();
						setL(argl35);
						pokeb(z, getL());
						break;
					}

				case 198: // SET 0,(HL)
					{
						int localbitSet4() { int argbit = 0x1; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte17 = localbitSet4();
						pokeb(z, argnewByte17);
						break;
					}

				case 199: // SET 0,(ID+y)->A
					{
						int argbit26 = 1;
						int argval_Renamed26 = peekb(z);
						modMain.regA = bitSet(argbit26, argval_Renamed26);
						pokeb(z, modMain.regA);
						break;
					}

				case 200: // SET 1,(ID+y)->B
					{
						int argbit27 = 2;
						int argval_Renamed27 = peekb(z);
						modMain.regB = bitSet(argbit27, argval_Renamed27);
						pokeb(z, modMain.regB);
						break;
					}

				case 201: // SET 1,(ID+y)->C
					{
						int argbit28 = 2;
						int argval_Renamed28 = peekb(z);
						modMain.regC = bitSet(argbit28, argval_Renamed28);
						pokeb(z, modMain.regC);
						break;
					}

				case 202: // SET 1,(ID+y)->D
					{
						int localbitSet5() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl36 = localbitSet5();
						setD(argl36);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 203: // SET 1,(ID+y)->E
					{
						int localbitSet6() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl37 = localbitSet6();
						setE(argl37);
						pokeb(z, getE());
						break;
					}

				case 204: // SET 1,(ID+y)->H
					{
						int localbitSet7() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl38 = localbitSet7();
						setH(argl38);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 205: // SET 1,(ID+y)->L
					{
						int localbitSet8() { int argbit = 2; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl39 = localbitSet8();
						setL(argl39);
						pokeb(z, getL());
						break;
					}

				case 206: // SET 1,(HL)
					{
						int localbitSet9() { int argbit = 0x2; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte18 = localbitSet9();
						pokeb(z, argnewByte18);
						break;
					}

				case 207: // SET 1,(ID+y)->A
					{
						int argbit29 = 2;
						int argval_Renamed29 = peekb(z);
						modMain.regA = bitSet(argbit29, argval_Renamed29);
						pokeb(z, modMain.regA);
						break;
					}

				case 208: // SET 2,(ID+y)->B
					{
						int argbit30 = 4;
						int argval_Renamed30 = peekb(z);
						modMain.regB = bitSet(argbit30, argval_Renamed30);
						pokeb(z, modMain.regB);
						break;
					}

				case 209: // SET 2,(ID+y)->C
					{
						int argbit31 = 4;
						int argval_Renamed31 = peekb(z);
						modMain.regC = bitSet(argbit31, argval_Renamed31);
						pokeb(z, modMain.regC);
						break;
					}

				case 210: // SET 2,(ID+y)->D
					{
						int localbitSet10() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl40 = localbitSet10();
						setD(argl40);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 211: // SET 2,(ID+y)->E
					{
						int localbitSet11() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl41 = localbitSet11();
						setE(argl41);
						pokeb(z, getE());
						break;
					}

				case 212: // SET 2,(ID+y)->H
					{
						int localbitSet12() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl42 = localbitSet12();
						setH(argl42);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 213: // SET 2,(ID+y)->L
					{
						int localbitSet13() { int argbit = 4; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl43 = localbitSet13();
						setL(argl43);
						pokeb(z, getL());
						break;
					}

				case 214: // SET 2,(HL)
					{
						int localbitSet14() { int argbit = 0x4; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte19 = localbitSet14();
						pokeb(z, argnewByte19);
						break;
					}

				case 215: // SET 2,(ID+y)->A
					{
						int argbit32 = 4;
						int argval_Renamed32 = peekb(z);
						modMain.regA = bitSet(argbit32, argval_Renamed32);
						pokeb(z, modMain.regA);
						break;
					}

				case 216: // SET 3,(ID+y)->B
					{
						int argbit33 = 8;
						int argval_Renamed33 = peekb(z);
						modMain.regB = bitSet(argbit33, argval_Renamed33);
						pokeb(z, modMain.regB);
						break;
					}

				case 217: // SET 3,(ID+y)->C
					{
						int argbit34 = 8;
						int argval_Renamed34 = peekb(z);
						modMain.regC = bitSet(argbit34, argval_Renamed34);
						pokeb(z, modMain.regC);
						break;
					}

				case 218: // SET 3,(ID+y)->D
					{
						int localbitSet15() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl44 = localbitSet15();
						setD(argl44);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 219: // SET 3,(ID+y)->E
					{
						int localbitSet16() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl45 = localbitSet16();
						setE(argl45);
						pokeb(z, getE());
						break;
					}

				case 220: // SET 3,(ID+y)->H
					{
						int localbitSet17() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl46 = localbitSet17();
						setH(argl46);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 221: // SET 3,(ID+y)->L
					{
						int localbitSet18() { int argbit = 8; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl47 = localbitSet18();
						setL(argl47);
						pokeb(z, getL());
						break;
					}

				case 222: // SET 3,(HL)
					{
						int localbitSet19() { int argbit = 0x8; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte20 = localbitSet19();
						pokeb(z, argnewByte20);
						break;
					}

				case 223: // SET 3,(ID+y)->A
					{
						int argbit35 = 8;
						int argval_Renamed35 = peekb(z);
						modMain.regA = bitSet(argbit35, argval_Renamed35);
						pokeb(z, modMain.regA);
						break;
					}

				case 224: // SET 4,(ID+y)->B
					{
						int argbit36 = 0x10;
						int argval_Renamed36 = peekb(z);
						modMain.regB = bitSet(argbit36, argval_Renamed36);
						pokeb(z, modMain.regB);
						break;
					}

				case 225: // SET 4,(ID+y)->C
					{
						int argbit37 = 0x10;
						int argval_Renamed37 = peekb(z);
						modMain.regC = bitSet(argbit37, argval_Renamed37);
						pokeb(z, modMain.regC);
						break;
					}

				case 226: // SET 4,(ID+y)->D
					{
						int localbitSet20() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl48 = localbitSet20();
						setD(argl48);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 227: // SET 4,(ID+y)->E
					{
						int localbitSet21() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl49 = localbitSet21();
						setE(argl49);
						pokeb(z, getE());
						break;
					}

				case 228: // SET 4,(ID+y)->H
					{
						int localbitSet22() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl50 = localbitSet22();
						setH(argl50);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 229: // SET 4,(ID+y)->L
					{
						int localbitSet23() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl51 = localbitSet23();
						setL(argl51);
						pokeb(z, getL());
						break;
					}

				case 230: // SET 4,(HL)
					{
						int localbitSet24() { int argbit = 0x10; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte21 = localbitSet24();
						pokeb(z, argnewByte21);
						break;
					}

				case 231: // SET 4,(ID+y)->A
					{
						int argbit38 = 0x10;
						int argval_Renamed38 = peekb(z);
						modMain.regA = bitSet(argbit38, argval_Renamed38);
						pokeb(z, modMain.regA);
						break;
					}

				case 232: // SET 5,(ID+y)->B
					{
						int argbit39 = 0x20;
						int argval_Renamed39 = peekb(z);
						modMain.regB = bitSet(argbit39, argval_Renamed39);
						pokeb(z, modMain.regB);
						break;
					}

				case 233: // SET 5,(ID+y)->C
					{
						int argbit40 = 0x20;
						int argval_Renamed40 = peekb(z);
						modMain.regC = bitSet(argbit40, argval_Renamed40);
						pokeb(z, modMain.regC);
						break;
					}

				case 234: // SET 5,(ID+y)->D
					{
						int localbitSet25() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl52 = localbitSet25();
						setD(argl52);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 235: // SET 5,(ID+y)->E
					{
						int localbitSet26() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl53 = localbitSet26();
						setE(argl53);
						pokeb(z, getE());
						break;
					}

				case 236: // SET 5,(ID+y)->H
					{
						int localbitSet27() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl54 = localbitSet27();
						setH(argl54);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 237: // SET 5,(ID+y)->L
					{
						int localbitSet28() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl55 = localbitSet28();
						setL(argl55);
						pokeb(z, getL());
						break;
					}

				case 238: // SET 5,(HL)
					{
						int localbitSet29() { int argbit = 0x20; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte22 = localbitSet29();
						pokeb(z, argnewByte22);
						break;
					}

				case 239: // SET 5,(ID+y)->A
					{
						int argbit41 = 0x20;
						int argval_Renamed41 = peekb(z);
						modMain.regA = bitSet(argbit41, argval_Renamed41);
						pokeb(z, modMain.regA);
						break;
					}

				case 240: // SET 6,(ID+y)->B
					{
						int argbit42 = 0x40;
						int argval_Renamed42 = peekb(z);
						modMain.regB = bitSet(argbit42, argval_Renamed42);
						pokeb(z, modMain.regB);
						break;
					}

				case 241: // SET 6,(ID+y)->C
					{
						int argbit43 = 0x40;
						int argval_Renamed43 = peekb(z);
						modMain.regC = bitSet(argbit43, argval_Renamed43);
						pokeb(z, modMain.regC);
						break;
					}

				case 242: // SET 6,(ID+y)->D
					{
						int localbitSet30() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl56 = localbitSet30();
						setD(argl56);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 243: // SET 6,(ID+y)->E
					{
						int localbitSet31() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl57 = localbitSet31();
						setE(argl57);
						pokeb(z, getE());
						break;
					}

				case 244: // SET 6,(ID+y)->H
					{
						int localbitSet32() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl58 = localbitSet32();
						setH(argl58);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 245: // SET 6,(ID+y)->L
					{
						int localbitSet33() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl59 = localbitSet33();
						setL(argl59);
						pokeb(z, getL());
						break;
					}

				case 246: // SET 6,(HL)
					{
						int localbitSet34() { int argbit = 0x40; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte23 = localbitSet34();
						pokeb(z, argnewByte23);
						break;
					}

				case 247: // SET 6,(ID+y)->A
					{
						int argbit44 = 0x40;
						int argval_Renamed44 = peekb(z);
						modMain.regA = bitSet(argbit44, argval_Renamed44);
						pokeb(z, modMain.regA);
						break;
					}

				case 248: // SET 7,(ID+y)->B
					{
						int argbit45 = 0x80;
						int argval_Renamed45 = peekb(z);
						modMain.regB = bitSet(argbit45, argval_Renamed45);
						pokeb(z, modMain.regB);
						break;
					}

				case 249: // SET 7,(ID+y)->C
					{
						int argbit46 = 0x80;
						int argval_Renamed46 = peekb(z);
						modMain.regC = bitSet(argbit46, argval_Renamed46);
						pokeb(z, modMain.regC);
						break;
					}

				case 250: // SET 7,(ID+y)->D
					{
						int localbitSet35() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl60 = localbitSet35();
						setD(argl60);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regDE]);
						break;
					}

				case 251: // SET 7,(ID+y)->E
					{
						int localbitSet36() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl61 = localbitSet36();
						setE(argl61);
						pokeb(z, getE());
						break;
					}

				case 252: // SET 7,(ID+y)->H
					{
						int localbitSet37() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl62 = localbitSet37();
						setH(argl62);
						pokeb(z, modMain.glMemAddrDiv256[modMain.regHL]);
						break;
					}

				case 253: // SET 7,(ID+y)->L
					{
						int localbitSet38() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argl63 = localbitSet38();
						setL(argl63);
						pokeb(z, getL());
						break;
					}

				case 254: // SET 7,(HL)
					{
						int localbitSet39() { int argbit = 0x80; int argval_Renamed = peekb(z); var ret = bitSet(argbit, argval_Renamed); return ret; }

						int argnewByte24 = localbitSet39();
						pokeb(z, argnewByte24);
						break;
					}

				case 255: // SET 7,A
					{
						int argbit47 = 0x80;
						int argval_Renamed47 = peekb(z);
						modMain.regA = bitSet(argbit47, argval_Renamed47);
						pokeb(z, modMain.regA);
						break;
					}
			}
		}

		private static void exx()
		{
			int t;
			t = modMain.regHL;
			modMain.regHL = modMain.regHL_;
			modMain.regHL_ = t;
			t = modMain.regDE;
			modMain.regDE = modMain.regDE_;
			modMain.regDE_ = t;
			t = modMain.regB * 256 | modMain.regC;
			setBC(modMain.regBC_);
			modMain.regBC_ = t;
		}

		public static int getAF()
		{
			int getAFRet = default;
			getAFRet = modMain.regA * 256 | getF();
			return getAFRet;
		}

		private static int getBC()
		{
			int getBCRet = default;
			getBCRet = modMain.regB * 256 | modMain.regC;
			return getBCRet;
		}

		private static int getD()
		{
			int getDRet = default;
			getDRet = modMain.glMemAddrDiv256[modMain.regDE];
			return getDRet;
		}

		private static int getE()
		{
			int getERet = default;
			getERet = modMain.regDE & 0xFF;
			return getERet;
		}

		public static int getF()
		{
			int getFRet = default;
			// // Another of Gonchucki's performance improvements
			// // I've modified it slightly to use the "-(fS)" to turn
			// // the boolean values into 0/1 rather than the slower
			// // "Abs(fS)" method
			getFRet = -modMain.fS * modMain.F_S | -modMain.fZ * modMain.F_Z | -modMain.f5 * modMain.F_5 | -modMain.fH * modMain.F_H | -modMain.f3 * modMain.F_3 | -modMain.fPV * modMain.F_PV | -modMain.fN * modMain.F_N | -modMain.fC * modMain.F_C;
			return getFRet;
			// If fS Then getF = getF Or F_S
			// If fZ Then getF = getF Or F_Z
			// If f5 Then getF = getF Or F_5
			// If fH Then getF = getF Or F_H
			// If f3 Then getF = getF Or F_3
			// If fPV Then getF = getF Or F_PV
			// If fN Then getF = getF Or F_N
			// If fC Then getF = getF Or F_C
		}

		private static int getH()
		{
			int getHRet = default;
			getHRet = modMain.glMemAddrDiv256[modMain.regHL];
			return getHRet;
		}

		private static int getIDH()
		{
			int getIDHRet = default;
			getIDHRet = modMain.glMemAddrDiv256[modMain.regID];
			return getIDHRet;
		}

		private static int getIDL()
		{
			int getIDLRet = default;
			getIDLRet = modMain.regID & 0xFF;
			return getIDLRet;
		}

		private static int getL()
		{
			int getLRet = default;
			getLRet = modMain.regHL & 0xFF;
			return getLRet;
		}

		private static int id_d()
		{
			int id_dRet = default;
			int d;
			d = nxtpcb();
			if (Conversions.ToBoolean(d & 128))
				d = -(256 - d);
			id_dRet = modMain.regID + d & 0xFFFF;
			return id_dRet;
		}

		private static void ld_a_i()
		{
			modMain.fS = Conversions.ToInteger((modMain.intI & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.intI & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.intI & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(modMain.intI == 0);
			modMain.fPV = modMain.intIFF2;
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			modMain.regA = modMain.intI;
		}

		private static void ld_a_r()
		{
			modMain.intRTemp = modMain.intRTemp & 0x7F;
			modMain.regA = modMain.intR & 0x80 | modMain.intRTemp;
			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fPV = modMain.intIFF2;
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
		}

		private static void neg_a()
		{
			int t;
			t = modMain.regA;
			modMain.regA = 0;
			sub_a(t);
		}

		private static int peekb_NoContendedDelay(int addr)
		{
			int peekb_NoContendedDelayRet = default;
			peekb_NoContendedDelayRet = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[addr]], addr & 16383];
			return peekb_NoContendedDelayRet;

			// // Handle memory contention with ULA
			// If (addr And 32768) = 0 Then
			// If (glTStates > glTStatesAtTop) And (glTStates < glTStatesAtBottom) Then
			// glContendedMemoryDelay = glContendedMemoryDelay + 4
			// End If
			// End If
		}

		private static void pokeb_NoContendedDelay(int addr, int newByte)
		{
			if (addr < 16384)
			{
				// // ROM
				return;
			}

			int lPage, lOffset;
			lPage = modMain.glPageAt[modMain.glMemAddrDiv16384[addr]];
			lOffset = addr & 16383;
			if (modMain.gRAMPage[lPage, lOffset] == newByte)
				return;
			modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[addr]], addr & 16383] = (byte)newByte;
			if (modMain.glUseScreen == 5 & addr < 23296)
			{
				modSpectrum.plot(addr);
				return;
			}
			else if (modMain.glUseScreen == 7)
			{
				if (addr > 49151 & addr < 56064)
				{
					int argaddr = addr & 32767;
					modSpectrum.plot(argaddr);
				}

				return;
			}
			else if (modMain.glUseScreen == 1001)
			{
				// // TC2048 "Screen 1" (Standard display, but based at 24576)
				if (addr > 24575 & addr < 32768)
				{
					int argaddr1 = addr - 8192;
					modSpectrum.plot(argaddr1);
				}

				return;
			}
			else if (modMain.glUseScreen == 1002)
			{
				// // TC2048 Hi-Colour Screen (6144 bytes display at 16384, 6144 bytes ATTRS at 24575)
				if (addr > 16383 & addr < 22528)
				{
					modSpectrum.plot(addr);
					return;
				}

				if (addr > 24575 & addr < 31620)
				{
					int argaddr2 = addr - 8192;
					modSpectrum.plot(argaddr2);
				}

				return;
			}
			else if (modMain.glUseScreen == 1006)
			{
				// // TC2048 Hi-Resolution Screen (512x192,2 colour)
				if (addr > 16383 & addr < 22528)
				{
					modSpectrum.plotTC2048HiResLowArea(addr);
					return;
				}
				else if (addr > 24575 & addr < 31620)
				{
					modSpectrum.plotTC2048HiResHiArea(addr);
				}

				return;
			}
		}

		private static void rld_a()
		{
			int t, q;
			t = peekb(modMain.regHL);
			q = t;
			t = t * 16 | modMain.regA & 0xF;
			modMain.regA = modMain.regA & 0xF0 | q / 16;
			int argnewByte = t & 0xFF;
			pokeb(modMain.regHL, argnewByte);
			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fPV = modMain.intIFF2;
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
		}

		private static void setIDH(int byteval)
		{
			modMain.regID = byteval * 256 & 0xFF00 | modMain.regID & 0xFF;
		}

		private static void setIDL(int byteval)
		{
			modMain.regID = modMain.regID & 0xFF00 | byteval & 0xFF;
		}

		private static int in_bc()
		{
			int in_bcRet = default;
			int ans;
			int argport = modMain.regB * 256 | modMain.regC;
			ans = modSpectrum.inb(argport);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fPV = modMain.Parity[ans];
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(false);
			in_bcRet = ans;
			return in_bcRet;
		}

		private static int inc8(int ans)
		{
			int inc8Ret = default;
			modMain.fPV = Conversions.ToInteger(ans == 0x7F);
			modMain.fH = Conversions.ToInteger(((short)(ans & 0xF) + 1 & modMain.F_H) != 0);
			ans = ans + 1 & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fN = Conversions.ToInteger(false);
			inc8Ret = ans;
			return inc8Ret;
		}

		private static int dec8(int ans)
		{
			int dec8Ret = default;
			modMain.fPV = Conversions.ToInteger(ans == 0x80);
			modMain.fH = Conversions.ToInteger(((short)(ans & 0xF) - 1 & modMain.F_H) != 0);
			ans = ans - 1 & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fN = Conversions.ToInteger(true);
			dec8Ret = ans;
			return dec8Ret;
		}

		private static int interrupt()
		{
			int interruptRet = default;
			int lSleep, lCounter;
			modMain.interruptCounter = modMain.interruptCounter + 1;

			// // Attribute flash (state alters every 1/2 second)
			if (modMain.interruptCounter % 16 == 0)
			{
				modSpectrum.refreshFlashChars();
			}

			// // Paint the frame
			modSpectrum.screenPaint();
			if (modSpectrum.glLastBorder != modSpectrum.glNewBorder)
			{
				My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(modSpectrum.glNewBorder);
				modSpectrum.glLastBorder = modSpectrum.glNewBorder;
			}

			// // PERFORM Z80 hardware functions

			// // If it's a maskable interrupt
			if (modMain.intIFF1 == Conversions.ToInteger(false))
			{
				interruptRet = 0;
			}
			else
			{
				switch (modMain.intIM)
				{
					case 0:
					case 1:
						{
							pushpc();
							modMain.intIFF1 = Conversions.ToInteger(false);
							modMain.intIFF2 = Conversions.ToInteger(false);
							modMain.regPC = 56;
							interruptRet = 13;
							break;
						}

					case 2:
						{
							pushpc();
							modMain.intIFF1 = Conversions.ToInteger(false);
							modMain.intIFF2 = Conversions.ToInteger(false);
							int argaddr = modMain.intI * 256 | 0xFF;
							modMain.regPC = peekw(argaddr);
							interruptRet = 19;
							break;
						}
				}
			}

			if (Conversions.ToBoolean(modMain.gbSoundEnabled))
			{
				modMain.glBufNum = modMain.glBufNum + 1;
				for (lCounter = modWaveOut.glWavePtr; lCounter <= modWaveOut.WAV_BUFFER_SIZE; lCounter++)
					modWaveOut.gcWaveOut[lCounter] = modWaveOut.gcWaveOut[modWaveOut.glWavePtr - 1];
				// CopyPtrFromStruct(gtWavHdr(glBufNum).lpData, gcWaveOut(0), WAV_BUFFER_SIZE)
				// waveOutWrite(glphWaveOut, gtWavHdr(glBufNum), Len(gtWavHdr(glBufNum)))
				if (modMain.glBufNum == modWaveOut.NUM_WAV_BUFFERS)
					modMain.glBufNum = 0;
			}

			modWaveOut.glWavePtr = 0;
			Application.DoEvents();
			// // Keep the emulation running at the correct speed by
			// // adding a delay to ensure that interrupts are
			// // generated at the correct frequency

			// // lSleep = #ms since last interrupt
			lSleep = modMain.timeGetTime() - modMain.glInterruptTimer;
			if (lSleep == 0)
				lSleep = 1;
			if (lSleep < modMain.glInterruptDelay + 1)
			{
				// // too quick, we need to put in a delay

				// // 20 - 12 - overage  (overage=2)
				lCounter = modMain.glInterruptDelay - lSleep - modMain.glDelayOverage;
				if (lCounter < 0)
				{
					modMain.glDelayOverage = -lCounter;
				}
				else
				{
					modMain.glDelayOverage = modMain.glDelayOverage - (modMain.glInterruptDelay - lSleep - lCounter);
					modMain.Sleep(lCounter);
				}
			}
			else
			{
				modMain.glDelayOverage = modMain.glDelayOverage + lSleep - modMain.glInterruptDelay;
				if (modMain.glDelayOverage > 48)
					modMain.glDelayOverage = 48;
			}

			modMain.glInterruptTimer = modMain.timeGetTime();
			return interruptRet;
		}

		private static void or_a(int b)
		{
			modMain.regA = modMain.regA | b;
			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fH = Conversions.ToInteger(false);
			modMain.fPV = modMain.Parity[modMain.regA];
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fC = Conversions.ToInteger(false);
		}

		private static int peekw(int addr)
		{
			int peekwRet = default;
			if ((addr & 49152) == 16384)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			peekwRet = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[addr]], addr & 16383];
			if ((addr + 1 & 49152) == 16384)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			peekwRet = peekwRet | modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[addr + 1]], addr + 1 & 16383] * 256;
			return peekwRet;

			// // peekw = peekb(addr) Or (peekb((addr + 1)) * 256&)
		}

		public static void pokeb(int addr, int newByte)
		{
			if (addr < 16384)
			{
				// // ROM
				return;
			}

			int lPage, lOffset;
			lPage = modMain.glPageAt[modMain.glMemAddrDiv16384[addr]];
			lOffset = addr & 16383;
			if (modMain.gRAMPage[lPage, lOffset] == newByte)
			{
				if ((addr & 32768) == 0)
					modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
				return;
			}

			modMain.gRAMPage[lPage, lOffset] = (byte)newByte;

			// // Handle memory contention with ULA
			if ((addr & 32768) == 0)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			if (modMain.glUseScreen == 5 & addr < 23296)
			{
				modSpectrum.plot(addr);
				return;
			}
			else if (modMain.glUseScreen == 7)
			{
				if (addr > 49151 & addr < 56064)
				{
					int argaddr = addr & 32767;
					modSpectrum.plot(argaddr);
				}

				return;
			}
			else if (modMain.glUseScreen == 1001)
			{
				// // TC2048 "Screen 1" (Standard display, but based at 24576)
				if (addr > 24575 & addr < 32768)
				{
					int argaddr1 = addr - 8192;
					modSpectrum.plot(argaddr1);
				}

				return;
			}
			else if (modMain.glUseScreen == 1002)
			{
				// // TC2048 Hi-Colour Screen (6144 bytes display at 16384, 6144 bytes ATTRS at 24575)
				if (addr > 16383 & addr < 22528)
				{
					modSpectrum.plot(addr);
					return;
				}

				if (addr > 24575 & addr < 31620)
				{
					int argaddr2 = addr - 8192;
					modSpectrum.plot(argaddr2);
				}

				return;
			}
			else if (modMain.glUseScreen == 1006)
			{
				// // TC2048 Hi-Resolution Screen (512x192,2 colour)
				if (addr > 16383 & addr < 22528)
				{
					modSpectrum.plotTC2048HiResLowArea(addr);
					return;
				}
				else if (addr > 24575 & addr < 31620)
				{
					modSpectrum.plotTC2048HiResHiArea(addr);
				}

				return;
			}
		}

		private static void pokew(int addr, int word)
		{
			int argnewByte = word & 0xFF;
			pokeb(addr, argnewByte);
			int argaddr = addr + 1;
			pokeb(argaddr, modMain.glMemAddrDiv256[word & 0xFF00]);
		}

		public static void poppc()
		{
			int localpeekb() { int argaddr = modMain.regSP + 1; var ret = peekb(argaddr); return ret; }

			modMain.regPC = peekb(modMain.regSP) | localpeekb() * 256;
			modMain.regSP = modMain.regSP + 2 & 0xFFFF;
		}

		private static int popw()
		{
			int popwRet = default;
			int localpeekb() { int argaddr = modMain.regSP + 1; var ret = peekb(argaddr); return ret; }

			popwRet = peekb(modMain.regSP) | localpeekb() * 256;
			modMain.regSP = modMain.regSP + 2 & 0xFFFF;
			return popwRet;
		}

		public static void pushpc()
		{
			modMain.regSP = modMain.regSP - 2 & 0xFFFF;
			pokew(modMain.regSP, modMain.regPC);
		}

		private static void pushw(int word)
		{
			modMain.regSP = modMain.regSP - 2 & 0xFFFF;
			pokew(modMain.regSP, word);
		}

		private static void REFRESH(int t)
		{
			modMain.intRTemp = modMain.intRTemp + t;
		}

		private static int rl(int ans)
		{
			int rlRet = default;
			int c;
			c = Conversions.ToInteger((ans & 0x80) != 0);
			rlRet = (ans * 2 | -modMain.fC) & 0xFF;
			modMain.fS = Conversions.ToInteger((rlRet & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((rlRet & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((rlRet & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(rlRet == 0);
			modMain.fPV = modMain.Parity[rlRet];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fC = c;
			return rlRet;
		}

		private static void rl_a()
		{
			int c;
			c = Conversions.ToInteger((modMain.regA & 0x80) != 0);
			modMain.regA = (modMain.regA * 2 | -modMain.fC) & 0xFF;
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(false);
			modMain.fC = c;
		}

		private static int rlc(int ans)
		{
			int rlcRet = default;
			modMain.fC = Conversions.ToInteger((ans & 0x80) != 0);
			rlcRet = (ans * 2 | -modMain.fC) & 0xFF;
			modMain.fS = Conversions.ToInteger((rlcRet & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((rlcRet & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((rlcRet & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(rlcRet == 0);
			modMain.fPV = modMain.Parity[rlcRet];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			return rlcRet;
		}

		private static void rlc_a()
		{
			modMain.fC = Conversions.ToInteger((modMain.regA & 0x80) != 0);
			modMain.regA = (modMain.regA * 2 | -modMain.fC) & 0xFF;
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(false);
		}

		private static int rr(int ans)
		{
			int rrRet = default;
			int c;
			c = Conversions.ToInteger((ans & 0x1) != 0);
			rrRet = ans / 2 | 0x80 * -modMain.fC;
			modMain.fS = Conversions.ToInteger((rrRet & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((rrRet & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((rrRet & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(rrRet == 0);
			modMain.fPV = modMain.Parity[rrRet];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fC = c;
			return rrRet;
		}

		private static void rr_a()
		{
			int c;
			c = Conversions.ToInteger((modMain.regA & 0x1) != 0);
			modMain.regA = modMain.regA / 2 | 0x80 * -modMain.fC;
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(false);
			modMain.fC = c;
		}

		private static int rrc(int ans)
		{
			int rrcRet = default;
			modMain.fC = Conversions.ToInteger((ans & 0x1) != 0);
			rrcRet = ans / 2 | 0x80 * -modMain.fC;
			modMain.fS = Conversions.ToInteger((rrcRet & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((rrcRet & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((rrcRet & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(rrcRet == 0);
			modMain.fPV = modMain.Parity[rrcRet];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			return rrcRet;
		}

		private static void rrd_a()
		{
			int t, q;
			t = peekb(modMain.regHL);
			q = t;
			t = t / 16 | modMain.regA * 16;
			modMain.regA = modMain.regA & 0xF0 | q & 0xF;
			pokeb(modMain.regHL, t);
			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fPV = modMain.intIFF2;
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
		}

		private static void rrc_a()
		{
			modMain.fC = Conversions.ToInteger((modMain.regA & 0x1) != 0);
			modMain.regA = modMain.regA / 2 | 0x80 * -modMain.fC;
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(false);
		}

		private static void sbc_a(int b)
		{
			int ans, c;
			c = -modMain.fC;
			ans = modMain.regA - b - c & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((modMain.regA - b - c & 0x100) != 0);
			modMain.fPV = Conversions.ToInteger(((modMain.regA ^ b) & (modMain.regA ^ ans) & 0x80) != 0);
			modMain.fH = Conversions.ToInteger(((short)((short)(modMain.regA & 0xF) - (short)(b & 0xF)) - c & modMain.F_H) != 0);
			modMain.fN = Conversions.ToInteger(true);
			modMain.regA = ans;
		}

		private static int sbc16(int a, int b)
		{
			int sbc16Ret = default;
			int c, ans;
			c = -modMain.fC;
			ans = a - b - c & 0xFFFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S * 256) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3 * 256) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5 * 256) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((a - b - c & 0x10000) != 0);
			modMain.fPV = Conversions.ToInteger(((a ^ b) & (a ^ ans) & 0x8000) != 0);
			modMain.fH = Conversions.ToInteger(((short)((short)(a & 0xFFF) - (short)(b & 0xFFF)) - c & 0x1000) != 0);
			modMain.fN = Conversions.ToInteger(true);
			sbc16Ret = ans;
			return sbc16Ret;
		}

		private static void scf()
		{
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fH = Conversions.ToInteger(false);
			modMain.fC = Conversions.ToInteger(true);
		}

		public static void setAF(int v)
		{
			modMain.regA = modMain.glMemAddrDiv256[v & 0xFF00];
			int argb = v & 0xFF;
			setF(argb);
		}

		public static void setBC(int nn)
		{
			modMain.regB = modMain.glMemAddrDiv256[nn & 0xFF00];
			modMain.regC = nn & 0xFF;
		}

		public static void execute()
		{
			int d, lTemp;
			int lScanLine, xxx, lTStates = default, lPrevScanLine = default;

			// // Yes, I appreciate that GOTO's and labels are a hideous blashphemy!
			// // However, this code is the fastest possible way of fetching and handling
			// // Z80 instructions I could come up with. There are only 8 compares per
			// // instruction fetch rather than between 1 and 255 as required in
			// // the previous version of vbSpec with it's huge Case statement.
			// //
			// // I know it's slightly harder to follow the new code, but I think the
			// // speed increase justifies it. <CC>

			modMain.glTStates = -modMain.glTstatesPerInterrupt;
			while (true)
			{
				if (modMain.glTStates >= 0)
				{
					// // Trigger an interrupt
					modMain.glTStates = modMain.glTStates - modMain.glTstatesPerInterrupt - interrupt();
				}

				if (modMain.glTStates >= modMain.glTStatesAtTop & modMain.glTStates <= modMain.glTStatesAtBottom)
				{
					// // We're in the main (non border) area of the display
					lScanLine = modMain.glTSToScanLine[-modMain.glTStates];
					// // When the scan line moves into a new line, paint the new line
					if (lPrevScanLine != lScanLine)
					{
						lPrevScanLine = lScanLine;
						modSpectrum.ScanlinePaint(lScanLine);
					}
				}

				// // REFRESH 1
				modMain.intRTemp = modMain.intRTemp + 1;
				modMain.glContendedMemoryDelay = 0;

				// // Inlined version of "xxx = nxtpcb()" suggested by Gonchuki and Woody
				if ((modMain.regPC & 49152) == 16384)
				{
					modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
				}

				xxx = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[modMain.regPC]], modMain.regPC & 16383];
				modMain.regPC = modMain.regPC + 1;
				if (Conversions.ToBoolean(xxx & 128))
					goto ex128_255;
				else
					goto ex0_127;
				ex0_127:
				;
				if (Conversions.ToBoolean(xxx & 64))
					goto ex64_127;
				else
					goto ex0_63;
				ex0_63:
				;
				if (Conversions.ToBoolean(xxx & 32))
					goto ex32_63;
				else
					goto ex0_31;
				ex0_31:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex16_31;
				else
					goto ex0_15;
				ex0_15:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex8_15;
				else
					goto ex0_7;
				ex0_7:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex4_7;
				else
					goto ex0_3;
				ex0_3:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex2_3;
				else
					goto ex0_1;
				ex0_1:
				;
				if (xxx == 0)
				{
					// 000 NOP
					lTStates = 4;
				}
				else
				{
					// 001 LD BC,nn
					int argnn = nxtpcw();
					setBC(argnn);
					lTStates = 10;
				}

				goto execute_end;
			ex2_3:
				;
				if (xxx == 2)
				{
					// 002 LD (BC),A
					int argaddr = modMain.regB * 256 | modMain.regC;
					pokeb(argaddr, modMain.regA);
					lTStates = 7;
				}
				else
				{
					// 003 INC BC
					int argnn1 = (ushort)(modMain.regB * 256 | modMain.regC) + 1 & 0xFFFF;
					setBC(argnn1);
					lTStates = 6;
				}

				goto execute_end;
			ex4_7:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex6_7;
				else
					goto ex4_5;
				ex4_5:
				;
				if (xxx == 4)
				{
					// 004 INC B
					modMain.regB = inc8(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 005 DEC B
					modMain.regB = dec8(modMain.regB);
					lTStates = 4;
				}

				goto execute_end;
			ex6_7:
				;
				if (xxx == 6)
				{
					// 006 LD B,n
					modMain.regB = nxtpcb();
					lTStates = 7;
				}
				else
				{
					// 007 RLCA
					rlc_a();
					lTStates = 4;
				}

				goto execute_end;
			ex8_15:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex12_15;
				else
					goto ex8_11;
				ex8_11:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex10_11;
				else
					goto ex8_9;
				ex8_9:
				;
				if (xxx == 8)
				{
					// 008 EX AF,AF'
					ex_af_af();
					lTStates = 4;
					if (modMain.regPC == 1233)
						modSpectrum.Hook_SABYTES();
				}
				else
				{
					// 009 ADD HL,BC
					int argb = modMain.regB * 256 | modMain.regC;
					modMain.regHL = add16(modMain.regHL, argb);
					lTStates = 11;
				}

				goto execute_end;
			ex10_11:
				;
				if (xxx == 10)
				{
					// 010 LD A,(BC)
					int argaddr1 = modMain.regB * 256 | modMain.regC;
					modMain.regA = peekb(argaddr1);
					lTStates = 7;
				}
				else
				{
					// 011 DEC BC
					int localdec16() { int arga = modMain.regB * 256 | modMain.regC; var ret = dec16(arga); return ret; }

					int argnn2 = localdec16();
					setBC(argnn2);
					lTStates = 6;
				}

				goto execute_end;
			ex12_15:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex14_15;
				else
					goto ex12_13;
				ex12_13:
				;
				if (xxx == 12)
				{
					// 012 INC C
					modMain.regC = inc8(modMain.regC);
					lTStates = 4;
				}
				else
				{
					// 013 DEC C
					modMain.regC = dec8(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex14_15:
				;
				if (xxx == 14)
				{
					// 014 LD C,n
					modMain.regC = nxtpcb();
					lTStates = 7;
				}
				else
				{
					// 015 RRCA
					rrc_a();
					lTStates = 4;
				}

				goto execute_end;
			ex16_31:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex24_31;
				else
					goto ex16_23;
				ex16_23:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex20_23;
				else
					goto ex16_19;
				ex16_19:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex18_19;
				else
					goto ex16_17;
				ex16_17:
				;
				if (xxx == 16)
				{
					// 016 DJNZ dis
					modMain.regB = modMain.regB - 1 & 0xFF;
					if (modMain.regB != 0)
					{
						d = nxtpcb();
						if (Conversions.ToBoolean(d & 128))
							d = -(256 - d);
						if ((modMain.regPC & 49152) == 16384)
						{
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
							modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
						}

						modMain.regPC = modMain.regPC + d & 0xFFFF;
						lTStates = 13;
					}
					else
					{
						modMain.regPC = modMain.regPC + 1 & 0xFFFF;
						lTStates = 8;
					}
				}
				else
				{
					// 017 LD DE,nn
					modMain.regDE = nxtpcw();
					lTStates = 10;
				}

				goto execute_end;
			ex18_19:
				;
				if (xxx == 18)
				{
					// 018 LD (DE),A
					pokeb(modMain.regDE, modMain.regA);
					lTStates = 7;
				}
				else
				{
					// 019 INC DE
					modMain.regDE = modMain.regDE + 1 & 0xFFFF;
					lTStates = 6;
				}

				goto execute_end;
			ex20_23:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex22_23;
				else
					goto ex20_21;
				ex20_21:
				;
				if (xxx == 20)
				{
					// 020 INC D
					int argl = inc8(modMain.glMemAddrDiv256[modMain.regDE]);
					setD(argl);
					lTStates = 4;
				}
				else
				{
					// 021 DEC D
					int argl1 = dec8(modMain.glMemAddrDiv256[modMain.regDE]);
					setD(argl1);
					lTStates = 4;
				}

				goto execute_end;
			ex22_23:
				;
				if (xxx == 22)
				{
					// 022 LD D,n
					int argl2 = nxtpcb();
					setD(argl2);
					lTStates = 7;
				}
				else
				{
					// 023 ' RLA
					rl_a();
					lTStates = 4;
				}

				goto execute_end;
			ex24_31:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex28_31;
				else
					goto ex24_27;
				ex24_27:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex26_27;
				else
					goto ex24_25;
				ex24_25:
				;
				if (xxx == 24)
				{
					// 024 JR dis
					d = nxtpcb();
					if (Conversions.ToBoolean(d & 128))
						d = -(256 - d);
					modMain.regPC = modMain.regPC + d & 0xFFFF;
					lTStates = 12;
				}
				else
				{
					// 025 ADD HL,DE
					modMain.regHL = add16(modMain.regHL, modMain.regDE);
					lTStates = 11;
				}

				goto execute_end;
			ex26_27:
				;
				if (xxx == 26)
				{
					// 026 LD A,(DE)
					modMain.regA = peekb(modMain.regDE);
					lTStates = 7;
				}
				else
				{
					// 027 DEC DE
					modMain.regDE = dec16(modMain.regDE);
					lTStates = 6;
				}

				goto execute_end;
			ex28_31:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex30_31;
				else
					goto ex28_29;
				ex28_29:
				;
				if (xxx == 28)
				{
					// 028 INC E
					int argl3 = inc8(getE());
					setE(argl3);
					lTStates = 4;
				}
				else
				{
					// 029 DEC E
					int argl4 = dec8(getE());
					setE(argl4);
					lTStates = 4;
				}

				goto execute_end;
			ex30_31:
				;
				if (xxx == 30)
				{
					// 030 LD E,n
					int argl5 = nxtpcb();
					setE(argl5);
					lTStates = 7;
				}
				else
				{
					// 031 RRA
					rr_a();
					lTStates = 4;
				}

				goto execute_end;
			ex32_63:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex48_63;
				else
					goto ex32_47;
				ex32_47:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex40_47;
				else
					goto ex32_39;
				ex32_39:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex36_39;
				else
					goto ex32_35;
				ex32_35:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex34_35;
				else
					goto ex32_33;
				ex32_33:
				;
				if (xxx == 32)
				{
					// 032 JR NZ dis
					if (modMain.fZ == Conversions.ToInteger(false))
					{
						d = nxtpcb();
						if (Conversions.ToBoolean(d & 128))
							d = -(256 - d);
						modMain.regPC = modMain.regPC + d & 0xFFFF;
						lTStates = 12;
					}
					else
					{
						modMain.regPC = modMain.regPC + 1 & 0xFFFF;
						lTStates = 7;
					}
				}
				else
				{
					// 033 LD HL,nn
					modMain.regHL = nxtpcw();
					lTStates = 10;
				}

				goto execute_end;
			ex34_35:
				;
				if (xxx == 34)
				{
					// 034 LD (nn),HL
					int argaddr2 = nxtpcw();
					pokew(argaddr2, modMain.regHL);
					lTStates = 16;
				}
				else
				{
					// 035 INC HL
					modMain.regHL = modMain.regHL + 1 & 0xFFFF;
					lTStates = 6;
				}

				goto execute_end;
			ex36_39:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex38_39;
				else
					goto ex36_37;
				ex36_37:
				;
				if (xxx == 36)
				{
					// 036 INC H
					int argl6 = inc8(modMain.glMemAddrDiv256[modMain.regHL]);
					setH(argl6);
					lTStates = 4;
				}
				else
				{
					// 037 DEC H
					int argl7 = dec8(modMain.glMemAddrDiv256[modMain.regHL]);
					setH(argl7);
					lTStates = 4;
				}

				goto execute_end;
			ex38_39:
				;
				if (xxx == 38)
				{
					// 038 LD H,n
					int argl8 = nxtpcb();
					setH(argl8);
					lTStates = 7;
				}
				else
				{
					// 039 DAA
					daa_a();
					lTStates = 4;
				}

				goto execute_end;
			ex40_47:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex44_47;
				else
					goto ex40_43;
				ex40_43:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex42_43;
				else
					goto ex40_41;
				ex40_41:
				;
				if (xxx == 40)
				{
					// 040 JR Z dis
					if (modMain.fZ == Conversions.ToInteger(true))
					{
						d = nxtpcb();
						if (Conversions.ToBoolean(d & 128))
							d = -(256 - d);
						modMain.regPC = modMain.regPC + d & 0xFFFF;
						lTStates = 12;
					}
					else
					{
						modMain.regPC = modMain.regPC + 1 & 0xFFFF;
						lTStates = 7;
					}
				}
				else
				{
					// 041 ADD HL,HL
					modMain.regHL = add16(modMain.regHL, modMain.regHL);
					lTStates = 11;
				}

				goto execute_end;
			ex42_43:
				;
				if (xxx == 42)
				{
					// 042 LD HL,(nn)
					int argaddr3 = nxtpcw();
					modMain.regHL = peekw(argaddr3);
					lTStates = 16;
				}
				else
				{
					// 043 DEC HL
					modMain.regHL = dec16(modMain.regHL);
					lTStates = 6;
				}

				goto execute_end;
			ex44_47:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex46_47;
				else
					goto ex44_45;
				ex44_45:
				;
				if (xxx == 44)
				{
					// 044 INC L
					int argl9 = inc8(modMain.regHL & 0xFF);
					setL(argl9);
					lTStates = 4;
				}
				else
				{
					// 045 DEC L
					int argl10 = dec8(modMain.regHL & 0xFF);
					setL(argl10);
					lTStates = 4;
				}

				goto execute_end;
			ex46_47:
				;
				if (xxx == 46)
				{
					// 046 LD L,n
					int argl11 = nxtpcb();
					setL(argl11);
					lTStates = 7;
				}
				else
				{
					// 047 CPL
					cpl_a();
					lTStates = 4;
				}

				goto execute_end;
			ex48_63:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex56_63;
				else
					goto ex48_55;
				ex48_55:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex52_55;
				else
					goto ex48_51;
				ex48_51:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex50_51;
				else
					goto ex48_49;
				ex48_49:
				;
				if (xxx == 48)
				{
					// 048 JR NC dis
					if (modMain.fC == Conversions.ToInteger(false))
					{
						d = nxtpcb();
						if (Conversions.ToBoolean(d & 128))
							d = -(256 - d);
						modMain.regPC = modMain.regPC + d & 0xFFFF;
						lTStates = 12;
					}
					else
					{
						modMain.regPC = modMain.regPC + 1 & 0xFFFF;
						lTStates = 7;
					}
				}
				else
				{
					// 049 LD SP,nn
					modMain.regSP = nxtpcw();
					lTStates = 10;
				}

				goto execute_end;
			ex50_51:
				;
				if (xxx == 50)
				{
					// 050 LD (nn),A
					pokeb(nxtpcw(), modMain.regA);
					lTStates = 13;
				}
				else
				{
					// 051 INC SP
					modMain.regSP = modMain.regSP + 1 & 0xFFFF;
					lTStates = 6;
				}

				goto execute_end;
			ex52_55:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex54_55;
				else
					goto ex52_53;
				ex52_53:
				;
				if (xxx == 52)
				{
					// 052 INC (HL)
					int argnewByte = inc8(peekb(modMain.regHL));
					pokeb(modMain.regHL, argnewByte);
					lTStates = 11;
				}
				else
				{
					// 053 DEC (HL)
					int argnewByte1 = dec8(peekb(modMain.regHL));
					pokeb(modMain.regHL, argnewByte1);
					lTStates = 11;
				}

				goto execute_end;
			ex54_55:
				;
				if (xxx == 54)
				{
					// 054 LD (HL),n
					int argnewByte2 = nxtpcb();
					pokeb(modMain.regHL, argnewByte2);
					lTStates = 10;
				}
				else
				{
					// 055 SCF
					scf();
					lTStates = 4;
				}

				goto execute_end;
			ex56_63:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex60_63;
				else
					goto ex56_59;
				ex56_59:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex58_59;
				else
					goto ex56_57;
				ex56_57:
				;
				if (xxx == 56)
				{
					// 056 JR C dis
					if (modMain.fC == Conversions.ToInteger(true))
					{
						d = nxtpcb();
						if (Conversions.ToBoolean(d & 128))
							d = -(256 - d);
						modMain.regPC = modMain.regPC + d & 0xFFFF;
						lTStates = 12;
					}
					else
					{
						modMain.regPC = modMain.regPC + 1 & 0xFFFF;
						lTStates = 7;
					}
				}
				else
				{
					// 057 ADD HL,SP
					modMain.regHL = add16(modMain.regHL, modMain.regSP);
					lTStates = 11;
				}

				goto execute_end;
			ex58_59:
				;
				if (xxx == 58)
				{
					// 058 LD A,(nn)
					int argaddr4 = nxtpcw();
					modMain.regA = peekb(argaddr4);
					lTStates = 13;
				}
				else
				{
					// 059 DEC SP
					modMain.regSP = dec16(modMain.regSP);
					lTStates = 6;
				}

				goto execute_end;
			ex60_63:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex62_63;
				else
					goto ex60_61;
				ex60_61:
				;
				if (xxx == 60)
				{
					// 060 INC A
					modMain.regA = inc8(modMain.regA);
					lTStates = 4;
				}
				else
				{
					// 061 DEC A
					modMain.regA = dec8(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex62_63:
				;
				if (xxx == 62)
				{
					// 062 LD A,n
					modMain.regA = nxtpcb();
					lTStates = 7;
				}
				else
				{
					// 063 CCF
					ccf();
					lTStates = 4;
				}

				goto execute_end;
			ex64_127:
				;
				if (Conversions.ToBoolean(xxx & 32))
					goto ex96_127;
				else
					goto ex64_95;
				ex64_95:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex80_95;
				else
					goto ex64_79;
				ex64_79:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex72_79;
				else
					goto ex64_71;
				ex64_71:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex68_71;
				else
					goto ex64_67;
				ex64_67:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex66_67;
				else
					goto ex64_65;
				ex64_65:
				;
				if (xxx == 64)
				{
					// LD B,B
					lTStates = 4;
				}
				else
				{
					// 65 ' LD B,C
					modMain.regB = modMain.regC;
					lTStates = 4;
				}

				goto execute_end;
			ex66_67:
				;
				if (xxx == 66)
				{
					// LD B,D
					modMain.regB = modMain.glMemAddrDiv256[modMain.regDE];
					lTStates = 4;
				}
				else
				{
					// 67 ' LD B,E
					modMain.regB = getE();
					lTStates = 4;
				}

				goto execute_end;
			ex68_71:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex70_71;
				else
					goto ex68_69;
				ex68_69:
				;
				if (xxx == 68)
				{
					// LD B,H
					modMain.regB = modMain.glMemAddrDiv256[modMain.regHL];
					lTStates = 4;
				}
				else
				{
					// 69 ' LD B,L
					modMain.regB = modMain.regHL & 0xFF;
					lTStates = 4;
				}

				goto execute_end;
			ex70_71:
				;
				if (xxx == 70)
				{
					// LD B,(HL)
					modMain.regB = peekb(modMain.regHL);
					lTStates = 7;
				}
				else
				{
					// 71 ' LD B,A
					modMain.regB = modMain.regA;
					lTStates = 4;
				}

				goto execute_end;
			ex72_79:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex76_79;
				else
					goto ex72_75;
				ex72_75:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex74_75;
				else
					goto ex72_73;
				ex72_73:
				;
				if (xxx == 72)
				{
					// 72 ' LD C,B
					modMain.regC = modMain.regB;
					lTStates = 4;
				}
				else
				{
					// 73 ' LD C,C
					lTStates = 4;
				}

				goto execute_end;
			ex74_75:
				;
				if (xxx == 74)
				{
					// 74 ' LD C,D
					modMain.regC = modMain.glMemAddrDiv256[modMain.regDE];
					lTStates = 4;
				}
				else
				{
					// 75 ' LD C,E
					modMain.regC = getE();
					lTStates = 4;
				}

				goto execute_end;
			ex76_79:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex78_79;
				else
					goto ex76_77;
				ex76_77:
				;
				if (xxx == 76)
				{
					// 76 ' LD C,H
					modMain.regC = modMain.glMemAddrDiv256[modMain.regHL];
					lTStates = 4;
				}
				else
				{
					// 77 ' LD C,L
					modMain.regC = modMain.regHL & 0xFF;
					lTStates = 4;
				}

				goto execute_end;
			ex78_79:
				;
				if (xxx == 78)
				{
					// 78 ' LD C,(HL)
					modMain.regC = peekb(modMain.regHL);
					lTStates = 7;
				}
				else
				{
					// 79 ' LD C,A
					modMain.regC = modMain.regA;
					lTStates = 4;
				}

				goto execute_end;
			ex80_95:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex88_95;
				else
					goto ex80_87;
				ex80_87:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex84_87;
				else
					goto ex80_83;
				ex80_83:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex82_83;
				else
					goto ex80_81;
				ex80_81:
				;
				if (xxx == 80)
				{
					// 80 ' LD D,B
					setD(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 81 ' LD D,C
					setD(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex82_83:
				;
				if (xxx == 82)
				{
					// 82 ' LD D,D
					lTStates = 4;
				}
				else
				{
					// 83 ' LD D,E
					setD(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex84_87:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex86_87;
				else
					goto ex84_85;
				ex84_85:
				;
				if (xxx == 84)
				{
					// 84 ' LD D,H
					setD(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 85 ' LD D,L
					int argl12 = modMain.regHL & 0xFF;
					setD(argl12);
					lTStates = 4;
				}

				goto execute_end;
			ex86_87:
				;
				if (xxx == 86)
				{
					// 86 ' LD D,(HL)
					int argl13 = peekb(modMain.regHL);
					setD(argl13);
					lTStates = 7;
				}
				else
				{
					// 87 ' LD D,A
					setD(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex88_95:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex92_95;
				else
					goto ex88_91;
				ex88_91:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex90_91;
				else
					goto ex88_89;
				ex88_89:
				;
				if (xxx == 88)
				{
					// 88 ' LD E,B
					setE(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 89 ' LD E,C
					setE(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex90_91:
				;
				if (xxx == 90)
				{
					// 90 ' LD E,D
					setE(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 91 ' LD E,E
					lTStates = 4;
				}

				goto execute_end;
			ex92_95:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex94_95;
				else
					goto ex92_93;
				ex92_93:
				;
				if (xxx == 92)
				{
					// 92 ' LD E,H
					setE(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 93 ' LD E,L
					int argl14 = modMain.regHL & 0xFF;
					setE(argl14);
					lTStates = 4;
				}

				goto execute_end;
			ex94_95:
				;
				if (xxx == 94)
				{
					// 94 ' LD E,(HL)
					int argl15 = peekb(modMain.regHL);
					setE(argl15);
					lTStates = 7;
				}
				else
				{
					// 95 ' LD E,A
					setE(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex96_127:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex112_127;
				else
					goto ex96_111;
				ex96_111:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex104_111;
				else
					goto ex96_103;
				ex96_103:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex100_103;
				else
					goto ex96_99;
				ex96_99:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex98_99;
				else
					goto ex96_97;
				ex96_97:
				;
				if (xxx == 96)
				{
					// 96 ' LD H,B
					setH(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 97 ' LD H,C
					setH(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex98_99:
				;
				if (xxx == 98)
				{
					// 98 ' LD H,D
					setH(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 99 ' LD H,E
					setH(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex100_103:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex102_103;
				else
					goto ex100_101;
				ex100_101:
				;
				if (xxx == 100)
				{
					// 100 ' LD H,H
					lTStates = 4;
				}
				else
				{
					// 101 ' LD H,L
					int argl16 = modMain.regHL & 0xFF;
					setH(argl16);
					lTStates = 4;
				}

				goto execute_end;
			ex102_103:
				;
				if (xxx == 102)
				{
					// 102 ' LD H,(HL)
					int argl17 = peekb(modMain.regHL);
					setH(argl17);
					lTStates = 7;
				}
				else
				{
					// 103 ' LD H,A
					setH(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex104_111:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex108_111;
				else
					goto ex104_107;
				ex104_107:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex106_107;
				else
					goto ex104_105;
				ex104_105:
				;
				if (xxx == 104)
				{
					// 104 ' LD L,B
					setL(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 105 ' LD L,C
					setL(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex106_107:
				;
				if (xxx == 106)
				{
					// 106 ' LD L,D
					setL(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 107 ' LD L,E
					setL(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex108_111:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex110_111;
				else
					goto ex108_109;
				ex108_109:
				;
				if (xxx == 108)
				{
					// 108 ' LD L,H
					setL(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 109 ' LD L,L
					lTStates = 4;
				}

				goto execute_end;
			ex110_111:
				;
				if (xxx == 110)
				{
					// 110 ' LD L,(HL)
					int argl18 = peekb(modMain.regHL);
					setL(argl18);
					lTStates = 7;
				}
				else
				{
					// 111 ' LD L,A
					setL(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex112_127:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex120_127;
				else
					goto ex112_119;
				ex112_119:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex116_119;
				else
					goto ex112_115;
				ex112_115:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex114_115;
				else
					goto ex112_113;
				ex112_113:
				;
				if (xxx == 112)
				{
					// 112 ' LD (HL),B
					pokeb(modMain.regHL, modMain.regB);
					lTStates = 7;
				}
				else
				{
					// 113 ' LD (HL),C
					pokeb(modMain.regHL, modMain.regC);
					lTStates = 7;
				}

				goto execute_end;
			ex114_115:
				;
				if (xxx == 114)
				{
					// 114 ' LD (HL),D
					pokeb(modMain.regHL, modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 7;
				}
				else
				{
					// 115 ' LD (HL),E
					pokeb(modMain.regHL, getE());
					lTStates = 7;
				}

				goto execute_end;
			ex116_119:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex118_119;
				else
					goto ex116_117;
				ex116_117:
				;
				if (xxx == 116)
				{
					// 116 ' LD (HL),H
					pokeb(modMain.regHL, modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 7;
				}
				else
				{
					// 117 ' LD (HL),L
					int argnewByte3 = modMain.regHL & 0xFF;
					pokeb(modMain.regHL, argnewByte3);
					lTStates = 7;
				}

				goto execute_end;
			ex118_119:
				;
				if (xxx == 118)
				{
					// 118 ' HALT
					lTStates = 4;
					if (modMain.glTStates + lTStates + modMain.glContendedMemoryDelay < 0)
						modMain.regPC = modMain.regPC - 1;
				}
				// lTemp = (((-glTStates - 1) \ 4) + 1)
				// lTStates = (lTemp * 4)
				// intRTemp = intRTemp + (lTemp - 1)
				else
				{
					// 119 ' LD (HL),A
					pokeb(modMain.regHL, modMain.regA);
					lTStates = 7;
				}

				goto execute_end;
			ex120_127:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex124_127;
				else
					goto ex120_123;
				ex120_123:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex122_123;
				else
					goto ex120_121;
				ex120_121:
				;
				if (xxx == 120)
				{
					// 120 ' LD A,B
					modMain.regA = modMain.regB;
					lTStates = 4;
				}
				else
				{
					// 121 ' LD A,C
					modMain.regA = modMain.regC;
					lTStates = 4;
				}

				goto execute_end;
			ex122_123:
				;
				if (xxx == 122)
				{
					// 122 ' LD A,D
					modMain.regA = modMain.glMemAddrDiv256[modMain.regDE];
					lTStates = 4;
				}
				else
				{
					// 123 ' LD A,E
					modMain.regA = getE();
					lTStates = 4;
				}

				goto execute_end;
			ex124_127:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex126_127;
				else
					goto ex124_125;
				ex124_125:
				;
				if (xxx == 124)
				{
					// 124 ' LD A,H
					modMain.regA = modMain.glMemAddrDiv256[modMain.regHL];
					lTStates = 4;
				}
				else
				{
					// 125 ' LD A,L
					modMain.regA = modMain.regHL & 0xFF;
					lTStates = 4;
				}

				goto execute_end;
			ex126_127:
				;
				if (xxx == 126)
				{
					// 126 ' LD A,(HL)
					modMain.regA = peekb(modMain.regHL);
					lTStates = 7;
				}
				else
				{
					// 127 ' LD A,A
					lTStates = 4;
				}

				goto execute_end;
			ex128_255:
				;
				if (Conversions.ToBoolean(xxx & 64))
					goto ex192_255;
				else
					goto ex128_191;
				ex128_191:
				;
				if (Conversions.ToBoolean(xxx & 32))
					goto ex160_191;
				else
					goto ex128_159;
				ex128_159:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex144_159;
				else
					goto ex128_143;
				ex128_143:
				;
				switch (xxx)
				{
					// // ADD A,*
					case 128: // ADD A,B
						{
							add_a(modMain.regB);
							lTStates = 4;
							break;
						}

					case 129: // ADD A,C
						{
							add_a(modMain.regC);
							lTStates = 4;
							break;
						}

					case 130: // ADD A,D
						{
							add_a(modMain.glMemAddrDiv256[modMain.regDE]);
							lTStates = 4;
							break;
						}

					case 131: // ADD A,E
						{
							add_a(getE());
							lTStates = 4;
							break;
						}

					case 132: // ADD A,H
						{
							add_a(modMain.glMemAddrDiv256[modMain.regHL]);
							lTStates = 4;
							break;
						}

					case 133: // ADD A,L
						{
							int argb1 = modMain.regHL & 0xFF;
							add_a(argb1);
							lTStates = 4;
							break;
						}

					case 134: // ADD A,(HL)
						{
							int argb2 = peekb(modMain.regHL);
							add_a(argb2);
							lTStates = 7;
							break;
						}

					case 135: // ADD A,A
						{
							add_a(modMain.regA);
							lTStates = 4;
							break;
						}

					case 136: // ADC A,B
						{
							adc_a(modMain.regB);
							lTStates = 4;
							break;
						}

					case 137: // ADC A,C
						{
							adc_a(modMain.regC);
							lTStates = 4;
							break;
						}

					case 138: // ADC A,D
						{
							adc_a(modMain.glMemAddrDiv256[modMain.regDE]);
							lTStates = 4;
							break;
						}

					case 139: // ADC A,E
						{
							adc_a(getE());
							lTStates = 4;
							break;
						}

					case 140: // ADC A,H
						{
							adc_a(modMain.glMemAddrDiv256[modMain.regHL]);
							lTStates = 4;
							break;
						}

					case 141: // ADC A,L
						{
							int argb3 = modMain.regHL & 0xFF;
							adc_a(argb3);
							lTStates = 4;
							break;
						}

					case 142: // ADC A,(HL)
						{
							int argb4 = peekb(modMain.regHL);
							adc_a(argb4);
							lTStates = 7;
							break;
						}

					case 143: // ADC A,A
						{
							adc_a(modMain.regA);
							lTStates = 4;
							break;
						}
				}

				goto execute_end;
			ex144_159:
				;
				switch (xxx)
				{
					case 144: // SUB B
						{
							sub_a(modMain.regB);
							lTStates = 4;
							break;
						}

					case 145: // SUB C
						{
							sub_a(modMain.regC);
							lTStates = 4;
							break;
						}

					case 146: // SUB D
						{
							sub_a(modMain.glMemAddrDiv256[modMain.regDE]);
							lTStates = 4;
							break;
						}

					case 147: // SUB E
						{
							sub_a(getE());
							lTStates = 4;
							break;
						}

					case 148: // SUB H
						{
							sub_a(modMain.glMemAddrDiv256[modMain.regHL]);
							lTStates = 4;
							break;
						}

					case 149: // SUB L
						{
							int argb5 = modMain.regHL & 0xFF;
							sub_a(argb5);
							lTStates = 4;
							break;
						}

					case 150: // SUB (HL)
						{
							int argb6 = peekb(modMain.regHL);
							sub_a(argb6);
							lTStates = 7;
							break;
						}

					case 151: // SUB A
						{
							sub_a(modMain.regA);
							lTStates = 4;
							break;
						}

					case 152: // SBC A,B
						{
							sbc_a(modMain.regB);
							lTStates = 4;
							break;
						}

					case 153: // SBC A,C
						{
							sbc_a(modMain.regC);
							lTStates = 4;
							break;
						}

					case 154: // SBC A,D
						{
							sbc_a(modMain.glMemAddrDiv256[modMain.regDE]);
							lTStates = 4;
							break;
						}

					case 155: // SBC A,E
						{
							sbc_a(getE());
							lTStates = 4;
							break;
						}

					case 156: // SBC A,H
						{
							sbc_a(modMain.glMemAddrDiv256[modMain.regHL]);
							lTStates = 4;
							break;
						}

					case 157: // SBC A,L
						{
							int argb7 = modMain.regHL & 0xFF;
							sbc_a(argb7);
							lTStates = 4;
							break;
						}

					case 158: // SBC A,(HL)
						{
							int argb8 = peekb(modMain.regHL);
							sbc_a(argb8);
							lTStates = 7;
							break;
						}

					case 159: // SBC A,A
						{
							sbc_a(modMain.regA);
							lTStates = 4;
							break;
						}
				}

				goto execute_end;
			ex160_191:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex176_191;
				else
					goto ex160_175;
				ex160_175:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex168_175;
				else
					goto ex160_167;
				ex160_167:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex164_167;
				else
					goto ex160_163;
				ex160_163:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex162_163;
				else
					goto ex160_161;
				ex164_167:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex166_167;
				else
					goto ex164_165;
				ex160_161:
				;
				if (xxx == 160)
				{
					// 160 ' AND B
					and_a(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 161 ' AND C
					and_a(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex162_163:
				;
				if (xxx == 162)
				{
					// 162 ' AND D
					and_a(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 163 ' AND E
					and_a(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex164_165:
				;
				if (xxx == 164)
				{
					// 164 ' AND H
					and_a(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 165 ' AND L
					int argb9 = modMain.regHL & 0xFF;
					and_a(argb9);
					lTStates = 4;
				}

				goto execute_end;
			ex166_167:
				;
				if (xxx == 166)
				{
					// 166 ' AND (HL)
					int argb10 = peekb(modMain.regHL);
					and_a(argb10);
					lTStates = 7;
				}
				else
				{
					// 167 ' AND A
					and_a(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex168_175:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex172_175;
				else
					goto ex168_171;
				ex168_171:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex170_171;
				else
					goto ex168_169;
				ex168_169:
				;
				if (xxx == 168)
				{
					// 168 ' XOR B
					xor_a(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 169 ' XOR C
					xor_a(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex170_171:
				;
				if (xxx == 170)
				{
					// 170 ' XOR D
					xor_a(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 171 ' XOR E
					xor_a(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex172_175:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex174_175;
				else
					goto ex172_173;
				ex172_173:
				;
				if (xxx == 172)
				{
					// 172 ' XOR H
					xor_a(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 173 ' XOR L
					int argb11 = modMain.regHL & 0xFF;
					xor_a(argb11);
					lTStates = 4;
				}

				goto execute_end;
			ex174_175:
				;
				if (xxx == 174)
				{
					// 174 ' XOR (HL)
					int argb12 = peekb(modMain.regHL);
					xor_a(argb12);
					lTStates = 7;
				}
				else
				{
					// 175 ' XOR A
					modMain.regA = 0;
					modMain.fS = Conversions.ToInteger(false);
					modMain.f3 = Conversions.ToInteger(false);
					modMain.f5 = Conversions.ToInteger(false);
					modMain.fH = Conversions.ToInteger(false);
					modMain.fPV = Conversions.ToInteger(true);
					modMain.fZ = Conversions.ToInteger(true);
					modMain.fN = Conversions.ToInteger(false);
					modMain.fC = Conversions.ToInteger(false);
					lTStates = 4;
				}

				goto execute_end;
			ex176_191:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex184_191;
				else
					goto ex176_183;
				ex176_183:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex180_183;
				else
					goto ex176_179;
				ex176_179:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex178_179;
				else
					goto ex176_177;
				ex176_177:
				;
				if (xxx == 176)
				{
					// 176 ' OR B
					or_a(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 177 ' OR C
					or_a(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex178_179:
				;
				if (xxx == 178)
				{
					// 178 ' OR D'
					or_a(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 179 ' OR E
					or_a(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex180_183:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex182_183;
				else
					goto ex180_181;
				ex180_181:
				;
				if (xxx == 180)
				{
					// 180 ' OR H
					or_a(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 181 ' OR L
					int argb13 = modMain.regHL & 0xFF;
					or_a(argb13);
					lTStates = 4;
				}

				goto execute_end;
			ex182_183:
				;
				if (xxx == 182)
				{
					// 182 ' OR (HL)
					int argb14 = peekb(modMain.regHL);
					or_a(argb14);
					lTStates = 7;
				}
				else
				{
					// 183 ' OR A
					or_a(modMain.regA);
					lTStates = 4;
				}

				goto execute_end;
			ex184_191:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex188_191;
				else
					goto ex184_187;
				ex184_187:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex186_187;
				else
					goto ex184_185;
				ex184_185:
				;
				if (xxx == 184)
				{
					// 184 ' CP B
					cp_a(modMain.regB);
					lTStates = 4;
				}
				else
				{
					// 185 ' CP C
					cp_a(modMain.regC);
					lTStates = 4;
				}

				goto execute_end;
			ex186_187:
				;
				if (xxx == 186)
				{
					// 186 ' CP D
					cp_a(modMain.glMemAddrDiv256[modMain.regDE]);
					lTStates = 4;
				}
				else
				{
					// 187 ' CP E
					cp_a(getE());
					lTStates = 4;
				}

				goto execute_end;
			ex188_191:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex190_191;
				else
					goto ex188_189;
				ex188_189:
				;
				if (xxx == 188)
				{
					// 188 ' CP H
					cp_a(modMain.glMemAddrDiv256[modMain.regHL]);
					lTStates = 4;
				}
				else
				{
					// 189 ' CP L
					int argb15 = modMain.regHL & 0xFF;
					cp_a(argb15);
					lTStates = 4;
				}

				goto execute_end;
			ex190_191:
				;
				if (xxx == 190)
				{
					// 190 ' CP (HL)
					int argb16 = peekb(modMain.regHL);
					cp_a(argb16);
					lTStates = 7;
				}
				else
				{
					// 191 ' CP A
					cp_a(modMain.regA);
					lTStates = 4;
					if (modMain.regPC == 1387)
					{
						if (modMain.ghTAPFile != 0)
						{
							modSpectrum.Hook_LDBYTES();
						}
						else if (Conversions.ToBoolean(modTZX.gbTZXInserted & ~modTZX.gbTZXPlaying))
						{
							modTZX.StartTape();
						}
					}
				}

				goto execute_end;
			ex192_255:
				;
				if (Conversions.ToBoolean(xxx & 32))
					goto ex224_255;
				else
					goto ex192_223;
				ex192_223:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex208_223;
				else
					goto ex192_207;
				ex192_207:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex200_207;
				else
					goto ex192_199;
				ex192_199:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex196_199;
				else
					goto ex192_195;
				ex192_195:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex194_195;
				else
					goto ex192_193;
				ex192_193:
				;
				if (xxx == 192)
				{
					// 192 ' RET NZ
					if (modMain.fZ == Conversions.ToInteger(false))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 193 ' POP BC
					setBC(popw());
					lTStates = 10;
				}

				goto execute_end;
			ex194_195:
				;
				if (xxx == 194)
				{
					// 194 ' JP NZ,nn
					if (modMain.fZ == Conversions.ToInteger(false))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 195 ' JP nn
					modMain.regPC = peekw(modMain.regPC);
					lTStates = 10;
				}

				goto execute_end;
			ex196_199:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex198_199;
				else
					goto ex196_197;
				ex196_197:
				;
				if (xxx == 196)
				{
					// 196 ' CALL NZ,nn
					if (modMain.fZ == Conversions.ToInteger(false))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 197 ' PUSH BC
					int argword = modMain.regB * 256 | modMain.regC;
					pushw(argword);
					lTStates = 11;
				}

				goto execute_end;
			ex198_199:
				;
				if (xxx == 198)
				{
					// 198 ' ADD A,n
					int argb17 = nxtpcb();
					add_a(argb17);
					lTStates = 7;
				}
				else
				{
					// 199 ' RST 0
					pushpc();
					modMain.regPC = 0;
					lTStates = 11;
				}

				goto execute_end;
			ex200_207:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex204_207;
				else
					goto ex200_203;
				ex200_203:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex202_203;
				else
					goto ex200_201;
				ex200_201:
				;
				if (xxx == 200)
				{
					// 200 ' RET Z
					if (Conversions.ToBoolean(modMain.fZ))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 201 ' RET
					poppc();
					lTStates = 10;
				}

				goto execute_end;
			ex202_203:
				;
				if (xxx == 202)
				{
					// 202 ' JP Z,nn
					if (Conversions.ToBoolean(modMain.fZ))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 203 ' Prefix CB
					lTStates = execute_cb();
				}

				goto execute_end;
			ex204_207:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex206_207;
				else
					goto ex204_205;
				ex204_205:
				;
				if (xxx == 204)
				{
					// 204 ' CALL Z,nn
					if (Conversions.ToBoolean(modMain.fZ))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 205 ' CALL nn
					lTemp = nxtpcw();
					pushw(modMain.regPC);
					modMain.regPC = lTemp;
					lTStates = 17;
				}

				goto execute_end;
			ex206_207:
				;
				if (xxx == 206)
				{
					// 206 ' ADC A,n
					int argb18 = nxtpcb();
					adc_a(argb18);
					lTStates = 7;
				}
				else
				{
					// 207 ' RST 8
					pushpc();
					modMain.regPC = 8;
					lTStates = 11;
				}

				goto execute_end;
			ex208_223:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex216_223;
				else
					goto ex208_215;
				ex208_215:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex212_215;
				else
					goto ex208_211;
				ex208_211:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex210_211;
				else
					goto ex208_209;
				ex208_209:
				;
				if (xxx == 208)
				{
					// 208 ' RET NC
					if (modMain.fC == Conversions.ToInteger(false))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 209 ' POP DE
					modMain.regDE = popw();
					lTStates = 10;
				}

				goto execute_end;
			ex210_211:
				;
				if (xxx == 210)
				{
					// 210 '  JP NC,nn
					if (modMain.fC == Conversions.ToInteger(false))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 211 ' OUT (n),A
					int argport = 256 * modMain.regA + nxtpcb();
					modSpectrum.outb(argport, modMain.regA);
					lTStates = 11;
				}

				goto execute_end;
			ex212_215:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex214_215;
				else
					goto ex212_213;
				ex212_213:
				;
				if (xxx == 212)
				{
					// 212 ' CALL NC,nn
					if (modMain.fC == Conversions.ToInteger(false))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 213 ' PUSH DE
					pushw(modMain.regDE);
					lTStates = 11;
				}

				goto execute_end;
			ex214_215:
				;
				if (xxx == 214)
				{
					// 214 ' SUB n
					int argb19 = nxtpcb();
					sub_a(argb19);
					lTStates = 7;
				}
				else
				{
					// 215 ' RST 16
					pushpc();
					modMain.regPC = 16;
					lTStates = 11;
				}

				goto execute_end;
			ex216_223:
				;
				switch (xxx)
				{
					case 216: // RET C
						{
							if (Conversions.ToBoolean(modMain.fC))
							{
								poppc();
								lTStates = 11;
							}
							else
							{
								lTStates = 5;
							}

							break;
						}

					case 217: // EXX
						{
							exx();
							lTStates = 4;
							break;
						}

					case 218: // JP C,nn
						{
							if (Conversions.ToBoolean(modMain.fC))
							{
								modMain.regPC = nxtpcw();
							}
							else
							{
								modMain.regPC = modMain.regPC + 2;
							}

							lTStates = 10;
							break;
						}

					case 219: // IN A,(n)
						{
							int argport1 = modMain.regA * 256 | nxtpcb();
							modMain.regA = modSpectrum.inb(argport1);
							lTStates = 11;
							break;
						}

					case 220: // CALL C,nn
						{
							if (Conversions.ToBoolean(modMain.fC))
							{
								lTemp = nxtpcw();
								pushw(modMain.regPC);
								modMain.regPC = lTemp;
								lTStates = 17;
							}
							else
							{
								modMain.regPC = modMain.regPC + 2;
								lTStates = 10;
							}

							break;
						}

					case 221: // prefix IX
						{
							modMain.regID = modMain.regIX;
							lTStates = execute_id();
							modMain.regIX = modMain.regID;
							break;
						}

					case 222: // SBC n
						{
							int argb20 = nxtpcb();
							sbc_a(argb20);
							lTStates = 7;
							break;
						}

					case 223: // RST 24
						{
							pushpc();
							modMain.regPC = 24;
							lTStates = 11;
							break;
						}
				}

				goto execute_end;
			ex224_255:
				;
				if (Conversions.ToBoolean(xxx & 16))
					goto ex240_255;
				else
					goto ex224_239;
				ex224_239:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex232_239;
				else
					goto ex224_231;
				ex224_231:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex228_231;
				else
					goto ex224_227;
				ex224_227:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex226_227;
				else
					goto ex224_225;
				ex224_225:
				;
				if (xxx == 224)
				{
					// 224 ' RET PO
					if (modMain.fPV == Conversions.ToInteger(false))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 225 ' POP HL
					modMain.regHL = popw();
					lTStates = 10;
				}

				goto execute_end;
			ex226_227:
				;
				if (xxx == 226)
				{
					// 226 ' JP PO,nn
					if (modMain.fPV == Conversions.ToInteger(false))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 227 ' EX (SP),HL
					lTemp = modMain.regHL;
					modMain.regHL = peekw(modMain.regSP);
					pokew(modMain.regSP, lTemp);
					lTStates = 19;
				}

				goto execute_end;
			ex228_231:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex230_231;
				else
					goto ex228_229;
				ex228_229:
				;
				if (xxx == 228)
				{
					// 228 ' CALL PO,nn
					if (modMain.fPV == Conversions.ToInteger(false))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 229 ' PUSH HL
					pushw(modMain.regHL);
					lTStates = 11;
				}

				goto execute_end;
			ex230_231:
				;
				if (xxx == 230)
				{
					// 230 ' AND n
					int argb21 = nxtpcb();
					and_a(argb21);
					lTStates = 7;
				}
				else
				{
					// 231 ' RST 32
					pushpc();
					modMain.regPC = 32;
					lTStates = 11;
				}

				goto execute_end;
			ex232_239:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex236_239;
				else
					goto ex232_235;
				ex232_235:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex234_235;
				else
					goto ex232_233;
				ex232_233:
				;
				if (xxx == 232)
				{
					// RET PE
					if (Conversions.ToBoolean(modMain.fPV))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 233 ' JP HL
					modMain.regPC = modMain.regHL;
					lTStates = 4;
				}

				goto execute_end;
			ex234_235:
				;
				if (xxx == 234)
				{
					// 234 ' JP PE,nn
					if (Conversions.ToBoolean(modMain.fPV))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 235 ' EX DE,HL
					lTemp = modMain.regHL;
					modMain.regHL = modMain.regDE;
					modMain.regDE = lTemp;
					lTStates = 4;
				}

				goto execute_end;
			ex236_239:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex238_239;
				else
					goto ex236_237;
				ex236_237:
				;
				if (xxx == 236)
				{
					// 236 ' CALL PE,nn
					if (Conversions.ToBoolean(modMain.fPV))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 237 ' prefix ED
					lTStates = execute_ed();
				}

				goto execute_end;
			ex238_239:
				;
				if (xxx == 238)
				{
					// 238 ' XOR n
					int argb22 = nxtpcb();
					xor_a(argb22);
					lTStates = 7;
				}
				else
				{
					// 239 ' RST 40
					pushpc();
					modMain.regPC = 40;
					lTStates = 11;
				}

				goto execute_end;
			ex240_255:
				;
				if (Conversions.ToBoolean(xxx & 8))
					goto ex248_255;
				else
					goto ex240_247;
				ex240_247:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex244_247;
				else
					goto ex240_243;
				ex240_243:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex242_243;
				else
					goto ex240_241;
				ex240_241:
				;
				if (xxx == 240)
				{
					// 240 RET P
					if (modMain.fS == Conversions.ToInteger(false))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 241 POP AF
					setAF(popw());
					lTStates = 10;
				}

				goto execute_end;
			ex242_243:
				;
				if (xxx == 242)
				{
					// 242 JP P,nn
					if (modMain.fS == Conversions.ToInteger(false))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 243 DI
					modMain.intIFF1 = Conversions.ToInteger(false);
					modMain.intIFF2 = Conversions.ToInteger(false);
					lTStates = 4;
				}

				goto execute_end;
			ex244_247:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex246_247;
				else
					goto ex244_245;
				ex244_245:
				;
				if (xxx == 244)
				{
					// 244 CALL P,nn
					if (modMain.fS == Conversions.ToInteger(false))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 245 PUSH AF
					int argword1 = modMain.regA * 256 | getF();
					pushw(argword1);
					lTStates = 11;
				}

				goto execute_end;
			ex246_247:
				;
				if (xxx == 246)
				{
					// 246 OR n
					int argb23 = nxtpcb();
					or_a(argb23);
					lTStates = 7;
				}
				else
				{
					// 247 RST 48
					pushpc();
					modMain.regPC = 48;
					lTStates = 11;
				}

				goto execute_end;
			ex248_255:
				;
				if (Conversions.ToBoolean(xxx & 4))
					goto ex252_255;
				else
					goto ex248_251;
				ex248_251:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex250_251;
				else
					goto ex248_249;
				ex248_249:
				;
				if (xxx == 248)
				{
					// 248 RET M
					if (Conversions.ToBoolean(modMain.fS))
					{
						poppc();
						lTStates = 11;
					}
					else
					{
						lTStates = 5;
					}
				}
				else
				{
					// 249 LD SP,HL
					modMain.regSP = modMain.regHL;
					lTStates = 6;
				}

				goto execute_end;
			ex250_251:
				;
				if (xxx == 250)
				{
					// 250 JP M,nn
					if (Conversions.ToBoolean(modMain.fS))
					{
						modMain.regPC = nxtpcw();
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
					}

					lTStates = 10;
				}
				else
				{
					// 251 EI
					modMain.intIFF1 = Conversions.ToInteger(true);
					modMain.intIFF2 = Conversions.ToInteger(true);
					lTStates = 4;
				}

				goto execute_end;
			ex252_255:
				;
				if (Conversions.ToBoolean(xxx & 2))
					goto ex254_255;
				else
					goto ex252_253;
				ex252_253:
				;
				if (xxx == 252)
				{
					// 252 CALL M,nn
					if (Conversions.ToBoolean(modMain.fS))
					{
						lTemp = nxtpcw();
						pushw(modMain.regPC);
						modMain.regPC = lTemp;
						lTStates = 17;
					}
					else
					{
						modMain.regPC = modMain.regPC + 2;
						lTStates = 10;
					}
				}
				else
				{
					// 253 prefix IY
					modMain.regID = modMain.regIY;
					lTStates = execute_id();
					modMain.regIY = modMain.regID;
				}

				goto execute_end;
			ex254_255:
				;
				if (xxx == 254)
				{
					// 254 CP n
					int argb24 = nxtpcb();
					cp_a(argb24);
					lTStates = 7;
				}
				else
				{
					// 255 RST 56
					pushpc();
					modMain.regPC = 56;
					lTStates = 11;
				}

				goto execute_end;
			execute_end:
				;
				lTStates = lTStates + modMain.glContendedMemoryDelay;

				// If a TZX is currently playing then update the
				// tape position inline with the opcode timing.
				// This ensures that the present ear state remains
				// in sync with turboloaders.
				if (Conversions.ToBoolean(modTZX.gbTZXPlaying))
					modTZX.UpdateTZXState(lTStates);
				modMain.glTStates = modMain.glTStates + lTStates;
				modWaveOut.AddSoundWave(lTStates);
			}
		}

		private static int qdec8(int a)
		{
			int qdec8Ret = default;
			qdec8Ret = a - 1 & 0xFF;
			return qdec8Ret;
		}

		private static int execute_id()
		{
			int execute_idRet = default;
			int lTemp, xxx, op;

			// // Yes, I appreciate that GOTO's and labels are a hideous blashphemy!
			// // However, this code is the fastest possible way of fetching and handling
			// // Z80 instructions I could come up with. There are only 8 compares per
			// // instruction fetch rather than between 1 and 255 as required in
			// // the previous version of vb81 with it's huge Case statement.
			// //
			// // I know it's slightly harder to follow the new code, but I think the
			// // speed increase justifies it. <CC>



			// // REFRESH 1
			modMain.intRTemp = modMain.intRTemp + 1;

			// // Inlined version of "xxx = nxtpcb()" suggested by Gonchuki and Woody
			if ((modMain.regPC & 49152) == 16384)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			xxx = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[modMain.regPC]], modMain.regPC & 16383];
			modMain.regPC = modMain.regPC + 1;
			if (Conversions.ToBoolean(xxx & 128))
				goto ex_id128_255;
			else
				goto ex_id0_127;
			ex_id0_127:
			;
			if (Conversions.ToBoolean(xxx & 64))
				goto ex_id64_127;
			else
				goto ex_id0_63;
			ex_id0_63:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_id32_63;
			else
				goto ex_id0_31;
			ex_id0_31:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_id16_31;
			else
				goto ex_id0_15;
			ex_id0_15:
			;
			switch (xxx)
			{
				case var @case when 0 <= @case && @case <= 8:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 9: // ADD ID,BC
					{
						int argb = modMain.regB * 256 | modMain.regC;
						modMain.regID = add16(modMain.regID, argb);
						execute_idRet = 15;
						break;
					}

				case var case1 when 10 <= case1 && case1 <= 15:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id16_31:
			;
			if (Conversions.ToBoolean(xxx & 8))
			{
				goto ex_id24_31;
			}
			else
			{
				// 16 To 23
				modMain.regPC = dec16(modMain.regPC);
				// // REFRESH -1
				modMain.intRTemp = modMain.intRTemp - 1;
				execute_idRet = 4;
			}

			return execute_idRet;
		ex_id24_31:
			;
			if (xxx == 24)
			{
				// 24
				modMain.regPC = dec16(modMain.regPC);
				// // REFRESH -1
				modMain.intRTemp = modMain.intRTemp - 1;
				execute_idRet = 4;
			}
			else if (xxx == 25)
			{
				// 25 ' ADD ID,DE
				modMain.regID = add16(modMain.regID, modMain.regDE);
				execute_idRet = 15;
			}
			else
			{
				// 26 To 31
				modMain.regPC = dec16(modMain.regPC);
				// // REFRESH -1
				modMain.intRTemp = modMain.intRTemp - 1;
				execute_idRet = 4;
			}

			return execute_idRet;
		ex_id32_63:
			;
			switch (xxx)
			{
				case 32:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 33: // LD ID,nn
					{
						modMain.regID = nxtpcw();
						execute_idRet = 14;
						break;
					}

				case 34: // LD (nn),ID
					{
						pokew(nxtpcw(), modMain.regID);
						execute_idRet = 20;
						break;
					}

				case 35: // INC ID
					{
						modMain.regID = modMain.regID + 1 & 0xFFFF;
						execute_idRet = 10;
						break;
					}

				case 36: // INC IDH
					{
						int argbyteval = inc8(getIDH());
						setIDH(argbyteval);
						execute_idRet = 8;
						break;
					}

				case 37: // DEC IDH
					{
						int argbyteval1 = dec8(getIDH());
						setIDH(argbyteval1);
						execute_idRet = 8;
						break;
					}

				case 38: // LD IDH,n
					{
						int argbyteval2 = nxtpcb();
						setIDH(argbyteval2);
						execute_idRet = 11;
						break;
					}

				case 39:
				case 40:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 41: // ADD ID,ID
					{
						lTemp = modMain.regID;
						modMain.regID = add16(lTemp, lTemp);
						execute_idRet = 15;
						break;
					}

				case 42: // LD ID,(nn)
					{
						modMain.regID = peekw(nxtpcw());
						execute_idRet = 20;
						break;
					}

				case 43: // DEC ID
					{
						modMain.regID = dec16(modMain.regID);
						execute_idRet = 10;
						break;
					}

				case 44: // INC IDL
					{
						int argbyteval3 = inc8(getIDL());
						setIDL(argbyteval3);
						execute_idRet = 8;
						break;
					}

				case 45: // DEC IDL
					{
						int argbyteval4 = dec8(getIDL());
						setIDL(argbyteval4);
						execute_idRet = 8;
						break;
					}

				case 46: // LD IDL,n
					{
						int argbyteval5 = nxtpcb();
						setIDL(argbyteval5);
						execute_idRet = 11;
						break;
					}

				case var case2 when 47 <= case2 && case2 <= 51:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 52: // INC (ID+d)
					{
						lTemp = id_d();
						int argnewByte = inc8(peekb(lTemp));
						pokeb(lTemp, argnewByte);
						execute_idRet = 23;
						break;
					}

				case 53: // DEC (ID+d)
					{
						lTemp = id_d();
						int argnewByte1 = dec8(peekb(lTemp));
						pokeb(lTemp, argnewByte1);
						execute_idRet = 23;
						break;
					}

				case 54: // LD (ID+d),n
					{
						lTemp = id_d();
						int argnewByte2 = nxtpcb();
						pokeb(lTemp, argnewByte2);
						execute_idRet = 19;
						break;
					}

				case 55:
				case 56:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 57: // ADD ID,SP
					{
						modMain.regID = add16(modMain.regID, modMain.regSP);
						execute_idRet = 15;
						break;
					}

				case var case3 when 58 <= case3 && case3 <= 63:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id64_127:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_id96_127;
			else
				goto ex_id64_95;
			ex_id64_95:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_id80_95;
			else
				goto ex_id64_79;
			ex_id64_79:
			;
			switch (xxx)
			{
				case var case4 when 64 <= case4 && case4 <= 67:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 68: // LD B,IDH
					{
						modMain.regB = getIDH();
						execute_idRet = 8;
						break;
					}

				case 69: // LD B,IDL
					{
						modMain.regB = getIDL();
						execute_idRet = 8;
						break;
					}

				case 70: // LD B,(ID+d)
					{
						modMain.regB = peekb(id_d());
						execute_idRet = 19;
						break;
					}

				case var case5 when 71 <= case5 && case5 <= 75:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 76: // LD C,IDH
					{
						modMain.regC = getIDH();
						execute_idRet = 8;
						break;
					}

				case 77: // LD C,IDL
					{
						modMain.regC = getIDL();
						execute_idRet = 8;
						break;
					}

				case 78: // LD C,(ID+d)
					{
						modMain.regC = peekb(id_d());
						execute_idRet = 19;
						break;
					}

				case 79:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id80_95:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_id88_95;
			else
				goto ex_id80_87;
			ex_id80_87:
			;
			switch (xxx)
			{
				case var case6 when 80 <= case6 && case6 <= 83:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 84: // LD D,IDH
					{
						setD(getIDH());
						execute_idRet = 8;
						break;
					}

				case 85: // LD D,IDL
					{
						setD(getIDL());
						execute_idRet = 8;
						break;
					}

				case 86: // LD D,(ID+d)
					{
						int argl = peekb(id_d());
						setD(argl);
						execute_idRet = 19;
						break;
					}

				case 87:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id88_95:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_id92_95;
			else
				goto ex_id88_91;
			ex_id88_91:
			;

			// 88 To 91
			modMain.regPC = dec16(modMain.regPC);
			// // REFRESH -1
			modMain.intRTemp = modMain.intRTemp - 1;
			execute_idRet = 4;
			return execute_idRet;
		ex_id92_95:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_id94_95;
			else
				goto ex_id92_93;
			ex_id92_93:
			;
			if (xxx == 92)
			{
				// 92 ' LD E,IDH
				setE(getIDH());
				execute_idRet = 8;
			}
			else
			{
				// 93 ' LD E,IDL
				setE(getIDL());
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id94_95:
			;
			if (xxx == 94)
			{
				// 94 ' LD E,(ID+d)
				int argl1 = peekb(id_d());
				setE(argl1);
				execute_idRet = 19;
			}
			else
			{
				// 95
				modMain.regPC = dec16(modMain.regPC);
				// // REFRESH -1
				modMain.intRTemp = modMain.intRTemp - 1;
				execute_idRet = 4;
			}

			return execute_idRet;
		ex_id96_127:
			;
			if (Conversions.ToBoolean(xxx & 16))
				goto ex_id112_127;
			else
				goto ex_id96_111;
			ex_id96_111:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_id104_111;
			else
				goto ex_id96_103;
			ex_id96_103:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_id100_103;
			else
				goto ex_id96_99;
			ex_id96_99:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_id98_99;
			else
				goto ex_id96_97;
			ex_id96_97:
			;
			if (xxx == 96)
			{
				// 96 ' LD IDH,B
				setIDH(modMain.regB);
				execute_idRet = 8;
			}
			else
			{
				// 97 ' LD IDH,C
				setIDH(modMain.regC);
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id98_99:
			;
			if (xxx == 98)
			{
				// 98 ' LD IDH,D
				setIDH(modMain.glMemAddrDiv256[modMain.regDE]);
				execute_idRet = 8;
			}
			else
			{
				// 99 ' LD IDH,E
				setIDH(getE());
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id100_103:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_id102_103;
			else
				goto ex_id100_101;
			ex_id100_101:
			;
			if (xxx == 100)
			{
				// 100 ' LD IDH,IDH
				execute_idRet = 8;
			}
			else
			{
				// 101 ' LD IDH,IDL
				setIDH(getIDL());
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id102_103:
			;
			if (xxx == 102)
			{
				// 102 ' LD H,(ID+d)
				int argl2 = peekb(id_d());
				setH(argl2);
				execute_idRet = 19;
			}
			else
			{
				// 103 ' LD IDH,A
				setIDH(modMain.regA);
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id104_111:
			;
			if (Conversions.ToBoolean(xxx & 4))
				goto ex_id108_111;
			else
				goto ex_id104_107;
			ex_id104_107:
			;
			switch (xxx)
			{
				case 104: // LD IDL,B
					{
						setIDL(modMain.regB);
						execute_idRet = 9;
						break;
					}

				case 105: // LD IDL,C
					{
						setIDL(modMain.regC);
						execute_idRet = 9;
						break;
					}

				case 106: // LD IDL,D
					{
						setIDL(modMain.glMemAddrDiv256[modMain.regDE]);
						execute_idRet = 9;
						break;
					}

				case 107: // LD IDL,E
					{
						setIDL(getE());
						execute_idRet = 9;
						break;
					}
			}

			return execute_idRet;
		ex_id108_111:
			;
			if (Conversions.ToBoolean(xxx & 2))
				goto ex_id110_111;
			else
				goto ex_id108_109;
			ex_id108_109:
			;
			if (xxx == 108)
			{
				// 108 ' LD IDL,IDH
				setIDL(getIDH());
				execute_idRet = 8;
			}
			else
			{
				// 109 ' LD IDL,IDL
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id110_111:
			;
			if (xxx == 110)
			{
				// 110 ' LD L,(ID+d)
				int argl3 = peekb(id_d());
				setL(argl3);
				execute_idRet = 19;
			}
			else
			{
				// 111 ' LD IDL,A
				setIDL(modMain.regA);
				execute_idRet = 8;
			}

			return execute_idRet;
		ex_id112_127:
			;
			if (Conversions.ToBoolean(xxx & 8))
				goto ex_id120_127;
			else
				goto ex_id112_119;
			ex_id112_119:
			;
			switch (xxx)
			{
				case 112: // LD (ID+d),B
					{
						pokeb(id_d(), modMain.regB);
						execute_idRet = 19;
						break;
					}

				case 113: // LD (ID+d),C
					{
						pokeb(id_d(), modMain.regC);
						execute_idRet = 19;
						break;
					}

				case 114: // LD (ID+d),D
					{
						pokeb(id_d(), modMain.glMemAddrDiv256[modMain.regDE]);
						execute_idRet = 19;
						break;
					}

				case 115: // LD (ID+d),E
					{
						pokeb(id_d(), getE());
						execute_idRet = 19;
						break;
					}

				case 116: // LD (ID+d),H
					{
						pokeb(id_d(), modMain.glMemAddrDiv256[modMain.regHL]);
						execute_idRet = 19;
						break;
					}

				case 117: // LD (ID+d),L
					{
						int argnewByte3 = modMain.regHL & 0xFF;
						pokeb(id_d(), argnewByte3);
						execute_idRet = 19;
						break;
					}

				case 118: // UNKNOWN
					{
						Interaction.MsgBox("Unknown ID instruction " + xxx + " at " + modMain.regPC);
						break;
					}

				case 119: // LD (ID+d),A
					{
						pokeb(id_d(), modMain.regA);
						execute_idRet = 19;
						break;
					}
			}

			return execute_idRet;
		ex_id120_127:
			;
			switch (xxx)
			{
				case var case7 when 120 <= case7 && case7 <= 123:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 124: // LD A,IDH
					{
						modMain.regA = getIDH();
						execute_idRet = 8;
						break;
					}

				case 125: // LD A,IDL
					{
						modMain.regA = getIDL();
						execute_idRet = 8;
						break;
					}

				case 126: // LD A,(ID+d)
					{
						modMain.regA = peekb(id_d());
						execute_idRet = 19;
						break;
					}

				case 127:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id128_255:
			;
			if (Conversions.ToBoolean(xxx & 64))
				goto ex_id192_255;
			else
				goto ex_id128_191;
			ex_id128_191:
			;
			if (Conversions.ToBoolean(xxx & 32))
				goto ex_id160_191;
			else
				goto ex_id128_159;
			ex_id128_159:
			;
			switch (xxx)
			{
				case var case8 when 128 <= case8 && case8 <= 131:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 132: // ADD A,IDH
					{
						add_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 133: // ADD A,IDL
					{
						add_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 134: // ADD A,(ID+d)
					{
						int argb1 = peekb(id_d());
						add_a(argb1);
						execute_idRet = 19;
						break;
					}

				case var case9 when 135 <= case9 && case9 <= 139:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 140: // ADC A,IDH
					{
						adc_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 141: // ADC A,IDL
					{
						adc_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 142: // ADC A,(ID+d)
					{
						int argb2 = peekb(id_d());
						adc_a(argb2);
						execute_idRet = 19;
						break;
					}

				case var case10 when 143 <= case10 && case10 <= 147:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 148: // SUB IDH
					{
						sub_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 149: // SUB IDL
					{
						sub_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 150: // SUB (ID+d)
					{
						int argb3 = peekb(id_d());
						sub_a(argb3);
						execute_idRet = 19;
						break;
					}

				case var case11 when 151 <= case11 && case11 <= 155:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 156: // SBC A,IDH
					{
						sbc_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 157: // SBC A,IDL
					{
						sbc_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 158: // SBC A,(ID+d)
					{
						int argb4 = peekb(id_d());
						sbc_a(argb4);
						execute_idRet = 19;
						break;
					}

				case 159:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id160_191:
			;
			switch (xxx)
			{
				case var case12 when 160 <= case12 && case12 <= 163:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 164: // AND IDH
					{
						and_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 165: // AND IDL
					{
						and_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 166: // AND (ID+d)
					{
						int argb5 = peekb(id_d());
						and_a(argb5);
						execute_idRet = 19;
						break;
					}

				case var case13 when 167 <= case13 && case13 <= 171:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 172: // XOR IDH
					{
						xor_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 173: // XOR IDL
					{
						xor_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 174: // XOR (ID+d)
					{
						int argb6 = peekb(id_d());
						xor_a(argb6);
						execute_idRet = 19;
						break;
					}

				case var case14 when 175 <= case14 && case14 <= 179:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 180: // OR IDH
					{
						or_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 181: // OR IDL
					{
						or_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 182: // OR (ID+d)
					{
						int argb7 = peekb(id_d());
						or_a(argb7);
						execute_idRet = 19;
						break;
					}

				case var case15 when 183 <= case15 && case15 <= 187:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 188: // CP IDH
					{
						cp_a(getIDH());
						execute_idRet = 8;
						break;
					}

				case 189: // CP IDL
					{
						cp_a(getIDL());
						execute_idRet = 8;
						break;
					}

				case 190: // CP (ID+d)
					{
						int argb8 = peekb(id_d());
						cp_a(argb8);
						execute_idRet = 19;
						break;
					}

				case 191:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		ex_id192_255:
			;
			switch (xxx)
			{
				case var case16 when 192 <= case16 && case16 <= 202:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 203: // prefix CB
					{
						lTemp = id_d();
						op = nxtpcb();
						execute_id_cb(op, lTemp);
						if ((op & 0xC0) == 0x40)
							execute_idRet = 20;
						else
							execute_idRet = 23;
						break;
					}

				case var case17 when 204 <= case17 && case17 <= 224:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 225: // POP ID
					{
						modMain.regID = popw();
						execute_idRet = 14;
						break;
					}

				case 226:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 227: // EX (SP),ID
					{
						lTemp = modMain.regID;
						modMain.regID = peekw(modMain.regSP);
						pokew(modMain.regSP, lTemp);
						execute_idRet = 23;
						break;
					}

				case 228:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 229: // PUSH ID
					{
						pushw(modMain.regID);
						execute_idRet = 15;
						break;
					}

				case var case18 when 230 <= case18 && case18 <= 232:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 233: // JP ID
					{
						modMain.regPC = modMain.regID;
						execute_idRet = 8;
						break;
					}

				case var case19 when 234 <= case19 && case19 <= 248:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				case 249: // LD SP,ID
					{
						modMain.regSP = modMain.regID;
						execute_idRet = 10;
						break;
					}

				case 253: // // Prefix FD
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}

				default:
					{
						modMain.regPC = dec16(modMain.regPC);
						// // REFRESH -1
						modMain.intRTemp = modMain.intRTemp - 1;
						execute_idRet = 4;
						break;
					}
			}

			return execute_idRet;
		}

		private static int inc16(int a)
		{
			int inc16Ret = default;
			inc16Ret = a + 1 & 0xFFFF;
			return inc16Ret;
		}

		private static int nxtpcw()
		{
			int nxtpcwRet = default;
			int localpeekb_NoContendedDelay() { int argaddr = modMain.regPC + 1; var ret = peekb_NoContendedDelay(argaddr); return ret; }

			nxtpcwRet = peekb(modMain.regPC) + localpeekb_NoContendedDelay() * 256;
			modMain.regPC = modMain.regPC + 2;
			return nxtpcwRet;
		}

		private static int nxtpcb()
		{
			int nxtpcbRet = default;
			nxtpcbRet = peekb(modMain.regPC);
			modMain.regPC = modMain.regPC + 1;
			return nxtpcbRet;
		}


		// MM 03.02.2003 - This function must be public to use the Poke/Peek Window
		public static int peekb(int addr)
		{
			int peekbRet = default;
			if ((addr & 49152) == 16384)
			{
				modMain.glContendedMemoryDelay = modMain.glContendedMemoryDelay + modMain.glContentionTable[-modMain.glTStates + 30];
			}

			peekbRet = modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[addr]], addr & 16383];
			return peekbRet;
		}

		private static void setD(int l)
		{
			modMain.regDE = l * 256 | modMain.regDE & 0xFF;
		}

		private static void setE(int l)
		{
			modMain.regDE = modMain.regDE & 0xFF00 | l;
		}

		public static void setF(int b)
		{
			modMain.fS = Conversions.ToInteger((b & modMain.F_S) != 0);
			modMain.fZ = Conversions.ToInteger((b & modMain.F_Z) != 0);
			modMain.f5 = Conversions.ToInteger((b & modMain.F_5) != 0);
			modMain.fH = Conversions.ToInteger((b & modMain.F_H) != 0);
			modMain.f3 = Conversions.ToInteger((b & modMain.F_3) != 0);
			modMain.fPV = Conversions.ToInteger((b & modMain.F_PV) != 0);
			modMain.fN = Conversions.ToInteger((b & modMain.F_N) != 0);
			modMain.fC = Conversions.ToInteger((b & modMain.F_C) != 0);
		}

		private static void setH(int l)
		{
			modMain.regHL = l * 256 | modMain.regHL & 0xFF;
		}

		private static void setL(int l)
		{
			modMain.regHL = modMain.regHL & 0xFF00 | l;
		}

		private static int sla(int ans)
		{
			int slaRet = default;
			modMain.fC = Conversions.ToInteger((ans & 0x80) != 0);
			ans = ans * 2 & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fPV = modMain.Parity[ans];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			slaRet = ans;
			return slaRet;
		}

		private static int sls(int ans)
		{
			int slsRet = default;
			modMain.fC = Conversions.ToInteger((ans & 0x80) != 0);
			ans = (ans * 2 | 0x1) & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fPV = modMain.Parity[ans];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			slsRet = ans;
			return slsRet;
		}

		private static int sra(int ans)
		{
			int sraRet = default;
			modMain.fC = Conversions.ToInteger((ans & 0x1) != 0);
			ans = ans / 2 | ans & 0x80;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fPV = modMain.Parity[ans];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			sraRet = ans;
			return sraRet;
		}

		private static int srl(int ans)
		{
			int srlRet = default;
			modMain.fC = Conversions.ToInteger((ans & 0x1) != 0);
			ans = ans / 2;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fPV = modMain.Parity[ans];
			modMain.fH = Conversions.ToInteger(false);
			modMain.fN = Conversions.ToInteger(false);
			srlRet = ans;
			return srlRet;
		}

		private static void sub_a(int b)
		{
			int wans, ans;
			wans = modMain.regA - b;
			ans = wans & 0xFF;
			modMain.fS = Conversions.ToInteger((ans & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((ans & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((ans & modMain.F_5) != 0);
			modMain.fZ = Conversions.ToInteger(ans == 0);
			modMain.fC = Conversions.ToInteger((wans & 0x100) != 0);
			modMain.fPV = Conversions.ToInteger(((modMain.regA ^ b) & (modMain.regA ^ ans) & 0x80) != 0);
			modMain.fH = Conversions.ToInteger(((short)(modMain.regA & 0xF) - (short)(b & 0xF) & modMain.F_H) != 0);
			modMain.fN = Conversions.ToInteger(true);
			modMain.regA = ans;
		}

		private static void xor_a(int b)
		{
			modMain.regA = (modMain.regA ^ b) & 0xFF;
			modMain.fS = Conversions.ToInteger((modMain.regA & modMain.F_S) != 0);
			modMain.f3 = Conversions.ToInteger((modMain.regA & modMain.F_3) != 0);
			modMain.f5 = Conversions.ToInteger((modMain.regA & modMain.F_5) != 0);
			modMain.fH = Conversions.ToInteger(false);
			modMain.fPV = modMain.Parity[modMain.regA];
			modMain.fZ = Conversions.ToInteger(modMain.regA == 0);
			modMain.fN = Conversions.ToInteger(false);
			modMain.fC = Conversions.ToInteger(false);
		}

		public static void Z80Reset()
		{
			modMain.regPC = 0;
			modMain.regSP = 65535;
			modMain.regA = 0;
			int argb = 0;
			setF(argb);
			int argnn = 0;
			setBC(argnn);
			modMain.regDE = 0;
			modMain.regHL = 0;
			exx();
			ex_af_af();
			modMain.regA = 0;
			int argb1 = 0;
			setF(argb1);
			int argnn1 = 0;
			setBC(argnn1);
			modMain.regDE = 0;
			modMain.regHL = 0;
			modMain.regIX = 0;
			modMain.regIY = 0;
			modMain.intR = 128;
			modMain.intRTemp = 0;
			modMain.intI = 0;
			modMain.intIFF1 = Conversions.ToInteger(false);
			modMain.intIFF2 = Conversions.ToInteger(false);
			modMain.intIM = 0;
			int arglModel = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "EmulatedModel", "0"));
			int argbSEBasicROM = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "SEBasicROM", "0"));
			modMain.SetEmulatedModel(ref arglModel, ref argbSEBasicROM);
			double argclock = 1773000d;
			int argsample_rate = modWaveOut.WAVE_FREQUENCY;
			int argsample_bits = 8;
			modAY8912.AY8912_init(ref argclock, ref argsample_rate, ref argsample_bits);
			My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName;
		}
	}
}