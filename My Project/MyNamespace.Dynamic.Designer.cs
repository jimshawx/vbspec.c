﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace vbSpec.My
{
	internal static partial class MyProject
	{
		internal partial class MyForms
		{
			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmAbout m_frmAbout;

			public frmAbout frmAbout
			{
				[DebuggerHidden]
				get
				{
					m_frmAbout = Create__Instance__(m_frmAbout);
					return m_frmAbout;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmAbout))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmAbout);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmDefButton m_frmDefButton;

			public frmDefButton frmDefButton
			{
				[DebuggerHidden]
				get
				{
					m_frmDefButton = Create__Instance__(m_frmDefButton);
					return m_frmDefButton;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmDefButton))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmDefButton);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmDefJoystick m_frmDefJoystick;

			public frmDefJoystick frmDefJoystick
			{
				[DebuggerHidden]
				get
				{
					m_frmDefJoystick = Create__Instance__(m_frmDefJoystick);
					return m_frmDefJoystick;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmDefJoystick))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmDefJoystick);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmDisplayOpt m_frmDisplayOpt;

			public frmDisplayOpt frmDisplayOpt
			{
				[DebuggerHidden]
				get
				{
					m_frmDisplayOpt = Create__Instance__(m_frmDisplayOpt);
					return m_frmDisplayOpt;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmDisplayOpt))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmDisplayOpt);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmKeyboard m_frmKeyboard;

			public frmKeyboard frmKeyboard
			{
				[DebuggerHidden]
				get
				{
					m_frmKeyboard = Create__Instance__(m_frmKeyboard);
					return m_frmKeyboard;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmKeyboard))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmKeyboard);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmLoadBinary m_frmLoadBinary;

			public frmLoadBinary frmLoadBinary
			{
				[DebuggerHidden]
				get
				{
					m_frmLoadBinary = Create__Instance__(m_frmLoadBinary);
					return m_frmLoadBinary;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmLoadBinary))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmLoadBinary);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmMainWnd m_frmMainWnd;

			public frmMainWnd frmMainWnd
			{
				[DebuggerHidden]
				get
				{
					m_frmMainWnd = Create__Instance__(m_frmMainWnd);
					return m_frmMainWnd;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmMainWnd))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmMainWnd);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmOptions m_frmOptions;

			public frmOptions frmOptions
			{
				[DebuggerHidden]
				get
				{
					m_frmOptions = Create__Instance__(m_frmOptions);
					return m_frmOptions;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmOptions))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmOptions);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmPoke m_frmPoke;

			public frmPoke frmPoke
			{
				[DebuggerHidden]
				get
				{
					m_frmPoke = Create__Instance__(m_frmPoke);
					return m_frmPoke;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmPoke))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmPoke);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmSaveBinary m_frmSaveBinary;

			public frmSaveBinary frmSaveBinary
			{
				[DebuggerHidden]
				get
				{
					m_frmSaveBinary = Create__Instance__(m_frmSaveBinary);
					return m_frmSaveBinary;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmSaveBinary))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmSaveBinary);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmTapePlayer m_frmTapePlayer;

			public frmTapePlayer frmTapePlayer
			{
				[DebuggerHidden]
				get
				{
					m_frmTapePlayer = Create__Instance__(m_frmTapePlayer);
					return m_frmTapePlayer;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmTapePlayer))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmTapePlayer);
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmZXPrinter m_frmZXPrinter;

			public frmZXPrinter frmZXPrinter
			{
				[DebuggerHidden]
				get
				{
					m_frmZXPrinter = Create__Instance__(m_frmZXPrinter);
					return m_frmZXPrinter;
				}

				[DebuggerHidden]
				set
				{
					if (ReferenceEquals(value, m_frmZXPrinter))
						return;
					if (value is object)
						throw new ArgumentException("Property can only be set to Nothing");
					Dispose__Instance__(ref m_frmZXPrinter);
				}
			}
		}
	}
}