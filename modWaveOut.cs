﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	static class modWaveOut
	{
		// /*******************************************************************************
		// modWaveOut.bas within vbSpec.vbp
		// 
		// API declarations and support routines for proving beeper emulation using
		// the Windows waveOut* API fucntions.
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2000 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/



		[DllImport("winmm.dll")]
		public static extern int waveOutClose(IntPtr hWaveOut);
		// UPGRADE_NOTE: err was upgraded to err_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		[DllImport("winmm.dll", EntryPoint = "waveOutGetErrorTextW", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int waveOutGetErrorText(int err_Renamed, [Out] string lpText, int uSize);
		[DllImport("winmm.dll")]
		public static extern int waveOutGetVolume(int uDeviceID, ref int lpdwVolume);
		[DllImport("winmm.dll")]
		public static extern int waveOutMessage(int hWaveOut, int msg, int dw1, int dw2);
		// UPGRADE_WARNING: Structure WAVEFORMAT may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("winmm.dll", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int waveOutOpen(ref IntPtr LPHWAVEOUT, int uDeviceID, ref WAVEFORMATEX lpFormat, IntPtr dwCallback, IntPtr dwInstance, uint dwFlags);
		[DllImport("winmm.dll")]
		public static extern int waveOutPause(int hWaveOut);
		// UPGRADE_WARNING: Structure WAVEHDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("winmm.dll")]
		public static extern int waveOutPrepareHeader(IntPtr hWaveOut, ref WAVEHDR lpWaveOutHdr, int uSize);
		[DllImport("winmm.dll")]
		public static extern int waveOutReset(IntPtr hWaveOut);
		[DllImport("winmm.dll")]
		public static extern int waveOutRestart(IntPtr hWaveOut);
		[DllImport("winmm.dll")]
		public static extern int waveOutSetVolume(int uDeviceID, int dwVolume);
		// UPGRADE_WARNING: Structure WAVEHDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("winmm.dll")]
		public static extern int waveOutUnprepareHeader(IntPtr hWaveOut, ref WAVEHDR lpWaveOutHdr, int uSize);
		// UPGRADE_WARNING: Structure WAVEHDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("winmm.dll")]
		public static extern int waveOutWrite(int hWaveOut, ref WAVEHDR lpWaveOutHdr, int uSize);

		// // Functions for allocations fixed blocks of memory to hold the waveform buffers
		[DllImport("kernel32")]
		public static extern int GlobalAlloc(int wFlags, int dwBytes);
		[DllImport("kernel32")]
		public static extern int GlobalLock(int hMem);
		[DllImport("kernel32")]
		public static extern int GlobalUnlock(int hMem);
		[DllImport("kernel32")]
		public static extern int GlobalFree(int hMem);

		public const int GMEM_FIXED = 0x0;
		public const int GMEM_ZEROINIT = 0x40;
		public const bool GPTR = true;

		// // Function for moving waveform data from a VB byte array into a block of memory
		// // allocated by GlobalAlloc()
		// UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		static extern void CopyPtrFromStruct(int ptr, ref object @struct, int cb);

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct WAVEFORMATEX
		{
			public short wFormatTag;
			public short nChannels;
			public int nSamplesPerSec;
			public int nAvgBytesPerSec;
			public short nBlockAlign;
			public short wBitsPerSample;
			public short cbSize;
		}

		public struct WAVEHDR
		{
			public int lpData;
			public int dwBufferLength;
			public int dwBytesRecorded;
			public int dwUser;
			public int dwFlags;
			public int dwLoops;
			public int lpNext;
			public int Reserved;
		}

		public const int WAVE_ALLOWSYNC = 0x2;
		public const int WAVE_FORMAT_1M08 = 0x1; // 11.025 kHz, Mono,   8-bit
		public const int WAVE_FORMAT_1M16 = 0x4; // 11.025 kHz, Mono,   16-bit
		public const int WAVE_FORMAT_1S08 = 0x2; // 11.025 kHz, Stereo, 8-bit
		public const int WAVE_FORMAT_1S16 = 0x8; // 11.025 kHz, Stereo, 16-bit
		public const int WAVE_FORMAT_2M08 = 0x10; // 22.05  kHz, Mono,   8-bit
		public const int WAVE_FORMAT_2M16 = 0x40; // 22.05  kHz, Mono,   16-bit
		public const int WAVE_FORMAT_2S08 = 0x20; // 22.05  kHz, Stereo, 8-bit
		public const int WAVE_FORMAT_2S16 = 0x80; // 22.05  kHz, Stereo, 16-bit
		public const int WAVE_FORMAT_4M08 = 0x100; // 44.1   kHz, Mono,   8-bit
		public const int WAVE_FORMAT_4M16 = 0x400; // 44.1   kHz, Mono,   16-bit
		public const int WAVE_FORMAT_4S08 = 0x200; // 44.1   kHz, Stereo, 8-bit
		public const int WAVE_FORMAT_4S16 = 0x800; // 44.1   kHz, Stereo, 16-bit
		public const int WAVE_FORMAT_DIRECT = 0x8;
		public const short WAVE_FORMAT_PCM = 1; // Needed in resource files so outside #ifndef RC_INVOKED
		public const int WAVE_FORMAT_QUERY = 0x1;
		public const bool WAVE_FORMAT_DIRECT_QUERY = true;
		public const int WAVE_INVALIDFORMAT = 0x0; // invalid format
		public const int WAVE_MAPPED = 0x4;
		public const int WAVE_MAPPER = -1;
		public const int WAVE_VALID = 0x3; // ;Internal
		public const int WAVECAPS_LRVOLUME = 0x8; // separate left-right volume control
		public const int WAVECAPS_PITCH = 0x1; // supports pitch control
		public const int WAVECAPS_PLAYBACKRATE = 0x2; // supports playback rate control
		public const int WAVECAPS_SYNC = 0x10;
		public const int WAVECAPS_VOLUME = 0x4; // supports volume control
		public const short WAVERR_BASE = 32;
		public const int WAVERR_BADFORMAT = WAVERR_BASE + 0; // unsupported wave format
		public const int WAVERR_LASTERROR = WAVERR_BASE + 3; // last error in range
		public const int WAVERR_STILLPLAYING = WAVERR_BASE + 1; // still something playing
		public const int WAVERR_SYNC = WAVERR_BASE + 3; // device is synchronous
		public const int WAVERR_UNPREPARED = WAVERR_BASE + 2; // header not prepared
		public const short MMSYSERR_NOERROR = 0; // no error
		public const int CALLBACK_WINDOW = 0x10000; // dwCallback is a HWND
		public const int CALLBACK_TYPEMASK = 0x70000; // callback type mask
		public const int CALLBACK_TASK = 0x20000; // dwCallback is a HTASK
		public const int CALLBACK_NULL = 0x0; // no callback
		public const int CALLBACK_FUNCTION = 0x30000; // dwCallback is a FARPROC
		public const int CALL_PENDING = 0x2;
		public const int WHDR_BEGINLOOP = 0x4; // loop start block
		public const int WHDR_DONE = 0x1; // done bit
		public const int WHDR_INQUEUE = 0x10; // reserved for driver
		public const int WHDR_ENDLOOP = 0x8; // loop end block
		public const int WHDR_PREPARED = 0x2; // set if this header has been prepared
		public const int WHDR_VALID = 0x1F; // valid flags      / ;Internal /
		public const int MM_WOM_CLOSE = 0x3BC;
		public const int MM_WOM_DONE = 0x3BD;
		public const int MM_WOM_OPEN = 0x3BB; // waveform output
		public const int WOM_DONE = MM_WOM_DONE;
		public const int WOM_OPEN = MM_WOM_OPEN;
		public const int WOM_CLOSE = MM_WOM_CLOSE;

		// // Variables and constants used by the beeper emulation
		public static IntPtr glphWaveOut;
		public const short NUM_WAV_BUFFERS = 20;
		public const short WAVE_FREQUENCY = 22050;
		public const short WAV_BUFFER_SIZE = 441; // (WAVE_FREQUENCY \ NUM_WAV_BUFFERS)
												  // UPGRADE_WARNING: Lower bound of array ghMem was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		public static int[] ghMem = new int[(NUM_WAV_BUFFERS + 1)];
		// UPGRADE_WARNING: Lower bound of array gpMem was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		public static int[] gpMem = new int[(NUM_WAV_BUFFERS + 1)];
		public static WAVEFORMATEX gtWavFormat;
		// UPGRADE_WARNING: Lower bound of array gtWavHdr was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		public static WAVEHDR[] gtWavHdr = new WAVEHDR[(NUM_WAV_BUFFERS + 1)];
		public static byte[] gcWaveOut = new byte[48001];
		public static int glWavePtr;
		public static int glWaveAddTStates;

		public static int WCount;
		public static int lCounter;

		public static void AddSoundWave(int ts)
		{
			var lEarVal = default(object);
			;
//#error Cannot convert LocalDeclarationStatementSyntax - see comment for details
			/* Cannot convert LocalDeclarationStatementSyntax, System.NotSupportedException: StaticKeyword not supported!
			   at ICSharpCode.CodeConverter.CSharp.SyntaxKindExtensions.ConvertToken(SyntaxKind t, TokenContext context)
			   at ICSharpCode.CodeConverter.CSharp.CommonConversions.ConvertModifier(SyntaxToken m, TokenContext context)
			   at ICSharpCode.CodeConverter.CSharp.CommonConversions.<ConvertModifiersCore>d__43.MoveNext()
			   at System.Linq.Enumerable.<ConcatIterator>d__59`1.MoveNext()
			   at System.Linq.Enumerable.WhereEnumerableIterator`1.MoveNext()
			   at System.Linq.Buffer`1..ctor(IEnumerable`1 source)
			   at System.Linq.OrderedEnumerable`1.<GetEnumerator>d__1.MoveNext()
			   at Microsoft.CodeAnalysis.SyntaxTokenList.CreateNode(IEnumerable`1 tokens)
			   at ICSharpCode.CodeConverter.CSharp.CommonConversions.ConvertModifiers(SyntaxNode node, IReadOnlyCollection`1 modifiers, TokenContext context, Boolean isVariableOrConst, SyntaxKind[] extraCsModifierKinds)
			   at ICSharpCode.CodeConverter.CSharp.MethodBodyExecutableStatementVisitor.<VisitLocalDeclarationStatement>d__31.MoveNext()
			--- End of stack trace from previous location where exception was thrown ---
			   at ICSharpCode.CodeConverter.CSharp.HoistedNodeStateVisitor.<AddLocalVariablesAsync>d__6.MoveNext()
			--- End of stack trace from previous location where exception was thrown ---
			   at ICSharpCode.CodeConverter.CSharp.CommentConvertingMethodBodyVisitor.<DefaultVisitInnerAsync>d__3.MoveNext()

			Input:
					Static WCount As Integer

			 */
			WCount = WCount + 1;
			if (WCount == 800)
			{
				modAY8912.AY8912Update_8();
				WCount = 0;
			};
//#error Cannot convert LocalDeclarationStatementSyntax - see comment for details
			/* Cannot convert LocalDeclarationStatementSyntax, System.NotSupportedException: StaticKeyword not supported!
			   at ICSharpCode.CodeConverter.CSharp.SyntaxKindExtensions.ConvertToken(SyntaxKind t, TokenContext context)
			   at ICSharpCode.CodeConverter.CSharp.CommonConversions.ConvertModifier(SyntaxToken m, TokenContext context)
			   at ICSharpCode.CodeConverter.CSharp.CommonConversions.<ConvertModifiersCore>d__43.MoveNext()
			   at System.Linq.Enumerable.<ConcatIterator>d__59`1.MoveNext()
			   at System.Linq.Enumerable.WhereEnumerableIterator`1.MoveNext()
			   at System.Linq.Buffer`1..ctor(IEnumerable`1 source)
			   at System.Linq.OrderedEnumerable`1.<GetEnumerator>d__1.MoveNext()
			   at Microsoft.CodeAnalysis.SyntaxTokenList.CreateNode(IEnumerable`1 tokens)
			   at ICSharpCode.CodeConverter.CSharp.CommonConversions.ConvertModifiers(SyntaxNode node, IReadOnlyCollection`1 modifiers, TokenContext context, Boolean isVariableOrConst, SyntaxKind[] extraCsModifierKinds)
			   at ICSharpCode.CodeConverter.CSharp.MethodBodyExecutableStatementVisitor.<VisitLocalDeclarationStatement>d__31.MoveNext()
			--- End of stack trace from previous location where exception was thrown ---
			   at ICSharpCode.CodeConverter.CSharp.HoistedNodeStateVisitor.<AddLocalVariablesAsync>d__6.MoveNext()
			--- End of stack trace from previous location where exception was thrown ---
			   at ICSharpCode.CodeConverter.CSharp.CommentConvertingMethodBodyVisitor.<DefaultVisitInnerAsync>d__3.MoveNext()

			Input:

					Static lCounter As Integer

			 */
			lCounter = lCounter + ts;
			if (Conversions.ToBoolean(modTZX.gbTZXPlaying))
			{
				// UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				if (modTZX.glEarBit == 64)
				{
					lEarVal = 15;
				}
				else
				{
					// UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					lEarVal = 0;
				}
			}

			while (lCounter >= glWaveAddTStates)
			{
				if (Conversions.ToBoolean(modMain.gbEmulateAYSound))
				{
					// UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					gcWaveOut[glWavePtr] = Conversions.ToByte(Operators.AddObject(modMain.glBeeperVal + modAY8912.RenderByte(), lEarVal));
				}
				else
				{
					// UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					gcWaveOut[glWavePtr] = Conversions.ToByte(Operators.AddObject(modMain.glBeeperVal, lEarVal));
				}

				glWavePtr = glWavePtr + 1;
				lCounter = lCounter - glWaveAddTStates;
			}
		}
	}
}