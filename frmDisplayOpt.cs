﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace vbSpec
{
	internal partial class frmDisplayOpt : Form
	{
		// /*******************************************************************************
		// frmDisplayOpt.frm within vbSpec.vbp
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// Full screen support: Miklos Muhi <vbspec@muhi.org>
		// 
		// Copyright (C)1999-2002 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/

		private void cmdCancel_Click(object eventSender, EventArgs eventArgs)
		{
			Close();
		}

		private void cmdOK_Click(object eventSender, EventArgs eventArgs)
		{
			// MM 16.04.2003
			int lFullHeight, lFullWidth;
			int lYFactor, lXFactor, lCommonFactor;
			if (optNormal.Checked)
			{
				// MM 16.04.2003
				modSpectrum.bFullScreen = false;
				int arglWidth = 256;
				int arglHeight = 192;
				modMain.SetDisplaySize(ref arglWidth, ref arglHeight);
			}
			else if (optDoubleWidth.Checked)
			{
				// MM 16.04.2003
				modSpectrum.bFullScreen = false;
				int arglWidth1 = 512;
				int arglHeight1 = 192;
				modMain.SetDisplaySize(ref arglWidth1, ref arglHeight1);
			}
			else if (optDoubleHeight.Checked)
			{
				// MM 16.04.2003
				modSpectrum.bFullScreen = false;
				int arglWidth2 = 256;
				int arglHeight2 = 384;
				modMain.SetDisplaySize(ref arglWidth2, ref arglHeight2);
			}
			else if (optDouble.Checked)
			{
				// MM 16.04.2003
				modSpectrum.bFullScreen = false;
				int arglWidth3 = 512;
				int arglHeight3 = 384;
				modMain.SetDisplaySize(ref arglWidth3, ref arglHeight3);
			}
			else if (optTriple.Checked)
			{
				// MM 16.04.2003
				modSpectrum.bFullScreen = false;
				int arglWidth4 = 768;
				int arglHeight4 = 576;
				modMain.SetDisplaySize(ref arglWidth4, ref arglHeight4);
			}

			// MM 16.04.2003
			else if (optFullScreen.Checked)
			{
				lXFactor = (int)(Conversion.Fix(Screen.PrimaryScreen.Bounds.Width) / 256d);
				lYFactor = (int)(Conversion.Fix(Screen.PrimaryScreen.Bounds.Height) / 176d);
				lCommonFactor = lXFactor;
				if (lYFactor < lXFactor)
				{
					lCommonFactor = lYFactor;
				}

				modSpectrum.bFullScreen = true;
				int arglWidth5 = 256 * lCommonFactor;
				int arglHeight5 = 192 * lCommonFactor;
				modMain.SetDisplaySize(ref arglWidth5, ref arglHeight5);
			}

			Close();
		}

		private void frmDisplayOpt_Load(object eventSender, EventArgs eventArgs)
		{
			if (modSpectrum.bFullScreen)
			{
				optFullScreen.Checked = true;
			}
			else if (modMain.glDisplayWidth == 256)
			{
				if (modMain.glDisplayHeight == 192)
				{
					optNormal.Checked = true;
				}
				else if (modMain.glDisplayHeight == 384)
				{
					optDoubleHeight.Checked = true;
				}
			}
			else if (modMain.glDisplayWidth == 512)
			{
				if (modMain.glDisplayHeight == 192)
				{
					optDoubleWidth.Checked = true;
				}
				else if (modMain.glDisplayHeight == 384)
				{
					optDouble.Checked = true;
				}
			}
			else if (modMain.glDisplayWidth == 768)
			{
				if (modMain.glDisplayHeight == 576)
				{
					optTriple.Checked = true;
				}
			}
		}
	}
}