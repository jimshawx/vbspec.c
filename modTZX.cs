﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	static class modTZX
	{
		// /*******************************************************************************
		// modTAP.bas within vbSpec.vbp
		// 
		// Routines to handle loading of ".TZX" files (Spectrum tape images)
		// 
		// Authors: Mark Woodmass <mark.woodmass@ntlworld.com>
		// Paul Dunn <paul.dunn4@ntlworld.com>
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/

		public static int gbTZXInserted, gbTZXPlaying;
		public static int glEarBit;
		public static int TZXCurBlock;
		public static int TZXNumBlocks;
		private static int[] TZXArray;
		private static int[] TZXOffsets;
		private static int[] TZXBlockLength;
		private static int[] BitValues = new int[8];
		private static bool TZXBlockIsStandardTiming;
		private static int[] TZXCallList;
		private static int TZXNumCalls, TZXCallCounter, TZXCallByte;
		private static int TZXTotalTs;
		private static int TZXState, TZXAimTStates;
		private static int TZXPointer, TZXCurBlockID;
		private static int TZXSync1Len, TZXPulseLen, TZXSync2Len;
		private static int TZXOneLen, TZXZeroLen, TZXPauseLen;
		private static int TZXROMDataLen, TZXDataLen, TZXUsedBits;
		private static int TZXByte;
		private static int TZXDataPos, TZXPulsesDone;
		private static int TZXBitLimit, TZXPulseToneLen, TZXBitCounter;
		private static int TZXLoopCounter, TZXLoopPoint;

		// ////////////////////////////////////////////////////////////////////////////////
		// // GetTZXBlockInfo()
		// //
		// // Retreives information about a specific TZX block in the current file
		// //
		// // lBlockNum  IN      Number of block to retrieve information on (zero based)
		// // lType      OUT     The type of block (see the TZX specification document)
		// // sText      OUT     Human-readable text describing the block
		// // lLen       OUT     Length of the block in bytes
		public static void GetTZXBlockInfo(ref int lBlockNum, ref int lType, ref string sText, ref int lLen)
		{
			int lPtr, l;
			lPtr = TZXOffsets[lBlockNum];
			lType = TZXArray[lPtr];
			switch (lType)
			{
				case 0x10:
					{
						sText = "Standard Block";
						break;
					}

				case 0x11:
					{
						sText = "Turbo Block";
						break;
					}

				case 0x12:
					{
						sText = "Pure Tone";
						break;
					}

				case 0x13:
					{
						sText = "Pulse Sequence";
						break;
					}

				case 0x14:
					{
						sText = "Pure Data Block";
						break;
					}

				case 0x15:
					{
						sText = "Direct Recording";
						break;
					}

				case 0x16:
					{
						sText = "C64 Standard Block";
						break;
					}

				case 0x17:
					{
						sText = "C64 Turbo Block";
						break;
					}

				case 0x20:
					{
						// // Pause/StopTape
						l = TZXArray[lPtr + 1] + TZXArray[lPtr + 2] * 256;
						if (l == 0)
						{
							sText = "Stop Tape";
						}
						else
						{
							sText = "Pause Tape for " + l.ToString() + "ms";
						}

						break;
					}

				case 0x21:
					{
						sText = "Group Start";
						break;
					}

				case 0x22:
					{
						sText = "Group End";
						break;
					}

				case 0x23:
					{
						sText = "Jump to Block";
						break;
					}

				case 0x24:
					{
						sText = "Loop Start";
						break;
					}

				case 0x25:
					{
						sText = "Loop End";
						break;
					}

				case 0x2A:
					{
						sText = "Stop Tape if 48K";
						break;
					}

				case 0x30:
					{
						sText = "";
						l = TZXArray[lPtr + 1];
						var loopTo = lPtr + 1 + l;
						for (l = lPtr + 2; l <= loopTo; l++)
							sText = sText + (char)TZXArray[l];
						break;
					}

				case 0x31:
					{
						sText = "Message Block";
						break;
					}

				case 0x32:
					{
						sText = "Archive Info";
						break;
					}

				case 0x33:
					{
						sText = "Hardware Type";
						break;
					}

				case 0x34:
					{
						sText = "Emulation Info";
						break;
					}

				case 0x35:
					{
						sText = "Custom Info Block";
						break;
					}

				case 0x40:
					{
						sText = "Snapshot Block";
						break;
					}

				case 0x5A:
					{
						sText = "Block Merge Marker";
						break;
					}

				case 0xFE:
					{
						sText = "End of Tape";
						break;
					}
			}

			lLen = TZXBlockLength[lBlockNum];
		}

		public static void StartTape()
		{
			gbTZXPlaying = Conversions.ToInteger(true);
			TZXTotalTs = 0;
			glEarBit = 0;
		}

		public static void StopTape()
		{
			if (Conversions.ToBoolean(gbTZXPlaying))
				gbTZXPlaying = Conversions.ToInteger(false);
		}

		public static void StartStopTape()
		{
			if (Conversions.ToBoolean(gbTZXPlaying))
				StopTape();
			else
				StartTape();
		}

		public static void OpenTZXFile(ref string sName)
		{
			int ReadLength;
			string s;
			int b, lCounter;
			int BlockID, F, BlockLen, ArrayLength;
			var BlockList = new int[2049];
			int BlockListNum;
			var BlockLengths = new int[2049];
			int BlockLengthsNum;
			int hTZXFile;
			b = 1;
			for (F = 0; F <= 7; F++)
			{
				BitValues[F] = b;
				b = b * 2;
			}

			// // If we currently have a TAP file open, then close it
			modTAP.CloseTAPFile();

			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (string.IsNullOrEmpty(FileSystem.Dir(sName)))
				return;
			hTZXFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hTZXFile, sName, OpenMode.Binary);
			ReadLength = (int)FileSystem.LOF(hTZXFile);
			if (ReadLength == 0)
			{
				FileSystem.FileClose(hTZXFile);
				return;
			}

			My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName + " - " + modMain.GetFilePart(sName);

			// Read the TZX file into TZXArray
			TZXArray = new int[ReadLength + 1 + 1];
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 6273


			Input:

					On Error Resume Next

			 */
			s = modMain.InputString16(hTZXFile, ReadLength);
			var loopTo = Strings.Len(s);
			for (lCounter = 1; lCounter <= loopTo; lCounter++)
				TZXArray[lCounter - 1] = modMain.Asc(Strings.Mid(s, lCounter, 1));
			TZXArray[ReadLength] = 0xFE; // end-of-tape block
			FileSystem.FileClose(hTZXFile);

			// Now decode the TZX file into an individual blocks list
			gbTZXPlaying = Conversions.ToInteger(false);
			gbTZXInserted = Conversions.ToInteger(false);
			s = "";
			ArrayLength = ReadLength + 1;
			for (F = 0; F <= 6; F++)
				s = s + (char)TZXArray[F];
			if (s != "ZXTape!")
			{
				FileSystem.FileClose(hTZXFile);
			}

			BlockListNum = 0;
			BlockLengthsNum = 0;
			gbTZXInserted = Conversions.ToInteger(true);
			F = 10;
			do
			{
				BlockID = TZXArray[F];
				BlockList[BlockListNum] = F;
				BlockListNum = BlockListNum + 1;
				F = F + 1;
				switch (BlockID)
				{
					case 0x10:
						{
							BlockLen = 256 * TZXArray[F + 3] + TZXArray[F + 2] + 4;
							break;
						}

					case 0x11:
						{
							BlockLen = TZXArray[F + 15] + TZXArray[F + 16] * 256 + TZXArray[F + 17] * 65536 + 18;
							break;
						}

					case 0x12:
						{
							BlockLen = 4;
							break;
						}

					case 0x13:
						{
							BlockLen = 1 + TZXArray[F] * 2;
							break;
						}

					case 0x14:
						{
							BlockLen = TZXArray[F + 7] + TZXArray[F + 8] * 256 + TZXArray[F + 9] * 65536 + 10;
							break;
						}

					case 0x15:
						{
							BlockLen = TZXArray[F + 5] + TZXArray[F + 6] * 256 + TZXArray[F + 7] * 65536 + 8;
							break;
						}

					case 0x20:
						{
							BlockLen = 2;
							break;
						}

					case 0x21:
						{
							BlockLen = TZXArray[F] + 1;
							break;
						}

					case 0x22:
						{
							BlockLen = 0;
							break;
						}

					case 0x23:
						{
							BlockLen = 2;
							break;
						}

					case 0x24:
						{
							BlockLen = 2;
							break;
						}

					case 0x25:
						{
							BlockLen = 0;
							break;
						}

					case 0x26:
						{
							BlockLen = TZXArray[F] + TZXArray[F + 1] * 256 * 2 + 2;
							break;
						}

					case 0x27:
						{
							BlockLen = 0;
							break;
						}

					case 0x28:
						{
							BlockLen = TZXArray[F] + TZXArray[F + 1] * 256 + 2;
							break;
						}

					case 0x2A:
						{
							BlockLen = 4;
							break;
						}

					case 0x30:
						{
							BlockLen = TZXArray[F] + 1;
							break;
						}

					case 0x31:
						{
							BlockLen = TZXArray[F + 1] + 2;
							break;
						}

					case 0x32:
						{
							BlockLen = TZXArray[F] + TZXArray[F + 1] * 256 + 2;
							break;
						}

					case 0x33:
						{
							BlockLen = TZXArray[F] * 3 + 1;
							break;
						}

					case 0x34:
						{
							BlockLen = 8;
							break;
						}

					case 0x35:
						{
							BlockLen = TZXArray[F + 16] + TZXArray[F + 17] * 256 + TZXArray[F + 18] * 65536 + TZXArray[F + 19] * 16777216 + 20;
							break;
						}

					case 0x40:
						{
							BlockLen = TZXArray[F + 1] + TZXArray[F + 2] * 256 + TZXArray[F + 3] * 65536 + 4;
							break;
						}

					case 0x5A:
						{
							BlockLen = 9;
							break;
						}

					case 0xFE:
						{
							BlockLen = 0;
							break;
						}

					case 0xFF:
						{
							BlockLen = 0;
							break;
						}

					default:
						{
							BlockLen = TZXArray[F] + TZXArray[F + 1] * 256 + TZXArray[F + 2] * 65536 + TZXArray[F + 3] * 16777216 + 4;
							break;
						}
				}

				F = F + BlockLen;
				BlockLengths[BlockLengthsNum] = BlockLen + 1;
				BlockLengthsNum = BlockLengthsNum + 1;
			}
			while (F < ArrayLength);
			TZXNumBlocks = BlockListNum;
			TZXOffsets = new int[TZXNumBlocks + 1];
			TZXBlockLength = new int[TZXNumBlocks + 1];
			var loopTo1 = TZXNumBlocks - 1;
			for (F = 0; F <= loopTo1; F++)
			{
				TZXOffsets[F] = BlockList[F];
				TZXBlockLength[F] = BlockLengths[F];
			}

			int argBlockNum = 0;
			SetCurTZXBlock(ref argBlockNum);
			My.MyProject.Forms.frmTapePlayer.UpdateTapeList();
		}

		public static void SetCurTZXBlock(ref int BlockNum)
		{
			int F;
			TZXBlockIsStandardTiming = false;
			TZXState = 5;
			TZXAimTStates = 0;
			TZXPointer = TZXOffsets[BlockNum];
			TZXCurBlockID = TZXArray[TZXPointer];
			switch (TZXCurBlockID)
			{
				case 0x10: // Standard ROM Loader block
					{
						TZXPulseLen = 2168;
						if (TZXArray[TZXPointer + 5] == 0xFF)
							TZXPulseToneLen = 3220;
						else
							TZXPulseToneLen = 8064;
						TZXSync1Len = 667;
						TZXSync2Len = 735;
						TZXZeroLen = 855;
						TZXOneLen = 1710;
						TZXPauseLen = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						TZXDataLen = TZXBlockLength[BlockNum] + TZXOffsets[BlockNum];
						TZXROMDataLen = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256;
						TZXUsedBits = 8;
						TZXState = 0; // State 0 - playing Pulse
						TZXAimTStates = TZXPulseLen;
						TZXByte = 0;
						TZXCurBlock = BlockNum;
						TZXDataPos = TZXPointer + 5;
						TZXPulsesDone = 2;
						TZXBlockIsStandardTiming = true;
						break;
					}

				case 0x11: // Non-Standard TAP block
					{
						TZXPulseLen = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						TZXPulseToneLen = TZXArray[TZXPointer + 11] + TZXArray[TZXPointer + 12] * 256;
						TZXSync1Len = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256;
						TZXSync2Len = TZXArray[TZXPointer + 5] + TZXArray[TZXPointer + 6] * 256;
						TZXZeroLen = TZXArray[TZXPointer + 7] + TZXArray[TZXPointer + 8] * 256;
						TZXOneLen = TZXArray[TZXPointer + 9] + TZXArray[TZXPointer + 10] * 256;
						TZXUsedBits = TZXArray[TZXPointer + 13];
						TZXPauseLen = TZXArray[TZXPointer + 14] + TZXArray[TZXPointer + 15] * 256;
						TZXState = 0; // State 0 - playing Pulse.
						TZXAimTStates = TZXPulseLen;
						TZXByte = 0;
						TZXCurBlock = BlockNum;
						TZXDataPos = TZXPointer + 19;
						TZXDataLen = TZXBlockLength[BlockNum] + TZXOffsets[BlockNum];
						TZXROMDataLen = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256;
						if (TZXPulseLen == 2168 & (TZXPulseToneLen == 3220 | TZXPulseToneLen == 8064) & TZXSync1Len == 667 & TZXSync2Len == 735 & TZXZeroLen == 855 & TZXOneLen == 1710)
							TZXBlockIsStandardTiming = true;
						else
							TZXBlockIsStandardTiming = false;
						break;
					}

				case 0x12: // Pure Tone
					{
						TZXState = 0; // playing a possible pilot tone
						TZXPulseLen = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						TZXPulseToneLen = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256;
						TZXAimTStates = TZXPulseLen;
						TZXByte = 1;
						TZXCurBlock = BlockNum;
						break;
					}

				case 0x13: // Row of Pulses
					{
						TZXState = 0; // playing a possible pilot tone
						TZXPulseToneLen = TZXArray[TZXPointer + 1]; // // NUMBER OF PULSES
						TZXPulseLen = TZXArray[TZXPointer + 2] + TZXArray[TZXPointer + 3] * 256;
						TZXPulsesDone = 1;
						TZXByte = TZXPointer + 4;
						TZXAimTStates = TZXPulseLen;
						TZXCurBlock = BlockNum;
						break;
					}

				case 0x14: // Pure Data block
					{
						TZXZeroLen = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						TZXOneLen = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256;
						TZXUsedBits = TZXArray[TZXPointer + 5];
						TZXPauseLen = TZXArray[TZXPointer + 6] + TZXArray[TZXPointer + 7] * 256;
						TZXDataLen = TZXBlockLength[BlockNum] + TZXOffsets[BlockNum];
						TZXState = 3; // Set to DATA Byte(s) output.
									  // // CC IN
						TZXDataPos = TZXPointer + 11;
						// // CC OUT
						TZXByte = TZXPointer + 11;
						if ((TZXArray[TZXByte] & 128) > 0)
							TZXAimTStates = TZXOneLen;
						else
							TZXAimTStates = TZXZeroLen;
						TZXPulsesDone = 2;
						if (TZXByte == TZXDataLen - 1)
							TZXBitLimit = BitValues[8 - TZXUsedBits];
						else
							TZXBitLimit = 1;
						TZXBitCounter = 128;
						TZXCurBlock = BlockNum;
						break;
					}

				case 0x15: // Direct Recording Block
					{
						TZXOneLen = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256; // Length of Sample (Ts)
						TZXPauseLen = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256; // (ms)
						TZXUsedBits = TZXArray[TZXPointer + 5]; // Samples used in last byte
						TZXDataLen = TZXArray[TZXPointer + 6] + TZXArray[TZXPointer + 7] * 256 + TZXArray[TZXPointer + 8] * 65536; // TZXBlockLength(BlockNum) + TZXOffsets(BlockNum)
						TZXByte = TZXPointer + 9;
						TZXState = 3; // Set to DATA bytes output
						TZXAimTStates = TZXOneLen;
						if (TZXByte == TZXDataLen - 1)
							TZXBitLimit = BitValues[8 - TZXUsedBits];
						else
							TZXBitLimit = 1;
						TZXBitCounter = 128;
						glEarBit = 64 * (TZXArray[TZXByte] / 128);
						TZXCurBlock = BlockNum;
						break;
					}

				case 0x20: // Pause or STOP tape.
					{
						TZXCurBlock = BlockNum;
						TZXPauseLen = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						if (TZXPauseLen == 0)
						{
							if (Conversions.ToBoolean(gbTZXPlaying))
								StartStopTape();
						}
						else
						{
							TZXAimTStates = TZXPauseLen * 3500;
							TZXState = 4;
						} // When the TZXTStates gets past TZXAimStates, the next block will be used

						break;
					}

				case 0x23: // Jump to block
					{
						TZXByte = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						if (TZXByte < 32768)
						{
							int argBlockNum = BlockNum + TZXByte;
							SetCurTZXBlock(ref argBlockNum);
						}
						else
						{
							int argBlockNum1 = BlockNum - (65536 - TZXByte);
							SetCurTZXBlock(ref argBlockNum1);
						}

						break;
					}

				case 0x24: // Loop Start
					{
						TZXLoopCounter = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256;
						TZXLoopPoint = BlockNum + 1;
						int argBlockNum2 = BlockNum + 1;
						SetCurTZXBlock(ref argBlockNum2);
						break;
					}

				case 0x25: // Loop End
					{
						TZXLoopCounter = TZXLoopCounter - 1;
						if (TZXLoopCounter > 0)
							SetCurTZXBlock(ref TZXLoopPoint);
						else
						{
							int argBlockNum3 = BlockNum + 1;
							SetCurTZXBlock(ref argBlockNum3);
						}

						break;
					}

				case 0x26: // Call Sequence
					{
						TZXNumCalls = TZXArray[TZXPointer + 1] + TZXArray[TZXPointer + 2] * 256 - 1;
						TZXCallByte = TZXNumCalls;
						TZXCallCounter = 0;
						TZXCallList = new int[TZXNumCalls + 1];
						var loopTo = TZXNumCalls - 1;
						for (F = 0; F <= loopTo; F++)
							TZXCallList[F] = TZXArray[TZXPointer + 4 + F * 2] + TZXArray[TZXPointer + 5 + F * 2 + 1] * 256;
						TZXCallByte = BlockNum;
						TZXByte = TZXArray[TZXPointer + 3] + TZXArray[TZXPointer + 4] * 256;
						if (TZXByte < 32768)
						{
							int argBlockNum4 = BlockNum + TZXByte;
							SetCurTZXBlock(ref argBlockNum4);
						}
						else
						{
							int argBlockNum5 = BlockNum - (65536 - TZXByte);
							SetCurTZXBlock(ref argBlockNum5);
						}

						break;
					}

				case 0x27: // CALL Return
					{
						if (TZXCallCounter < TZXNumCalls)
						{
							TZXCallCounter = TZXCallCounter + 1;
							TZXByte = TZXCallList[TZXCallCounter];
							if (TZXByte < 32768)
							{
								int argBlockNum6 = TZXCallByte + TZXByte;
								SetCurTZXBlock(ref argBlockNum6);
							}
							else
							{
								int argBlockNum7 = TZXCallByte - (65536 - TZXByte);
								SetCurTZXBlock(ref argBlockNum7);
							}
						}

						break;
					}

				case 0x2A: // Stop tape in 48k Mode
					{
						if (modMain.glEmulatedModel == 0) // 48k Speccy?
						{
							if (Conversions.ToBoolean(gbTZXPlaying))
								StartStopTape();
						}

						TZXCurBlock = BlockNum;
						break;
					}

				case 0xFE: // End of Tape
					{
						TZXAimTStates = 30;
						TZXCurBlock = BlockNum;
						if (Conversions.ToBoolean(gbTZXPlaying))
						{
							int argBlockNum8 = 0;
							SetCurTZXBlock(ref argBlockNum8);
						}

						StopTape();
						break;
					}

				default:
					{
						TZXCurBlock = BlockNum;
						break;
					}
			}

			// If TZXCurBlock > TZXLastDataBlock Then TZXState = 5
			if (My.MyProject.Forms.frmTapePlayer.Visible)
				My.MyProject.Forms.frmTapePlayer.UpdateCurBlock();
		}

		public static void UpdateTZXState(int TapeTStates)
		{
			int LastEarBit, F;
			TZXTotalTs = TZXTotalTs + TapeTStates;
			while ((Conversions.ToInteger(TZXTotalTs >= TZXAimTStates) & gbTZXPlaying) != 0)
			{
				TZXTotalTs = TZXTotalTs - TZXAimTStates;
				switch (TZXCurBlockID)
				{
					case 0x10:
					case 0x11:
					case 0x14:
						{
							switch (TZXState)
							{
								case 0: // Playing Pilot tone.
									{
										glEarBit = glEarBit ^ 64;
										if (TZXByte < TZXPulseToneLen) // TZXByte holds number of pulses
										{
											TZXAimTStates = TZXPulseLen;
											TZXByte = TZXByte + 1;
										}
										else
										{
											TZXByte = 0;
											TZXState = 1; // Set to SYNC1 Pulse output
											TZXAimTStates = TZXSync1Len;
										}

										break;
									}

								case 1: // SYNC 1
									{
										glEarBit = glEarBit ^ 64;
										TZXState = 2; // Set to SYNC2 Pulse output
										TZXAimTStates = TZXSync2Len;
										break;
									}

								case 2: // SYNC 2
									{
										glEarBit = glEarBit ^ 64;
										TZXState = 3; // Set to DATA Byte(s) output
										TZXByte = TZXDataPos;
										if ((TZXArray[TZXByte] & 128) > 0) // Set next pulse length
										{
											TZXAimTStates = TZXOneLen;
										}
										else
										{
											TZXAimTStates = TZXZeroLen;
										}

										TZXPulsesDone = 2; // *2* edges per Data BIT, one on, one off
										TZXBitCounter = 128; // Start with the full byte
										TZXBitLimit = 1;
										break;
									}

								case 3: // DATA Bytes out
									{
										glEarBit = glEarBit ^ 64;
										TZXPulsesDone = TZXPulsesDone - 1;
										if (TZXPulsesDone == 0) // Done both pulses for this bit?
										{
											if (TZXBitCounter > TZXBitLimit) // Done all the bits for this byte?
											{
												TZXBitCounter = TZXBitCounter / 2; // Bitcounter counts *down*
												TZXPulsesDone = 2;
												if ((TZXArray[TZXByte] & TZXBitCounter) > 0)
												{
													TZXAimTStates = TZXOneLen;
												}
												else
												{
													TZXAimTStates = TZXZeroLen;
												}
											}
											else // all bits done, setup for next byte
											{
												TZXByte = TZXByte + 1;
												if (TZXByte < TZXDataLen) // last byte?
												{
													if (TZXByte == TZXDataLen - 1)
													{
														TZXBitLimit = BitValues[8 - TZXUsedBits]; // if so, set up the last bits used
													}
													else
													{
														TZXBitLimit = 1;
													} // else use full 8 bits

													TZXBitCounter = 128;
													TZXPulsesDone = 2;
													if ((TZXArray[TZXByte] & 128) > 0)
													{
														TZXAimTStates = TZXOneLen;
													}
													else
													{
														TZXAimTStates = TZXZeroLen;
													}
												}
												else if (TZXPauseLen > 0)
												{
													TZXAimTStates = TZXPauseLen * 3500;
													TZXState = 4; // Set to Pause output
												}
												else
												{
													TZXState = 0;
													int argBlockNum = TZXCurBlock + 1;
													SetCurTZXBlock(ref argBlockNum);
												}
											}
										}
										else if ((TZXArray[TZXByte] & TZXBitCounter) > 0) // Not done both pulses, flip the ear bit next time
										{
											TZXAimTStates = TZXOneLen;
										}
										else
										{
											TZXAimTStates = TZXZeroLen;
										}

										break;
									}

								case 4: // End Pause output
									{
										int argBlockNum1 = TZXCurBlock + 1;
										SetCurTZXBlock(ref argBlockNum1);
										break;
									}
							}

							break;
						}

					case 0x12:
						{
							glEarBit = glEarBit ^ 64;
							if (TZXByte < TZXPulseToneLen)
							{
								TZXAimTStates = TZXPulseLen;
								TZXByte = TZXByte + 1;
							}
							else
							{
								int argBlockNum2 = TZXCurBlock + 1;
								SetCurTZXBlock(ref argBlockNum2);
							}

							break;
						}

					case 0x13:
						{
							glEarBit = glEarBit ^ 64;
							if (TZXPulsesDone < TZXPulseToneLen)
							{
								TZXPulseLen = TZXArray[TZXByte] + TZXArray[TZXByte + 1] * 256;
								TZXAimTStates = TZXPulseLen;
								TZXByte = TZXByte + 2;
								TZXPulsesDone = TZXPulsesDone + 1;
							}
							else
							{
								int argBlockNum3 = TZXCurBlock + 1;
								SetCurTZXBlock(ref argBlockNum3);
							}

							break;
						}

					case 0x15: // *UnTested* - any TZX actually use a DRB block?
						{
							LastEarBit = glEarBit;
							if (TZXBitCounter > TZXBitLimit) // Done all the bits for this byte?
							{
								TZXBitCounter = TZXBitCounter / 2; // Bitcounter counts *down*
								if ((TZXArray[TZXByte] & TZXBitCounter) > 0) // Set the ear bit
								{
									glEarBit = 64;
								}
								else
								{
									glEarBit = 0;
								}

								TZXAimTStates = TZXOneLen;
							}
							else // all bits done, setup for next byte
							{
								TZXByte = TZXByte + 1;
								if (TZXByte < TZXDataLen) // last byte?
								{
									if (TZXByte == TZXDataLen - 1)
									{
										TZXBitLimit = BitValues[8 - TZXUsedBits]; // if so, set up the last bits used
									}
									else
									{
										TZXBitLimit = 1;
									} // else use full 8 bits

									TZXBitCounter = 128;
									if ((TZXArray[TZXByte] & TZXBitCounter) > 0) // Set the ear bit
									{
										glEarBit = 64;
									}
									else
									{
										glEarBit = 0;
									}

									TZXAimTStates = TZXOneLen;
								}
								else if (TZXPauseLen > 0)
								{
									TZXAimTStates = TZXPauseLen * 3500;
									TZXState = 4; // Set to Pause output
								}
								else
								{
									TZXState = 0;
									int argBlockNum4 = TZXCurBlock + 1;
									SetCurTZXBlock(ref argBlockNum4);
								}
							}

							break;
						}

					case 0xFE:
						{
							StopTape();
							break;
						}

					default:
						{
							int argBlockNum5 = TZXCurBlock + 1;
							SetCurTZXBlock(ref argBlockNum5);
							break;
						}
				}
			}
		}
	}
}