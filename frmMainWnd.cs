﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using VB = Microsoft.VisualBasic;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmMainWnd : Form
	{
		// /*******************************************************************************
		// frmMainWnd.frm within vbSpec.vbp
		// 
		// Main application window. Contains the Spectrum display output, processes
		// Keypresses, and contains a timer object used for maintaining the emulator
		// at 50 frames/sec on a fast enough machine, and a common dialog control for
		// use by file open/save operations.
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2001  Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		// MM 16.04.2003
		private int lPopUp;
		private string sNewCaption;

		// That is a new caption property. If the Speccy is in full screen modus, the normal
		// caption of the form should not be set. The Speccy saves the data an restores it,
		// if the full screen modus is off
		public string NewCaption
		{
			set
			{
				if (modSpectrum.bFullScreen)
				{
					sNewCaption = value;
				}
				else
				{
					Text = value;
				}
			}
		}

		// This method sets the caption to an empty string and saves the original caption
		public void FullScreenOn()
		{
			sNewCaption = Text;
			Text = Constants.vbNullString;
			mnuFileMain.Visible = false;
			mnuOptionsMain.Visible = false;
			mnuHelpMain.Visible = false;
			picStatus.Visible = false;
			// Me.Line1(1).Visible = False
		}

		// This method restores the original caption
		public void FullScreenOff()
		{
			modSpectrum.bFullScreen = false;
			NewCaption = sNewCaption;
			mnuFileMain.Visible = true;
			mnuOptionsMain.Visible = true;
			mnuHelpMain.Visible = true;
			picStatus.Visible = true;
			// Me.Line1(1).Visible = True
			SetBounds(0, 0, 0, 0, BoundsSpecified.X | BoundsSpecified.Y);
		}

		public void FileCreateNewTap()
		{
			string sName;
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 3008


			Input:

					On Error Resume Next

			 */
			Information.Err().Clear();
			dlgCommonOpen.Title = "Create New Tap File";
			dlgCommonSave.Title = "Create New Tap File";
			dlgCommonOpen.DefaultExt = ".tap";
			dlgCommonSave.DefaultExt = ".tap";
			dlgCommonOpen.FileName = "*.tap";
			dlgCommonSave.FileName = "*.tap";
			// UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			dlgCommonOpen.Filter = "Tape files (*.tap)|*.tap|All Files|*.*";
			dlgCommonSave.Filter = "Tape files (*.tap)|*.tap|All Files|*.*";
			// UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			dlgCommonOpen.ShowReadOnly = false;
			// UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
			// UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			dlgCommonSave.OverwritePrompt = true;
			dlgCommonOpen.CheckPathExists = true;
			dlgCommonSave.CheckPathExists = true;
			// UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			// UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// dlgCommon.CancelError = True
			dlgCommonSave.ShowDialog();
			dlgCommonOpen.FileName = dlgCommonSave.FileName;
			// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			if (Information.Err().Number == (int)DialogResult.Cancel)
			{
				return;
			}

			sName = dlgCommonOpen.FileName;
			if (!string.IsNullOrEmpty(sName))
			{
				if (modMain.ghTAPFile > 0)
					FileSystem.FileClose(modMain.ghTAPFile);
				modTZX.StopTape(); // // Stop the TZX tape player
				FileSystem.Kill(sName);
				modMain.ghTAPFile = FileSystem.FreeFile();
				FileSystem.FileOpen(modMain.ghTAPFile, sName, OpenMode.Binary);
				modMain.gsTAPFileName = sName;
				NewCaption = My.MyProject.Application.Info.ProductName + " - " + modMain.GetFilePart(sName);
				modMain.gMRU.AddMRUFile(ref sName);
				SetMRUMenu();
			}
		}

		public void FileOpenDialog([Optional, DefaultParameterValue("")] ref string sName)
		{
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 7842


			Input:
					On Error Resume Next

			 */
			if (string.IsNullOrEmpty(sName))
			{
				Information.Err().Clear();
				dlgCommonOpen.Title = "Open Snapshot or ROM image";
				dlgCommonSave.Title = "Open Snapshot or ROM image";
				dlgCommonOpen.DefaultExt = ".sna";
				dlgCommonSave.DefaultExt = ".sna";
				dlgCommonOpen.FileName = "";
				dlgCommonSave.FileName = "";
				// UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
				dlgCommonOpen.Filter = "All Spectrum Files (*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr)|*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr|SNA snapshots (*.sna)|*.sna|Z80 snapshots (*.z80)|*.z80|ROM images (*.rom)|*.rom|Tape files (*.tap;*.tzx)|*.tap;*.tzx|Screen images (*.scr)|*.scr|All Files|*.*";
				dlgCommonSave.Filter = "All Spectrum Files (*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr)|*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr|SNA snapshots (*.sna)|*.sna|Z80 snapshots (*.z80)|*.z80|ROM images (*.rom)|*.rom|Tape files (*.tap;*.tzx)|*.tap;*.tzx|Screen images (*.scr)|*.scr|All Files|*.*";
				// UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.CheckFileExists which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
				dlgCommonOpen.CheckFileExists = true;
				dlgCommonOpen.CheckPathExists = true;
				dlgCommonSave.CheckPathExists = true;
				// UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
				// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
				// UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
				// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
				// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
				// dlgCommon.CancelError = True
				dlgCommonOpen.ShowDialog();
				dlgCommonSave.FileName = dlgCommonOpen.FileName;
				// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
				if (Information.Err().Number == (int)DialogResult.Cancel)
				{
					return;
				}

				sName = dlgCommonOpen.FileName;
			}

			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (!string.IsNullOrEmpty(sName) & !string.IsNullOrEmpty(FileSystem.Dir(sName)))
			{
				modMain.gMRU.AddMRUFile(ref sName);
				switch (Strings.LCase(Strings.Right(sName, 4)) ?? "")
				{
					case ".z80":
						{
							modMain.LoadZ80Snap(ref sName);
							break;
						}

					case ".rom":
						{
							modZ80.Z80Reset();
							if (modMain.glEmulatedModel != 0 & modMain.glEmulatedModel != 5)
							{
								int arglModel = 0;
								int argbSEBasicROM = Conversions.ToInteger(false);
								modMain.SetEmulatedModel(ref arglModel, ref argbSEBasicROM);
							}

							int arglROMPage = 8;
							modMain.LoadROM(ref sName, lROMPage: ref arglROMPage);
							break;
						}

					case ".sna":
						{
							modMain.LoadSNASnap(ref sName);
							break;
						}

					case ".tap":
						{
							modTAP.OpenTAPFile(ref sName);
							break;
						}

					case ".tzx":
						{
							modTZX.OpenTZXFile(ref sName);
							break;
						}

					case ".scr":
						{
							modMain.LoadScreenSCR(ref sName);
							break;
						}

					default:
						{
							// try opening it as a SNA file
							modMain.LoadSNASnap(ref sName);
							break;
						}
				}

				SetMRUMenu();
			}
		}

		private void FileSaveAsDialog()
		{
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 12199


			Input:
					On Error Resume Next

			 */
			Information.Err().Clear();
			dlgCommonOpen.Title = "Save As";
			dlgCommonSave.Title = "Save As";
			dlgCommonOpen.DefaultExt = ".z80";
			dlgCommonSave.DefaultExt = ".z80";
			dlgCommonOpen.FileName = "";
			dlgCommonSave.FileName = "";
			// UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			dlgCommonOpen.Filter = "Z80 snapshot (*.z80)|*.z80|SNA snapshot (*.sna)|*.sna|ROM image (*.rom)|*.rom|Screen Bitmap (*.bmp)|*.bmp|Screen Image (*.scr)|*.scr";
			dlgCommonSave.Filter = "Z80 snapshot (*.z80)|*.z80|SNA snapshot (*.sna)|*.sna|ROM image (*.rom)|*.rom|Screen Bitmap (*.bmp)|*.bmp|Screen Image (*.scr)|*.scr";
			// UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			dlgCommonOpen.CheckPathExists = true;
			dlgCommonSave.CheckPathExists = true;
			// UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			dlgCommonSave.OverwritePrompt = true;
			// UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			// UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			// UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// dlgCommonOpen.ShowReadOnly = False
			// UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
			// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// dlgCommon.CancelError = True
			dlgCommonSave.ShowDialog();
			dlgCommonOpen.FileName = dlgCommonSave.FileName;
			// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			if (Information.Err().Number == (int)DialogResult.Cancel)
			{
				return;
			}

			if (!string.IsNullOrEmpty(dlgCommonOpen.FileName))
			{
				string argsFile = dlgCommonOpen.FileName;
				modMain.gMRU.AddMRUFile(ref argsFile);
				dlgCommonOpen.FileName = argsFile;
				switch (Strings.LCase(Strings.Right(dlgCommonOpen.FileName, 4)) ?? "")
				{
					case ".z80":
						{
							string argsFileName = dlgCommonOpen.FileName;
							modMain.SaveZ80Snap(ref argsFileName);
							dlgCommonOpen.FileName = argsFileName;
							break;
						}

					case ".rom":
						{
							string argsFileName1 = dlgCommonOpen.FileName;
							modMain.SaveROM(ref argsFileName1);
							dlgCommonOpen.FileName = argsFileName1;
							break;
						}

					case ".sna":
						{
							string argsFileName2 = dlgCommonOpen.FileName;
							modMain.SaveSNASnap(ref argsFileName2);
							dlgCommonOpen.FileName = argsFileName2;
							break;
						}

					case ".bmp":
						{
							string argsFileName3 = dlgCommonOpen.FileName;
							modMain.SaveScreenBMP(ref argsFileName3);
							dlgCommonOpen.FileName = argsFileName3;
							break;
						}

					case ".scr":
						{
							string argsFileName4 = dlgCommonOpen.FileName;
							modMain.SaveScreenSCR(ref argsFileName4);
							dlgCommonOpen.FileName = argsFileName4;
							break;
						}

					default:
						{
							// save it as a Z80 file
							string argsFileName5 = dlgCommonOpen.FileName;
							modMain.SaveZ80Snap(ref argsFileName5);
							dlgCommonOpen.FileName = argsFileName5;
							break;
						}
				}

				SetMRUMenu();
			}
		}

		private object SetMRUMenu()
		{
			int l;
			for (l = 10; l <= 15; l++)
				mnuFile[(short)l].Visible = false;
			var loopTo = modMain.gMRU.GetMRUCount();
			for (l = 1; l <= loopTo; l++)
			{
				if (Strings.Len(modMain.gMRU.GetMRUFile(ref l)) <= 40)
				{
					mnuFile[(short)(10 + l)].Text = "&" + l.ToString() + " " + modMain.gMRU.GetMRUFile(ref l);
				}
				else
				{
					mnuFile[(short)(10 + l)].Text = "&" + l.ToString() + " " + modMain.GetFilePart(modMain.gMRU.GetMRUFile(ref l));
				}

				mnuFile[(short)(10 + l)].Visible = true;
			}

			if (modMain.gMRU.GetMRUCount() > 0)
				mnuFile[10].Visible = true;
			return default;
		}

		private void frmMainWnd_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (Conversions.ToBoolean(Shift & 4))
				return;
			bool argdown = true;
			modSpectrum.doKey(ref argdown, ref KeyCode, ref Shift);
			// MM 16.04.2003
			if (KeyCode == 27 & Shift == 1 & modSpectrum.bFullScreen)
			{
				lPopUp = -1;
				// UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// Me.PopupMenu(mnuFullScreenMenu)
				// If lPopUp = 0 Then
				// 'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// Me.PopupMenu(mnuFileMain)
				// End If
				// If lPopUp = 1 Then
				// 'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// Me.PopupMenu(mnuOptionsMain)
				// End If
				// If lPopUp = 2 Then
				// 'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// Me.PopupMenu(mnuHelpMain)
				// End If
			}
		}

		// MM 16.04.2003
		public void mnuFullScreenFile_Click(object eventSender, EventArgs eventArgs)
		{
			lPopUp = 0;
		}

		public void mnuFullScreenOptions_Click(object eventSender, EventArgs eventArgs)
		{
			lPopUp = 1;
		}

		public void mnuFullScreenHelp_Click(object eventSender, EventArgs eventArgs)
		{
			lPopUp = 2;
		}

		public void mnuFullNormalView_Click(object eventSender, EventArgs eventArgs)
		{
			FullScreenOff();
			int arglWidth = 256;
			int arglHeight = 192;
			modMain.SetDisplaySize(ref arglWidth, ref arglHeight);
			Resize_Renamed();
		}

		private void frmMainWnd_KeyUp(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			bool argdown = false;
			modSpectrum.doKey(ref argdown, ref KeyCode, ref Shift);
		}

		private void frmMainWnd_Load(object eventSender, EventArgs eventArgs)
		{
			int X, y;
			int lCounter;
			// MM JD
			var sKey = default(string);
			modMain.gMRU = new MRUList();
			SetMRUMenu();
			X = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "MainWndX", "-1"));
			y = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "MainWndY", "-1"));
			if (X >= 0 & X <= Screen.PrimaryScreen.Bounds.Width - 16)
			{
				Left = X;
			}

			if (y >= 0 & y <= Screen.PrimaryScreen.Bounds.Height - 16)
			{
				Top = y;
			}

			// MM JD
			// Load standard joystick tables
			// Joystick 1
			// Kepmston
			modSpectrum.PortValueToKeyStroke(modSpectrum.KEMPSTON_PUP, modSpectrum.KEMPSTON_UP, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_UP].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_UP].lPort = modSpectrum.KEMPSTON_PUP;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_UP].lValue = modSpectrum.KEMPSTON_UP;
			modSpectrum.PortValueToKeyStroke(modSpectrum.KEMPSTON_PDOWN, modSpectrum.KEMPSTON_DOWN, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_DOWN].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_DOWN].lPort = modSpectrum.KEMPSTON_PDOWN;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_DOWN].lValue = modSpectrum.KEMPSTON_DOWN;
			modSpectrum.PortValueToKeyStroke(modSpectrum.KEMPSTON_PLEFT, modSpectrum.KEMPSTON_LEFT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_LEFT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_LEFT].lPort = modSpectrum.KEMPSTON_PLEFT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_LEFT].lValue = modSpectrum.KEMPSTON_LEFT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.KEMPSTON_PRIGHT, modSpectrum.KEMPSTON_RIGHT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_RIGHT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_RIGHT].lPort = modSpectrum.KEMPSTON_PRIGHT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_RIGHT].lValue = modSpectrum.KEMPSTON_RIGHT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.KEMPSTON_PFIRE, modSpectrum.KEMPSTON_FIRE, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_BUTTON1].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_BUTTON1].lPort = modSpectrum.KEMPSTON_PFIRE;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_BUTTON1].lValue = modSpectrum.KEMPSTON_FIRE;
			for (lCounter = 2; lCounter <= modSpectrum.JDT_MAXBUTTONS; lCounter++)
			{
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_BUTTON_BASE + lCounter].sKey = Constants.vbNullString;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort = 0;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjKempston, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue = 0;
			}
			// Cursor
			modSpectrum.PortValueToKeyStroke(modSpectrum.CURSOR_PUP, modSpectrum.CURSOR_UP, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_UP].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_UP].lPort = modSpectrum.CURSOR_PUP;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_UP].lValue = modSpectrum.CURSOR_UP;
			modSpectrum.PortValueToKeyStroke(modSpectrum.CURSOR_PDOWN, modSpectrum.CURSOR_DOWN, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_DOWN].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_DOWN].lPort = modSpectrum.CURSOR_PDOWN;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_DOWN].lValue = modSpectrum.CURSOR_DOWN;
			modSpectrum.PortValueToKeyStroke(modSpectrum.CURSOR_PLEFT, modSpectrum.CURSOR_LEFT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_LEFT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_LEFT].lPort = modSpectrum.CURSOR_PLEFT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_LEFT].lValue = modSpectrum.CURSOR_LEFT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.CURSOR_PRIGHT, modSpectrum.CURSOR_RIGHT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_RIGHT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_RIGHT].lPort = modSpectrum.CURSOR_PRIGHT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_RIGHT].lValue = modSpectrum.CURSOR_RIGHT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.CURSOR_PFIRE, modSpectrum.CURSOR_FIRE, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_BUTTON1].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_BUTTON1].lPort = modSpectrum.CURSOR_PFIRE;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_BUTTON1].lValue = modSpectrum.CURSOR_FIRE;
			for (lCounter = 2; lCounter <= modSpectrum.JDT_MAXBUTTONS; lCounter++)
			{
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_BUTTON_BASE + lCounter].sKey = Constants.vbNullString;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort = 0;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjCursor, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue = 0;
			}
			// Sinclair 1
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR1_PUP, modSpectrum.SINCLAIR1_UP, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_UP].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_UP].lPort = modSpectrum.SINCLAIR1_PUP;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_UP].lValue = modSpectrum.SINCLAIR1_UP;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR1_PDOWN, modSpectrum.SINCLAIR1_DOWN, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_DOWN].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_DOWN].lPort = modSpectrum.SINCLAIR1_PDOWN;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_DOWN].lValue = modSpectrum.SINCLAIR1_DOWN;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR1_PLEFT, modSpectrum.SINCLAIR1_LEFT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_LEFT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_LEFT].lPort = modSpectrum.SINCLAIR1_PLEFT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_LEFT].lValue = modSpectrum.SINCLAIR1_LEFT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR1_PRIGHT, modSpectrum.SINCLAIR1_RIGHT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_RIGHT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_RIGHT].lPort = modSpectrum.SINCLAIR1_PRIGHT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_RIGHT].lValue = modSpectrum.SINCLAIR1_RIGHT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR1_PFIRE, modSpectrum.SINCLAIR1_FIRE, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_BUTTON1].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_BUTTON1].lPort = modSpectrum.SINCLAIR1_PFIRE;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_BUTTON1].lValue = modSpectrum.SINCLAIR1_FIRE;
			for (lCounter = 2; lCounter <= modSpectrum.JDT_MAXBUTTONS; lCounter++)
			{
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_BUTTON_BASE + lCounter].sKey = Constants.vbNullString;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort = 0;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue = 0;
			}
			// Sinclair 2
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR2_PUP, modSpectrum.SINCLAIR2_UP, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_UP].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_UP].lPort = modSpectrum.SINCLAIR2_PUP;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_UP].lValue = modSpectrum.SINCLAIR2_UP;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR2_PDOWN, modSpectrum.SINCLAIR2_DOWN, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_DOWN].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_DOWN].lPort = modSpectrum.SINCLAIR2_PDOWN;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_DOWN].lValue = modSpectrum.SINCLAIR2_DOWN;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR2_PLEFT, modSpectrum.SINCLAIR2_LEFT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_LEFT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_LEFT].lPort = modSpectrum.SINCLAIR2_PLEFT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_LEFT].lValue = modSpectrum.SINCLAIR2_LEFT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR2_PRIGHT, modSpectrum.SINCLAIR2_RIGHT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_RIGHT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_RIGHT].lPort = modSpectrum.SINCLAIR2_PRIGHT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_RIGHT].lValue = modSpectrum.SINCLAIR2_RIGHT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.SINCLAIR2_PFIRE, modSpectrum.SINCLAIR2_FIRE, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_BUTTON1].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_BUTTON1].lPort = modSpectrum.SINCLAIR2_PFIRE;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_BUTTON1].lValue = modSpectrum.SINCLAIR2_FIRE;
			for (lCounter = 2; lCounter <= modSpectrum.JDT_MAXBUTTONS; lCounter++)
			{
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_BUTTON_BASE + lCounter].sKey = Constants.vbNullString;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort = 0;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue = 0;
			}
			// Fuller box
			modSpectrum.PortValueToKeyStroke(modSpectrum.FULLER_PUP, modSpectrum.FULLER_UP, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_UP].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_UP].lPort = modSpectrum.FULLER_PUP;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_UP].lValue = modSpectrum.FULLER_UP;
			modSpectrum.PortValueToKeyStroke(modSpectrum.FULLER_PDOWN, modSpectrum.FULLER_DOWN, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_DOWN].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_DOWN].lPort = modSpectrum.FULLER_PDOWN;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_DOWN].lValue = modSpectrum.FULLER_DOWN;
			modSpectrum.PortValueToKeyStroke(modSpectrum.FULLER_PLEFT, modSpectrum.FULLER_LEFT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_LEFT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_LEFT].lPort = modSpectrum.FULLER_PLEFT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_LEFT].lValue = modSpectrum.FULLER_LEFT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.FULLER_PRIGHT, modSpectrum.FULLER_RIGHT, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_RIGHT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_RIGHT].lPort = modSpectrum.FULLER_PRIGHT;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_RIGHT].lValue = modSpectrum.FULLER_RIGHT;
			modSpectrum.PortValueToKeyStroke(modSpectrum.FULLER_PFIRE, modSpectrum.FULLER_FIRE, ref sKey);
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_BUTTON1].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_BUTTON1].lPort = modSpectrum.FULLER_PFIRE;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_BUTTON1].lValue = modSpectrum.FULLER_FIRE;
			for (lCounter = 2; lCounter <= modSpectrum.JDT_MAXBUTTONS; lCounter++)
			{
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_BUTTON_BASE + lCounter].sKey = Constants.vbNullString;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort = 0;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue = 0;
			}
			// User defined
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_UP].sKey = Constants.vbNullString;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_UP].lPort = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_UP].lValue = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_DOWN].sKey = Constants.vbNullString;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_DOWN].lPort = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_DOWN].lValue = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_LEFT].sKey = Constants.vbNullString;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_LEFT].lPort = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_LEFT].lValue = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_RIGHT].sKey = Constants.vbNullString;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_RIGHT].lPort = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_RIGHT].lValue = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON1].sKey = Constants.vbNullString;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON1].lPort = 0;
			modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON1].lValue = 0;
			for (lCounter = 2; lCounter <= modSpectrum.JDT_MAXBUTTONS; lCounter++)
			{
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON_BASE + lCounter].sKey = Constants.vbNullString;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort = 0;
				modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue = 0;
			}

			// MM 12.03.2003 - Joystick support code
			// =================================================================================
			// API call result
			int lResult;
			var uJoyCap = default(modSpectrum.JOYCAPS);

			// Init
			modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjInvalid;
			modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjInvalid;
			modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid;
			modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid;
			modSpectrum.bJoystick1Valid = false;

			// Errorhandler
			modSpectrum.bJoystick2Valid = false;
			;

			// Try to initiate PC-joystick nr. 1
			lResult = modSpectrum.joyGetDevCaps(modSpectrum.JOYSTICKID1, ref uJoyCap, Strings.Len(uJoyCap));
			// Init succeded
			if (lResult == modSpectrum.JOYERR_OK)
			{
				// PC-joystick nr. 1 can be set up
				modSpectrum.bJoystick1Valid = true;
				// Init for Kempston
				modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjKempston;
				modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbButton1;
				// Number of buttons
				modSpectrum.lPCJoystick1Buttons = uJoyCap.wNumButtons;
			}
			// Init for PC-joystick nr.1 failed
			else
			{
				// There is no posibility to set up the PC-joystick nr. 1
				modSpectrum.bJoystick1Valid = false;
				modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjInvalid;
				modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid;
				// Number of buttons
				modSpectrum.lPCJoystick1Buttons = -1;
			}

			// Try to initiate PC-joystick nr. 2
			lResult = modSpectrum.joyGetDevCaps(modSpectrum.JOYSTICKID2, ref uJoyCap, Strings.Len(uJoyCap));
			// Init succeded
			if (lResult == modSpectrum.JOYERR_OK)
			{
				// PC-joystick nr. 2 can be set up
				modSpectrum.bJoystick2Valid = true;
				// Init for Kempston
				modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjKempston;
				modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbButton1;
				// Number of buttons
				modSpectrum.lPCJoystick2Buttons = uJoyCap.wNumButtons;
			}
			// Init for PC-joystick nr.2 failed
			else
			{
				// There is no posibility to set up the PC-joystick nr. 2
				modSpectrum.bJoystick2Valid = false;
				modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjInvalid;
				modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid;
				// Number of buttons
				modSpectrum.lPCJoystick2Buttons = -1;
			}

			// All right
			return;
		LOAD_ERROR:
			;


			// MM 03.02.2003 -- END
			// =================================================================================

		}
		// UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
		// UPGRADE_ISSUE: Form event Form.OLEDragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		private void Form_OLEDragDrop(ref object Data, ref int Effect, ref short Button, ref short Shift, ref float X, ref float y)
		{
			// UPGRADE_ISSUE: Constant vbCFFiles was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: DataObject method Data.GetFormat was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Data.GetFormat(vbCFFiles) Then
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If (LCase(VB.Right(Data.Files(1), 4)) = ".sna") Or (LCase(VB.Right(Data.Files(1), 4)) = ".z80") Or (LCase(VB.Right(Data.Files(1), 4)) = ".tap") Or (LCase(VB.Right(Data.Files(1), 4)) = ".rom") Or (LCase(VB.Right(Data.Files(1), 4)) = ".scr") Then
			// If (Effect And System.Windows.Forms.DragDropEffects.Copy) Then
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// Select Case LCase(VB.Right(Data.Files(1), 4))
			// Case ".z80"
			// System.Windows.Forms.Application.DoEvents()
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// 'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Dir(Data.Files(1)) <> "" Then LoadZ80Snap(Data.Files(1))
			// Case ".rom"
			// System.Windows.Forms.Application.DoEvents()
			// Z80Reset()
			// If glEmulatedModel <> 0 And glEmulatedModel <> 5 Then
			// SetEmulatedModel(0, False)
			// End If
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// 'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Dir(Data.Files(1)) <> "" Then LoadROM(Data.Files(1))
			// Case ".sna"
			// System.Windows.Forms.Application.DoEvents()
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// 'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Dir(Data.Files(1)) <> "" Then LoadSNASnap(Data.Files(1))
			// Case ".tap"
			// System.Windows.Forms.Application.DoEvents()
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// 'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Dir(Data.Files(1)) <> "" Then OpenTAPFile(Data.Files(1))
			// Case ".scr"
			// System.Windows.Forms.Application.DoEvents()
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// 'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Dir(Data.Files(1)) <> "" Then LoadScreenSCR(Data.Files(1))
			// End Select
			// End If
			// End If
			// End If
		}

		// UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
		// UPGRADE_ISSUE: Form event Form.OLEDragOver was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		private void Form_OLEDragOver(ref object Data, ref int Effect, ref short Button, ref short Shift, ref float X, ref float y, ref short State)
		{
			// UPGRADE_ISSUE: Constant vbCFFiles was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: DataObject method Data.GetFormat was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If Data.GetFormat(vbCFFiles) Then
			// 'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
			// If (LCase(VB.Right(Data.Files(1), 4)) = ".sna") Or (LCase(VB.Right(Data.Files(1), 4)) = ".z80") Or (LCase(VB.Right(Data.Files(1), 4)) = ".tap") Or (LCase(VB.Right(Data.Files(1), 4)) = ".rom") Or (LCase(VB.Right(Data.Files(1), 4)) = ".scr") Then
			// If (Effect And System.Windows.Forms.DragDropEffects.Copy) Then
			// Effect = System.Windows.Forms.DragDropEffects.Copy
			// Else
			// Effect = System.Windows.Forms.DragDropEffects.None
			// End If
			// Else
			// Effect = System.Windows.Forms.DragDropEffects.None
			// End If
			// End If
		}

		private void frmMainWnd_FormClosing(object eventSender, FormClosingEventArgs eventArgs)
		{
			bool Cancel = eventArgs.Cancel;
			var UnloadMode = eventArgs.CloseReason;
			if (!My.MyProject.Forms.frmZXPrinter.Visible)
			{
				Interaction.SaveSetting("Grok", "vbSpec", "EmulateZXPrinter", "0");
			}

			if (!My.MyProject.Forms.frmTapePlayer.Visible)
			{
				Interaction.SaveSetting("Grok", "vbSpec", "TapeControlsVisible", "0");
			}

			eventArgs.Cancel = Cancel;
		}

		// MM 16.04.2003
		// UPGRADE_WARNING: Event frmMainWnd.Resize may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void frmMainWnd_Resize(object eventSender, EventArgs eventArgs)
		{
			Resize_Renamed();
		}
		// UPGRADE_WARNING: Form event frmMainWnd.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		private void frmMainWnd_Activated(object eventSender, EventArgs eventArgs)
		{
			Resize_Renamed();
		}

		private void frmMainWnd_FormClosed(object eventSender, FormClosedEventArgs eventArgs)
		{
			if (Conversions.ToBoolean(modMain.gbSoundEnabled))
				modMain.CloseWaveOut();

			// UPGRADE_NOTE: Object gMRU may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			modMain.gMRU = null;
			Interaction.SaveSetting("Grok", "vbSpec", "MainWndX", Left.ToString());
			Interaction.SaveSetting("Grok", "vbSpec", "MainWndY", Top.ToString());
			modMain.timeEndPeriod(1);
			Environment.Exit(0);
		}

		public void mnuFile_Click(object eventSender, EventArgs eventArgs)
		{
			short Index = mnuFile.GetIndex((ToolStripMenuItem)eventSender);
			int lWndProc;
			mnuFileMain.Visible = !modSpectrum.bFullScreen;
			switch (Index)
			{
				case 1: // Open
					{
						if (Conversions.ToBoolean(modMain.gbSoundEnabled))
							modWaveOut.waveOutReset(modWaveOut.glphWaveOut);
						string argsName = "";
						FileOpenDialog(sName: ref argsName);
						break;
					}

				case 2: // Save As
					{
						if (Conversions.ToBoolean(modMain.gbSoundEnabled))
							modWaveOut.waveOutReset(modWaveOut.glphWaveOut);
						FileSaveAsDialog();
						break;
					}

				case 4: // Create blank TAP file
					{
						if (Conversions.ToBoolean(modMain.gbSoundEnabled))
							modWaveOut.waveOutReset(modWaveOut.glphWaveOut);
						FileCreateNewTap();
						break;
					}

				case 6: // Load Binary
					{
						My.MyProject.Forms.frmLoadBinary.ShowDialog();
						break;
					}

				case 7: // Save Binary
					{
						My.MyProject.Forms.frmSaveBinary.ShowDialog();
						break;
					}

				case 9: // Reset
					{
						modZ80.Z80Reset();
						break;
					}

				default:
					{
						if (mnuFile[Index].Text == "E&xit")
						{
							Close();
						}
						else
						{
							// // MRU File
							string localGetMRUFile() { int argl = Index - 10; var ret = modMain.gMRU.GetMRUFile(ref argl); return ret; }

							string argsName1 = localGetMRUFile();
							FileOpenDialog(ref argsName1);
						}

						break;
					}
			}
		}

		public void mnuHelp_Click(object eventSender, EventArgs eventArgs)
		{
			short Index = mnuHelp.GetIndex((ToolStripMenuItem)eventSender);
			mnuHelpMain.Visible = !modSpectrum.bFullScreen;
			switch (Index)
			{
				case 1: // // Show spectrum keyboard layout
					{
						My.MyProject.Forms.frmKeyboard.Show();
						break;
					}

				case 3: // // About... Dialog
					{
						My.MyProject.Forms.frmAbout.ShowDialog();
						break;
					}
			}
		}

		public void mnuOptions_Click(object eventSender, EventArgs eventArgs)
		{
			short Index = mnuOptions.GetIndex((ToolStripMenuItem)eventSender);
			mnuOptionsMain.Visible = !modSpectrum.bFullScreen;
			switch (Index)
			{
				case 1:
					{
						My.MyProject.Forms.frmOptions.ShowDialog();
						break;
					}

				case 2:
					{
						My.MyProject.Forms.frmDisplayOpt.ShowDialog();
						frmMainWnd_Resize(this, new EventArgs());
						break;
					}

				case 4:
					{
						if (mnuOptions[Index].Checked)
						{
							My.MyProject.Forms.frmTapePlayer.Hide();
							mnuOptions[Index].Checked = false;
						}
						else
						{
							// // GS: Fix for tapeplayer window always-on-top
							VB.Compatibility.VB6.Support.ShowForm(My.MyProject.Forms.frmTapePlayer, 0, this);
							mnuOptions[Index].Checked = true;
						}

						Interaction.SaveSetting("Grok", "vbSpec", "TapeControlsVisible", Conversions.ToString(Interaction.IIf(mnuOptions[Index].Checked, "-1", "0")));
						break;
					}

				case 5:
					{
						if (mnuOptions[Index].Checked)
						{
							My.MyProject.Forms.frmZXPrinter.Hide();
							mnuOptions[Index].Checked = false;
						}
						else
						{
							VB.Compatibility.VB6.Support.ShowForm(My.MyProject.Forms.frmZXPrinter, 0, this);
							mnuOptions[Index].Checked = true;
						}

						Interaction.SaveSetting("Grok", "vbSpec", "EmulateZXPrinter", Conversions.ToString(Interaction.IIf(mnuOptions[Index].Checked, "-1", "0")));
						break;
					}
				// MM 03.02.2003 - BEGIN
				case 7:
					{
						mnuOptions[Index].Checked = !mnuOptions[Index].Checked;
						break;
					}

				case 8:
					{
						My.MyProject.Forms.frmPoke.ShowDialog();
						break;
					}
					// MM 03.02.2003 - END
			}
		}

		private void picDisplay_MouseDown(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			modMain.glMouseBtn = modMain.glMouseBtn | (int)Button;
		}

		private void picDisplay_MouseUp(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			modMain.glMouseBtn = modMain.glMouseBtn ^ Button;
		}


		// UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
		// UPGRADE_ISSUE: PictureBox event picDisplay.OLEDragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		private void picDisplay_OLEDragDrop(ref object Data, ref int Effect, ref short Button, ref short Shift, ref float X, ref float y)
		{
			// UPGRADE_ISSUE: Form event Form.OLEDragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
			Form_OLEDragDrop(ref Data, ref Effect, ref Button, ref Shift, ref X, ref y);
		}

		// UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
		// UPGRADE_ISSUE: PictureBox event picDisplay.OLEDragOver was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		private void picDisplay_OLEDragOver(ref object Data, ref int Effect, ref short Button, ref short Shift, ref float X, ref float y, ref short State)
		{
			// UPGRADE_ISSUE: Form event Form.OLEDragOver was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
			Form_OLEDragOver(ref Data, ref Effect, ref Button, ref Shift, ref X, ref y, ref State);
		}

		// Resize operations
		// UPGRADE_NOTE: Resize was upgraded to Resize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		public void Resize_Renamed()
		{
			// Variables
			int lDisplayWidth, lDisplayHeight;
			int lNewLeft, lNewTop;
			// Get display width and height
			lDisplayWidth = ClientRectangle.Width;
			lDisplayHeight = ClientRectangle.Height - picStatus.Height;
			// Center the paper
			lNewLeft = (int)((lDisplayWidth - picDisplay.Width) / 2d);
			lNewTop = (int)((lDisplayHeight - picDisplay.Height) / 2d);
			// Preserve minimum size
			if (lNewLeft < 12)
			{
				if (modMain.glDisplayXMultiplier == 1)
				{
					Width = 288;
					return;
				}

				if (modMain.glDisplayXMultiplier == 2)
				{
					Width = 544;
					return;
				}

				if (modMain.glDisplayXMultiplier == 3)
				{
					Width = 800;
					return;
				}
			}

			if (lNewTop < 14)
			{
				if (modMain.glDisplayYMultiplier == 1)
				{
					Height = 288;
					return;
				}

				if (modMain.glDisplayYMultiplier == 2)
				{
					Height = 480;
					return;
				}

				if (modMain.glDisplayYMultiplier == 3)
				{
					Height = 672;
					return;
				}
			}
			// Set size
			picDisplay.Top = lNewTop;
			picDisplay.Left = lNewLeft;
		}
	}
}