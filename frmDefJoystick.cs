﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmDefJoystick : Form
	{
		// /*******************************************************************************
		// frmDefJoystick.frm within vbSpec.vbp
		// 
		// "Options->General settings->Joystick->Define" dialog for vbSpec
		// 
		// Author: Miklos Muhi <vbspec@muhi.org>
		// http://www.muhi.org/vbspec/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/
		// Joystick definition rules:
		// 1. You can only then define a joystick, if the joystick is connected.
		// 2. If you open this configuration window, you will see the current joystick settigns,
		// described with ports.
		// 3. You can save and load joystick configuration files (myconfig.jcf)
		// The file format of then JCF-files looks like this:
		// [vbSpec joystick configuration file 0.01]
		// UP=<Key>*<Port>*<Value>
		// DOWN=<Key>*<Port>*<Value>
		// LEFT=<Key>*<Port>*<Value>
		// RIGHT=<Key>*<Port>*<Value>
		// BUTTON=<Key>*<Port>*<Value>
		// ...
		// BUTTON=<Key>*<Port>*<Value>
		// 
		// 4. You can redefine define each supported joystick.

		// Variables
		private bool bIsFirstActivate;
		// The joystick you redefine
		private int lJoystickNo;
		// The current settings
		private int lJoystickSetting;
		// Fire button
		private int lFireButton;
		// Changes
		private bool bEdit;
		// Keys, ports and  values
		private string sKey;
		private int lPort, lValue;
		// Edit flags
		private bool bKey, bPortValue;
		// OK flag
		private bool bOK;

		// This property sets the joystick you are going to redefine
		internal int JoystickNo
		{
			set
			{
				lJoystickNo = value;
			}
		}
		// This property provides the joysticks current settings
		internal int JoystickSetting
		{
			set
			{
				lJoystickSetting = value;
			}
		}
		// This property provides the current fire button setting
		internal int FireButton
		{
			set
			{
				lFireButton = value;
			}
		}
		// Get the OK-Flag
		internal bool OK
		{
			get
			{
				bool OKRet = default;
				OKRet = bOK;
				return OKRet;
			}
		}

		// The form will be loaded
		private void frmDefJoystick_Load(object eventSender, EventArgs eventArgs)
		{
			// Variables
			ColumnHeader oColHeader;
			// Setup the controls
			txtUp.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtUp.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtUp.MaxLength = 3;
			txtUp.ReadOnly = true;
			txtPortUp.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtPortUp.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtPortUp.MaxLength = 5;
			txtValueUp.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtValueUp.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtValueUp.MaxLength = 3;
			txtDown.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtDown.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtDown.MaxLength = 3;
			txtDown.ReadOnly = true;
			txtPortDown.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtPortDown.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtPortDown.MaxLength = 5;
			txtValueDown.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtValueDown.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtValueDown.MaxLength = 3;
			txtLeft.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtLeft.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtLeft.MaxLength = 3;
			txtLeft.ReadOnly = true;
			txtPortLeft.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtPortLeft.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtPortLeft.MaxLength = 5;
			txtValueLeft.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtValueLeft.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtValueLeft.MaxLength = 3;
			txtRight.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtRight.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtRight.MaxLength = 3;
			txtRight.ReadOnly = true;
			txtPortRight.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtPortRight.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtPortRight.MaxLength = 5;
			txtValueRight.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtValueRight.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtValueRight.MaxLength = 3;
			{
				var withBlock = lvButtons;
				withBlock.AllowColumnReorder = false;
				// UPGRADE_ISSUE: MSComctlLib.ListView property lvButtons.Appearance was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// .Appearance = System.Windows.Forms.BorderStyle.Fixed3D
				// UPGRADE_ISSUE: MSComctlLib.ListView property lvButtons.FlatScrollBar was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// .FlatScrollBar = False
				withBlock.FullRowSelect = true;
				withBlock.GridLines = true;
				withBlock.HideSelection = false;
				withBlock.LabelEdit = false;
				withBlock.MultiSelect = false;
				withBlock.View = View.Details;
				oColHeader = withBlock.Columns.Add("Button");
				oColHeader.Width = (int)(withBlock.Width / 100d) * 25;
				oColHeader = withBlock.Columns.Add("Key");
				oColHeader.Width = (int)(withBlock.Width / 100d) * 19;
				oColHeader = withBlock.Columns.Add("Port");
				oColHeader.Width = (int)(withBlock.Width / 100d) * 23;
				oColHeader.TextAlign = HorizontalAlignment.Right;
				oColHeader = withBlock.Columns.Add("Value");
				oColHeader.Width = (int)(withBlock.Width / 100d) * 22;
				oColHeader.TextAlign = HorizontalAlignment.Right;
			}
			// Default values
			lJoystickNo = 1;
			lJoystickSetting = (int)modSpectrum.ZXJOYSTICKS.zxjKempston;
			lFireButton = 1;
			bEdit = true;
			bKey = false;
			bPortValue = false;
			bOK = false;
			// Now it's time for the first activation
			bIsFirstActivate = true;
		}

		// The form will be activated
		// UPGRADE_WARNING: Form event frmDefJoystick.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		private void frmDefJoystick_Activated(object eventSender, EventArgs eventArgs)
		{
			// Variables
			int lCounter;
			ListViewItem oItem;
			var sKey = default(string);
			int lPort, lValue;
			// Only by the first activation
			if (bIsFirstActivate)
			{
				// If there is no valid joystick
				if (!(modSpectrum.bJoystick1Valid & lJoystickNo == 1 | modSpectrum.bJoystick2Valid & lJoystickNo == 2))
				{
					// Close window
					Close();
					return;
				}
				// Set caption
				Text = "Redefine joystick " + Strings.Trim(lJoystickNo.ToString());
				// You cannot set an invalid joystick
				if (lJoystickSetting == (int)modSpectrum.ZXJOYSTICKS.zxjInvalid)
				{
					Close();
					return;
				}
				// Set values
				txtPortUp.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_UP].lPort.ToString());
				txtPortDown.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_DOWN].lPort.ToString());
				txtPortLeft.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_LEFT].lPort.ToString());
				txtPortRight.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_RIGHT].lPort.ToString());
				txtValueUp.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_UP].lValue.ToString());
				txtValueDown.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_DOWN].lValue.ToString());
				txtValueLeft.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_LEFT].lValue.ToString());
				txtValueRight.Text = Strings.Trim(modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_RIGHT].lValue.ToString());
				if (lJoystickNo == 1)
				{
					var loopTo = modSpectrum.lPCJoystick1Buttons;
					for (lCounter = 1; lCounter <= loopTo; lCounter++)
					{
						oItem = lvButtons.Items.Add("Button" + Strings.Trim(lCounter.ToString()));
						lPort = modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort;
						lValue = modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue;
						if (lPort != 0 & lValue != 0)
						{
							modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 1)
							{
								oItem.SubItems[1].Text = sKey;
							}
							else
							{
								oItem.SubItems.Insert(1, new ListViewItem.ListViewSubItem(null, sKey));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 2)
							{
								oItem.SubItems[2].Text = lPort.ToString();
							}
							else
							{
								oItem.SubItems.Insert(2, new ListViewItem.ListViewSubItem(null, lPort.ToString()));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 3)
							{
								oItem.SubItems[3].Text = lValue.ToString();
							}
							else
							{
								oItem.SubItems.Insert(3, new ListViewItem.ListViewSubItem(null, lValue.ToString()));
							}
						}
						else
						{
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 1)
							{
								oItem.SubItems[1].Text = " ";
							}
							else
							{
								oItem.SubItems.Insert(1, new ListViewItem.ListViewSubItem(null, " "));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 2)
							{
								oItem.SubItems[2].Text = " ";
							}
							else
							{
								oItem.SubItems.Insert(2, new ListViewItem.ListViewSubItem(null, " "));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 3)
							{
								oItem.SubItems[3].Text = " ";
							}
							else
							{
								oItem.SubItems.Insert(3, new ListViewItem.ListViewSubItem(null, " "));
							}
						}
					}
				}

				if (lJoystickNo == 2)
				{
					var loopTo1 = modSpectrum.lPCJoystick2Buttons;
					for (lCounter = 1; lCounter <= loopTo1; lCounter++)
					{
						oItem = lvButtons.Items.Add(" ");
						lPort = modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_BUTTON_BASE + lCounter].lPort;
						lValue = modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, lJoystickSetting, modSpectrum.JDT_BUTTON_BASE + lCounter].lValue;
						if (lPort != 0 & lValue != 0)
						{
							modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 1)
							{
								oItem.SubItems[1].Text = sKey;
							}
							else
							{
								oItem.SubItems.Insert(1, new ListViewItem.ListViewSubItem(null, sKey));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 2)
							{
								oItem.SubItems[2].Text = lPort.ToString();
							}
							else
							{
								oItem.SubItems.Insert(2, new ListViewItem.ListViewSubItem(null, lPort.ToString()));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 3)
							{
								oItem.SubItems[3].Text = lValue.ToString();
							}
							else
							{
								oItem.SubItems.Insert(3, new ListViewItem.ListViewSubItem(null, lValue.ToString()));
							}
						}
						else
						{
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 1)
							{
								oItem.SubItems[1].Text = " ";
							}
							else
							{
								oItem.SubItems.Insert(1, new ListViewItem.ListViewSubItem(null, " "));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 2)
							{
								oItem.SubItems[2].Text = " ";
							}
							else
							{
								oItem.SubItems.Insert(2, new ListViewItem.ListViewSubItem(null, " "));
							}
							// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
							if (oItem.SubItems.Count > 3)
							{
								oItem.SubItems[3].Text = " ";
							}
							else
							{
								oItem.SubItems.Insert(3, new ListViewItem.ListViewSubItem(null, " "));
							}
						}
					}
				}
				// Select the first button
				// UPGRADE_WARNING: Lower bound of collection lvButtons.ListItems has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				lvButtons.FocusedItem = lvButtons.Items[1];
				// UPGRADE_ISSUE: MSComctlLib.ListView event lvButtons.ItemClick was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
				lvButtons_ItemClick(lvButtons.FocusedItem);
				// No edit
				bEdit = false;
				// The first activation ends here
				bIsFirstActivate = false;
			}
		}

		// The form will be unloaded
		private void frmDefJoystick_FormClosed(object eventSender, FormClosedEventArgs eventArgs)
		{
			// Variables
			MsgBoxResult vbmrValue;
			// If there are unsaved data
			if (bEdit)
			{
				// Ask the user
				vbmrValue = Interaction.MsgBox("Do you want to take over your changes?", (MsgBoxStyle)((int)MsgBoxStyle.Question + (int)MsgBoxStyle.YesNoCancel + (int)MsgBoxStyle.DefaultButton1));
				// Analyse
				switch (vbmrValue)
				{
					// Save changes and exit
					case MsgBoxResult.Yes:
						{
							// Save changes
							OKButton_Click(OKButton, new EventArgs());
							break;
						}
					// Do not save changes
					case MsgBoxResult.No:
						{
							// No changes
							bOK = false;
							break;
						}
					// Cancel
					case MsgBoxResult.Cancel:
						{
							break;
						}
						// UPGRADE_ISSUE: Event parameter Cancel was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FB723E3C-1C06-4D2B-B083-E6CD0D334DA8"'
						// Cancel = 1
				}
			}
		}

		// Close and save
		private void OKButton_Click(object eventSender, EventArgs eventArgs)
		{

			// Variablen
			var sKey = default(string);
			int lPort;
			int lValue;

			// Validate
			if (!Validate_Renamed())
			{
				return;
			}

			// Take over changes
			lPort = Conversions.ToInteger(txtPortUp.Text);
			lValue = Conversions.ToInteger(txtValueUp.Text);
			modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_UP].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_UP].lPort = lPort;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_UP].lValue = lValue;
			lPort = Conversions.ToInteger(txtPortDown.Text);
			lValue = Conversions.ToInteger(txtValueDown.Text);
			modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_DOWN].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_DOWN].lPort = lPort;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_DOWN].lValue = lValue;
			lPort = Conversions.ToInteger(txtPortLeft.Text);
			lValue = Conversions.ToInteger(txtValueLeft.Text);
			modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_LEFT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_LEFT].lPort = lPort;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_LEFT].lValue = lValue;
			lPort = Conversions.ToInteger(txtPortRight.Text);
			lValue = Conversions.ToInteger(txtValueRight.Text);
			modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_RIGHT].sKey = sKey;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_RIGHT].lPort = lPort;
			modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_RIGHT].lValue = lValue;
			foreach (ListViewItem oItem in lvButtons.Items)
			{
				// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				if (!string.IsNullOrEmpty(Strings.Trim(oItem.SubItems[2].Text)) & !string.IsNullOrEmpty(Strings.Trim(oItem.SubItems[3].Text)))
				{
					// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
					lPort = Conversions.ToInteger(oItem.SubItems[2].Text);
					// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
					lValue = Conversions.ToInteger(oItem.SubItems[3].Text);
					modSpectrum.PortValueToKeyStroke(lPort, lValue, ref sKey);
					modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON_BASE + oItem.Index].sKey = sKey;
					modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON_BASE + oItem.Index].lPort = lPort;
					modSpectrum.aJoystikDefinitionTable[lJoystickNo - 1, (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined, modSpectrum.JDT_BUTTON_BASE + oItem.Index].lValue = lValue;
				}
			}

			// No edit
			bEdit = false;
			// Taken over
			bOK = true;
			// Close form
			Close();
		}

		// Close without save
		private void CancelButton_Renamed_Click(object eventSender, EventArgs eventArgs)
		{
			bOK = false;
			Close();
		}

		// Save the joystick configuration
		private void cmdSave_Click(object eventSender, EventArgs eventArgs)
		{

			// Variables
			string sFileName;

			// Error handling
			int hFile;
			;

			// Initialise
			// UPGRADE_WARNING: CommonDialog variable was not upgraded Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="671167DC-EA81-475D-B690-7A40C7BF4A23"'
			// With comdlgFile
			// 'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// .CancelError = True
			// .Title = "Save joystick configuration"
			// .DefaultExt = ".jcf"
			// .FileName = ""
			// 'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// .Filter = "vbSpec joystick configuration file (*.jcf)|*.jcf|"
			// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// .CheckPathExists = True
			// .CheckPathExists = True
			// 'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// .OverwritePrompt = True
			// 'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// .Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			// 'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// .Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			// 'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// .ShowReadOnly = False
			// 'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// .Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
			// .ShowDialog()
			// End With
			// Get file name
			sFileName = comdlgFileOpen.FileName;

			// Start
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Output);

			// Header
			FileSystem.PrintLine(hFile, "[vbSpec joystick configuration file 0.01]");
			// Up
			FileSystem.PrintLine(hFile, "UP=" + Strings.Trim(txtUp.Text) + "*" + Strings.Trim(txtPortUp.Text) + "*" + Strings.Trim(txtValueUp.Text));
			// Down
			FileSystem.PrintLine(hFile, "DOWN=" + Strings.Trim(txtDown.Text) + "*" + Strings.Trim(txtPortDown.Text) + "*" + Strings.Trim(txtValueDown.Text));
			// Left
			FileSystem.PrintLine(hFile, "LEFT=" + Strings.Trim(txtLeft.Text) + "*" + Strings.Trim(txtPortLeft.Text) + "*" + Strings.Trim(txtValueLeft.Text));
			// Right
			FileSystem.PrintLine(hFile, "RIGHT=" + Strings.Trim(txtRight.Text) + "*" + Strings.Trim(txtPortRight.Text) + "*" + Strings.Trim(txtValueRight.Text));
			// Buttons
			foreach (ListViewItem oItem in lvButtons.Items)
				// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				FileSystem.PrintLine(hFile, "BUTTON=" + Strings.Trim(oItem.SubItems[1].Text) + "*" + Strings.Trim(oItem.SubItems[2].Text) + "*" + Strings.Trim(oItem.SubItems[3].Text));

			// Close file
			FileSystem.FileClose(hFile);
			return;
		CMDSAVECLICK_ERROR:
			;
		}

		// Load a joystick configuration
		private void cmdLoad_Click(object eventSender, EventArgs eventArgs)
		{

			// Variables
			// Dim sFileName As String
			// Dim hFile As Integer
			// Dim oItem As System.Windows.Forms.ListViewItem
			// Dim sRow As String
			// Dim vRow As Object

			// 'Error handling
			// On Error GoTo CMDLOADCLICK_ERROR

			// 'Initialise
			// 'UPGRADE_WARNING: CommonDialog variable was not upgraded Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="671167DC-EA81-475D-B690-7A40C7BF4A23"'
			// With comdlgFile
			// 'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// .CancelError = True
			// .Title = "Load joystick configuration"
			// .DefaultExt = ".jcf"
			// .FileName = ""
			// 'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			// .Filter = "vbSpec joystick configuration file (*.jcf)|*.jcf|"
			// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// .CheckPathExists = True
			// .CheckPathExists = True
			// 'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// .OverwritePrompt = True
			// 'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// '.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			// 'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// '.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			// 'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// '.ShowReadOnly = False
			// 'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// '.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
			// .ShowDialog()
			// End With
			// 'Get file name
			// sFileName = comdlgFileOpen.FileName

			// 'Start
			// hFile = FreeFile
			// FileOpen(hFile, sFileName, OpenMode.Input)

			// 'Read the first row
			// Input(hFile, sRow)
			// 'The file must begin with the first row
			// If sRow = "[vbSpec joystick configuration file 0.01]" Then
			// 'Clear the button list
			// lvButtons.Items.Clear()
			// 'Scan the whole file
			// Do While Not EOF(hFile)
			// 'Up
			// If UCase(VB.Left(sRow, 3)) = "UP=" Then
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// vRow = Split(VB.Right(sRow, Len(sRow) - 3), "*")
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtPortUp.Text = vRow(1)
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtValueUp.Text = vRow(2)
			// End If
			// 'Down
			// If UCase(VB.Left(sRow, 5)) = "DOWN=" Then
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// vRow = Split(VB.Right(sRow, Len(sRow) - 5), "*")
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtPortDown.Text = vRow(1)
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtValueDown.Text = vRow(2)
			// End If
			// 'Left
			// If UCase(VB.Left(sRow, 5)) = "LEFT=" Then
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// vRow = Split(VB.Right(sRow, Len(sRow) - 5), "*")
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtPortLeft.Text = vRow(1)
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtValueLeft.Text = vRow(2)
			// End If
			// 'Right
			// If UCase(VB.Left(sRow, 6)) = "RIGHT=" Then
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// vRow = Split(VB.Right(sRow, Len(sRow) - 6), "*")
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtPortRight.Text = vRow(1)
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// txtValueRight.Text = vRow(2)
			// End If
			// 'Button
			// If UCase(VB.Left(sRow, 7)) = "BUTTON=" Then
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// vRow = Split(VB.Right(sRow, Len(sRow) - 7), "*")
			// If lJoystickNo = 1 Then
			// If lvButtons.Items.Count < lPCJoystick1Buttons Then
			// oItem = lvButtons.Items.Add("Button" & Trim(CStr(lvButtons.Items.Count + 1)))
			// 'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// If oItem.SubItems.Count > 1 Then
			// oItem.SubItems(1).Text = vRow(0)
			// Else
			// oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(0)))
			// End If
			// 'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// If oItem.SubItems.Count > 2 Then
			// oItem.SubItems(2).Text = vRow(1)
			// Else
			// oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(1)))
			// End If
			// 'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// If oItem.SubItems.Count > 3 Then
			// oItem.SubItems(3).Text = vRow(2)
			// Else
			// oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(2)))
			// End If
			// End If
			// Else
			// If lvButtons.Items.Count < lPCJoystick2Buttons Then
			// oItem = lvButtons.Items.Add("Button" & Trim(CStr(lvButtons.Items.Count + 1)))
			// 'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// If oItem.SubItems.Count > 1 Then
			// oItem.SubItems(1).Text = vRow(0)
			// Else
			// oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(0)))
			// End If
			// 'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// If oItem.SubItems.Count > 2 Then
			// oItem.SubItems(2).Text = vRow(1)
			// Else
			// oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(1)))
			// End If
			// 'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			// 'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			// If oItem.SubItems.Count > 3 Then
			// oItem.SubItems(3).Text = vRow(2)
			// Else
			// oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(2)))
			// End If
			// End If
			// End If
			// End If
			// 'Read the next row
			// Input(hFile, sRow)
			// Loop 
			// End If

			// 'Close file
			// FileClose(hFile)

			// Exit Sub
			// CMDLOADCLICK_ERROR: 
		}

		// The selection is changed
		// UPGRADE_ISSUE: MSComctlLib.ListView event lvButtons.ItemClick was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		private void lvButtons_ItemClick(ListViewItem Item)
		{
			// You can define a button only if it has no definition yet
			// UPGRADE_WARNING: Lower bound of collection Item has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			cmdDefine.Enabled = string.IsNullOrEmpty(Strings.Trim(Item.SubItems[2].Text));
			// You can undefine a button if it has a definition yet
			cmdUndefine.Enabled = !cmdDefine.Enabled;
		}

		// Define button
		private void cmdDefine_Click(object eventSender, EventArgs eventArgs)
		{
			// Variables
			// Error handel
			frmDefButton frmDefButtonWnd;
			;
			// Initialise
			frmDefButtonWnd = new frmDefButton();
			// UPGRADE_ISSUE: Load statement is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
			// Load(frmDefButtonWnd)
			frmDefButtonWnd.Button = lvButtons.FocusedItem.Index;
			// Show form
			frmDefButtonWnd.ShowDialog();
			// If the user wants to save
			if (frmDefButtonWnd.OK)
			{
				// Get changes
				// UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				if (lvButtons.FocusedItem.SubItems.Count > 1)
				{
					lvButtons.FocusedItem.SubItems[1].Text = Strings.Trim(frmDefButtonWnd.ButtonChar);
				}
				else
				{
					lvButtons.FocusedItem.SubItems.Insert(1, new ListViewItem.ListViewSubItem(null, Strings.Trim(frmDefButtonWnd.ButtonChar)));
				}
				// UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				if (lvButtons.FocusedItem.SubItems.Count > 2)
				{
					lvButtons.FocusedItem.SubItems[2].Text = Strings.Trim(frmDefButtonWnd.ButtonPort.ToString());
				}
				else
				{
					lvButtons.FocusedItem.SubItems.Insert(2, new ListViewItem.ListViewSubItem(null, Strings.Trim(frmDefButtonWnd.ButtonPort.ToString())));
				}
				// UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				if (lvButtons.FocusedItem.SubItems.Count > 3)
				{
					lvButtons.FocusedItem.SubItems[3].Text = Strings.Trim(frmDefButtonWnd.ButtonValue.ToString());
				}
				else
				{
					lvButtons.FocusedItem.SubItems.Insert(3, new ListViewItem.ListViewSubItem(null, Strings.Trim(frmDefButtonWnd.ButtonValue.ToString())));
				}
				// That's a change
				bEdit = true;
			}
			// Anihilate objects
			if (frmDefButtonWnd is object)
			{
				// UPGRADE_NOTE: Object frmDefButtonWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
				frmDefButtonWnd = null;
			}
			// End procedure
			return;
		CMDDEFINECLICK_ERROR:
			;

			// Report error
			Interaction.MsgBox(Information.Err().Description, (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
			// Anihilate objects
			if (frmDefButtonWnd is object)
			{
				// UPGRADE_NOTE: Object frmDefButtonWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
				frmDefButtonWnd = null;
			}
		}

		// Anihilate a button definition
		private void cmdUndefine_Click(object eventSender, EventArgs eventArgs)
		{
			// Anihilate definition
			// UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			if (lvButtons.FocusedItem.SubItems.Count > 1)
			{
				lvButtons.FocusedItem.SubItems[1].Text = " ";
			}
			else
			{
				lvButtons.FocusedItem.SubItems.Insert(1, new ListViewItem.ListViewSubItem(null, " "));
			}
			// UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			if (lvButtons.FocusedItem.SubItems.Count > 2)
			{
				lvButtons.FocusedItem.SubItems[2].Text = " ";
			}
			else
			{
				lvButtons.FocusedItem.SubItems.Insert(2, new ListViewItem.ListViewSubItem(null, " "));
			}
			// UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			if (lvButtons.FocusedItem.SubItems.Count > 3)
			{
				lvButtons.FocusedItem.SubItems[3].Text = " ";
			}
			else
			{
				lvButtons.FocusedItem.SubItems.Insert(3, new ListViewItem.ListViewSubItem(null, " "));
			}
			// Regulate selection
			// UPGRADE_ISSUE: MSComctlLib.ListView event lvButtons.ItemClick was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
			lvButtons_ItemClick(lvButtons.FocusedItem);
			// That's a change
			bEdit = true;
		}

		// Default change hanlder
		private void DefaultChangeHanlder()
		{
			bEdit = true;
		}

		// Default focus hanlder
		private void DefaultFocusHanlder(ref TextBox oTextBox)
		{
			if (!string.IsNullOrEmpty(oTextBox.Text))
			{
				oTextBox.SelectionStart = 0;
				oTextBox.SelectionLength = Strings.Len(oTextBox.Text);
			}
		}

		// Define per keystroke
		private void txtUp_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (!bPortValue)
			{
				bKey = true;
				modSpectrum.KeyStrokeToPortValue(KeyCode, Shift, ref sKey, ref lPort, ref lValue);
				txtUp.Text = sKey;
				txtPortUp.Text = Strings.Trim(lPort.ToString());
				txtValueUp.Text = Strings.Trim(lValue.ToString());
				bKey = false;
			}
		}

		private void txtDown_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (!bPortValue)
			{
				bKey = true;
				modSpectrum.KeyStrokeToPortValue(KeyCode, Shift, ref sKey, ref lPort, ref lValue);
				txtDown.Text = sKey;
				txtPortDown.Text = Strings.Trim(lPort.ToString());
				txtValueDown.Text = Strings.Trim(lValue.ToString());
				bKey = false;
			}
		}

		private void txtLeft_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (!bPortValue)
			{
				bKey = true;
				modSpectrum.KeyStrokeToPortValue(KeyCode, Shift, ref sKey, ref lPort, ref lValue);
				txtLeft.Text = sKey;
				txtPortLeft.Text = Strings.Trim(lPort.ToString());
				txtValueLeft.Text = Strings.Trim(lValue.ToString());
				bKey = false;
			}
		}

		private void txtRight_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (!bPortValue)
			{
				bKey = true;
				modSpectrum.KeyStrokeToPortValue(KeyCode, Shift, ref sKey, ref lPort, ref lValue);
				txtRight.Text = sKey;
				txtPortRight.Text = Strings.Trim(lPort.ToString());
				txtValueRight.Text = Strings.Trim(lValue.ToString());
				bKey = false;
			}
		}

		// Define per port, in value
		// UPGRADE_WARNING: Event txtPortUp.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtPortUp_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortUp.Text) & Information.IsNumeric(txtValueUp.Text)))
				{
					txtUp.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortUp.Text), Conversions.ToInteger(txtValueUp.Text), ref sKey);
				txtUp.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtValueUp.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtValueUp_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortUp.Text) & Information.IsNumeric(txtValueUp.Text)))
				{
					txtUp.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortUp.Text), Conversions.ToInteger(txtValueUp.Text), ref sKey);
				txtUp.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtPortDown.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtPortDown_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortDown.Text) & Information.IsNumeric(txtValueDown.Text)))
				{
					txtDown.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortDown.Text), Conversions.ToInteger(txtValueDown.Text), ref sKey);
				txtDown.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtValueDown.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtValueDown_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortDown.Text) & Information.IsNumeric(txtValueDown.Text)))
				{
					txtDown.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortDown.Text), Conversions.ToInteger(txtValueDown.Text), ref sKey);
				txtDown.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtPortLeft.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtPortLeft_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortLeft.Text) & Information.IsNumeric(txtValueLeft.Text)))
				{
					txtLeft.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortLeft.Text), Conversions.ToInteger(txtValueLeft.Text), ref sKey);
				txtLeft.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtValueLeft.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtValueLeft_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortLeft.Text) & Information.IsNumeric(txtValueLeft.Text)))
				{
					txtLeft.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortLeft.Text), Conversions.ToInteger(txtValueLeft.Text), ref sKey);
				txtLeft.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtPortRight.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtPortRight_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortRight.Text) & Information.IsNumeric(txtValueRight.Text)))
				{
					txtRight.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortRight.Text), Conversions.ToInteger(txtValueRight.Text), ref sKey);
				txtRight.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}
		// UPGRADE_WARNING: Event txtValueRight.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtValueRight_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortRight.Text) & Information.IsNumeric(txtValueRight.Text)))
				{
					txtRight.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortRight.Text), Conversions.ToInteger(txtValueRight.Text), ref sKey);
				txtRight.Text = sKey;
				bPortValue = false;
			}

			DefaultChangeHanlder();
		}

		// Focus
		private void txtPortUp_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtPortUp;
			DefaultFocusHanlder(ref argoTextBox);
			txtPortUp = argoTextBox;
		}

		private void txtValueUp_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtValueUp;
			DefaultFocusHanlder(ref argoTextBox);
			txtValueUp = argoTextBox;
		}

		private void txtPortDown_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtPortDown;
			DefaultFocusHanlder(ref argoTextBox);
			txtPortDown = argoTextBox;
		}

		private void txtValueDown_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtValueDown;
			DefaultFocusHanlder(ref argoTextBox);
			txtValueDown = argoTextBox;
		}

		private void txtPortLeft_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtPortLeft;
			DefaultFocusHanlder(ref argoTextBox);
			txtPortLeft = argoTextBox;
		}

		private void txtValueLeft_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtValueLeft;
			DefaultFocusHanlder(ref argoTextBox);
			txtValueLeft = argoTextBox;
		}

		private void txtPortRight_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtPortRight;
			DefaultFocusHanlder(ref argoTextBox);
			txtPortRight = argoTextBox;
		}

		private void txtValueRight_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtValueRight;
			DefaultFocusHanlder(ref argoTextBox);
			txtValueRight = argoTextBox;
		}

		// Validate joystick settings
		// UPGRADE_NOTE: Validate was upgraded to Validate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'

		// Variables
		private bool Validate_Renamed()
		{
			bool Validate_RenamedRet = default;

			// Preset
			Validate_RenamedRet = false;

			// All values must be numeric
			if (!Information.IsNumeric(txtPortUp.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the up port.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortUp.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtPortDown.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the down port.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortDown.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtPortLeft.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the left port.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortLeft.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtPortRight.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the right port.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortRight.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtValueUp.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the up in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueUp.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtValueDown.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the down in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueDown.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtValueLeft.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the left in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueLeft.Focus();
				return Validate_RenamedRet;
			}

			if (!Information.IsNumeric(txtValueRight.Text))
			{
				Interaction.MsgBox("Please enter a numeric value for the right in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueRight.Focus();
				return Validate_RenamedRet;
			}

			// All port values must be between 0 and 65535
			if (Conversions.ToInteger(txtPortUp.Text) < 0 | Conversions.ToInteger(txtPortUp.Text) > 65535)
			{
				Interaction.MsgBox("Please enter a value between 0 and 65535 for the up port value.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortUp.Focus();
				return Validate_RenamedRet;
			}

			if (Conversions.ToInteger(txtPortDown.Text) < 0 | Conversions.ToInteger(txtPortDown.Text) > 65535)
			{
				Interaction.MsgBox("Please enter a value between 0 and 65535 for the down port value.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortDown.Focus();
				return Validate_RenamedRet;
			}

			if (Conversions.ToInteger(txtPortLeft.Text) < 0 | Conversions.ToInteger(txtPortLeft.Text) > 65535)
			{
				Interaction.MsgBox("Please enter a value between 0 and 65535 for the left port value.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortLeft.Focus();
				return Validate_RenamedRet;
			}

			if (Conversions.ToInteger(txtPortRight.Text) < 0 | Conversions.ToInteger(txtPortRight.Text) > 65535)
			{
				Interaction.MsgBox("Please enter a value between 0 and 65535 for the right port value.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtPortRight.Focus();
				return Validate_RenamedRet;
			}

			// All in values must be between 0 and 255
			if (Conversions.ToInteger(txtValueUp.Text) < 0 | Conversions.ToInteger(txtValueUp.Text) > 255)
			{
				Interaction.MsgBox("Please enter a value between 0 and 255 for the up in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueUp.Focus();
				return Validate_RenamedRet;
			}

			if (Conversions.ToInteger(txtValueDown.Text) < 0 | Conversions.ToInteger(txtValueDown.Text) > 255)
			{
				Interaction.MsgBox("Please enter a value between 0 and 255 for the down in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueDown.Focus();
				return Validate_RenamedRet;
			}

			if (Conversions.ToInteger(txtValueLeft.Text) < 0 | Conversions.ToInteger(txtValueLeft.Text) > 255)
			{
				Interaction.MsgBox("Please enter a value between 0 and 255 for the left in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueLeft.Focus();
				return Validate_RenamedRet;
			}

			if (Conversions.ToInteger(txtValueRight.Text) < 0 | Conversions.ToInteger(txtValueRight.Text) > 255)
			{
				Interaction.MsgBox("Please enter a value between 0 and 255 for the right in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
				txtValueRight.Focus();
				return Validate_RenamedRet;
			}

			// Validate list items
			foreach (ListViewItem oItem in lvButtons.Items)
			{
				// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				if (!string.IsNullOrEmpty(Strings.Trim(oItem.SubItems[2].Text)) & !string.IsNullOrEmpty(Strings.Trim(oItem.SubItems[3].Text)))
				{
					// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
					if (!Information.IsNumeric(oItem.SubItems[2].Text))
					{
						Interaction.MsgBox("Please enter numeric value for button " + Strings.Trim(oItem.Index.ToString()) + " port.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
						lvButtons.FocusedItem = oItem;
						return Validate_RenamedRet;
					}
					// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
					if (!Information.IsNumeric(oItem.SubItems[3].Text))
					{
						Interaction.MsgBox("Please enter numeric value for button " + Strings.Trim(oItem.Index.ToString()) + " in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
						lvButtons.FocusedItem = oItem;
						return Validate_RenamedRet;
					}
					// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
					if (Conversions.ToInteger(oItem.SubItems[2].Text) < 0 | Conversions.ToInteger(oItem.SubItems[2].Text) > 65535)
					{
						Interaction.MsgBox("Please enter a value between 0 and 65535 for button " + Strings.Trim(oItem.Index.ToString()) + " port.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
						lvButtons.FocusedItem = oItem;
						return Validate_RenamedRet;
					}
					// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
					if (!Information.IsNumeric(oItem.SubItems[3].Text))
					{
						Interaction.MsgBox("Please enter numeric value for button " + Strings.Trim(oItem.Index.ToString()) + " in.", (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
						lvButtons.FocusedItem = oItem;
						return Validate_RenamedRet;
					}
				}
				// UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				else if (!string.IsNullOrEmpty(Strings.Trim(oItem.SubItems[2].Text)) | !string.IsNullOrEmpty(Strings.Trim(oItem.SubItems[3].Text)))
				{
					Interaction.MsgBox("You must enter a port number and an in value to define a button.");
					lvButtons.FocusedItem = oItem;
					return Validate_RenamedRet;
				}
			}

			// That shoul be OK
			Validate_RenamedRet = true;
			return Validate_RenamedRet;
		}
	}
}