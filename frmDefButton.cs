﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmDefButton : Form
	{
		// /*******************************************************************************
		// frmDefButton.frm within vbSpec.vbp
		// 
		// "Options->General settings->Joystick->Define" dialog for vbSpec
		// 
		// Author: Miklos Muhi <vbspec@muhi.org>
		// http://www.muhi.org/vbspec/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/

		// Variables
		private bool bIsFirstActivate;
		// Button
		private int lButton;
		// If the Form was Closed with OK
		private bool bOK;
		// If there was any change
		private bool bEdit;
		// Button definitions
		private string sButton;
		private int lButtonPort;
		private int lButtonValue;
		// Keys, ports and  values
		private string sKey;
		private int lPort, lValue;
		// Edit flags
		private bool bKey, bPortValue;

		// This property provides the number of the button wich you set up
		internal int Button
		{
			set
			{
				lButton = value;
			}
		}
		// Provides, if the form was closed with OK
		internal bool OK
		{
			get
			{
				bool OKRet = default;
				OKRet = bOK;
				return OKRet;
			}
		}
		// Provider the button data
		internal string ButtonChar
		{
			get
			{
				string ButtonCharRet = default;
				ButtonCharRet = sButton;
				return ButtonCharRet;
			}
		}

		internal int ButtonPort
		{
			get
			{
				int ButtonPortRet = default;
				ButtonPortRet = lButtonPort;
				return ButtonPortRet;
			}
		}

		internal int ButtonValue
		{
			get
			{
				int ButtonValueRet = default;
				ButtonValueRet = lButtonValue;
				return ButtonValueRet;
			}
		}

		// The form will be loaded
		private void frmDefButton_Load(object eventSender, EventArgs eventArgs)
		{
			// Setup the controls
			lblButton.Text = Constants.vbNullString;
			txtFire.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtFire.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtFire.MaxLength = 3;
			txtFire.ReadOnly = true;
			txtPortFire.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtPortFire.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtPortFire.MaxLength = 5;
			txtValueFire.Text = Constants.vbNullString;
			// UPGRADE_WARNING: TextBox property txtValueFire.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
			txtValueFire.MaxLength = 3;
			// Default values
			lButton = 1;
			bOK = false;
			bEdit = false;
			sButton = Constants.vbNullString;
			lButtonPort = 0;
			lButtonValue = 0;
			bKey = false;
			bPortValue = false;
			// Now it's time for the first activation
			bIsFirstActivate = true;
		}

		// The form will be activated
		// UPGRADE_WARNING: Form event frmDefButton.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		private void frmDefButton_Activated(object eventSender, EventArgs eventArgs)
		{
			// Only by the first activation
			if (bIsFirstActivate)
			{
				// If there is no valid joystick
				if (!(modSpectrum.bJoystick1Valid | modSpectrum.bJoystick2Valid))
				{
					// Close window
					Close();
					return;
				}
				// Set label caption
				lblButton.Text = "Button " + Strings.Trim(lButton.ToString()) + " translated into:";
				// The first activation ends here
				bIsFirstActivate = false;
			}
		}

		// The form will be unloaded
		private void frmDefButton_FormClosed(object eventSender, FormClosedEventArgs eventArgs)
		{
			// Variables
			MsgBoxResult vbmrValue;
			// If there are unsaved data
			if (bEdit)
			{
				// Ask the user
				vbmrValue = Interaction.MsgBox("Do you want to save your changes?", (MsgBoxStyle)((int)MsgBoxStyle.Question + (int)MsgBoxStyle.YesNoCancel + (int)MsgBoxStyle.DefaultButton1));
				// Analyse
				switch (vbmrValue)
				{
					// Save changes and exit
					case MsgBoxResult.Yes:
						{
							// Save changes
							OKButton_Click(OKButton, new EventArgs());
							break;
						}
					// Do not save changes
					case MsgBoxResult.No:
						{
							// No saved data
							bOK = false;
							break;
						}
					// Cancel
					case MsgBoxResult.Cancel:
						{
							break;
						}
						// UPGRADE_ISSUE: Event parameter Cancel was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FB723E3C-1C06-4D2B-B083-E6CD0D334DA8"'
						// Cancel = 1
				}
			}
		}

		// Close and save
		private void OKButton_Click(object eventSender, EventArgs eventArgs)
		{
			// Save changes
			sButton = txtFire.Text;
			if (Information.IsNumeric(txtPortFire.Text))
			{
				lButtonPort = Conversions.ToInteger(txtPortFire.Text);
			}
			else
			{
				lButtonPort = 0;
			}

			if (Information.IsNumeric(txtValueFire.Text))
			{
				lButtonValue = Conversions.ToInteger(txtValueFire.Text);
			}
			else
			{
				lButtonValue = 0;
			}
			// Changes over
			bEdit = false;
			// Close window
			bOK = true;
			Close();
		}

		// Close without save
		private void CancelButton_Renamed_Click(object eventSender, EventArgs eventArgs)
		{
			bOK = false;
			Close();
		}

		// There was a change
		private void DefaultChange()
		{
			bEdit = true;
		}

		// Default focus hanlder
		private void DefaultFocusHanlder(ref TextBox oTextBox)
		{
			if (!string.IsNullOrEmpty(oTextBox.Text))
			{
				oTextBox.SelectionStart = 0;
				oTextBox.SelectionLength = Strings.Len(oTextBox.Text);
			}
		}

		// Define per keystroke
		private void txtFire_KeyDown(object eventSender, KeyEventArgs eventArgs)
		{
			short KeyCode = (short)eventArgs.KeyCode;
			short Shift = (short)((int)eventArgs.KeyData / 0x10000);
			if (!bPortValue)
			{
				bKey = true;
				modSpectrum.KeyStrokeToPortValue(KeyCode, Shift, ref sKey, ref lPort, ref lValue);
				txtFire.Text = sKey;
				txtPortFire.Text = Strings.Trim(lPort.ToString());
				txtValueFire.Text = Strings.Trim(lValue.ToString());
				bKey = false;
			}
		}
		// UPGRADE_WARNING: Event txtFire.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtFire_TextChanged(object eventSender, EventArgs eventArgs)
		{
			DefaultChange();
		}
		// UPGRADE_WARNING: Event txtPortFire.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtPortFire_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortFire.Text) & Information.IsNumeric(txtValueFire.Text)))
				{
					txtFire.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortFire.Text), Conversions.ToInteger(txtValueFire.Text), ref sKey);
				txtFire.Text = sKey;
				bPortValue = false;
			}

			DefaultChange();
		}
		// UPGRADE_WARNING: Event txtValueFire.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void txtValueFire_TextChanged(object eventSender, EventArgs eventArgs)
		{
			if (!bKey)
			{
				bPortValue = true;
				if (!(Information.IsNumeric(txtPortFire.Text) & Information.IsNumeric(txtValueFire.Text)))
				{
					txtFire.Text = Constants.vbNullString;
					return;
				}

				modSpectrum.PortValueToKeyStroke(Conversions.ToInteger(txtPortFire.Text), Conversions.ToInteger(txtValueFire.Text), ref sKey);
				txtFire.Text = sKey;
				bPortValue = false;
			}

			DefaultChange();
		}

		// Focus
		private void txtPortFire_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtPortFire;
			DefaultFocusHanlder(ref argoTextBox);
			txtPortFire = argoTextBox;
		}

		private void txtValueFire_Enter(object eventSender, EventArgs eventArgs)
		{
			var argoTextBox = txtValueFire;
			DefaultFocusHanlder(ref argoTextBox);
			txtValueFire = argoTextBox;
		}
	}
}