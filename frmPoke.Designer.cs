﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmPoke
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmPoke() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdPeek.Name = "cmdPeek";
			_cmdCancel.Name = "cmdCancel";
			_cmdPoke.Name = "cmdPoke";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _cmdPeek;

		public Button cmdPeek
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdPeek;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdPeek != null)
				{
					_cmdPeek.Click -= cmdPeek_Click;
				}

				_cmdPeek = value;
				if (_cmdPeek != null)
				{
					_cmdPeek.Click += cmdPeek_Click;
				}
			}
		}

		private Button _cmdCancel;

		public Button cmdCancel
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdCancel;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdCancel != null)
				{
					_cmdCancel.Click -= cmdCancel_Click;
				}

				_cmdCancel = value;
				if (_cmdCancel != null)
				{
					_cmdCancel.Click += cmdCancel_Click;
				}
			}
		}

		private Button _cmdPoke;

		public Button cmdPoke
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdPoke;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdPoke != null)
				{
					_cmdPoke.Click -= cmdPoke_Click;
				}

				_cmdPoke = value;
				if (_cmdPoke != null)
				{
					_cmdPoke.Click += cmdPoke_Click;
				}
			}
		}

		public TextBox txtValue;
		public TextBox txtAddress;
		public Label lblValue;
		public Label lblAddress;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmPoke));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_cmdPeek = new Button();
			_cmdPeek.Click += new EventHandler(cmdPeek_Click);
			_cmdCancel = new Button();
			_cmdCancel.Click += new EventHandler(cmdCancel_Click);
			_cmdPoke = new Button();
			_cmdPoke.Click += new EventHandler(cmdPoke_Click);
			txtValue = new TextBox();
			txtAddress = new TextBox();
			lblValue = new Label();
			lblAddress = new Label();
			SuspendLayout();
			ToolTip1.Active = true;
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "Poke Memory";
			ClientSize = new Size(312, 85);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmPoke";
			_cmdPeek.TextAlign = ContentAlignment.MiddleCenter;
			_cmdPeek.Text = "Peek";
			_cmdPeek.Size = new Size(109, 22);
			_cmdPeek.Location = new Point(196, 6);
			_cmdPeek.TabIndex = 6;
			_cmdPeek.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdPeek.BackColor = SystemColors.Control;
			_cmdPeek.CausesValidation = true;
			_cmdPeek.Enabled = true;
			_cmdPeek.ForeColor = SystemColors.ControlText;
			_cmdPeek.Cursor = Cursors.Default;
			_cmdPeek.RightToLeft = RightToLeft.No;
			_cmdPeek.TabStop = true;
			_cmdPeek.Name = "_cmdPeek";
			_cmdCancel.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _cmdCancel;
			_cmdCancel.Text = "&Close";
			_cmdCancel.Size = new Size(109, 22);
			_cmdCancel.Location = new Point(196, 54);
			_cmdCancel.TabIndex = 5;
			ToolTip1.SetToolTip(_cmdCancel, "This button closes the window (the others don't)");
			_cmdCancel.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdCancel.BackColor = SystemColors.Control;
			_cmdCancel.CausesValidation = true;
			_cmdCancel.Enabled = true;
			_cmdCancel.ForeColor = SystemColors.ControlText;
			_cmdCancel.Cursor = Cursors.Default;
			_cmdCancel.RightToLeft = RightToLeft.No;
			_cmdCancel.TabStop = true;
			_cmdCancel.Name = "_cmdCancel";
			_cmdPoke.TextAlign = ContentAlignment.MiddleCenter;
			_cmdPoke.Text = "&Poke";
			AcceptButton = _cmdPoke;
			_cmdPoke.Size = new Size(109, 22);
			_cmdPoke.Location = new Point(196, 30);
			_cmdPoke.TabIndex = 4;
			ToolTip1.SetToolTip(_cmdPoke, "The poke-operation will be completed and the controls reset.");
			_cmdPoke.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdPoke.BackColor = SystemColors.Control;
			_cmdPoke.CausesValidation = true;
			_cmdPoke.Enabled = true;
			_cmdPoke.ForeColor = SystemColors.ControlText;
			_cmdPoke.Cursor = Cursors.Default;
			_cmdPoke.RightToLeft = RightToLeft.No;
			_cmdPoke.TabStop = true;
			_cmdPoke.Name = "_cmdPoke";
			txtValue.AutoSize = false;
			txtValue.TextAlign = HorizontalAlignment.Right;
			txtValue.Size = new Size(87, 21);
			txtValue.Location = new Point(99, 29);
			txtValue.TabIndex = 3;
			ToolTip1.SetToolTip(txtValue, "Here comes the value that must be poked (0..255)");
			txtValue.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			txtValue.AcceptsReturn = true;
			txtValue.BackColor = SystemColors.Window;
			txtValue.CausesValidation = true;
			txtValue.Enabled = true;
			txtValue.ForeColor = SystemColors.WindowText;
			txtValue.HideSelection = true;
			txtValue.ReadOnly = false;
			txtValue.MaxLength = 0;
			txtValue.Cursor = Cursors.IBeam;
			txtValue.Multiline = false;
			txtValue.RightToLeft = RightToLeft.No;
			txtValue.ScrollBars = ScrollBars.None;
			txtValue.TabStop = true;
			txtValue.Visible = true;
			txtValue.BorderStyle = BorderStyle.Fixed3D;
			txtValue.Name = "txtValue";
			txtAddress.AutoSize = false;
			txtAddress.TextAlign = HorizontalAlignment.Right;
			txtAddress.Size = new Size(87, 21);
			txtAddress.Location = new Point(99, 6);
			txtAddress.TabIndex = 1;
			ToolTip1.SetToolTip(txtAddress, "Enter here a number greater than 16384 (Spectrum RAM)");
			txtAddress.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			txtAddress.AcceptsReturn = true;
			txtAddress.BackColor = SystemColors.Window;
			txtAddress.CausesValidation = true;
			txtAddress.Enabled = true;
			txtAddress.ForeColor = SystemColors.WindowText;
			txtAddress.HideSelection = true;
			txtAddress.ReadOnly = false;
			txtAddress.MaxLength = 0;
			txtAddress.Cursor = Cursors.IBeam;
			txtAddress.Multiline = false;
			txtAddress.RightToLeft = RightToLeft.No;
			txtAddress.ScrollBars = ScrollBars.None;
			txtAddress.TabStop = true;
			txtAddress.Visible = true;
			txtAddress.BorderStyle = BorderStyle.Fixed3D;
			txtAddress.Name = "txtAddress";
			lblValue.TextAlign = ContentAlignment.TopRight;
			lblValue.Text = "&Value";
			lblValue.Size = new Size(84, 13);
			lblValue.Location = new Point(9, 34);
			lblValue.TabIndex = 2;
			lblValue.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblValue.BackColor = SystemColors.Control;
			lblValue.Enabled = true;
			lblValue.ForeColor = SystemColors.ControlText;
			lblValue.Cursor = Cursors.Default;
			lblValue.RightToLeft = RightToLeft.No;
			lblValue.UseMnemonic = true;
			lblValue.Visible = true;
			lblValue.AutoSize = false;
			lblValue.BorderStyle = BorderStyle.None;
			lblValue.Name = "lblValue";
			lblAddress.TextAlign = ContentAlignment.TopRight;
			lblAddress.Text = "&Address";
			lblAddress.Size = new Size(84, 13);
			lblAddress.Location = new Point(9, 10);
			lblAddress.TabIndex = 0;
			lblAddress.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblAddress.BackColor = SystemColors.Control;
			lblAddress.Enabled = true;
			lblAddress.ForeColor = SystemColors.ControlText;
			lblAddress.Cursor = Cursors.Default;
			lblAddress.RightToLeft = RightToLeft.No;
			lblAddress.UseMnemonic = true;
			lblAddress.Visible = true;
			lblAddress.AutoSize = false;
			lblAddress.BorderStyle = BorderStyle.None;
			lblAddress.Name = "lblAddress";
			Controls.Add(_cmdPeek);
			Controls.Add(_cmdCancel);
			Controls.Add(_cmdPoke);
			Controls.Add(txtValue);
			Controls.Add(txtAddress);
			Controls.Add(lblValue);
			Controls.Add(lblAddress);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}