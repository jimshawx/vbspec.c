﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	static class modAY8912
	{
		// /*******************************************************************************
		// modAY8912.bas within vbSpec.vbp
		// 
		// Routines for emulating the 128K Spectrum's AY-3-8912 sound generator
		// 
		// Written using technical information and techniques gleaned from
		// the MAME AY-8910 module "src/sound/ay8910.c". See http://www.mame.net/
		// for further information.
		// 
		// This version of the code is offered under the GPL with the kind
		// permission of Nicola Salmoria and the MAME authors.
		// 
		// Original Visual Basic Port Written By:
		// James Bagg <chipmunk_uk_1@hotmail.com>
		// 
		// Minor VB-specific optimisations and mods by
		// Chris Cowley <ccowley@grok.co.uk>
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/

		public const int MAX_OUTPUT = 63;
		public const int AY_STEP = 32768;
		public const int MAXVOL = 0x1F;

		// // AY register ID's
		public const int AY_AFINE = 0;
		public const int AY_ACOARSE = 1;
		public const int AY_BFINE = 2;
		public const int AY_BCOARSE = 3;
		public const int AY_CFINE = 4;
		public const int AY_CCOARSE = 5;
		public const int AY_NOISEPER = 6;
		public const int AY_ENABLE = 7;
		public const int AY_AVOL = 8;
		public const int AY_BVOL = 9;
		public const int AY_CVOL = 10;
		public const int AY_EFINE = 11;
		public const int AY_ECOARSE = 12;
		public const int AY_ESHAPE = 13;
		public const int AY_PORTA = 14;
		public const int AY_PORTB = 15;

		// UPGRADE_WARNING: Arrays in structure AYPSG may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
		public static AY8912 AYPSG;

		public struct AY8912
		{
			public int sampleRate;
			public int register_latch;
			[VBFixedArray(16)]
			public int[] Regs;
			public double UpdateStep;
			public int PeriodA;
			public int PeriodB;
			public int PeriodC;
			public int PeriodN;
			public int PeriodE;
			public int CountA;
			public int CountB;
			public int CountC;
			public int CountN;
			public int CountE;
			public int VolA;
			public int VolB;
			public int VolC;
			public int VolE;
			public int EnvelopeA;
			public int EnvelopeB;
			public int EnvelopeC;
			public int OutputA;
			public int OutputB;
			public int OutputC;
			public int OutputN;
			public int CountEnv;
			public int Hold;
			public int Alternate;
			public int Attack;
			public int Holding;
			[VBFixedArray(64)]
			public int[] VolTable2;

			// UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
			public void Initialize()
			{
				Regs = new int[17];
				VolTable2 = new int[65];
			}
		}

		public static int AY_OutNoise;
		public static int VolB, VolA, VolC;
		private static int lOut2, lOut1, lOut3;
		public static int AY_Left;
		public static int AY_NextEvent;
		public static int Buffer_Length;

		public static void AY8912_reset()
		{
			int i;
			{
				AYPSG.register_latch = 0;
				AYPSG.OutputA = 0;
				AYPSG.OutputB = 0;
				AYPSG.OutputC = 0;
				AYPSG.OutputN = 0xFF;
				AYPSG.PeriodA = 0;
				AYPSG.PeriodB = 0;
				AYPSG.PeriodC = 0;
				AYPSG.PeriodN = 0;
				AYPSG.PeriodE = 0;
				AYPSG.CountA = 0;
				AYPSG.CountB = 0;
				AYPSG.CountC = 0;
				AYPSG.CountN = 0;
				AYPSG.CountE = 0;
				AYPSG.VolA = 0;
				AYPSG.VolB = 0;
				AYPSG.VolC = 0;
				AYPSG.VolE = 0;
				AYPSG.EnvelopeA = 0;
				AYPSG.EnvelopeB = 0;
				AYPSG.EnvelopeC = 0;
				AYPSG.CountEnv = 0;
				AYPSG.Hold = 0;
				AYPSG.Alternate = 0;
				AYPSG.Holding = 0;
				AYPSG.Attack = 0;
			}

			for (i = 0; i <= AY_PORTA; i++)
			{
				int argv = 0;
				AYWriteReg(ref i, ref argv);
			}
		}

		public static void AY8912_set_clock(ref double clock)
		{
			double t1;

			// // Calculate the number of AY_STEPs which happen during one sample
			// // at the given sample rate. No. of events = sample rate / (clock/8).
			// // AY_STEP is a multiplier used to turn the fraction into a fixed point
			// // number.
			t1 = AY_STEP * (double)AYPSG.sampleRate * 8;
			AYPSG.UpdateStep = t1 / clock;
		}


		// // AY8912_set_volume()
		// //
		// // Initialize the volume table
		public static void AY8912InitVolumeTable()
		{
			// // The following volume levels are taken from the sound.c & sound.h files
			// // in the FUSE emulator (suitably rescaled to 00-3F from 0000-FFFF) and
			// // apparently more accurately represent real volume levels as measured
			// // from a 128K Spectrum than the original algorithm used in previous
			// // versions of vbSpec.
			AYPSG.Initialize();
			AYPSG.VolTable2[0] = 0;
			AYPSG.VolTable2[1] = 0;
			AYPSG.VolTable2[2] = 1;
			AYPSG.VolTable2[3] = 1;
			AYPSG.VolTable2[4] = 1;
			AYPSG.VolTable2[5] = 1;
			AYPSG.VolTable2[6] = 2;
			AYPSG.VolTable2[7] = 2;
			AYPSG.VolTable2[8] = 3;
			AYPSG.VolTable2[9] = 3;
			AYPSG.VolTable2[10] = 4;
			AYPSG.VolTable2[11] = 4;
			AYPSG.VolTable2[12] = 5;
			AYPSG.VolTable2[13] = 5;
			AYPSG.VolTable2[14] = 9;
			AYPSG.VolTable2[15] = 9;
			AYPSG.VolTable2[16] = 11;
			AYPSG.VolTable2[17] = 11;
			AYPSG.VolTable2[18] = 17;
			AYPSG.VolTable2[19] = 17;
			AYPSG.VolTable2[20] = 23;
			AYPSG.VolTable2[21] = 23;
			AYPSG.VolTable2[22] = 29;
			AYPSG.VolTable2[23] = 29;
			AYPSG.VolTable2[24] = 37;
			AYPSG.VolTable2[25] = 37;
			AYPSG.VolTable2[26] = 44;
			AYPSG.VolTable2[27] = 44;
			AYPSG.VolTable2[28] = 54;
			AYPSG.VolTable2[29] = 54;
			AYPSG.VolTable2[30] = 63;
			AYPSG.VolTable2[31] = 63;
		}

		public static void AYWriteReg(ref int r, ref int v)
		{
			int old;
			AYPSG.Regs[r] = v;

			// // A note about the period of tones, noise and envelope: for speed reasons,
			// // we count down from the period to 0, but careful studies of the chip
			// // output prove that it instead counts up from 0 until the counter becomes
			// // greater or equal to the period. This is an important difference when the
			// // program is rapidly changing the period to modulate the sound.
			// // To compensate for the difference, when the period is changed we adjust
			// // our internal counter.
			// // Also, note that period = 0 is the same as period = 1. This is mentioned
			// // in the YM2203 data sheets. However, this does NOT apply to the Envelope
			// // period. In that case, period = 0 is half as period = 1.
			switch (r)
			{
				case AY_AFINE:
				case AY_ACOARSE:
					{
						AYPSG.Regs[AY_ACOARSE] = AYPSG.Regs[AY_ACOARSE] & 0xF;
						old = AYPSG.PeriodA;
						AYPSG.PeriodA = (int)((AYPSG.Regs[AY_AFINE] + 256 * AYPSG.Regs[AY_ACOARSE]) * AYPSG.UpdateStep);
						if (AYPSG.PeriodA == 0)
							AYPSG.PeriodA = (int)AYPSG.UpdateStep;
						AYPSG.CountA = AYPSG.CountA + (AYPSG.PeriodA - old);
						if (AYPSG.CountA <= 0)
							AYPSG.CountA = 1;
						break;
					}

				case AY_BFINE:
				case AY_BCOARSE:
					{
						AYPSG.Regs[AY_BCOARSE] = AYPSG.Regs[AY_BCOARSE] & 0xF;
						old = AYPSG.PeriodB;
						AYPSG.PeriodB = (int)((AYPSG.Regs[AY_BFINE] + 256 * AYPSG.Regs[AY_BCOARSE]) * AYPSG.UpdateStep);
						if (AYPSG.PeriodB == 0)
							AYPSG.PeriodB = (int)AYPSG.UpdateStep;
						AYPSG.CountB = AYPSG.CountB + AYPSG.PeriodB - old;
						if (AYPSG.CountB <= 0)
							AYPSG.CountB = 1;
						break;
					}

				case AY_CFINE:
				case AY_CCOARSE:
					{
						AYPSG.Regs[AY_CCOARSE] = AYPSG.Regs[AY_CCOARSE] & 0xF;
						old = AYPSG.PeriodC;
						AYPSG.PeriodC = (int)((AYPSG.Regs[AY_CFINE] + 256 * AYPSG.Regs[AY_CCOARSE]) * AYPSG.UpdateStep);
						if (AYPSG.PeriodC == 0)
							AYPSG.PeriodC = (int)AYPSG.UpdateStep;
						AYPSG.CountC = AYPSG.CountC + (AYPSG.PeriodC - old);
						if (AYPSG.CountC <= 0)
							AYPSG.CountC = 1;
						break;
					}

				case AY_NOISEPER:
					{
						AYPSG.Regs[AY_NOISEPER] = AYPSG.Regs[AY_NOISEPER] & 0x1F;
						old = AYPSG.PeriodN;
						AYPSG.PeriodN = (int)(AYPSG.Regs[AY_NOISEPER] * AYPSG.UpdateStep);
						if (AYPSG.PeriodN == 0)
							AYPSG.PeriodN = (int)AYPSG.UpdateStep;
						AYPSG.CountN = AYPSG.CountN + (AYPSG.PeriodN - old);
						if (AYPSG.CountN <= 0)
							AYPSG.CountN = 1;
						break;
					}

				case AY_AVOL:
					{
						AYPSG.Regs[AY_AVOL] = AYPSG.Regs[AY_AVOL] & 0x1F;
						AYPSG.EnvelopeA = AYPSG.Regs[AY_AVOL] & 0x10;
						if (AYPSG.EnvelopeA != 0)
						{
							AYPSG.VolA = AYPSG.VolE;
						}
						else if (AYPSG.Regs[AY_AVOL] != 0)
						{
							AYPSG.VolA = AYPSG.VolTable2[AYPSG.Regs[AY_AVOL] * 2 + 1];
						}
						else
						{
							AYPSG.VolA = AYPSG.VolTable2[0];
						}

						break;
					}

				case AY_BVOL:
					{
						AYPSG.Regs[AY_BVOL] = AYPSG.Regs[AY_BVOL] & 0x1F;
						AYPSG.EnvelopeB = AYPSG.Regs[AY_BVOL] & 0x10;
						if (AYPSG.EnvelopeB != 0)
						{
							AYPSG.VolB = AYPSG.VolE;
						}
						else if (AYPSG.Regs[AY_BVOL] != 0)
						{
							AYPSG.VolB = AYPSG.VolTable2[AYPSG.Regs[AY_BVOL] * 2 + 1];
						}
						else
						{
							AYPSG.VolB = AYPSG.VolTable2[0];
						}

						break;
					}

				case AY_CVOL:
					{
						AYPSG.Regs[AY_CVOL] = AYPSG.Regs[AY_CVOL] & 0x1F;
						AYPSG.EnvelopeC = AYPSG.Regs[AY_CVOL] & 0x10;
						if (AYPSG.EnvelopeC != 0)
						{
							AYPSG.VolC = AYPSG.VolE;
						}
						else if (AYPSG.Regs[AY_CVOL] != 0)
						{
							AYPSG.VolC = AYPSG.VolTable2[AYPSG.Regs[AY_CVOL] * 2 + 1];
						}
						else
						{
							AYPSG.VolC = AYPSG.VolTable2[0];
						}

						break;
					}

				case AY_EFINE:
				case AY_ECOARSE:
					{
						old = AYPSG.PeriodE;
						AYPSG.PeriodE = (int)((AYPSG.Regs[AY_EFINE] + 256 * AYPSG.Regs[AY_ECOARSE]) * AYPSG.UpdateStep);
						if (AYPSG.PeriodE == 0)
							AYPSG.PeriodE = (int)((long)AYPSG.UpdateStep / 2L);
						AYPSG.CountE = AYPSG.CountE + (AYPSG.PeriodE - old);
						if (AYPSG.CountE <= 0)
							AYPSG.CountE = 1;
						break;
					}

				case AY_ESHAPE:
					{
						// // envelope shapes:
						// // C AtAlH
						// // 0 0 x x  \___
						// //
						// // 0 1 x x  /___
						// //
						// // 1 0 0 0  \\\\
						// //
						// // 1 0 0 1  \___
						// //
						// // 1 0 1 0  \/\/
						// //          ___
						// // 1 0 1 1  \
						// //
						// // 1 1 0 0  ////
						// //           ___
						// // 1 1 0 1  /
						// //
						// // 1 1 1 0  /\/\
						// //
						// // 1 1 1 1  /___
						// //
						// // The envelope counter on the AY-3-8910 has 16 AY_STEPs. On the YM2149 it
						// // has twice the AY_STEPs, happening twice as fast. Since the end result is
						// // just a smoother curve, we always use the YM2149 behaviour.
						if (AYPSG.Regs[AY_ESHAPE] != 0xFF)
						{
							AYPSG.Regs[AY_ESHAPE] = AYPSG.Regs[AY_ESHAPE] & 0xF;
							if ((AYPSG.Regs[AY_ESHAPE] & 0x4) == 0x4)
							{
								AYPSG.Attack = MAXVOL;
							}
							else
							{
								AYPSG.Attack = 0x0;
							}

							AYPSG.Hold = AYPSG.Regs[AY_ESHAPE] & 0x1;
							AYPSG.Alternate = AYPSG.Regs[AY_ESHAPE] & 0x2;
							AYPSG.CountE = AYPSG.PeriodE;
							AYPSG.CountEnv = MAXVOL; // // &h1f
							AYPSG.Holding = 0;
							AYPSG.VolE = AYPSG.VolTable2[AYPSG.CountEnv ^ AYPSG.Attack];
							if (AYPSG.EnvelopeA != 0)
								AYPSG.VolA = AYPSG.VolE;
							if (AYPSG.EnvelopeB != 0)
								AYPSG.VolB = AYPSG.VolE;
							if (AYPSG.EnvelopeC != 0)
								AYPSG.VolC = AYPSG.VolE;
						}

						break;
					}
			}
		}

		public static int AY8912_init(ref double clock, ref int sample_rate, ref int sample_bits)
		{
			int AY8912_initRet = default;
			AYPSG.sampleRate = sample_rate;
			AY8912_set_clock(ref clock);
			AY8912InitVolumeTable();
			AY8912_reset();
			AY8912_initRet = 0;
			return AY8912_initRet;
		}

		public static void AY8912Update_8()
		{
			int Buffer_Length;
			Buffer_Length = 400;

			// // The 8910 has three outputs, each output is the mix of one of the three
			// // tone generators and of the (single) noise generator. The two are mixed
			// // BEFORE going into the DAC. The formula to mix each channel is:
			// // (ToneOn | ToneDisable) & (NoiseOn | NoiseDisable).
			// // Note that this means that if both tone and noise are disabled, the output
			// // is 1, not 0, and can be modulated changing the volume.

			// // If the channels are disabled, set their output to 1, and increase the
			// // counter, if necessary, so they will not be inverted during this update.
			// // Setting the output to 1 is necessary because a disabled channel is locked
			// // into the ON state (see above); and it has no effect if the volume is 0.
			// // If the volume is 0, increase the counter, but don't touch the output.
			if ((AYPSG.Regs[AY_ENABLE] & 0x1) == 0x1)
			{
				if (AYPSG.CountA <= Buffer_Length * AY_STEP)
					AYPSG.CountA = AYPSG.CountA + Buffer_Length * AY_STEP;
				AYPSG.OutputA = 1;
			}
			else if (AYPSG.Regs[AY_AVOL] == 0)
			{
				// // note that I do count += Buffer_Length, NOT count = Buffer_Length + 1. You might think
				// // it's the same since the volume is 0, but doing the latter could cause
				// // interference when the program is rapidly modulating the volume.
				if (AYPSG.CountA <= Buffer_Length * AY_STEP)
					AYPSG.CountA = AYPSG.CountA + Buffer_Length * AY_STEP;
			}

			if ((AYPSG.Regs[AY_ENABLE] & 0x2) == 0x2)
			{
				if (AYPSG.CountB <= Buffer_Length * AY_STEP)
					AYPSG.CountB = AYPSG.CountB + Buffer_Length * AY_STEP;
				AYPSG.OutputB = 1;
			}
			else if (AYPSG.Regs[AY_BVOL] == 0)
			{
				if (AYPSG.CountB <= Buffer_Length * AY_STEP)
					AYPSG.CountB = AYPSG.CountB + Buffer_Length * AY_STEP;
			}

			if ((AYPSG.Regs[AY_ENABLE] & 0x4) == 0x4)
			{
				if (AYPSG.CountC <= Buffer_Length * AY_STEP)
					AYPSG.CountC = AYPSG.CountC + Buffer_Length * AY_STEP;
				AYPSG.OutputC = 1;
			}
			else if (AYPSG.Regs[AY_CVOL] == 0)
			{
				if (AYPSG.CountC <= Buffer_Length * AY_STEP)
					AYPSG.CountC = AYPSG.CountC + Buffer_Length * AY_STEP;
			}

			// // for the noise channel we must not touch OutputN - it's also not necessary
			// //since we use AY_OutNoise.
			if ((AYPSG.Regs[AY_ENABLE] & 0x38) == 0x38) // // all off
			{
				if (AYPSG.CountN <= Buffer_Length * AY_STEP)
					AYPSG.CountN = AYPSG.CountN + Buffer_Length * AY_STEP;
			}

			AY_OutNoise = AYPSG.OutputN | AYPSG.Regs[AY_ENABLE];
		}

		public static int RenderByte()
		{
			int RenderByteRet = default;
			VolA = 0;
			VolB = 0;
			VolC = 0;

			// // VolA, VolB and VolC keep track of how long each square wave stays
			// // in the 1 position during the sample period.

			AY_Left = AY_STEP;
			do
			{
				AY_NextEvent = 0;
				if (AYPSG.CountN < AY_Left)
				{
					AY_NextEvent = AYPSG.CountN;
				}
				else
				{
					AY_NextEvent = AY_Left;
				}

				if ((AY_OutNoise & 0x8) == 0x8)
				{
					if (AYPSG.OutputA == 1)
						VolA = VolA + AYPSG.CountA;
					AYPSG.CountA = AYPSG.CountA - AY_NextEvent;
					// // PeriodA is the half period of the square wave. Here, in each
					// // loop I add PeriodA twice, so that at the end of the loop the
					// // square wave is in the same status (0 or 1) it was at the start.
					// // vola is also incremented by PeriodA, since the wave has been 1
					// // exactly half of the time, regardless of the initial position.
					// // If we exit the loop in the middle, OutputA has to be inverted
					// // and vola incremented only if the exit status of the square
					// // wave is 1.

					while (AYPSG.CountA <= 0)
					{
						AYPSG.CountA = AYPSG.CountA + AYPSG.PeriodA;
						if (AYPSG.CountA > 0)
						{
							if ((AYPSG.Regs[AY_ENABLE] & 1) == 0)
								AYPSG.OutputA = AYPSG.OutputA ^ 1;
							if (Conversions.ToBoolean(AYPSG.OutputA))
								VolA = VolA + AYPSG.PeriodA;
							break;
						}

						AYPSG.CountA = AYPSG.CountA + AYPSG.PeriodA;
						VolA = VolA + AYPSG.PeriodA;
					}

					if (AYPSG.OutputA == 1)
						VolA = VolA - AYPSG.CountA;
				}
				else
				{
					AYPSG.CountA = AYPSG.CountA - AY_NextEvent;
					while (AYPSG.CountA <= 0)
					{
						AYPSG.CountA = AYPSG.CountA + AYPSG.PeriodA;
						if (AYPSG.CountA > 0)
						{
							AYPSG.OutputA = AYPSG.OutputA ^ 1;
							break;
						}

						AYPSG.CountA = AYPSG.CountA + AYPSG.PeriodA;
					}
				}

				if ((AY_OutNoise & 0x10) == 0x10)
				{
					if (AYPSG.OutputB == 1)
						VolB = VolB + AYPSG.CountB;
					AYPSG.CountB = AYPSG.CountB - AY_NextEvent;
					while (AYPSG.CountB <= 0)
					{
						AYPSG.CountB = AYPSG.CountB + AYPSG.PeriodB;
						if (AYPSG.CountB > 0)
						{
							if ((AYPSG.Regs[AY_ENABLE] & 2) == 0)
								AYPSG.OutputB = AYPSG.OutputB ^ 1;
							if (Conversions.ToBoolean(AYPSG.OutputB))
								VolB = VolB + AYPSG.PeriodB;
							break;
						}

						AYPSG.CountB = AYPSG.CountB + AYPSG.PeriodB;
						VolB = VolB + AYPSG.PeriodB;
					}

					if (AYPSG.OutputB == 1)
						VolB = VolB - AYPSG.CountB;
				}
				else
				{
					AYPSG.CountB = AYPSG.CountB - AY_NextEvent;
					while (AYPSG.CountB <= 0)
					{
						AYPSG.CountB = AYPSG.CountB + AYPSG.PeriodB;
						if (AYPSG.CountB > 0)
						{
							AYPSG.OutputB = AYPSG.OutputB ^ 1;
							break;
						}

						AYPSG.CountB = AYPSG.CountB + AYPSG.PeriodB;
					}
				}

				if ((AY_OutNoise & 0x20) == 0x20)
				{
					if (AYPSG.OutputC == 1)
						VolC = VolC + AYPSG.CountC;
					AYPSG.CountC = AYPSG.CountC - AY_NextEvent;
					while (AYPSG.CountC <= 0)
					{
						AYPSG.CountC = AYPSG.CountC + AYPSG.PeriodC;
						if (AYPSG.CountC > 0)
						{
							if ((AYPSG.Regs[AY_ENABLE] & 4) == 0)
								AYPSG.OutputC = AYPSG.OutputC ^ 1;
							if (Conversions.ToBoolean(AYPSG.OutputC))
								VolC = VolC + AYPSG.PeriodC;
							break;
						}

						AYPSG.CountC = AYPSG.CountC + AYPSG.PeriodC;
						VolC = VolC + AYPSG.PeriodC;
					}

					if (AYPSG.OutputC == 1)
						VolC = VolC - AYPSG.CountC;
				}
				else
				{
					AYPSG.CountC = AYPSG.CountC - AY_NextEvent;
					while (AYPSG.CountC <= 0)
					{
						AYPSG.CountC = AYPSG.CountC + AYPSG.PeriodC;
						if (AYPSG.CountC > 0)
						{
							AYPSG.OutputC = AYPSG.OutputC ^ 1;
							break;
						}

						AYPSG.CountC = AYPSG.CountC + AYPSG.PeriodC;
					}
				}

				AYPSG.CountN = AYPSG.CountN - AY_NextEvent;
				if (AYPSG.CountN <= 0)
				{
					// // Is noise output going to change?
					AYPSG.OutputN = (int)(Conversion.Int(VBMath.Rnd(1f) * 2f) * 255f);
					AY_OutNoise = AYPSG.OutputN | AYPSG.Regs[AY_ENABLE];
					AYPSG.CountN = AYPSG.CountN + AYPSG.PeriodN;
				}

				AY_Left = AY_Left - AY_NextEvent;
			}
			while (AY_Left > 0);
			if (AYPSG.Holding == 0)
			{
				AYPSG.CountE = AYPSG.CountE - AY_STEP;
				if (AYPSG.CountE <= 0)
				{
					do
					{
						AYPSG.CountEnv = AYPSG.CountEnv - 1;
						AYPSG.CountE = AYPSG.CountE + AYPSG.PeriodE;
					}
					while (AYPSG.CountE <= 0);

					// // check envelope current position
					if (AYPSG.CountEnv < 0)
					{
						if (Conversions.ToBoolean(AYPSG.Hold))
						{
							if (Conversions.ToBoolean(AYPSG.Alternate))
							{
								AYPSG.Attack = AYPSG.Attack ^ MAXVOL; // &h1f
							}

							AYPSG.Holding = 1;
							AYPSG.CountEnv = 0;
						}
						else
						{
							// // if CountEnv has looped an odd number of times (usually 1),
							// // invert the output.
							if (Conversions.ToBoolean(AYPSG.Alternate & Conversions.ToInteger((AYPSG.CountEnv & 0x20) == 0x20)))
							{
								AYPSG.Attack = AYPSG.Attack ^ MAXVOL; // // &h1f
							}

							AYPSG.CountEnv = AYPSG.CountEnv & MAXVOL;
						} // // &h1f
					}

					AYPSG.VolE = AYPSG.VolTable2[AYPSG.CountEnv ^ AYPSG.Attack];
					// // reload volume
					if (AYPSG.EnvelopeA != 0)
						AYPSG.VolA = AYPSG.VolE;
					if (AYPSG.EnvelopeB != 0)
						AYPSG.VolB = AYPSG.VolE;
					if (AYPSG.EnvelopeC != 0)
						AYPSG.VolC = AYPSG.VolE;
				}
			}

			lOut1 = VolA * AYPSG.VolA / 65535;
			lOut2 = VolB * AYPSG.VolB / 65535;
			lOut3 = VolC * AYPSG.VolC / 65535;
			RenderByteRet = lOut1 + lOut2 + lOut3;
			return RenderByteRet;
		}
	}
}