﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using VB = Microsoft.VisualBasic;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	static class modMain
	{
		// /*******************************************************************************
		// modMain.bas within vbSpec.vbp
		// 
		// Public variable declarations, startup and initialization code,
		// and routines for loading the ROM and snapshots
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2002 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		// // Pretty much all variables are declared as long, even those that only hold boolean
		// // values or byte values. This is done for performance, because VB handles longs more
		// // efficiently than any other type.

		public static int glTstatesPerInterrupt;
		public static int[] Parity = new int[257];
		public static int glInterruptTimer;

		// // Memory haindling variables for 128K spectrum emulation
		public static int glMemPagingType; // // 0 = No mem paging (48K speccy)
										   // // 1 = 128K/+2 paging available
										   // // 2 = 128K/+2 and +2A/+3 special paging available
		public static byte[,] gRAMPage = new byte[12, 16384]; // // (pages 0 to 7 are RAM, and pages 8 to 11 are ROM)
		public static int[] glPageAt = new int[5];
		public static int glLastOut7FFD;
		public static int glLastOut1FFD;
		public static int glUseScreen;
		public static int glEmulatedModel;
		public static int gbEmulateAYSound;
		public static int[] glContentionTable = new int[70961];
		public static int[] glTSToScanLine = new int[70931];
		public static int gbTextOut;
		public static int glTextLineLen;
		// Public gbInInput As Long

		// // TAP/TZX file parameters
		public static string gsTAPFileName;
		public static int ghTAPFile;
		public static int gbSoundEnabled;
		public static int glSoundRegister; // // Contains currently indexed AY-3-8912 sound register
		public static int glBufNum; // // ID of the last Wave buffer used
		public static int glKeyPortMask; // // Mask used for reading keyboard port (&HBF on Speccys, &H1F on TC2048)

		// // Main Z80 registers //
		public static int regA;
		public static int regHL;
		public static int regB;
		public static int regC;
		public static int regDE;

		// // Z80 Flags
		public static int fS;
		public static int fZ;
		public static int f5;
		public static int fH;
		public static int f3;
		public static int fPV;
		public static int fN;
		public static int fC;

		// // Flag bit positions
		public const int F_C = 1;
		public const int F_N = 2;
		public const int F_PV = 4;
		public const int F_3 = 8;
		public const int F_H = 16;
		public const int F_5 = 32;
		public const int F_Z = 64;
		public const int F_S = 128;

		// // Alternate registers //
		public static int regAF_;
		public static int regHL_;
		public static int regBC_;
		public static int regDE_;

		// // Index registers  - ID used as temp for ix/iy
		public static int regIX;
		public static int regIY;
		public static int regID;

		// // Stack pointer and program counter
		public static int regSP;
		public static int regPC;

		// // Interrupt registers and flip-flops, and refresh registers
		public static int intI;
		public static int intR;
		public static int intRTemp;
		public static int intIFF1;
		public static int intIFF2;
		public static int intIM;

		// //////////////////////////////////////////////////
		// // Variables used by the video display rountines
		// //////////////////////////////////////////////////
		public static int[,] ScrnLines = new int[192, 66]; // // 192 scanlines (0-191) and either 32 bytes per line or 64 in TC2048 hires mode, plus two flag bytes
		public static int ScrnNeedRepaint; // // Set to true when an area of the display changes, and back to false by the ScreenPaint function
		public static int bFlashInverse; // // Cycles between true/false to indicate the status of 'flashing' attributes
		public static int[,] glScreenMem = new int[192, 32]; // // Static lookup table that maps Y,X screen coords to the correct Speccy memory address
		public static int[,] glScreenMemTC2048HiRes = new int[192, 64]; // // As above, but for the TC2048 512x192 display mode
		public static int glTC2048HiResColour; // // The attribute value for the TC2048 512x192, two-colour display mode
		public static int glTC2048LastFFOut; // // Contains the last value OUTed to port FF in TC2048 mode (indicates the screen mode in use)
		public static int glLastFEOut; // // Contains the last value OUTed to any port with bit 0 reset (saved in snapshots, etc)
		public static int glTopMost; // // Top-most row of the screen that has changed since the last ScreenPaint
		public static int glBottomMost; // // Bottom-most row of the screen that has changed since the last ScreenPaint
		public static int glLeftMost; // // Left-most column of the screen that has changed since the last ScreenPaint
		public static int glRightMost; // // Right-most column "  "    "      "   "     "      "    "   "         "

		// // Bob Woodring's (RGW) video performance improvements use the following lookup tables
		public static int[] glRowIndex = new int[192];
		public static int[] glColIndex = new int[192];

		public struct tBitTable
		{
			public int dw0;
			public int dw1;
		}

		public static tBitTable[,] gtBitTable = new tBitTable[256, 256];
		public static int[] glMemAddrDiv16384 = new int[81920]; // // Lookup table used in pokeb() & elsewhere
		public static int[] glMemAddrDiv256 = new int[81920]; // // Lookup table used by pokeb() - faster
		public static int[] glMemAddrDiv32 = new int[81920]; // // Lookup table used by pokeb() -  than
		public static int[] glMemAddrDiv4 = new int[81920]; // // Lookup table used by pokeb() -   integer division!


		// // RGW -- Variables used by scanline video routines
		public static int glTStatesPerLine; // // Contains the # of T-states per display line (different on 48K and 128K spectrums)
		public static int glTStatesAtTop; // // # of t-states before the start of the first screen line (excluding border)
		public static int glTStatesAtBottom; // // # of t-states after the end of the last screen line (excluding border)
		public static int glTStates; // // Number of T-States for current frame (counts down towards zero, at which time an interrupt occurs)
		public static int glContendedMemoryDelay; // // Contains number of tstates added due to memory contention for the current opcode

		// // Array of colour values (speeds up screen painting by avoiding
		// // multiple calls to RGB() )
		public static int[] glBrightColor = new int[8];
		public static int[] glNormalColor = new int[8];

		// // Global picDisplay variable to speed things up
		public static PictureBox gpicDisplay;
		public static IntPtr gpicDC;
		public static Graphics gpicGraphics;

		// // Interrupts/Screen refreshing
		public static int interruptCounter;
		public static int glInterruptDelay;
		public static int glDelayOverage;

		// // Keypresses
		public static int keyB_SPC;
		public static int keyH_ENT;
		public static int keyY_P;
		public static int key6_0;
		public static int key1_5;
		public static int keyQ_T;
		public static int keyA_G;
		public static int keyCAPS_V;

		// // Sadly, I needed to use these high res timer functions to precisely control the
		// // speed of emulation. I had hoped to do this without resorting to any API calls :(
		[DllImport("winmm.dll")]
		public static extern int timeGetTime();
		[DllImport("winmm.dll")]
		public static extern int timeBeginPeriod(int uPeriod);
		[DllImport("winmm.dll")]
		public static extern int timeEndPeriod(int uPeriod);
		[DllImport("kernel32")]
		public static extern void Sleep(int dwMilliseconds);

		// // If built with the USE_WINAPI compiler directive defined, vbSpec uses Windows API
		// // functions to paint the display. This is faster than raw VB code, and provides the
		// // option for selecting scaled display sizes (double and triple size, and so on).
		public const int SRCCOPY = 0xCC0020; // (DWORD) dest = source
											 // UPGRADE_WARNING: Structure BITMAPINFO may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
											 // UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
		[DllImport("gdi32")]
		public static extern int StretchDIBits(IntPtr hdc, int XDest, int YDest, int nDestWidth, int nDestHeight, int XSrc, int YSrc, int nSrcWidth, int nSrcHeight, object lpBits, [In] ref BITMAPINFO lpBitsInfo, uint iUsage, uint dwRop);
		[DllImport("user32")]
		public static extern int GetSystemMetrics(int nIndex);
		// UPGRADE_WARNING: Structure BITMAPINFOMONO may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		// UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
		//[DllImport("gdi32", EntryPoint = "StretchDIBits")]
		//public static extern int StretchDIBitsMono(int hdc, int X, int y, int dx, int dy, int SrcX, int SrcY, int wSrcWidth, int wSrcHeight, object lpBits, BITMAPINFOMONO lpBitsInfo, int wUsage, int dwRop);

		public const int SM_CYCAPTION = 4;
		public const int SM_CYMENU = 15;
		public const int SM_CXFRAME = 32;
		public const int SM_CYFRAME = 33;

		[DllImport("gdi32.dll", EntryPoint = "BitBlt", SetLastError = true)]
		public static extern bool BitBlt([In] IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, [In] IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

		[DllImport("gdi32")]
		public static extern int SetPixelV(int hdc, int X, int y, int crColor);

		public struct BITMAPINFOHEADER // 40 bytes
		{
			public int biSize;
			public int biWidth;
			public int biHeight;
			public short biPlanes;
			public short biBitCount;
			public int biCompression;
			public int biSizeImage;
			public int biXPelsPerMeter;
			public int biYPelsPerMeter;
			public int biClrUsed;
			public int biClrImportant;
		}

		public struct RGBQUAD
		{
			public byte rgbBlue;
			public byte rgbGreen;
			public byte rgbRed;
			public byte rgbReserved;
		}

		public struct BITMAPINFO
		{
			public BITMAPINFOHEADER bmiHeader;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
			[VBFixedArray(16)]
			public RGBQUAD[] bmiColors;

			// UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
			public void Initialize()
			{
				bmiColors = new RGBQUAD[17];
			}
		}

		public struct BITMAPINFOMONO
		{
			public BITMAPINFOHEADER bmiHeader;
			[VBFixedArray(2)]
			public RGBQUAD[] bmiColors;

			// UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
			public void Initialize()
			{
				bmiColors = new RGBQUAD[3];
			}
		}

		public const short BI_RGB = 0;
		public const uint DIB_RGB_COLORS = 0; // color table in RGBs

		// UPGRADE_WARNING: Arrays in structure bmiBuffer may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
		public static BITMAPINFO bmiBuffer;
		public static int[] glBufferBits = new int[24577];
		public static int glDisplayHeight;
		public static int glDisplayWidth;
		public static int glDisplayVSource; // // Set to glDisplayHeight - 1 to improve display speed
		public static int glDisplayVSize; // // Set to -glDisplayHeight to improve display speed
		public static int glDisplayXMultiplier;
		public static int glDisplayYMultiplier;

		// // Used by the ZX Printer emulation
		// UPGRADE_WARNING: Arrays in structure bmiZXPrinter may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
		public static BITMAPINFOMONO bmiZXPrinter;
		public static byte[] gcZXPrinterBits; // // 32 cols * 1152 rows
		public static int glZXPrinterBMPHeight;
		public static int glBeeperVal;

		// // ShellExecute is used for the clickable web URL in the "About..." dialog, not
		// //the actual emulator itself
		[DllImport("shell32.dll", EntryPoint = "ShellExecuteA")]
		public static extern int ShellExecute(int hWnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);

		// // MouseCapture functions required for emulating the Kempston Mouse Interface
		public struct POINTAPI
		{
			public int X;
			public int y;
		}
		// UPGRADE_WARNING: Structure POINTAPI may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern int GetCursorPos(out POINTAPI lpPoint);
		[DllImport("user32")]
		public static extern short GetKeyState(int nVirtKey);

		public const int VK_LBUTTON = 0x1;
		public const int VK_RBUTTON = 0x2;
		public const int VK_MBUTTON = 0x4;
		public static int glMouseType;
		public const short MOUSE_NONE = 0;
		public const short MOUSE_KEMPSTON = 1;
		public const short MOUSE_AMIGA = 2;
		public static int gbMouseGlobal;
		public static int glMouseBtn;

		// // Most Recently Used (MRU) file class
		public static MRUList gMRU;

		// // Flag whether SE BASIC ROM is to be used or not
		public static int gbSEBasicROM;

		// MM 16.04.2003
		public struct RECT
		{
			public short iLeft;
			public short iTop;
			public short iRight;
			public short iBottom;
		}

		public struct POINT
		{
			public int lX;
			public int lY;
		}
		// UPGRADE_WARNING: Structure RECT may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("user32")]
		public static extern void GetClientRect(IntPtr hWnd, out RECT lpRect);
		// UPGRADE_WARNING: Structure POINT may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("user32")]
		public static extern void ClientToScreen(IntPtr hWnd, out POINT lpPoint);
		[DllImport("user32")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		private static int CompressMemoryBlock(ref int lBlock, ref string sData)
		{
			int CompressMemoryBlockRet = default;
			int i = default, lLength;
			var bLastWasED = default(bool);
			byte cRepChar;
			while (i < 16384)
			{
				// // The last byte, just write it out
				if (i == 16383)
				{
					sData = sData + (char)gRAMPage[lBlock, i];
					break;
				}

				if (gRAMPage[lBlock, i] == gRAMPage[lBlock, i + 1] & !bLastWasED)
				{
					// // It's a run of bytes and we're not immediately following an ED
					cRepChar = gRAMPage[lBlock, i];
					i = i + 2;
					lLength = 2;
					bLastWasED = false;

					// // Find the length of the run (but cap it at 255 bytes)
					while (i < 16384 & gRAMPage[lBlock, i] == cRepChar & lLength < 255)
					{
						lLength = lLength + 1;
						i = i + 1;
					}

					if (lLength >= 5 | cRepChar == 0xED)
					{
						sData = sData + 'í' + 'í' + (char)lLength + (char)cRepChar;
					}
					else
					{
						// // Not compressible, just write out the raw data
						sData = sData + new string((char)cRepChar, lLength);
					}
				}
				else
				{
					// // Not a run of bytes
					cRepChar = gRAMPage[lBlock, i];
					sData = sData + (char)cRepChar;
					if (cRepChar == 0xED)
						bLastWasED = true;
					else
						bLastWasED = false;
					i = i + 1;
				}
			}

			if (Strings.Len(sData) > 16384)
			{
				// // If the compressed block is longer than the original
				// // just store the uncompressed version
				sData = "";
				for (i = 0; i <= 16383; i++)
					sData = sData + (char)gRAMPage[lBlock, i];
				CompressMemoryBlockRet = 65535; // // block is uncompressed
			}
			else
			{
				CompressMemoryBlockRet = Strings.Len(sData);
			}

			return CompressMemoryBlockRet;
		}

		private static void InitAmigaMouseTables()
		{
			modSpectrum.glAmigaMouseX[0] = 5; // 0101
			modSpectrum.glAmigaMouseX[1] = 1; // 0001
			modSpectrum.glAmigaMouseX[2] = 0; // 0000
			modSpectrum.glAmigaMouseX[3] = 4; // 0100
			modSpectrum.glAmigaMouseY[0] = 10; // 1010
			modSpectrum.glAmigaMouseY[1] = 8; // 1000
			modSpectrum.glAmigaMouseY[2] = 0; // 0000
			modSpectrum.glAmigaMouseY[3] = 2; // 0010
		}

		public static void InitScreenIndexs()
		{
			int n;
			for (n = 0; n <= 191; n++)
			{
				glRowIndex[n] = 6144 + n / 8 * 32;
				glColIndex[n] = n * 256 / 4;
			}

			for (n = 0; n <= 81919; n++)
			{
				glMemAddrDiv16384[n] = (n & 65535) / 16384;
				glMemAddrDiv256[n] = (n & 65535) / 256;
				glMemAddrDiv32[n] = (n & 65535) / 32;
				glMemAddrDiv4[n] = (n & 65535) / 4;
			}
		}

		public static void CloseWaveOut()
		{
			int lRet;
			lRet = modWaveOut.waveOutReset(modWaveOut.glphWaveOut);
			for (lRet = 1; lRet <= modWaveOut.NUM_WAV_BUFFERS; lRet++)
				modWaveOut.waveOutUnprepareHeader(modWaveOut.glphWaveOut, ref modWaveOut.gtWavHdr[lRet], Strings.Len(modWaveOut.gtWavHdr[lRet]));
			for (lRet = 1; lRet <= modWaveOut.NUM_WAV_BUFFERS; lRet++)
			{
				modWaveOut.GlobalUnlock(modWaveOut.ghMem[lRet]);
				modWaveOut.GlobalFree(modWaveOut.ghMem[lRet]);
			}

			modWaveOut.waveOutClose(modWaveOut.glphWaveOut);
			gbSoundEnabled = Conversions.ToInteger(false);
		}

		public static void CreateScreenBuffer()
		{
			bmiBuffer.bmiHeader.biSize = Strings.Len(bmiBuffer.bmiHeader);
			bmiBuffer.bmiHeader.biWidth = 256;
			bmiBuffer.bmiHeader.biHeight = 192;
			bmiBuffer.bmiHeader.biPlanes = 1;
			bmiBuffer.bmiHeader.biBitCount = 8;
			bmiBuffer.bmiHeader.biCompression = BI_RGB;
			bmiBuffer.bmiHeader.biSizeImage = 0;
			bmiBuffer.bmiHeader.biXPelsPerMeter = 200;
			bmiBuffer.bmiHeader.biYPelsPerMeter = 200;
			bmiBuffer.bmiHeader.biClrUsed = 16;
			bmiBuffer.bmiHeader.biClrImportant = 16;
		}

		private static int GetBorderIndex(ref int lRGB)
		{
			int GetBorderIndexRet = default;
			int lCounter;
			for (lCounter = 0; lCounter <= 7; lCounter++)
			{
				if (glNormalColor[lCounter] == lRGB)
				{
					GetBorderIndexRet = lCounter;
				}
			}

			return GetBorderIndexRet;
		}

		public static string GetFilePart(string sFileName)
		{
			string GetFilePartRet = default;
			if (Strings.InStr(sFileName, ":") > 0)
				sFileName = Strings.Mid(sFileName, Strings.InStr(sFileName, ":") + 1);
			while (Strings.InStr(sFileName, @"\") > 0)
				sFileName = Strings.Mid(sFileName, Strings.InStr(sFileName, @"\") + 1);
			GetFilePartRet = sFileName;
			return GetFilePartRet;
		}

		public static bool InitializeWaveOut()
		{
			bool InitializeWaveOutRet = default;
			int lCounter, lRet, lCounter2;
			string sErrMsg;
			if (Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "SoundEnabled", 1.ToString())) == 0d)
			{
				InitializeWaveOutRet = false;
				return InitializeWaveOutRet;
			}

			glBeeperVal = 128;
			{
				modWaveOut.gtWavFormat.wFormatTag = modWaveOut.WAVE_FORMAT_PCM;
				modWaveOut.gtWavFormat.nChannels = 1;
				modWaveOut.gtWavFormat.nSamplesPerSec = modWaveOut.WAVE_FREQUENCY;
				modWaveOut.gtWavFormat.nAvgBytesPerSec = modWaveOut.WAVE_FREQUENCY;
				modWaveOut.gtWavFormat.nBlockAlign = 1;
				modWaveOut.gtWavFormat.wBitsPerSample = 8;
				modWaveOut.gtWavFormat.cbSize = 0;
			}
			int size = Marshal.SizeOf(modWaveOut.gtWavFormat);

			lRet = modWaveOut.waveOutOpen(ref modWaveOut.glphWaveOut, modWaveOut.WAVE_MAPPER, ref modWaveOut.gtWavFormat, IntPtr.Zero, IntPtr.Zero, modWaveOut.CALLBACK_NULL);
			if (lRet != modWaveOut.MMSYSERR_NOERROR)
			{
				sErrMsg = Strings.Space(255);
				modWaveOut.waveOutGetErrorText(lRet, sErrMsg, Strings.Len(sErrMsg));
				sErrMsg = Strings.Left(sErrMsg, Strings.InStr(sErrMsg, Conversions.ToString('\0')) - 1);
				Interaction.MsgBox("Error initialising WaveOut device." + Constants.vbCrLf + Constants.vbCrLf + sErrMsg, MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "vbSpec");
				InitializeWaveOutRet = false;
				return InitializeWaveOutRet;
			}

			for (lCounter = 1; lCounter <= modWaveOut.NUM_WAV_BUFFERS; lCounter++)
			{
				modWaveOut.ghMem[lCounter] = modWaveOut.GlobalAlloc(Conversions.ToInteger(modWaveOut.GPTR), modWaveOut.WAV_BUFFER_SIZE);
				modWaveOut.gpMem[lCounter] = modWaveOut.GlobalLock(modWaveOut.ghMem[lCounter]);
				{
					modWaveOut.gtWavHdr[lCounter].lpData = modWaveOut.gpMem[lCounter];
					modWaveOut.gtWavHdr[lCounter].dwBufferLength = modWaveOut.WAV_BUFFER_SIZE;
					modWaveOut.gtWavHdr[lCounter].dwUser = 0;
					modWaveOut.gtWavHdr[lCounter].dwFlags = 0;
					modWaveOut.gtWavHdr[lCounter].dwLoops = 0;
					modWaveOut.gtWavHdr[lCounter].lpNext = 0;
				}

				lRet = modWaveOut.waveOutPrepareHeader(modWaveOut.glphWaveOut, ref modWaveOut.gtWavHdr[lCounter], Strings.Len(modWaveOut.gtWavHdr[lCounter]));
				if (lRet != modWaveOut.MMSYSERR_NOERROR)
				{
					sErrMsg = Strings.Space(255);
					modWaveOut.waveOutGetErrorText(lRet, sErrMsg, Strings.Len(sErrMsg));
					sErrMsg = Strings.Left(sErrMsg, Strings.InStr(sErrMsg, Conversions.ToString('\0')) - 1);
					Interaction.MsgBox("Error preparing wave header." + Constants.vbCrLf + Constants.vbCrLf + sErrMsg, MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "vbSpec");
					lRet = modWaveOut.waveOutClose(modWaveOut.glphWaveOut);
					for (lCounter2 = 1; lCounter2 <= modWaveOut.NUM_WAV_BUFFERS; lCounter2++)
					{
						modWaveOut.GlobalUnlock(modWaveOut.ghMem[lCounter2]);
						modWaveOut.GlobalFree(modWaveOut.ghMem[lCounter2]);
					}

					InitializeWaveOutRet = false;
					return InitializeWaveOutRet;
				}
			}

			for (lCounter = 0; lCounter <= 48000; lCounter++)
				modWaveOut.gcWaveOut[lCounter] = (byte)glBeeperVal;
			InitializeWaveOutRet = true;
			return InitializeWaveOutRet;
		}

		public static void initParity()
		{
			int lCounter;
			byte j;
			bool p;
			for (lCounter = 0; lCounter <= 255; lCounter++)
			{
				p = true;
				for (j = 0; j <= 7; j++)
				{
					if ((lCounter & (long)Math.Pow(2d, j)) != 0L)
						p = !p;
				}

				Parity[lCounter] = Conversions.ToInteger(p);
			}
		}

		public static void initscreen()
		{
			int i, X;
			glTopMost = 0;
			glBottomMost = 191;
			glLeftMost = 0;
			glRightMost = 31;
			for (i = 0; i <= 191; i++)
			{
				for (X = 0; X <= 64; X++)
					ScrnLines[i, X] = Conversions.ToInteger(true);
			}

			ScrnNeedRepaint = Conversions.ToInteger(true);
		}

		public static void InitScreenMemTable()
		{
			int X, y;
			for (y = 0; y <= 191; y++)
			{
				for (X = 0; X <= 31; X++)
				{
					glScreenMem[y, X] = y / 8 * 32 + y % 8 * 256 + y / 64 * 2048 - y / 64 * 256 + X;
					glScreenMemTC2048HiRes[y, X * 2] = y / 8 * 32 + y % 8 * 256 + y / 64 * 2048 - y / 64 * 256 + X;
					glScreenMemTC2048HiRes[y, X * 2 + 1] = glScreenMemTC2048HiRes[y, X * 2] + 8192;
				}
			}
		}

		public static byte Asc(string s)
		{
			return (byte)s[0];
		}
		public static byte Asc(char s)
		{
			return (byte)s;
		}

		public static string InputString16(int hFile, int lCounter)
		{
			var sb = new StringBuilder();
			int i;
			var b = default(byte);
			string rv;
			var loopTo = lCounter - 1;
			for (i = 0; i <= loopTo; i++)
			{
				if (FileSystem.EOF(hFile))
					break;
				FileSystem.FileGet(hFile, ref b);
				sb.Append((char)b);
			}

			rv = sb.ToString();
			return rv;
			// Return InputString(hFile, lCounter)
		}

		public static void LoadROM([Optional, DefaultParameterValue("spectrum.rom")] ref string sROMFile, [Optional, DefaultParameterValue(8)] ref int lROMPage)
		{
			int hFile, lCounter;
			string sROM;
			;

			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (string.IsNullOrEmpty(FileSystem.Dir(sROMFile)))
			{
				Interaction.MsgBox("The ROM image file '" + sROMFile + "' could not be found.", MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly);
				return;
			}

			Information.Err().Clear();
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sROMFile, OpenMode.Binary);
			if (Information.Err().Number != 0)
			{
				Interaction.MsgBox("Unable to open ROM image file: " + sROMFile, MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly);
				FileSystem.FileClose(hFile);
				return;
			}

			// // Read the ROM image into sROM
			Information.Err().Clear();
			sROM = InputString16(hFile, 16384);
			FileSystem.FileClose(hFile);
			if (Information.Err().Number != 0)
			{
				Interaction.MsgBox("An error ocurred whilst reading the ROM image file: " + sROMFile, MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly);
				return;
			}

			// // Copy the ROM into the appropriate memory page
			for (lCounter = 1; lCounter <= 16384; lCounter++)
				gRAMPage[lROMPage, lCounter - 1] = (byte)modMain.Asc(Strings.Mid(sROM, lCounter, 1));
			resetKeyboard();
		}

		public static void LoadScreenSCR(ref string sFileName)
		{
			int n, hFile, m;
			string sData;
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Binary);
			// // 6912 - Standard Screen
			// // 6144+6144 = HiColour TC2048
			// // 6144+6145 = HiRes TC2048

			if (FileSystem.LOF(hFile) == 6912L)
			{
				sData = InputString16(hFile, 6912);
				if (glUseScreen == 1001)
				{
					// // TC2048 Screen1
					for (n = 0; n <= 6911; n++)
						gRAMPage[5, n + 8192] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
				}
				else if (glUseScreen == 1002)
				{
					// // TC2048 HiColour

					// // Copy the mono bitmap
					for (n = 0; n <= 6143; n++)
						gRAMPage[5, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
					// // Then expand the normal 768 attributes into the 6144 of the hi-colour mode
					for (n = 0; n <= 255; n++)
					{
						for (m = 0; m <= 7; m++)
						{
							gRAMPage[5, n + 8192 + m * 256] = (byte)modMain.Asc(Strings.Mid(sData, n + 6145, 1));
							gRAMPage[5, n + 10240 + m * 256] = (byte)modMain.Asc(Strings.Mid(sData, n + 6401, 1));
							gRAMPage[5, n + 12288 + m * 256] = (byte)modMain.Asc(Strings.Mid(sData, n + 6657, 1));
						}
					}
				}
				else if (glUseScreen == 1006)
				{
					// // TC2048 -- We're in hires mode, but we just copy the screen in as usual
					for (n = 0; n <= 6911; n++)
						gRAMPage[5, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
				}
				else
				{
					for (n = 0; n <= 6911; n++)
						gRAMPage[glUseScreen, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
				}
			}
			else if (FileSystem.LOF(hFile) == 12288L)
			{
				sData = InputString16(hFile, 12288);
				// // This is a TC2048 HiColour screen
				if (glUseScreen != 1002)
				{
					if (Interaction.MsgBox("This file contains a TC2048 high-colour screen, which does not match the current display mode." + Constants.vbCrLf + Constants.vbCrLf + "Load it anyway?", MsgBoxStyle.YesNo | MsgBoxStyle.DefaultButton1 | MsgBoxStyle.Question, "vbSpec") == MsgBoxResult.No)
						return;
				}

				if (glUseScreen >= 1000)
				{
					for (n = 0; n <= 6143; n++)
					{
						gRAMPage[5, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
						gRAMPage[5, n + 8192] = (byte)modMain.Asc(Strings.Mid(sData, n + 6145, 1));
					}
				}
				else
				{
					for (n = 0; n <= 6143; n++)
					{
						gRAMPage[glUseScreen, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
						gRAMPage[glUseScreen, n + 8192] = (byte)modMain.Asc(Strings.Mid(sData, n + 6145, 1));
					}
				}

				if (glEmulatedModel == 5)
				{
					int argport = 255;
					int argoutbyte = 2;
					modSpectrum.outb(argport, argoutbyte);
				}
			}
			else if (FileSystem.LOF(hFile) == 12289L)
			{
				sData = InputString16(hFile, 12289);
				// // This is a TC2048 HiRes screen
				if (glUseScreen != 1006)
				{
					if (Interaction.MsgBox("This file contains a TC2048 high-resolution screen, which does not match the current display mode." + Constants.vbCrLf + Constants.vbCrLf + "Load it anyway?", MsgBoxStyle.YesNo | MsgBoxStyle.DefaultButton1 | MsgBoxStyle.Question, "vbSpec") == MsgBoxResult.No)
						return;
				}

				if (glUseScreen >= 1000)
				{
					for (n = 0; n <= 6143; n++)
					{
						gRAMPage[5, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
						gRAMPage[5, n + 8192] = (byte)modMain.Asc(Strings.Mid(sData, n + 6145, 1));
					}
				}
				else
				{
					for (n = 0; n <= 6143; n++)
					{
						gRAMPage[glUseScreen, n] = (byte)modMain.Asc(Strings.Mid(sData, n + 1, 1));
						gRAMPage[glUseScreen, n + 8192] = (byte)modMain.Asc(Strings.Mid(sData, n + 6145, 1));
					}
				}

				if (glEmulatedModel == 5)
				{
					int argport1 = 255;
					int argoutbyte1 = modMain.Asc(Strings.Right(sData, 1));
					modSpectrum.outb(argport1, argoutbyte1);
				}
			}
			else
			{
				// // Invalid SCR length
				FileSystem.FileClose(hFile);
				Interaction.MsgBox("This file does not contain a valid ZX Spectrum or TC2048 screen image.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "vbSpec");
				return;
			}

			FileSystem.FileClose(hFile);
			initscreen();
			modSpectrum.screenPaint();
			resetKeyboard();
		}

		private static void LoadSNA128Snap(ref int hFile)
		{
			string sData, sTemp;
			int lBank, lOut7FFD, lCounter;

			// // Read first three banks
			sData = InputString16(hFile, 49152);

			// // PC
			sTemp = InputString16(hFile, 2);
			regPC = modMain.Asc(Strings.Right(sTemp, 1)) * 256 + modMain.Asc(Strings.Left(sTemp, 1));

			// // Last out to 0x7FFD
			lOut7FFD = modMain.Asc(InputString16(hFile, 1));

			// // Is TR-DOS paged? (ignored by vbSpec)
			sTemp = InputString16(hFile, 1);

			// Setup first three banks
			for (lCounter = 0; lCounter <= 16383; lCounter++)
			{
				gRAMPage[5, lCounter] = (byte)modMain.Asc(Strings.Mid(sData, lCounter + 1, 1));
				gRAMPage[2, lCounter] = (byte)modMain.Asc(Strings.Mid(sData, lCounter + 16385, 1));
				gRAMPage[lOut7FFD & 7, lCounter] = (byte)modMain.Asc(Strings.Mid(sData, lCounter + 32769, 1));
			}

			lBank = 0;
			while (lBank < 8)
			{
				sData = InputString16(hFile, 16384);
				if (lBank == 5 | lBank == 2 | lBank == (lOut7FFD & 7))
				{
					lBank = lBank + 1;
				}

				if (lBank < 8)
				{
					for (lCounter = 0; lCounter <= 16383; lCounter++)
						gRAMPage[lBank, lCounter] = (byte)modMain.Asc(Strings.Mid(sData, lCounter + 1, 1));
					lBank = lBank + 1;
				}
			}

			modSpectrum.glLastBorder = -1;
			int argport = 0x7FFD;
			modSpectrum.outb(argport, lOut7FFD);
		}

		public static void LoadSNASnap(ref string sFileName)
		{
			int hFile, iCounter;
			string sData;
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Binary);
			sData = InputString16(hFile, 1);
			intI = modMain.Asc(sData);
			sData = InputString16(hFile, 2);
			regHL_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			regDE_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			regBC_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			regAF_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			regHL = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			regDE = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			int argnn = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			modZ80.setBC(argnn);
			sData = InputString16(hFile, 2);
			regIY = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 2);
			regIX = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			modSpectrum.glLastBorder = -1;
			sData = InputString16(hFile, 1);
			if (Conversions.ToBoolean(modMain.Asc(sData) & 4))
			{
				intIFF1 = Conversions.ToInteger(true);
				intIFF2 = Conversions.ToInteger(true);
			}
			else
			{
				intIFF1 = Conversions.ToInteger(false);
				intIFF2 = Conversions.ToInteger(false);
			}

			sData = InputString16(hFile, 1);
			intR = modMain.Asc(sData);
			intRTemp = intR;
			sData = InputString16(hFile, 2);
			int argv = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			modZ80.setAF(argv);
			sData = InputString16(hFile, 2);
			regSP = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			sData = InputString16(hFile, 1);
			intIM = modMain.Asc(sData);
			sData = InputString16(hFile, 1);
			// // Border color
			modSpectrum.glNewBorder = modMain.Asc(sData) & 0x7;
			if (FileSystem.LOF(hFile) > 49180L)
			{
				LoadSNA128Snap(ref hFile);
				FileSystem.FileClose(hFile);
				initscreen();

				// // Set the initial border color
				My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(glNormalColor[modSpectrum.glNewBorder]);
				if (glEmulatedModel == 0 | glEmulatedModel == 5)
				{
					int arglModel = 1;
					SetEmulatedModel(ref arglModel, ref gbSEBasicROM);
				}

				My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName + " - " + GetFilePart(sFileName);
				modSpectrum.screenPaint();
				resetKeyboard();
			}
			else
			{
				// // Load a 48K Snapshot file
				Information.Err().Clear();
				sData = InputString16(hFile, 49153);
				FileSystem.FileClose(hFile);
				for (iCounter = 0; iCounter <= 16383; iCounter++)
				{
					gRAMPage[5, iCounter] = (byte)modMain.Asc(Strings.Mid(sData, iCounter + 1, 1));
					gRAMPage[1, iCounter] = (byte)modMain.Asc(Strings.Mid(sData, iCounter + 16385, 1));
					gRAMPage[2, iCounter] = (byte)modMain.Asc(Strings.Mid(sData, iCounter + 32769, 1));
				}

				initscreen();

				// // Set the initial border color
				My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(glNormalColor[modSpectrum.glNewBorder]);

				// // if not a 48K speccy or a TC2048, then emulate a 48K
				if (glEmulatedModel != 0 & glEmulatedModel != 5)
				{
					int arglModel1 = 0;
					SetEmulatedModel(ref arglModel1, ref gbSEBasicROM);
				}
				else if (glEmulatedModel == 5)
				{
					// // If we're on a TC2048, ensure we're using the speccy-compatible screen mode
					int argport = 255;
					int argoutbyte = 0;
					modSpectrum.outb(argport, argoutbyte);
				}

				// MM 16.04.2003
				My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName + " - " + GetFilePart(sFileName);
				modSpectrum.screenPaint();
				resetKeyboard();
				modZ80.poppc();
			}
		}

		public static void LoadZ80Snap(ref string sFileName)
		{
			int hFile, iCounter;
			string sData;
			var bCompressed = default(bool);
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Binary);
			glPageAt[0] = 8;
			glPageAt[1] = 5;
			glPageAt[2] = 1;
			glPageAt[3] = 2;
			glPageAt[4] = 8;
			modSpectrum.glLastBorder = -1;
			modZ80.Z80Reset();
			if (Conversions.ToBoolean(gbSoundEnabled))
				modAY8912.AY8912_reset();

			// byte 0 - A register
			sData = InputString16(hFile, 1);
			regA = modMain.Asc(sData);
			// byte 1 - F register
			sData = InputString16(hFile, 1);
			int argb = modMain.Asc(sData);
			modZ80.setF(argb);
			// bytes 2 + 3 - BC register pair (C first, then B)
			sData = InputString16(hFile, 2);
			int argnn = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			modZ80.setBC(argnn);
			// bytes 4 + 5 - HL register pair
			sData = InputString16(hFile, 2);
			regHL = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// bytes 6 + 7 - PC (this is zero for v2.x or v3.0 Z80 files)
			sData = InputString16(hFile, 2);
			regPC = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// bytes 8 + 9 - SP
			sData = InputString16(hFile, 2);
			regSP = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// byte 10 - Interrupt register
			sData = InputString16(hFile, 1);
			intI = modMain.Asc(sData);
			// byte 11 - Refresh register
			sData = InputString16(hFile, 1);
			intR = modMain.Asc(sData) & 127;

			// byte 12 - bitfield
			sData = InputString16(hFile, 1);
			// if byte 12 = 255 then it must be treated as if it = 1, for compatibility with other emulators
			if (modMain.Asc(sData) == 255)
				sData = Conversions.ToString('\u0001');
			// bit 0 - bit 7 of R
			if ((modMain.Asc(sData) & 1) == 1)
				intR = intR | 128;
			intRTemp = intR;
			// bits 1,2 and 3 - border color
			modSpectrum.glNewBorder = (modMain.Asc(sData) & 14) / 2;
			// bit 4 - 1 if SamROM switched in (we don't care about this!)
			// bit 5 - if 1 and PC<>0 then the snapshot is compressed using the
			// rudimentary Z80 run-length encoding scheme
			if (Conversions.ToBoolean(modMain.Asc(sData) & 0x20))
				bCompressed = true;
			// bits 6 + 7 - no meaning

			// bytes 13 + 14 - DE register pair
			sData = InputString16(hFile, 2);
			regDE = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// bytes 15 + 16 - BC' register pair
			sData = InputString16(hFile, 2);
			regBC_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// bytes 17 + 18 - DE' register pair
			sData = InputString16(hFile, 2);
			regDE_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// bytes 19 + 20 - HL' register pair
			sData = InputString16(hFile, 2);
			regHL_ = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// bytes 21 + 22 - AF' register pair (A first then F - not Z80 byte order!!)
			sData = InputString16(hFile, 2);
			regAF_ = modMain.Asc(Strings.Left(sData, 1)) * 256 + modMain.Asc(Strings.Right(sData, 1));
			// byte 23 + 24 - IY register pair
			sData = InputString16(hFile, 2);
			regIY = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// byte 25 + 26 - IX register pair
			sData = InputString16(hFile, 2);
			regIX = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
			// byte 27 - Interrupt flipflop (0=DI, else EI)
			sData = InputString16(hFile, 1);
			if (modMain.Asc(sData) == 0)
			{
				intIFF1 = Conversions.ToInteger(false);
				intIFF2 = Conversions.ToInteger(false);
			}
			else
			{
				intIFF1 = Conversions.ToInteger(true);
				intIFF2 = Conversions.ToInteger(true);
			}
			// byte 28 - IFF2 (ignored)
			sData = InputString16(hFile, 1);
			// byte 29 - Interrupt mode (bits 2 - 7 contain info about joystick modes etc, which we ignore)
			sData = InputString16(hFile, 1);
			intIM = modMain.Asc(sData) & 3;
			if (regPC == 0)
			{
				// This is a V2 or V3 Z80 file
				ReadZ80V2orV3Snap(ref hFile);
				FileSystem.FileClose(hFile);
			}
			else
			{
				// // V1 .Z80 snapshots are all 48K

				// // if not a 48K speccy or a TC2048, then emulate a 48K
				if (glEmulatedModel != 0 & glEmulatedModel != 5)
				{
					int arglModel = 0;
					SetEmulatedModel(ref arglModel, ref gbSEBasicROM);
				}
				else if (glEmulatedModel == 5)
				{
					// // If we're on a TC2048, ensure we're using the speccy-compatible screen mode
					int argport = 255;
					int argoutbyte = 0;
					modSpectrum.outb(argport, argoutbyte);
				}
				// // PC<>0, so lets check to see if this is a compressed V1 Z80 file
				if (bCompressed)
				{
					// Uncompress the RAM data
					ReadZ80V1Snap(ref hFile);
					FileSystem.FileClose(hFile);
				}
				else
				{
					// // Uncompressed Z80 file
					sData = InputString16(hFile, 49153);
					FileSystem.FileClose(hFile);

					// // Copy the RAM data to addressable memory space
					for (iCounter = 16384; iCounter <= 65535; iCounter++)
						gRAMPage[glPageAt[iCounter / 16384], iCounter & 16383] = (byte)modMain.Asc(Strings.Mid(sData, iCounter - 16383, 1));
				}
			}

			initscreen();

			// // Set the initial border color
			switch (modSpectrum.glNewBorder)
			{
				case 0:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(0);
						break;
					}

				case 1:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(0, 0, 192));
						break;
					}

				case 2:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(192, 0, 0));
						break;
					}

				case 3:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(192, 0, 192));
						break;
					}

				case 4:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(0, 192, 0));
						break;
					}

				case 5:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(0, 192, 192));
						break;
					}

				case 6:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(192, 192, 0));
						break;
					}

				case 7:
					{
						My.MyProject.Forms.frmMainWnd.BackColor = ColorTranslator.FromOle(Information.RGB(192, 192, 192));
						break;
					}
			}

			My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName + " - " + GetFilePart(sFileName);
			initscreen();
			modSpectrum.screenPaint();
			resetKeyboard();
			gpicDisplay.Refresh();
			Application.DoEvents();
		}

		[STAThread]
		// UPGRADE_WARNING: Application will terminate when Sub Main() finishes. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="E08DDC71-66BA-424F-A612-80AF11498FF8"'
		public static void Main()
		{
			InitColorArrays();
			InitScreenMemTable();
			modSpectrum.InitReverseBitValues(); // // Used by the ZX Printer emulation - see modSpectrum

			// // RGW's performance improvements
			InitScreenMask();
			InitScreenIndexs();
			My.MyProject.Forms.frmMainWnd.Show();
			gpicDisplay = My.MyProject.Forms.frmMainWnd.picDisplay;
			CreateScreenBuffer();
			glDisplayWidth = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "DisplayWidth", "256"));
			glDisplayHeight = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "DisplayHeight", "192"));
			modSpectrum.bFullScreen = false; // CBool(GetSetting("Grok", "vbSpec", "FullScreen", Trim(CStr(CShort(False)))))
			SetDisplaySize(ref glDisplayWidth, ref glDisplayHeight);
			double argclock = 1773000d;
			int argsample_rate = modWaveOut.WAVE_FREQUENCY;
			int argsample_bits = 8;
			modAY8912.AY8912_init(ref argclock, ref argsample_rate, ref argsample_bits);
			glInterruptDelay = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "InterruptDelay", "20"));
			if (Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "TapeControlsVisible", "0")) != 0d)
			{
				VB.Compatibility.VB6.Support.ShowForm(My.MyProject.Forms.frmTapePlayer, 0, My.MyProject.Forms.frmMainWnd);
				My.MyProject.Forms.frmMainWnd.mnuOptions[4].Checked = true;
			}

			if (Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "EmulateZXPrinter", "0")) != 0d)
			{
				VB.Compatibility.VB6.Support.ShowForm(My.MyProject.Forms.frmZXPrinter, 0, My.MyProject.Forms.frmMainWnd);
				My.MyProject.Forms.frmMainWnd.mnuOptions[5].Checked = true;
			}
			// // Make sure the main window has focus
			My.MyProject.Forms.frmMainWnd.Activate();

			// // Initialize everything
			initParity();
			modZ80.Z80Reset();
			if (Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "SEBasicROM", "0")) != 0d)
			{
				gbSEBasicROM = Conversions.ToInteger(true);
			}
			else
			{
				gbSEBasicROM = Conversions.ToInteger(false);
			}

			int arglModel = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "EmulatedModel", "0"));
			SetEmulatedModel(ref arglModel, ref gbSEBasicROM);

			// // Emulated Mouse support (MOUSE_NONE by default)
			glMouseType = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "MouseType", MOUSE_NONE.ToString()));
			gbMouseGlobal = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "MouseGlobal", "0"));
			if (glMouseType == MOUSE_NONE)
			{
				My.MyProject.Forms.frmMainWnd.picDisplay.Cursor = Cursors.Default;
			}
			else
			{
				// UPGRADE_ISSUE: PictureBox property picDisplay.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
				// frmMainWnd.picDisplay.Cursor = vbCustom
			}

			InitAmigaMouseTables();
			initscreen();
			resetKeyboard();
			timeBeginPeriod(1);

			// // If we have a command line parameter, try to open it as a snapshot/tape/rom
			if (!string.IsNullOrEmpty(Interaction.Command()))
			{
				string argsName = Interaction.Command();
				My.MyProject.Forms.frmMainWnd.FileOpenDialog(ref argsName);
			}

			glInterruptTimer = timeGetTime();
			gbSoundEnabled = Conversions.ToInteger(InitializeWaveOut());

			// // Begin the Z80 execution loop, this drives the whole emulation
			modZ80.execute();
		}

		public static void InitScreenMask()
		{
			// RGW Prefill the screen color & attribute lookup table
			// with all possible combinations
			// When drawing the screen in the bit buffer
			// a simple lookup produces the required bytes

			int fC; // fore color
			int BC; // back color
			int Bright;
			int Flash;
			int bits;
			var Color = new int[2];
			int lTemp;
			for (Flash = 0; Flash <= 1; Flash++)
			{
				for (Bright = 0; Bright <= 1; Bright++)
				{
					for (fC = 0; fC <= 7; fC++)
					{
						for (BC = 0; BC <= 7; BC++)
						{
							for (bits = 0; bits <= 255; bits++)
							{
								if (Flash == 0)
								{
									Color[1] = fC + Bright * 8;
									Color[0] = BC + Bright * 8;
								}
								else
								{
									Color[1] = BC + Bright * 8;
									Color[0] = fC + Bright * 8;
								}

								lTemp = Flash * 128 + Bright * 64 + BC * 8 + fC;
								gtBitTable[bits, lTemp].dw0 = Color[Math.Abs(Conversions.ToInteger((bits & 16) == 16))] * 16777216 + Color[Math.Abs(Conversions.ToInteger((bits & 32) == 32))] * 65536 + Color[Math.Abs(Conversions.ToInteger((bits & 64) == 64))] * 256 + Color[Math.Abs(Conversions.ToInteger((bits & 128) == 128))];
								gtBitTable[bits, lTemp].dw1 = Color[Math.Abs(Conversions.ToInteger((bits & 1) == 1))] * 16777216 + Color[Math.Abs(Conversions.ToInteger((bits & 2) == 2))] * 65536 + Color[Math.Abs(Conversions.ToInteger((bits & 4) == 4))] * 256 + Color[Math.Abs(Conversions.ToInteger((bits & 8) == 8))];
							}
						}
					}
				}
			}
		}

		public static void InitColorArrays()
		{
			bmiBuffer.Initialize();
			bmiBuffer.bmiColors[0].rgbRed = 0;
			bmiBuffer.bmiColors[0].rgbGreen = 0;
			bmiBuffer.bmiColors[0].rgbBlue = 0;
			bmiBuffer.bmiColors[0].rgbReserved = 255;
			bmiBuffer.bmiColors[1].rgbRed = 0;
			bmiBuffer.bmiColors[1].rgbGreen = 0;
			bmiBuffer.bmiColors[1].rgbBlue = 192;
			bmiBuffer.bmiColors[1].rgbReserved = 255;
			bmiBuffer.bmiColors[2].rgbRed = 192;
			bmiBuffer.bmiColors[2].rgbGreen = 0;
			bmiBuffer.bmiColors[2].rgbBlue = 0;
			bmiBuffer.bmiColors[2].rgbReserved = 255;
			bmiBuffer.bmiColors[3].rgbRed = 192;
			bmiBuffer.bmiColors[3].rgbGreen = 0;
			bmiBuffer.bmiColors[3].rgbBlue = 192;
			bmiBuffer.bmiColors[3].rgbReserved = 255;
			bmiBuffer.bmiColors[4].rgbRed = 0;
			bmiBuffer.bmiColors[4].rgbGreen = 192;
			bmiBuffer.bmiColors[4].rgbBlue = 0;
			bmiBuffer.bmiColors[4].rgbReserved = 255;
			bmiBuffer.bmiColors[5].rgbRed = 0;
			bmiBuffer.bmiColors[5].rgbGreen = 192;
			bmiBuffer.bmiColors[5].rgbBlue = 192;
			bmiBuffer.bmiColors[5].rgbReserved = 255;
			bmiBuffer.bmiColors[6].rgbRed = 192;
			bmiBuffer.bmiColors[6].rgbGreen = 192;
			bmiBuffer.bmiColors[6].rgbBlue = 0;
			bmiBuffer.bmiColors[6].rgbReserved = 255;
			bmiBuffer.bmiColors[7].rgbRed = 192;
			bmiBuffer.bmiColors[7].rgbGreen = 192;
			bmiBuffer.bmiColors[7].rgbBlue = 192;
			bmiBuffer.bmiColors[7].rgbReserved = 255;
			bmiBuffer.bmiColors[8].rgbRed = 0;
			bmiBuffer.bmiColors[8].rgbGreen = 0;
			bmiBuffer.bmiColors[8].rgbBlue = 0;
			bmiBuffer.bmiColors[8].rgbReserved = 255;
			bmiBuffer.bmiColors[9].rgbRed = 0;
			bmiBuffer.bmiColors[9].rgbGreen = 0;
			bmiBuffer.bmiColors[9].rgbBlue = 255;
			bmiBuffer.bmiColors[9].rgbReserved = 255;
			bmiBuffer.bmiColors[10].rgbRed = 255;
			bmiBuffer.bmiColors[10].rgbGreen = 0;
			bmiBuffer.bmiColors[10].rgbBlue = 0;
			bmiBuffer.bmiColors[10].rgbReserved = 255;
			bmiBuffer.bmiColors[11].rgbRed = 255;
			bmiBuffer.bmiColors[11].rgbGreen = 0;
			bmiBuffer.bmiColors[11].rgbBlue = 255;
			bmiBuffer.bmiColors[11].rgbReserved = 255;
			bmiBuffer.bmiColors[12].rgbRed = 0;
			bmiBuffer.bmiColors[12].rgbGreen = 255;
			bmiBuffer.bmiColors[12].rgbBlue = 0;
			bmiBuffer.bmiColors[12].rgbReserved = 255;
			bmiBuffer.bmiColors[13].rgbRed = 0;
			bmiBuffer.bmiColors[13].rgbGreen = 255;
			bmiBuffer.bmiColors[13].rgbBlue = 255;
			bmiBuffer.bmiColors[13].rgbReserved = 255;
			bmiBuffer.bmiColors[14].rgbRed = 255;
			bmiBuffer.bmiColors[14].rgbGreen = 255;
			bmiBuffer.bmiColors[14].rgbBlue = 0;
			bmiBuffer.bmiColors[14].rgbReserved = 255;
			bmiBuffer.bmiColors[15].rgbRed = 255;
			bmiBuffer.bmiColors[15].rgbGreen = 255;
			bmiBuffer.bmiColors[15].rgbBlue = 255;
			bmiBuffer.bmiColors[15].rgbReserved = 255;
			glBrightColor[0] = 0;
			glBrightColor[1] = Information.RGB(0, 0, 255);
			glBrightColor[2] = Information.RGB(255, 0, 0);
			glBrightColor[3] = Information.RGB(255, 0, 255);
			glBrightColor[4] = Information.RGB(0, 255, 0);
			glBrightColor[5] = Information.RGB(0, 255, 255);
			glBrightColor[6] = Information.RGB(255, 255, 0);
			glBrightColor[7] = Information.RGB(255, 255, 255);
			glNormalColor[0] = 0;
			glNormalColor[1] = Information.RGB(0, 0, 192);
			glNormalColor[2] = Information.RGB(192, 0, 0);
			glNormalColor[3] = Information.RGB(192, 0, 192);
			glNormalColor[4] = Information.RGB(0, 192, 0);
			glNormalColor[5] = Information.RGB(0, 192, 192);
			glNormalColor[6] = Information.RGB(192, 192, 0);
			glNormalColor[7] = Information.RGB(192, 192, 192);
		}

		private static void ReadZ80V1Snap(ref int hFile)
		{
			int lDataLen, lBlockLen;
			string sData;
			int lMemPos, lCounter, lBlockCounter;
			lDataLen = (int)(FileSystem.LOF(hFile) - FileSystem.Seek(hFile) + 1L);
			// // read the compressed data into sData
			sData = InputString16(hFile, lDataLen);

			// // Uncompress the block to memory
			lCounter = 1;
			lMemPos = 16384;
			do
			{
				if (modMain.Asc(Strings.Mid(sData, lCounter, 1)) == 0xED)
				{
					if (modMain.Asc(Strings.Mid(sData, lCounter + 1, 1)) == 0xED)
					{
						// // This is an encoded block
						lCounter = lCounter + 2;
						lBlockLen = modMain.Asc(Strings.Mid(sData, lCounter, 1));
						lCounter = lCounter + 1;
						var loopTo = lBlockLen;
						for (lBlockCounter = 1; lBlockCounter <= loopTo; lBlockCounter++)
						{
							gRAMPage[glPageAt[lMemPos / 16384], lMemPos & 16383] = (byte)modMain.Asc(Strings.Mid(sData, lCounter, 1));
							lMemPos = lMemPos + 1;
						}
					}
					else
					{
						// // Just a single ED, write it out
						gRAMPage[glPageAt[lMemPos / 16384], lMemPos & 16383] = 0xED;
						lMemPos = lMemPos + 1;
					}
				}
				else
				{
					gRAMPage[glPageAt[lMemPos / 16384], lMemPos & 16383] = (byte)modMain.Asc(Strings.Mid(sData, lCounter, 1));
					lMemPos = lMemPos + 1;
				}

				lCounter = lCounter + 1;
			}
			while (lCounter <= Strings.Len(sData) - 4);
			if ((Strings.Mid(sData, lCounter, 4) ?? "") != (Conversions.ToString('\0') + 'í' + 'í' + '\0' ?? ""))
			{
				Interaction.MsgBox("Error in compressed Z80 file. Block end marker 0x00EDED00 is not present.");
			}
		}

		private static void ReadZ80V2orV3Snap(ref int hFile)
		{
			int lHeaderLen;
			string sData;
			var lCounter = default(int);
			bool bHardwareSupported;
			int lBlockCounter, lMemPage = default, lMemPos;
			var b128K = default(bool);
			var lOutFFFD = default(int);
			var bTimex = default(bool);
			sData = InputString16(hFile, 2);
			lHeaderLen = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));

			// // offset 32 - 2 bytes - PC
			if (lCounter < lHeaderLen)
			{
				sData = InputString16(hFile, 2);
				regPC = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
				lCounter = lCounter + 2;
			}

			// // offset 34 - 1 byte - hardware mode
			if (lCounter < lHeaderLen)
			{
				sData = InputString16(hFile, 1);
				switch (modMain.Asc(sData))
				{
					case 0: // // 48K spectrum
						{
							bHardwareSupported = true;
							// // if not currently emulating a 48K speccy or a TC2048, then emulate a 48K
							if (glEmulatedModel != 0 & glEmulatedModel != 5)
							{
								int arglModel = 0;
								SetEmulatedModel(ref arglModel, ref gbSEBasicROM);
							}
							else if (glEmulatedModel == 5)
							{
								// // If we're on a TC2048, ensure we're using the speccy-compatible screen mode
								int argport = 255;
								int argoutbyte = 0;
								modSpectrum.outb(argport, argoutbyte);
							}

							break;
						}

					case 1: // // 48K spectrum + Interface 1
						{
							bHardwareSupported = true;
							int arglModel1 = 0;
							SetEmulatedModel(ref arglModel1, ref gbSEBasicROM);
							break;
						}

					case 2: // // SamROM
						{
							sData = "SamROM";
							bHardwareSupported = false;
							break;
						}

					case 3:
						{
							if (lHeaderLen == 23)
							{
								sData = "128K Spectrum";
								bHardwareSupported = true;
								b128K = true;
								if (glEmulatedModel == 0 | glEmulatedModel == 5)
								{
									int arglModel2 = 1;
									SetEmulatedModel(ref arglModel2, ref gbSEBasicROM);
								}
							}
							else
							{
								sData = "48K Spectrum + M.G.T.";
								bHardwareSupported = true;
								int arglModel3 = 0;
								SetEmulatedModel(ref arglModel3, ref gbSEBasicROM);
							}

							break;
						}

					case 4:
						{
							if (lHeaderLen == 23)
							{
								sData = "128K Spectrum + Interface 1";
								bHardwareSupported = true;
								b128K = true;
								if (glEmulatedModel == 0 | glEmulatedModel == 5)
								{
									int arglModel4 = 1;
									SetEmulatedModel(ref arglModel4, ref gbSEBasicROM);
								}
							}
							else
							{
								sData = "128K Spectrum";
								bHardwareSupported = true;
								b128K = true;
								if (glEmulatedModel == 0 | glEmulatedModel == 5)
								{
									int arglModel5 = 1;
									SetEmulatedModel(ref arglModel5, ref gbSEBasicROM);
								}
							}

							break;
						}

					case 5:
						{
							bHardwareSupported = true;
							sData = "128K Spectrum + Interface 1";
							b128K = true;
							if (glEmulatedModel == 0 | glEmulatedModel == 5)
							{
								int arglModel6 = 1;
								SetEmulatedModel(ref arglModel6, ref gbSEBasicROM);
							}

							break;
						}

					case 6:
						{
							bHardwareSupported = false;
							sData = "128K Spectrum + M.G.T.";
							b128K = true;
							if (glEmulatedModel == 0 | glEmulatedModel == 5)
							{
								int arglModel7 = 1;
								SetEmulatedModel(ref arglModel7, ref gbSEBasicROM);
							}

							break;
						}

					case 7:
						{
							bHardwareSupported = false;
							sData = "ZX Spectrum +3";
							b128K = true;
							if (glEmulatedModel == 0 | glEmulatedModel == 5)
							{
								int arglModel8 = 2;
								SetEmulatedModel(ref arglModel8, ref gbSEBasicROM);
							}

							break;
						}

					case 14:
						{
							bHardwareSupported = true;
							bTimex = true;
							int arglModel9 = 5;
							SetEmulatedModel(ref arglModel9, ref gbSEBasicROM);
							// // If we're on a TC2048, ensure we're using the speccy-compatible screen mode
							int argport1 = 255;
							int argoutbyte1 = 0;
							modSpectrum.outb(argport1, argoutbyte1);
							break;
						}

					default:
						{
							bHardwareSupported = false;
							sData = "Unknown hardware platform";
							break;
						}
				}

				if (bHardwareSupported == false)
				{
					Interaction.MsgBox("vbSpec does not support the required hardware platform (" + sData + ") for this snapshot. This snapshot may not function correctly in vbSpec.");
				}

				lCounter = lCounter + 1;
			}

			// // offset 35 - 1 byte - last out to 0x7FFD - not required for 48K spectrum
			if (lCounter < lHeaderLen)
			{
				if (b128K)
				{
					int argport2 = 0x7FFD;
					int argoutbyte2 = modMain.Asc(InputString16(hFile, 1));
					modSpectrum.outb(argport2, argoutbyte2);
				}
				else
				{
					sData = InputString16(hFile, 1);
					glPageAt[0] = 8;
					glPageAt[1] = 5;
					glPageAt[2] = 1;
					glPageAt[3] = 2;
					glPageAt[4] = 8;
				}

				lCounter = lCounter + 1;
			}

			// // offset 36 - 1 byte - 0xFF if Interface 1 ROM is paged in
			if (lCounter < lHeaderLen)
			{
				sData = InputString16(hFile, 1);
				lCounter = lCounter + 1;
				if (bTimex)
				{
					int argport3 = 0xFF;
					int argoutbyte3 = modMain.Asc(sData);
					modSpectrum.outb(argport3, argoutbyte3);
				}
				else if (modMain.Asc(sData) == 255)
				{
					Interaction.MsgBox("This snapshot was saved with the Interface 1 ROM paged in. It may not run correctly in vbSpec.");
				}
			}

			// // offset 37 - 1 byte (bit 0: 1=intR emulation on, bit 1: 1=LDIR emulation on
			if (lCounter < lHeaderLen)
			{
				sData = InputString16(hFile, 1);
				lCounter = lCounter + 1;
			}

			// // offset 38 - Last out to 0xFFFD (+2/+3 sound chip register number)
			if (lCounter < lHeaderLen)
			{
				lOutFFFD = modMain.Asc(InputString16(hFile, 1));
				lCounter = lCounter + 1;
			}

			// // offset 39 - 16 bytes - contents of the sound chip registers
			if (lCounter < lHeaderLen)
			{
				if (b128K)
				{
					int argr = 0;
					int argv = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr, ref argv);
					int argr1 = 1;
					int argv1 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr1, ref argv1);
					int argr2 = 2;
					int argv2 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr2, ref argv2);
					int argr3 = 3;
					int argv3 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr3, ref argv3);
					int argr4 = 4;
					int argv4 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr4, ref argv4);
					int argr5 = 5;
					int argv5 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr5, ref argv5);
					int argr6 = 6;
					int argv6 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr6, ref argv6);
					int argr7 = 7;
					int argv7 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr7, ref argv7);
					int argr8 = 8;
					int argv8 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr8, ref argv8);
					int argr9 = 9;
					int argv9 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr9, ref argv9);
					int argr10 = 10;
					int argv10 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr10, ref argv10);
					int argr11 = 11;
					int argv11 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr11, ref argv11);
					int argr12 = 12;
					int argv12 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr12, ref argv12);
					int argr13 = 13;
					int argv13 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr13, ref argv13);
					int argr14 = 14;
					int argv14 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr14, ref argv14);
					int argr15 = 15;
					int argv15 = modMain.Asc(InputString16(hFile, 1));
					modAY8912.AYWriteReg(ref argr15, ref argv15);
				}
				else
				{
					sData = InputString16(hFile, 16);
				}

				lCounter = lCounter + 16;
			}

			if (b128K)
			{
				int argport4 = 0xFFFD;
				modSpectrum.outb(argport4, lOutFFFD);
			}

			// // read the remaining bytes of the header (we don't care what information they hold)
			if (lCounter < lHeaderLen)
			{
				sData = InputString16(hFile, lHeaderLen - lCounter);
			}

			do
			{
				// // read a block
				sData = InputString16(hFile, 2);
				if (FileSystem.EOF(hFile))
					break;
				lHeaderLen = modMain.Asc(Strings.Right(sData, 1)) * 256 + modMain.Asc(Strings.Left(sData, 1));
				sData = InputString16(hFile, 1);
				switch (modMain.Asc(sData))
				{
					case 0: // // Spectrum ROM
						{
							if (b128K)
								lMemPage = 9;
							else
								lMemPage = 8;
							break;
						}

					case 1: // // Interface 1 ROM, or similar (we discard these blocks)
						{
							lMemPage = -1;
							break;
						}

					case 2: // // 128K ROM (reset)
						{
							if (b128K)
								lMemPage = 8;
							else
								lMemPage = -1;
							break;
						}

					case 3: // // Page 0 (not used by 48K Spectrum)
						{
							if (b128K)
								lMemPage = 0;
							else
								lMemPage = -1;
							break;
						}

					case 4: // // Page 1 RAM at 0x8000
						{
							lMemPage = 1;
							break;
						}

					case 5: // // Page 2 RAM at 0xC000
						{
							lMemPage = 2;
							break;
						}

					case 6: // // Page 3 (not used by 48K Spectrum)
						{
							if (b128K)
								lMemPage = 3;
							else
								lMemPage = -1;
							break;
						}

					case 7: // // Page 4 (not used by 48K Spectrum)
						{
							if (b128K)
								lMemPage = 4;
							else
								lMemPage = -1;
							break;
						}

					case 8: // // Page 5 RAM at 0x4000
						{
							lMemPage = 5;
							break;
						}

					case 9: // // Page 6 (not used by 48K Spectrum)
						{
							if (b128K)
								lMemPage = 6;
							else
								lMemPage = -1;
							break;
						}

					case 10: // // Page 7 (not used by 48K Spectrum)
						{
							if (b128K)
								lMemPage = 7;
							else
								lMemPage = -1;
							break;
						}

					case 11: // // Multiface ROM
						{
							lMemPage = -1;
							break;
						}
				}

				if (lMemPage != -1)
				{
					if (lHeaderLen == 0xFFFF)
					{
						sData = InputString16(hFile, 16384);
						// Not a compressed block, just copy it straight into RAM
						for (lCounter = 0; lCounter <= 16383; lCounter++)
							gRAMPage[lMemPage, lCounter] = (byte)modMain.Asc(Strings.Mid(sData, lCounter + 1, 1));
					}
					else
					{
						sData = InputString16(hFile, lHeaderLen);
						// // Uncompress the block to memory
						lCounter = 1;
						lMemPos = 0;
						do
						{
							if (modMain.Asc(Strings.Mid(sData, lCounter, 1)) == 0xED)
							{
								if (modMain.Asc(Strings.Mid(sData, lCounter + 1, 1)) == 0xED)
								{
									// // This is an encoded block
									lCounter = lCounter + 2;
									lHeaderLen = modMain.Asc(Strings.Mid(sData, lCounter, 1));
									lCounter = lCounter + 1;
									if (lMemPos + lHeaderLen - 1 > 16383)
										goto ErrBlockTooBig;
									var loopTo = lHeaderLen - 1;
									for (lBlockCounter = 0; lBlockCounter <= loopTo; lBlockCounter++)
										gRAMPage[lMemPage, lMemPos + lBlockCounter] = (byte)modMain.Asc(Strings.Mid(sData, lCounter, 1));
									lMemPos = lMemPos + lBlockCounter;
								}
								else
								{
									// // Just a single ED, write it out
									gRAMPage[lMemPage, lMemPos] = 0xED;
									lMemPos = lMemPos + 1;
								}
							}
							else
							{
								gRAMPage[lMemPage, lMemPos] = (byte)modMain.Asc(Strings.Mid(sData, lCounter, 1));
								lMemPos = lMemPos + 1;
							}

							if (lMemPos > 16384)
								goto ErrBlockTooBig;
							lCounter = lCounter + 1;
						}
						while (lCounter <= Strings.Len(sData));
					}
				}
			}
			while (!FileSystem.EOF(hFile));
			return;
		ErrBlockTooBig:
			;
			Interaction.MsgBox("Errors were encountered in the z80 file. Compressed memory block [" + (lMemPage + 3).ToString() + "] has an uncompressed length of more than 16384 bytes.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "vbSpec");
		}

		public static void resetKeyboard()
		{
			keyB_SPC = 0xFF;
			keyH_ENT = 0xFF;
			keyY_P = 0xFF;
			key6_0 = 0xFF;
			key1_5 = 0xFF;
			keyQ_T = 0xFF;
			keyA_G = 0xFF;
			keyCAPS_V = 0xFF;
		}

		public static void SaveROM(ref string sFileName)
		{
			int hFile, lCounter;
			;
//#error Cannot convert OnErrorGoToStatementSyntax - see comment for details
			/* Cannot convert OnErrorGoToStatementSyntax, CONVERSION ERROR: Conversion for OnErrorGoToLabelStatement not implemented, please report this issue in 'On Error GoTo SaveROM_Err' at character 82120


			Input:

					On Error GoTo SaveROM_Err

			 */
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Output);
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[0], lCounter]);
			SaveROM_Err:
			;
			FileSystem.FileClose(hFile);
		}

		public static void SaveScreenBMP(ref string sFileName)
		{
			;

			// UPGRADE_WARNING: SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			My.MyProject.Forms.frmMainWnd.picDisplay.Image.Save(sFileName);
		}

		public static void SaveScreenSCR(ref string sFileName)
		{
			int hFile, n;
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Output);
			if (glUseScreen == 1001)
			{
				// // TC2048 second screen
				for (n = 8192; n <= 15103; n++)
					FileSystem.Print(hFile, (object)(char)gRAMPage[5, n]);
			}
			else if (glUseScreen == 1002)
			{
				// // TC2048 hicolour

				// // Save the mono bitmap data first
				for (n = 0; n <= 6143; n++)
					FileSystem.Print(hFile, (object)(char)gRAMPage[5, n]);
				// // Immediately followed by the colour data
				for (n = 8192; n <= 14335; n++)
					FileSystem.Print(hFile, (object)(char)gRAMPage[5, n]);
			}
			else if (glUseScreen == 1006)
			{
				// // TC2048 hires
				// // Save columns 0,2,4,6... first
				for (n = 0; n <= 6143; n++)
					FileSystem.Print(hFile, (object)(char)gRAMPage[5, n]);
				// // Immediately followed by columns 1,3,5,7...
				for (n = 8192; n <= 14335; n++)
					FileSystem.Print(hFile, (object)(char)gRAMPage[5, n]);
				// // And finally an extra byte to indicate the screen colour
				FileSystem.Print(hFile, (object)(char)glTC2048LastFFOut);
			}
			else if (glUseScreen < 1000)
			{
				for (n = 0; n <= 6911; n++)
					FileSystem.Print(hFile, (object)(char)gRAMPage[glUseScreen, n]);
			}

			FileSystem.FileClose(hFile);
		}

		private static void SaveSNA128Snap(ref int hFile)
		{
			int lBank, lCounter;
			FileSystem.Print(hFile, (object)(char)intI);
			FileSystem.Print(hFile, Conversions.ToString((char)(regHL_ & 0xFF)) + (char)(regHL_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regDE_ & 0xFF)) + (char)(regDE_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regBC_ & 0xFF)) + (char)(regBC_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regAF_ & 0xFF)) + (char)(regAF_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regHL & 0xFF)) + (char)(regHL / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regDE & 0xFF)) + (char)(regDE / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)regC) + (char)regB);
			FileSystem.Print(hFile, Conversions.ToString((char)(regIY & 0xFF)) + (char)(regIY / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regIX & 0xFF)) + (char)(regIX / 256));

			// Interrupt flipflops
			if (intIFF1 == Conversions.ToInteger(true))
			{
				FileSystem.Print(hFile, (object)'\u0004');
			}
			else
			{
				FileSystem.Print(hFile, (object)'\0');
			}

			// R
			intRTemp = intRTemp & 127;
			FileSystem.Print(hFile, (object)(char)(intR & 0x80 | intRTemp));

			// // AF
			FileSystem.Print(hFile, Conversions.ToString((char)(modZ80.getAF() & 0xFF)) + (char)(modZ80.getAF() / 256));

			// // SP
			FileSystem.Print(hFile, Conversions.ToString((char)(regSP & 0xFF)) + (char)(regSP / 256));

			// // Interrupt Mode
			FileSystem.Print(hFile, (object)(char)intIM);
			int localGetBorderIndex() { int arglRGB = ColorTranslator.ToOle(My.MyProject.Forms.frmMainWnd.BackColor); var ret = GetBorderIndex(ref arglRGB); return ret; }

			FileSystem.Print(hFile, (object)(char)localGetBorderIndex());

			// // Save the three currently-paged RAM banks
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[1], lCounter]);
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[2], lCounter]);
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[3], lCounter]);

			// // PC
			FileSystem.Print(hFile, Conversions.ToString((char)(regPC & 0xFF)) + (char)(regPC / 256));

			// // Last out to 0x7FFD
			FileSystem.Print(hFile, (object)(char)glLastOut7FFD);

			// // Is TR-DOS paged? (0=not paged, 1=paged)
			FileSystem.Print(hFile, (object)'\0');

			// // Save the remaining RAM banks
			lBank = 0;
			while (lBank < 8)
			{
				if (lBank != glPageAt[1] & lBank != glPageAt[2] & lBank != glPageAt[3])
				{
					for (lCounter = 0; lCounter <= 16383; lCounter++)
						FileSystem.Print(hFile, (object)(char)gRAMPage[lBank, lCounter]);
				}

				lBank = lBank + 1;
			}
		}

		public static void SaveSNASnap(ref string sFileName)
		{
			int hFile, lCounter;
			string sData;
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Output);
			if (glEmulatedModel != 0 & glEmulatedModel != 5)
			{
				// // We're running in 128 mode
				SaveSNA128Snap(ref hFile);
				FileSystem.FileClose(hFile);
				return;
			}

			modZ80.pushpc();
			FileSystem.Print(hFile, (object)(char)intI);
			FileSystem.Print(hFile, Conversions.ToString((char)(regHL_ & 0xFF)) + (char)(regHL_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regDE_ & 0xFF)) + (char)(regDE_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regBC_ & 0xFF)) + (char)(regBC_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regAF_ & 0xFF)) + (char)(regAF_ / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regHL & 0xFF)) + (char)(regHL / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regDE & 0xFF)) + (char)(regDE / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)regC) + (char)regB);
			FileSystem.Print(hFile, Conversions.ToString((char)(regIY & 0xFF)) + (char)(regIY / 256));
			FileSystem.Print(hFile, Conversions.ToString((char)(regIX & 0xFF)) + (char)(regIX / 256));

			// Interrupt flipflops
			if (intIFF1 == Conversions.ToInteger(true))
			{
				FileSystem.Print(hFile, (object)'\u0004');
			}
			else
			{
				FileSystem.Print(hFile, (object)'\0');
			}

			// R
			intRTemp = intRTemp & 127;
			FileSystem.Print(hFile, (object)(char)(intR & 0x80 | intRTemp));

			// // AF
			FileSystem.Print(hFile, Conversions.ToString((char)(modZ80.getAF() & 0xFF)) + (char)(modZ80.getAF() / 256));

			// // SP
			FileSystem.Print(hFile, Conversions.ToString((char)(regSP & 0xFF)) + (char)(regSP / 256));

			// // Interrupt Mode
			FileSystem.Print(hFile, (object)(char)intIM);
			int localGetBorderIndex() { int arglRGB = ColorTranslator.ToOle(My.MyProject.Forms.frmMainWnd.BackColor); var ret = GetBorderIndex(ref arglRGB); return ret; }

			FileSystem.Print(hFile, (object)(char)localGetBorderIndex());
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[1], lCounter]);
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[2], lCounter]);
			for (lCounter = 0; lCounter <= 16383; lCounter++)
				FileSystem.Print(hFile, (object)(char)gRAMPage[glPageAt[3], lCounter]);
			FileSystem.FileClose(hFile);
			modZ80.poppc();
		}

		public static void SaveZ80Snap(ref string sFileName)
		{
			int lCounter, hFile, lBufSize;
			string sData;
			hFile = FileSystem.FreeFile();
			FileSystem.FileOpen(hFile, sFileName, OpenMode.Output);

			// A,F
			FileSystem.Print(hFile, Conversions.ToString((char)regA) + (char)modZ80.getF());
			// BC
			FileSystem.Print(hFile, Conversions.ToString((char)regC) + (char)regB);
			// HL
			FileSystem.Print(hFile, Conversions.ToString((char)(regHL & 0xFF)) + (char)(regHL / 256));
			// Set PC to zero to indicate a v2.01 Z80 file
			FileSystem.Print(hFile, Conversions.ToString('\0') + '\0');
			// SP
			FileSystem.Print(hFile, Conversions.ToString((char)(regSP & 0xFF)) + (char)(regSP / 256));
			// I
			FileSystem.Print(hFile, (object)(char)intI);
			// R (7 bits)
			intRTemp = intRTemp & 0x7F;
			FileSystem.Print(hFile, (object)(char)intRTemp);
			// bitfield
			int localGetBorderIndex() { int arglRGB = ColorTranslator.ToOle(My.MyProject.Forms.frmMainWnd.BackColor); var ret = GetBorderIndex(ref arglRGB); return ret; }

			FileSystem.Print(hFile, (object)Conversions.ToChar(Operators.OrObject(Interaction.IIf(Conversions.ToBoolean(intR & Conversions.ToInteger(0x80 == 0x80)), 1, 0), localGetBorderIndex() * 2)));
			// DE
			FileSystem.Print(hFile, Conversions.ToString((char)(regDE & 0xFF)) + (char)(regDE / 256));
			// BC'
			FileSystem.Print(hFile, Conversions.ToString((char)(regBC_ & 0xFF)) + (char)(regBC_ / 256));
			// DE'
			FileSystem.Print(hFile, Conversions.ToString((char)(regDE_ & 0xFF)) + (char)(regDE_ / 256));
			// HL'
			FileSystem.Print(hFile, Conversions.ToString((char)(regHL_ & 0xFF)) + (char)(regHL_ / 256));
			// AF'
			FileSystem.Print(hFile, Conversions.ToString((char)(regAF_ / 256)) + (char)(regAF_ & 0xFF));
			// IY
			FileSystem.Print(hFile, Conversions.ToString((char)(regIY & 0xFF)) + (char)(regIY / 256));
			// IX
			FileSystem.Print(hFile, Conversions.ToString((char)(regIX & 0xFF)) + (char)(regIX / 256));
			// Interrupt flipflops
			if (intIFF1 == Conversions.ToInteger(true))
			{
				FileSystem.Print(hFile, (object)'ÿ');
				FileSystem.Print(hFile, (object)'ÿ');
			}
			else
			{
				FileSystem.Print(hFile, (object)'\0');
				FileSystem.Print(hFile, (object)'\0');
			}
			// // Interrupt Mode
			FileSystem.Print(hFile, (object)(char)intIM);

			// // V2.01 info
			FileSystem.Print(hFile, Conversions.ToString('\u0017') + '\0');
			// PC
			FileSystem.Print(hFile, Conversions.ToString((char)(regPC & 0xFF)) + (char)(regPC / 256));
			// Hardware mode
			if (glEmulatedModel == 0)
			{
				FileSystem.Print(hFile, (object)'\0'); // // 48K Spectrum
				FileSystem.Print(hFile, (object)'\0');
			}
			else if (glEmulatedModel == 5)
			{
				FileSystem.Print(hFile, (object)'\u000e');
				FileSystem.Print(hFile, (object)'\0');
			}
			else
			{
				FileSystem.Print(hFile, (object)'\u0003'); // // 128K Spectrum
														   // Last out to 7FFD
				FileSystem.Print(hFile, (object)(char)glLastOut7FFD);
			}

			if (glEmulatedModel == 5)
			{
				// Last out to 00FF
				FileSystem.Print(hFile, (object)(char)(glTC2048LastFFOut & 0xFF));
			}
			else
			{
				// IF.1 Paged in
				FileSystem.Print(hFile, (object)'\0');
			}
			// 1=R emulation on,2=LDIR emulation on
			FileSystem.Print(hFile, (object)'\u0003');
			// Last out to FFFD
			FileSystem.Print(hFile, (object)(char)glSoundRegister);
			// AY-3-8912 register contents
			for (lCounter = 0; lCounter <= 15; lCounter++)
				FileSystem.Print(hFile, (object)(char)modAY8912.AYPSG.Regs[lCounter]);
			if (glEmulatedModel == 0 | glEmulatedModel == 5)
			{
				// // Block 1
				sData = "";
				lBufSize = CompressMemoryBlock(ref glPageAt[1], ref sData);
				// Buffer length
				FileSystem.Print(hFile, Conversions.ToString((char)(lBufSize & 0xFF)) + (char)(lBufSize / 256));
				// Block number
				FileSystem.Print(hFile, (object)(char)(glPageAt[1] + 3));
				FileSystem.Print(hFile, sData);
				// // Block 2
				sData = "";
				lBufSize = CompressMemoryBlock(ref glPageAt[2], ref sData);
				// Buffer length
				FileSystem.Print(hFile, Conversions.ToString((char)(lBufSize & 0xFF)) + (char)(lBufSize / 256));
				// Block number
				FileSystem.Print(hFile, (object)(char)(glPageAt[2] + 3));
				FileSystem.Print(hFile, sData);
				// // Block 3
				sData = "";
				lBufSize = CompressMemoryBlock(ref glPageAt[3], ref sData);
				// Buffer length
				FileSystem.Print(hFile, Conversions.ToString((char)(lBufSize & 0xFF)) + (char)(lBufSize / 256));
				// Block number
				FileSystem.Print(hFile, (object)(char)(glPageAt[3] + 3));
				FileSystem.Print(hFile, sData);
			}
			else
			{
				for (lCounter = 0; lCounter <= 7; lCounter++)
				{
					sData = "";
					lBufSize = CompressMemoryBlock(ref lCounter, ref sData);
					// Buffer length
					FileSystem.Print(hFile, Conversions.ToString((char)(lBufSize & 0xFF)) + (char)(lBufSize / 256));
					// Block number
					FileSystem.Print(hFile, (object)(char)(lCounter + 3));
					FileSystem.Print(hFile, sData);
				}
			}

			FileSystem.FileClose(hFile);
			resetKeyboard();
		}

		private static void SEPatch128ROM()
		{
			gRAMPage[8, 576] = 0;
			gRAMPage[8, 577] = 0;
			gRAMPage[8, 578] = 0;
			gRAMPage[8, 0x37F] = 0;
			gRAMPage[8, 0x380] = 0;
			gRAMPage[8, 0x381] = 0x15;
			gRAMPage[8, 0x382] = 0;
			gRAMPage[8, 0x383] = 0;
			gRAMPage[8, 0x384] = 0;
			gRAMPage[8, 0x3A3] = 0x3E;
			gRAMPage[8, 0x3A4] = 0x20;
			gRAMPage[8, 0x3A5] = 0xD7;
			gRAMPage[8, 0x3A6] = 0x0;
			gRAMPage[8, 0x3A7] = 0x0;
			gRAMPage[8, 0x3A8] = 0x0;
			gRAMPage[8, 0x33B4] = 0xCB;
		}

		private static void SEPatchPlus2ROM()
		{
			gRAMPage[8, 576] = 0;
			gRAMPage[8, 577] = 0;
			gRAMPage[8, 578] = 0;
			gRAMPage[8, 0x37F] = 0;
			gRAMPage[8, 0x380] = 0;
			gRAMPage[8, 0x381] = 0x15;
			gRAMPage[8, 0x382] = 0;
			gRAMPage[8, 0x383] = 0;
			gRAMPage[8, 0x384] = 0;
			gRAMPage[8, 0x3A3] = 0x3E;
			gRAMPage[8, 0x3A4] = 0x20;
			gRAMPage[8, 0x3A5] = 0xD7;
			gRAMPage[8, 0x3A6] = 0x0;
			gRAMPage[8, 0x3A7] = 0x0;
			gRAMPage[8, 0x3A8] = 0x0;
			gRAMPage[8, 0x33DA] = 0xCB;
		}

		public static void SetDisplaySize(ref int lWidth, ref int lHeight)
		{
			short i;
			int y, X;

			// MM 16.04.2003
			var rectWindow = default(RECT);
			POINT pointWindow;
			int lRightMargin, lNewTop, lNewLeft, lBottomMargin;
			int lTopMargin;

			// No full screen modus
			if (!modSpectrum.bFullScreen)
			{
				// Prepear window
				My.MyProject.Forms.frmMainWnd.FullScreenOff();
			}

			y = GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYMENU) + GetSystemMetrics(SM_CYFRAME) * 2 + My.MyProject.Forms.frmMainWnd.picStatus.Height;
			X = GetSystemMetrics(SM_CXFRAME) * 2;
			glDisplayWidth = lWidth;
			glDisplayHeight = lHeight;
			glDisplayVSource = lHeight - 1;
			glDisplayVSize = -lHeight;
			Interaction.SaveSetting("Grok", "vbSpec", "DisplayWidth", glDisplayWidth.ToString());
			Interaction.SaveSetting("Grok", "vbSpec", "DisplayHeight", glDisplayHeight.ToString());
			// MM 16.04.2003
			Interaction.SaveSetting("Grok", "vbSpec", "FullScreen", Conversions.ToShort(modSpectrum.bFullScreen).ToString());
			switch (lWidth)
			{
				case 256: // // Standard
					{
						// MM 16.04.2003
						glDisplayXMultiplier = 1;
						if (My.MyProject.Forms.frmMainWnd.WindowState == FormWindowState.Normal)
						{
							My.MyProject.Forms.frmMainWnd.SetBounds(My.MyProject.Forms.frmMainWnd.Left, My.MyProject.Forms.frmMainWnd.Top, 280 + X, My.MyProject.Forms.frmMainWnd.Height);
						}

						break;
					}

				case 512:
					{
						glDisplayXMultiplier = 2;
						if (My.MyProject.Forms.frmMainWnd.WindowState == FormWindowState.Normal)
						{
							My.MyProject.Forms.frmMainWnd.SetBounds(My.MyProject.Forms.frmMainWnd.Left, My.MyProject.Forms.frmMainWnd.Top, 536 + X, My.MyProject.Forms.frmMainWnd.Height);
						}

						break;
					}

				case 768:
					{
						glDisplayXMultiplier = 3;
						if (My.MyProject.Forms.frmMainWnd.WindowState == FormWindowState.Normal)
						{
							My.MyProject.Forms.frmMainWnd.SetBounds(My.MyProject.Forms.frmMainWnd.Left, My.MyProject.Forms.frmMainWnd.Top, 792 + X, My.MyProject.Forms.frmMainWnd.Height);
						}

						break;
					}
			}

			switch (lHeight)
			{
				case 192:
					{
						glDisplayYMultiplier = 1;
						if (My.MyProject.Forms.frmMainWnd.WindowState == FormWindowState.Normal)
						{
							My.MyProject.Forms.frmMainWnd.SetBounds(My.MyProject.Forms.frmMainWnd.Left, My.MyProject.Forms.frmMainWnd.Top, My.MyProject.Forms.frmMainWnd.Width, 220 + y);
						}

						break;
					}

				case 384:
					{
						glDisplayYMultiplier = 2;
						if (My.MyProject.Forms.frmMainWnd.WindowState == FormWindowState.Normal)
						{
							My.MyProject.Forms.frmMainWnd.SetBounds(My.MyProject.Forms.frmMainWnd.Left, My.MyProject.Forms.frmMainWnd.Top, My.MyProject.Forms.frmMainWnd.Width, 412 + y);
						}

						break;
					}

				case 576:
					{
						glDisplayYMultiplier = 3;
						if (My.MyProject.Forms.frmMainWnd.WindowState == FormWindowState.Normal)
						{
							My.MyProject.Forms.frmMainWnd.SetBounds(My.MyProject.Forms.frmMainWnd.Left, My.MyProject.Forms.frmMainWnd.Top, My.MyProject.Forms.frmMainWnd.Width, 604 + y);
						}

						break;
					}
			}

			// MM 16.04.2003
			// If this speccy is in full screen modus
			if (modSpectrum.bFullScreen)
			{
				// Prepear window
				My.MyProject.Forms.frmMainWnd.FullScreenOn();
				// Get parameter
				GetClientRect(My.MyProject.Forms.frmMainWnd.Handle, out rectWindow);
				pointWindow.lX = rectWindow.iLeft;
				pointWindow.lY = rectWindow.iTop;
				ClientToScreen(My.MyProject.Forms.frmMainWnd.Handle, out pointWindow);
				// Margins
				lNewLeft = My.MyProject.Forms.frmMainWnd.Left - pointWindow.lX;
				lNewTop = My.MyProject.Forms.frmMainWnd.Top - pointWindow.lY;
				lRightMargin = Math.Abs(My.MyProject.Forms.frmMainWnd.Width - My.MyProject.Forms.frmMainWnd.Left) - rectWindow.iRight;
				lBottomMargin = Math.Abs(My.MyProject.Forms.frmMainWnd.Height - My.MyProject.Forms.frmMainWnd.Top) - rectWindow.iBottom;
				// Set modus
				My.MyProject.Forms.frmMainWnd.SetBounds(0, 0, 2 * Screen.PrimaryScreen.Bounds.Width, 2 * Screen.PrimaryScreen.Bounds.Height);
				SetForegroundWindow(My.MyProject.Forms.frmMainWnd.Handle);
				glDisplayXMultiplier = (int)(lWidth / 265d);
				glDisplayYMultiplier = glDisplayXMultiplier;
			}

			gpicDisplay.Width = lWidth;
			gpicDisplay.Height = lHeight;
			initscreen();
		}

		public static void SetEmulatedModel(ref int lModel, [Optional, DefaultParameterValue(0)] ref int bSEBasicROM)
		{
			var sModel = default(string);
			glEmulatedModel = lModel;
			switch (lModel)
			{
				case 0:
					{
						// // A 48K Spectrum has 69888 tstates per interrupt (3.50000 MHz)
						sModel = "ZX Spectrum 48K";
						glTstatesPerInterrupt = 69888;
						modWaveOut.glWaveAddTStates = 158; // 58
						glMemPagingType = 0;
						glUseScreen = 5;
						gbEmulateAYSound = Conversions.ToInteger(false);
						glKeyPortMask = 0xBF;
						glPageAt[0] = 8;
						glPageAt[1] = 5;
						glPageAt[2] = 1;
						glPageAt[3] = 2;
						glPageAt[4] = 8;

						// // T-state information
						glTStatesPerLine = 224;
						glTStatesAtTop = -glTstatesPerInterrupt + 14336;
						glTStatesAtBottom = -glTstatesPerInterrupt + 14336 + 43007;

						// // load the ROM image into memory
						if (Conversions.ToBoolean(bSEBasicROM))
						{
							string argsROMFile = My.MyProject.Application.Info.DirectoryPath + @"\sebasic.rom";
							int arglROMPage = 8;
							LoadROM(ref argsROMFile, ref arglROMPage);
						}
						else
						{
							string argsROMFile1 = My.MyProject.Application.Info.DirectoryPath + @"\spectrum.rom";
							int arglROMPage1 = 8;
							LoadROM(ref argsROMFile1, ref arglROMPage1);
						}

						SetupContentionTable();
						break;
					}

				case 1:
					{
						// // A 128K Spectrum has 70908 tstates per interrupt (3.54690 MHz)
						sModel = "ZX Spectrum 128";
						glTstatesPerInterrupt = 70908;
						modWaveOut.glWaveAddTStates = 160;
						glMemPagingType = 1;
						glUseScreen = 5;
						gbEmulateAYSound = Conversions.ToInteger(true);
						glKeyPortMask = 0xBF;
						glPageAt[0] = 8;
						glPageAt[1] = 5;
						glPageAt[2] = 2;
						glPageAt[3] = 0;
						glPageAt[4] = 8;

						// // T-state information
						glTStatesPerLine = 228;
						glTStatesAtTop = -glTstatesPerInterrupt + 14364;
						glTStatesAtBottom = -glTstatesPerInterrupt + 14364 + 43775;
						if (Conversions.ToBoolean(bSEBasicROM))
						{
							string argsROMFile2 = My.MyProject.Application.Info.DirectoryPath + @"\sebasic.rom";
							int arglROMPage2 = 9;
							LoadROM(ref argsROMFile2, ref arglROMPage2);
							string argsROMFile3 = My.MyProject.Application.Info.DirectoryPath + @"\zx128_0.rom";
							int arglROMPage3 = 8;
							LoadROM(ref argsROMFile3, ref arglROMPage3);
							SEPatch128ROM();
						}
						else
						{
							string argsROMFile4 = My.MyProject.Application.Info.DirectoryPath + @"\zx128_1.rom";
							int arglROMPage4 = 9;
							LoadROM(ref argsROMFile4, ref arglROMPage4);
							string argsROMFile5 = My.MyProject.Application.Info.DirectoryPath + @"\zx128_0.rom";
							int arglROMPage5 = 8;
							LoadROM(ref argsROMFile5, ref arglROMPage5);
						}

						SetupContentionTable();
						break;
					}

				case 2:
					{
						// // A Spectrum +2 has 70908 tstates per interrupt (3.54690 MHz)
						sModel = "ZX Spectrum +2";
						glTstatesPerInterrupt = 70908;
						modWaveOut.glWaveAddTStates = 160;
						glMemPagingType = 1;
						glUseScreen = 5;
						gbEmulateAYSound = Conversions.ToInteger(true);
						glKeyPortMask = 0xBF;
						glPageAt[0] = 8;
						glPageAt[1] = 5;
						glPageAt[2] = 2;
						glPageAt[3] = 0;
						glPageAt[4] = 8;

						// // T-state information
						glTStatesPerLine = 228;
						glTStatesAtTop = -glTstatesPerInterrupt + 14364;
						glTStatesAtBottom = -glTstatesPerInterrupt + 14364 + 43775;
						if (Conversions.ToBoolean(bSEBasicROM))
						{
							string argsROMFile6 = My.MyProject.Application.Info.DirectoryPath + @"\sebasic.rom";
							int arglROMPage6 = 9;
							LoadROM(ref argsROMFile6, ref arglROMPage6);
							string argsROMFile7 = My.MyProject.Application.Info.DirectoryPath + @"\plus2_0.rom";
							int arglROMPage7 = 8;
							LoadROM(ref argsROMFile7, ref arglROMPage7);
							SEPatchPlus2ROM();
						}
						else
						{
							string argsROMFile8 = My.MyProject.Application.Info.DirectoryPath + @"\plus2_1.rom";
							int arglROMPage8 = 9;
							LoadROM(ref argsROMFile8, ref arglROMPage8);
							string argsROMFile9 = My.MyProject.Application.Info.DirectoryPath + @"\plus2_0.rom";
							int arglROMPage9 = 8;
							LoadROM(ref argsROMFile9, ref arglROMPage9);
						}

						SetupContentionTable();
						break;
					}

				case 3: // // +2A
					{
						sModel = "ZX Spectrum +2A";
						glTstatesPerInterrupt = 70908;
						modWaveOut.glWaveAddTStates = 160;
						glMemPagingType = 2;
						glUseScreen = 5;
						gbEmulateAYSound = Conversions.ToInteger(true);
						glKeyPortMask = 0xBF;
						glPageAt[0] = 8;
						glPageAt[1] = 5;
						glPageAt[2] = 1;
						glPageAt[3] = 2;
						glPageAt[4] = 8;

						// // T-state information
						glTStatesPerLine = 228;
						glTStatesAtTop = -glTstatesPerInterrupt + 14364;
						glTStatesAtBottom = -glTstatesPerInterrupt + 14364 + 43775;
						string argsROMFile10 = My.MyProject.Application.Info.DirectoryPath + @"\plus2a_3.rom";
						int arglROMPage10 = 11;
						LoadROM(ref argsROMFile10, ref arglROMPage10);
						string argsROMFile11 = My.MyProject.Application.Info.DirectoryPath + @"\plus2a_2.rom";
						int arglROMPage11 = 10;
						LoadROM(ref argsROMFile11, ref arglROMPage11);
						string argsROMFile12 = My.MyProject.Application.Info.DirectoryPath + @"\plus2a_1.rom";
						int arglROMPage12 = 9;
						LoadROM(ref argsROMFile12, ref arglROMPage12);
						string argsROMFile13 = My.MyProject.Application.Info.DirectoryPath + @"\plus2a_0.rom";
						int arglROMPage13 = 8;
						LoadROM(ref argsROMFile13, ref arglROMPage13);
						SetupContentionTable();
						break;
					}

				case 4: // // +3
					{
						sModel = "ZX Spectrum +3";
						glTstatesPerInterrupt = 70908;
						modWaveOut.glWaveAddTStates = 160;
						glMemPagingType = 2;
						glUseScreen = 5;
						gbEmulateAYSound = Conversions.ToInteger(true);
						glKeyPortMask = 0xBF;
						glPageAt[0] = 8;
						glPageAt[1] = 5;
						glPageAt[2] = 1;
						glPageAt[3] = 2;
						glPageAt[4] = 8;

						// // T-state information
						glTStatesPerLine = 228;
						glTStatesAtTop = -glTstatesPerInterrupt + 14364;
						glTStatesAtBottom = -glTstatesPerInterrupt + 14364 + 43775;
						string argsROMFile14 = My.MyProject.Application.Info.DirectoryPath + @"\plus3_3.rom";
						int arglROMPage14 = 11;
						LoadROM(ref argsROMFile14, ref arglROMPage14);
						string argsROMFile15 = My.MyProject.Application.Info.DirectoryPath + @"\plus3_2.rom";
						int arglROMPage15 = 10;
						LoadROM(ref argsROMFile15, ref arglROMPage15);
						string argsROMFile16 = My.MyProject.Application.Info.DirectoryPath + @"\plus3_1.rom";
						int arglROMPage16 = 9;
						LoadROM(ref argsROMFile16, ref arglROMPage16);
						string argsROMFile17 = My.MyProject.Application.Info.DirectoryPath + @"\plus3_0.rom";
						int arglROMPage17 = 8;
						LoadROM(ref argsROMFile17, ref arglROMPage17);
						SetupContentionTable();
						break;
					}

				case 5: // // TC2048
					{
						sModel = "Timex TC2048";
						glTstatesPerInterrupt = 69888;
						modWaveOut.glWaveAddTStates = 158;
						glMemPagingType = 0;
						glUseScreen = 5;
						gbEmulateAYSound = Conversions.ToInteger(false);
						glKeyPortMask = 0x1F;
						glPageAt[0] = 8;
						glPageAt[1] = 5;
						glPageAt[2] = 1;
						glPageAt[3] = 2;
						glPageAt[4] = 8;

						// // T-state information
						glTStatesPerLine = 224;
						glTStatesAtTop = -glTstatesPerInterrupt + 14336;
						glTStatesAtBottom = -glTstatesPerInterrupt + 14336 + 43007;
						if (Conversions.ToBoolean(bSEBasicROM))
						{
							string argsROMFile18 = My.MyProject.Application.Info.DirectoryPath + @"\sebasic.rom";
							int arglROMPage18 = 8;
							LoadROM(ref argsROMFile18, ref arglROMPage18);
						}
						else
						{
							string argsROMFile19 = My.MyProject.Application.Info.DirectoryPath + @"\tc2048.rom";
							int arglROMPage19 = 8;
							LoadROM(ref argsROMFile19, ref arglROMPage19);
						}

						SetupContentionTable();
						break;
					}
			}

			SetupTStatesToScanLines();
			bmiBuffer.bmiHeader.biWidth = 256;
			SetStatus(ref sModel);
		}

		public static void SetStatus(ref string sMsg)
		{
			My.MyProject.Forms.frmMainWnd.lblStatusMsg.Text = sMsg;
		}

		public static void SetupContentionTable()
		{
			int z = default, l, y;
			var X = new int[9];
			X[0] = 6; // 6
			X[1] = 5; // 5
			X[2] = 4; // 4
			X[3] = 3; // 3
			X[4] = 2; // 2
			X[5] = 1; // 1
			X[6] = 0; // 0
			X[7] = 0; // 0
			l = -glTstatesPerInterrupt;
			while (l <= 0)
			{
				if (l >= glTStatesAtTop & l <= glTStatesAtBottom)
				{
					var loopTo = glTStatesPerLine;
					for (y = 0; y <= loopTo; y++)
					{
						if (y < 128)
						{
							glContentionTable[-l - y + 30] = X[z];
							z = z + 1;
							if (z > 7)
								z = 0;
						}
						else
						{
							glContentionTable[-l - y + 30] = 0;
						}
					}

					z = 0;
					l = l + glTStatesPerLine - 1;
				}
				else
				{
					glContentionTable[-l + 30] = 0;
				}

				l = l + 1;
			}
		}

		private static void SetupTStatesToScanLines()
		{
			int n;
			for (n = -glTstatesPerInterrupt; n <= 0; n++)
			{
				if (n >= glTStatesAtTop & n <= glTStatesAtBottom)
				{
					glTSToScanLine[-n] = (n - glTStatesAtTop) / glTStatesPerLine;
				}
				else
				{
					glTSToScanLine[-n] = -1;
				} // // In the border area or vertical retrace
			}
		}
	}
}