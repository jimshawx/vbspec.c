﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmDefJoystick
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmDefJoystick() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdLoad.Name = "cmdLoad";
			_cmdSave.Name = "cmdSave";
			_CancelButton_Renamed.Name = "CancelButton_Renamed";
			_OKButton.Name = "OKButton";
			_cmdUndefine.Name = "cmdUndefine";
			_cmdDefine.Name = "cmdDefine";
			_txtValueDown.Name = "txtValueDown";
			_txtPortDown.Name = "txtPortDown";
			_txtValueLeft.Name = "txtValueLeft";
			_txtPortLeft.Name = "txtPortLeft";
			_txtValueRight.Name = "txtValueRight";
			_txtPortRight.Name = "txtPortRight";
			_txtValueUp.Name = "txtValueUp";
			_txtPortUp.Name = "txtPortUp";
			_txtRight.Name = "txtRight";
			_txtLeft.Name = "txtLeft";
			_txtDown.Name = "txtDown";
			_txtUp.Name = "txtUp";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _cmdLoad;

		public Button cmdLoad
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdLoad;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdLoad != null)
				{
					_cmdLoad.Click -= cmdLoad_Click;
				}

				_cmdLoad = value;
				if (_cmdLoad != null)
				{
					_cmdLoad.Click += cmdLoad_Click;
				}
			}
		}

		private Button _cmdSave;

		public Button cmdSave
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdSave;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdSave != null)
				{
					_cmdSave.Click -= cmdSave_Click;
				}

				_cmdSave = value;
				if (_cmdSave != null)
				{
					_cmdSave.Click += cmdSave_Click;
				}
			}
		}

		public OpenFileDialog comdlgFileOpen;
		public SaveFileDialog comdlgFileSave;
		private Button _CancelButton_Renamed;

		public Button CancelButton_Renamed
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _CancelButton_Renamed;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_CancelButton_Renamed != null)
				{
					_CancelButton_Renamed.Click -= CancelButton_Renamed_Click;
				}

				_CancelButton_Renamed = value;
				if (_CancelButton_Renamed != null)
				{
					_CancelButton_Renamed.Click += CancelButton_Renamed_Click;
				}
			}
		}

		private Button _OKButton;

		public Button OKButton
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _OKButton;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_OKButton != null)
				{
					_OKButton.Click -= OKButton_Click;
				}

				_OKButton = value;
				if (_OKButton != null)
				{
					_OKButton.Click += OKButton_Click;
				}
			}
		}

		public ListView lvButtons;
		private Button _cmdUndefine;

		public Button cmdUndefine
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdUndefine;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdUndefine != null)
				{
					_cmdUndefine.Click -= cmdUndefine_Click;
				}

				_cmdUndefine = value;
				if (_cmdUndefine != null)
				{
					_cmdUndefine.Click += cmdUndefine_Click;
				}
			}
		}

		private Button _cmdDefine;

		public Button cmdDefine
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdDefine;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdDefine != null)
				{
					_cmdDefine.Click -= cmdDefine_Click;
				}

				_cmdDefine = value;
				if (_cmdDefine != null)
				{
					_cmdDefine.Click += cmdDefine_Click;
				}
			}
		}

		public GroupBox fraButtonDefintions;
		private TextBox _txtValueDown;

		public TextBox txtValueDown
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtValueDown;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtValueDown != null)
				{
					_txtValueDown.TextChanged -= txtValueDown_TextChanged;
					_txtValueDown.Enter -= txtValueDown_Enter;
				}

				_txtValueDown = value;
				if (_txtValueDown != null)
				{
					_txtValueDown.TextChanged += txtValueDown_TextChanged;
					_txtValueDown.Enter += txtValueDown_Enter;
				}
			}
		}

		private TextBox _txtPortDown;

		public TextBox txtPortDown
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtPortDown;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtPortDown != null)
				{
					_txtPortDown.TextChanged -= txtPortDown_TextChanged;
					_txtPortDown.Enter -= txtPortDown_Enter;
				}

				_txtPortDown = value;
				if (_txtPortDown != null)
				{
					_txtPortDown.TextChanged += txtPortDown_TextChanged;
					_txtPortDown.Enter += txtPortDown_Enter;
				}
			}
		}

		private TextBox _txtValueLeft;

		public TextBox txtValueLeft
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtValueLeft;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtValueLeft != null)
				{
					_txtValueLeft.TextChanged -= txtValueLeft_TextChanged;
					_txtValueLeft.Enter -= txtValueLeft_Enter;
				}

				_txtValueLeft = value;
				if (_txtValueLeft != null)
				{
					_txtValueLeft.TextChanged += txtValueLeft_TextChanged;
					_txtValueLeft.Enter += txtValueLeft_Enter;
				}
			}
		}

		private TextBox _txtPortLeft;

		public TextBox txtPortLeft
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtPortLeft;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtPortLeft != null)
				{
					_txtPortLeft.TextChanged -= txtPortLeft_TextChanged;
					_txtPortLeft.Enter -= txtPortLeft_Enter;
				}

				_txtPortLeft = value;
				if (_txtPortLeft != null)
				{
					_txtPortLeft.TextChanged += txtPortLeft_TextChanged;
					_txtPortLeft.Enter += txtPortLeft_Enter;
				}
			}
		}

		private TextBox _txtValueRight;

		public TextBox txtValueRight
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtValueRight;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtValueRight != null)
				{
					_txtValueRight.TextChanged -= txtValueRight_TextChanged;
					_txtValueRight.Enter -= txtValueRight_Enter;
				}

				_txtValueRight = value;
				if (_txtValueRight != null)
				{
					_txtValueRight.TextChanged += txtValueRight_TextChanged;
					_txtValueRight.Enter += txtValueRight_Enter;
				}
			}
		}

		private TextBox _txtPortRight;

		public TextBox txtPortRight
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtPortRight;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtPortRight != null)
				{
					_txtPortRight.TextChanged -= txtPortRight_TextChanged;
					_txtPortRight.Enter -= txtPortRight_Enter;
				}

				_txtPortRight = value;
				if (_txtPortRight != null)
				{
					_txtPortRight.TextChanged += txtPortRight_TextChanged;
					_txtPortRight.Enter += txtPortRight_Enter;
				}
			}
		}

		private TextBox _txtValueUp;

		public TextBox txtValueUp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtValueUp;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtValueUp != null)
				{
					_txtValueUp.TextChanged -= txtValueUp_TextChanged;
					_txtValueUp.Enter -= txtValueUp_Enter;
				}

				_txtValueUp = value;
				if (_txtValueUp != null)
				{
					_txtValueUp.TextChanged += txtValueUp_TextChanged;
					_txtValueUp.Enter += txtValueUp_Enter;
				}
			}
		}

		private TextBox _txtPortUp;

		public TextBox txtPortUp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtPortUp;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtPortUp != null)
				{
					_txtPortUp.TextChanged -= txtPortUp_TextChanged;
					_txtPortUp.Enter -= txtPortUp_Enter;
				}

				_txtPortUp = value;
				if (_txtPortUp != null)
				{
					_txtPortUp.TextChanged += txtPortUp_TextChanged;
					_txtPortUp.Enter += txtPortUp_Enter;
				}
			}
		}

		private TextBox _txtRight;

		public TextBox txtRight
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtRight;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtRight != null)
				{
					_txtRight.KeyDown -= txtRight_KeyDown;
				}

				_txtRight = value;
				if (_txtRight != null)
				{
					_txtRight.KeyDown += txtRight_KeyDown;
				}
			}
		}

		private TextBox _txtLeft;

		public TextBox txtLeft
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtLeft;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtLeft != null)
				{
					_txtLeft.KeyDown -= txtLeft_KeyDown;
				}

				_txtLeft = value;
				if (_txtLeft != null)
				{
					_txtLeft.KeyDown += txtLeft_KeyDown;
				}
			}
		}

		private TextBox _txtDown;

		public TextBox txtDown
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtDown;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtDown != null)
				{
					_txtDown.KeyDown -= txtDown_KeyDown;
				}

				_txtDown = value;
				if (_txtDown != null)
				{
					_txtDown.KeyDown += txtDown_KeyDown;
				}
			}
		}

		private TextBox _txtUp;

		public TextBox txtUp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtUp;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtUp != null)
				{
					_txtUp.KeyDown -= txtUp_KeyDown;
				}

				_txtUp = value;
				if (_txtUp != null)
				{
					_txtUp.KeyDown += txtUp_KeyDown;
				}
			}
		}

		public Label _lblOr_3;
		public Label _lblOr_2;
		public Label _lblOr_1;
		public Label _lblOr_0;
		public Label _lblKomma_3;
		public Label _lblKomma_2;
		public Label _lblKomma_1;
		public Label _lblKomma_0;
		public Label lblRight;
		public Label lblLeft;
		public Label lblDown;
		public Label lblUp;
		public GroupBox fraDirectionDefintions;
		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblKomma;
		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblOr;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmDefJoystick));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_cmdLoad = new Button();
			_cmdLoad.Click += new EventHandler(cmdLoad_Click);
			_cmdSave = new Button();
			_cmdSave.Click += new EventHandler(cmdSave_Click);
			comdlgFileOpen = new OpenFileDialog();
			comdlgFileSave = new SaveFileDialog();
			_CancelButton_Renamed = new Button();
			_CancelButton_Renamed.Click += new EventHandler(CancelButton_Renamed_Click);
			_OKButton = new Button();
			_OKButton.Click += new EventHandler(OKButton_Click);
			fraButtonDefintions = new GroupBox();
			lvButtons = new ListView();
			_cmdUndefine = new Button();
			_cmdUndefine.Click += new EventHandler(cmdUndefine_Click);
			_cmdDefine = new Button();
			_cmdDefine.Click += new EventHandler(cmdDefine_Click);
			fraDirectionDefintions = new GroupBox();
			_txtValueDown = new TextBox();
			_txtValueDown.TextChanged += new EventHandler(txtValueDown_TextChanged);
			_txtValueDown.Enter += new EventHandler(txtValueDown_Enter);
			_txtPortDown = new TextBox();
			_txtPortDown.TextChanged += new EventHandler(txtPortDown_TextChanged);
			_txtPortDown.Enter += new EventHandler(txtPortDown_Enter);
			_txtValueLeft = new TextBox();
			_txtValueLeft.TextChanged += new EventHandler(txtValueLeft_TextChanged);
			_txtValueLeft.Enter += new EventHandler(txtValueLeft_Enter);
			_txtPortLeft = new TextBox();
			_txtPortLeft.TextChanged += new EventHandler(txtPortLeft_TextChanged);
			_txtPortLeft.Enter += new EventHandler(txtPortLeft_Enter);
			_txtValueRight = new TextBox();
			_txtValueRight.TextChanged += new EventHandler(txtValueRight_TextChanged);
			_txtValueRight.Enter += new EventHandler(txtValueRight_Enter);
			_txtPortRight = new TextBox();
			_txtPortRight.TextChanged += new EventHandler(txtPortRight_TextChanged);
			_txtPortRight.Enter += new EventHandler(txtPortRight_Enter);
			_txtValueUp = new TextBox();
			_txtValueUp.TextChanged += new EventHandler(txtValueUp_TextChanged);
			_txtValueUp.Enter += new EventHandler(txtValueUp_Enter);
			_txtPortUp = new TextBox();
			_txtPortUp.TextChanged += new EventHandler(txtPortUp_TextChanged);
			_txtPortUp.Enter += new EventHandler(txtPortUp_Enter);
			_txtRight = new TextBox();
			_txtRight.KeyDown += new KeyEventHandler(txtRight_KeyDown);
			_txtLeft = new TextBox();
			_txtLeft.KeyDown += new KeyEventHandler(txtLeft_KeyDown);
			_txtDown = new TextBox();
			_txtDown.KeyDown += new KeyEventHandler(txtDown_KeyDown);
			_txtUp = new TextBox();
			_txtUp.KeyDown += new KeyEventHandler(txtUp_KeyDown);
			_lblOr_3 = new Label();
			_lblOr_2 = new Label();
			_lblOr_1 = new Label();
			_lblOr_0 = new Label();
			_lblKomma_3 = new Label();
			_lblKomma_2 = new Label();
			_lblKomma_1 = new Label();
			_lblKomma_0 = new Label();
			lblRight = new Label();
			lblLeft = new Label();
			lblDown = new Label();
			lblUp = new Label();
			lblKomma = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			lblOr = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			fraButtonDefintions.SuspendLayout();
			fraDirectionDefintions.SuspendLayout();
			SuspendLayout();
			ToolTip1.Active = true;
			((System.ComponentModel.ISupportInitialize)lblKomma).BeginInit();
			((System.ComponentModel.ISupportInitialize)lblOr).BeginInit();
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "User defined joystick";
			ClientSize = new Size(377, 271);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmDefJoystick";
			_cmdLoad.TextAlign = ContentAlignment.MiddleCenter;
			_cmdLoad.Text = "Load...";
			_cmdLoad.Size = new Size(73, 25);
			_cmdLoad.Location = new Point(299, 94);
			_cmdLoad.TabIndex = 32;
			ToolTip1.SetToolTip(_cmdLoad, "Loads a joystick configuration from a file");
			_cmdLoad.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdLoad.BackColor = SystemColors.Control;
			_cmdLoad.CausesValidation = true;
			_cmdLoad.Enabled = true;
			_cmdLoad.ForeColor = SystemColors.ControlText;
			_cmdLoad.Cursor = Cursors.Default;
			_cmdLoad.RightToLeft = RightToLeft.No;
			_cmdLoad.TabStop = true;
			_cmdLoad.Name = "_cmdLoad";
			_cmdSave.TextAlign = ContentAlignment.MiddleCenter;
			_cmdSave.Text = "Save...";
			_cmdSave.Size = new Size(73, 25);
			_cmdSave.Location = new Point(299, 66);
			_cmdSave.TabIndex = 31;
			ToolTip1.SetToolTip(_cmdSave, "Saves the current joystick configuration into a file");
			_cmdSave.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdSave.BackColor = SystemColors.Control;
			_cmdSave.CausesValidation = true;
			_cmdSave.Enabled = true;
			_cmdSave.ForeColor = SystemColors.ControlText;
			_cmdSave.Cursor = Cursors.Default;
			_cmdSave.RightToLeft = RightToLeft.No;
			_cmdSave.TabStop = true;
			_cmdSave.Name = "_cmdSave";
			_CancelButton_Renamed.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _CancelButton_Renamed;
			_CancelButton_Renamed.Text = "Cancel";
			_CancelButton_Renamed.Size = new Size(73, 25);
			_CancelButton_Renamed.Location = new Point(299, 38);
			_CancelButton_Renamed.TabIndex = 30;
			_CancelButton_Renamed.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_CancelButton_Renamed.BackColor = SystemColors.Control;
			_CancelButton_Renamed.CausesValidation = true;
			_CancelButton_Renamed.Enabled = true;
			_CancelButton_Renamed.ForeColor = SystemColors.ControlText;
			_CancelButton_Renamed.Cursor = Cursors.Default;
			_CancelButton_Renamed.RightToLeft = RightToLeft.No;
			_CancelButton_Renamed.TabStop = true;
			_CancelButton_Renamed.Name = "_CancelButton_Renamed";
			_OKButton.TextAlign = ContentAlignment.MiddleCenter;
			_OKButton.Text = "OK";
			AcceptButton = _OKButton;
			_OKButton.Size = new Size(73, 25);
			_OKButton.Location = new Point(299, 10);
			_OKButton.TabIndex = 29;
			_OKButton.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_OKButton.BackColor = SystemColors.Control;
			_OKButton.CausesValidation = true;
			_OKButton.Enabled = true;
			_OKButton.ForeColor = SystemColors.ControlText;
			_OKButton.Cursor = Cursors.Default;
			_OKButton.RightToLeft = RightToLeft.No;
			_OKButton.TabStop = true;
			_OKButton.Name = "_OKButton";
			fraButtonDefintions.Text = "Buttons";
			fraButtonDefintions.Size = new Size(290, 135);
			fraButtonDefintions.Location = new Point(3, 131);
			fraButtonDefintions.TabIndex = 25;
			fraButtonDefintions.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			fraButtonDefintions.BackColor = SystemColors.Control;
			fraButtonDefintions.Enabled = true;
			fraButtonDefintions.ForeColor = SystemColors.ControlText;
			fraButtonDefintions.RightToLeft = RightToLeft.No;
			fraButtonDefintions.Visible = true;
			fraButtonDefintions.Padding = new Padding(0);
			fraButtonDefintions.Name = "fraButtonDefintions";
			lvButtons.Size = new Size(186, 108);
			lvButtons.Location = new Point(8, 17);
			lvButtons.TabIndex = 26;
			lvButtons.LabelWrap = true;
			lvButtons.HideSelection = false;
			lvButtons.FullRowSelect = true;
			lvButtons.GridLines = true;
			lvButtons.ForeColor = SystemColors.WindowText;
			lvButtons.BackColor = SystemColors.Window;
			lvButtons.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lvButtons.LabelEdit = true;
			lvButtons.BorderStyle = BorderStyle.Fixed3D;
			lvButtons.Name = "lvButtons";
			_cmdUndefine.TextAlign = ContentAlignment.MiddleCenter;
			_cmdUndefine.Text = "Undefine";
			_cmdUndefine.Size = new Size(73, 25);
			_cmdUndefine.Location = new Point(199, 45);
			_cmdUndefine.TabIndex = 28;
			_cmdUndefine.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdUndefine.BackColor = SystemColors.Control;
			_cmdUndefine.CausesValidation = true;
			_cmdUndefine.Enabled = true;
			_cmdUndefine.ForeColor = SystemColors.ControlText;
			_cmdUndefine.Cursor = Cursors.Default;
			_cmdUndefine.RightToLeft = RightToLeft.No;
			_cmdUndefine.TabStop = true;
			_cmdUndefine.Name = "_cmdUndefine";
			_cmdDefine.TextAlign = ContentAlignment.MiddleCenter;
			_cmdDefine.Text = "Define";
			_cmdDefine.Size = new Size(73, 25);
			_cmdDefine.Location = new Point(199, 17);
			_cmdDefine.TabIndex = 27;
			_cmdDefine.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdDefine.BackColor = SystemColors.Control;
			_cmdDefine.CausesValidation = true;
			_cmdDefine.Enabled = true;
			_cmdDefine.ForeColor = SystemColors.ControlText;
			_cmdDefine.Cursor = Cursors.Default;
			_cmdDefine.RightToLeft = RightToLeft.No;
			_cmdDefine.TabStop = true;
			_cmdDefine.Name = "_cmdDefine";
			fraDirectionDefintions.Text = "Directions";
			fraDirectionDefintions.Size = new Size(291, 125);
			fraDirectionDefintions.Location = new Point(3, 5);
			fraDirectionDefintions.TabIndex = 0;
			fraDirectionDefintions.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			fraDirectionDefintions.BackColor = SystemColors.Control;
			fraDirectionDefintions.Enabled = true;
			fraDirectionDefintions.ForeColor = SystemColors.ControlText;
			fraDirectionDefintions.RightToLeft = RightToLeft.No;
			fraDirectionDefintions.Visible = true;
			fraDirectionDefintions.Padding = new Padding(0);
			fraDirectionDefintions.Name = "fraDirectionDefintions";
			_txtValueDown.AutoSize = false;
			_txtValueDown.TextAlign = HorizontalAlignment.Right;
			_txtValueDown.Size = new Size(26, 21);
			_txtValueDown.Location = new Point(258, 41);
			_txtValueDown.TabIndex = 12;
			_txtValueDown.Text = "255";
			_txtValueDown.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtValueDown.AcceptsReturn = true;
			_txtValueDown.BackColor = SystemColors.Window;
			_txtValueDown.CausesValidation = true;
			_txtValueDown.Enabled = true;
			_txtValueDown.ForeColor = SystemColors.WindowText;
			_txtValueDown.HideSelection = true;
			_txtValueDown.ReadOnly = false;
			_txtValueDown.MaxLength = 0;
			_txtValueDown.Cursor = Cursors.IBeam;
			_txtValueDown.Multiline = false;
			_txtValueDown.RightToLeft = RightToLeft.No;
			_txtValueDown.ScrollBars = ScrollBars.None;
			_txtValueDown.TabStop = true;
			_txtValueDown.Visible = true;
			_txtValueDown.BorderStyle = BorderStyle.Fixed3D;
			_txtValueDown.Name = "_txtValueDown";
			_txtPortDown.AutoSize = false;
			_txtPortDown.TextAlign = HorizontalAlignment.Right;
			_txtPortDown.Size = new Size(38, 21);
			_txtPortDown.Location = new Point(212, 41);
			_txtPortDown.TabIndex = 10;
			_txtPortDown.Text = "65535";
			_txtPortDown.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtPortDown.AcceptsReturn = true;
			_txtPortDown.BackColor = SystemColors.Window;
			_txtPortDown.CausesValidation = true;
			_txtPortDown.Enabled = true;
			_txtPortDown.ForeColor = SystemColors.WindowText;
			_txtPortDown.HideSelection = true;
			_txtPortDown.ReadOnly = false;
			_txtPortDown.MaxLength = 0;
			_txtPortDown.Cursor = Cursors.IBeam;
			_txtPortDown.Multiline = false;
			_txtPortDown.RightToLeft = RightToLeft.No;
			_txtPortDown.ScrollBars = ScrollBars.None;
			_txtPortDown.TabStop = true;
			_txtPortDown.Visible = true;
			_txtPortDown.BorderStyle = BorderStyle.Fixed3D;
			_txtPortDown.Name = "_txtPortDown";
			_txtValueLeft.AutoSize = false;
			_txtValueLeft.TextAlign = HorizontalAlignment.Right;
			_txtValueLeft.Size = new Size(26, 21);
			_txtValueLeft.Location = new Point(258, 65);
			_txtValueLeft.TabIndex = 18;
			_txtValueLeft.Text = "255";
			_txtValueLeft.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtValueLeft.AcceptsReturn = true;
			_txtValueLeft.BackColor = SystemColors.Window;
			_txtValueLeft.CausesValidation = true;
			_txtValueLeft.Enabled = true;
			_txtValueLeft.ForeColor = SystemColors.WindowText;
			_txtValueLeft.HideSelection = true;
			_txtValueLeft.ReadOnly = false;
			_txtValueLeft.MaxLength = 0;
			_txtValueLeft.Cursor = Cursors.IBeam;
			_txtValueLeft.Multiline = false;
			_txtValueLeft.RightToLeft = RightToLeft.No;
			_txtValueLeft.ScrollBars = ScrollBars.None;
			_txtValueLeft.TabStop = true;
			_txtValueLeft.Visible = true;
			_txtValueLeft.BorderStyle = BorderStyle.Fixed3D;
			_txtValueLeft.Name = "_txtValueLeft";
			_txtPortLeft.AutoSize = false;
			_txtPortLeft.TextAlign = HorizontalAlignment.Right;
			_txtPortLeft.Size = new Size(38, 21);
			_txtPortLeft.Location = new Point(212, 65);
			_txtPortLeft.TabIndex = 16;
			_txtPortLeft.Text = "65535";
			_txtPortLeft.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtPortLeft.AcceptsReturn = true;
			_txtPortLeft.BackColor = SystemColors.Window;
			_txtPortLeft.CausesValidation = true;
			_txtPortLeft.Enabled = true;
			_txtPortLeft.ForeColor = SystemColors.WindowText;
			_txtPortLeft.HideSelection = true;
			_txtPortLeft.ReadOnly = false;
			_txtPortLeft.MaxLength = 0;
			_txtPortLeft.Cursor = Cursors.IBeam;
			_txtPortLeft.Multiline = false;
			_txtPortLeft.RightToLeft = RightToLeft.No;
			_txtPortLeft.ScrollBars = ScrollBars.None;
			_txtPortLeft.TabStop = true;
			_txtPortLeft.Visible = true;
			_txtPortLeft.BorderStyle = BorderStyle.Fixed3D;
			_txtPortLeft.Name = "_txtPortLeft";
			_txtValueRight.AutoSize = false;
			_txtValueRight.TextAlign = HorizontalAlignment.Right;
			_txtValueRight.Size = new Size(26, 21);
			_txtValueRight.Location = new Point(258, 89);
			_txtValueRight.TabIndex = 24;
			_txtValueRight.Text = "255";
			_txtValueRight.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtValueRight.AcceptsReturn = true;
			_txtValueRight.BackColor = SystemColors.Window;
			_txtValueRight.CausesValidation = true;
			_txtValueRight.Enabled = true;
			_txtValueRight.ForeColor = SystemColors.WindowText;
			_txtValueRight.HideSelection = true;
			_txtValueRight.ReadOnly = false;
			_txtValueRight.MaxLength = 0;
			_txtValueRight.Cursor = Cursors.IBeam;
			_txtValueRight.Multiline = false;
			_txtValueRight.RightToLeft = RightToLeft.No;
			_txtValueRight.ScrollBars = ScrollBars.None;
			_txtValueRight.TabStop = true;
			_txtValueRight.Visible = true;
			_txtValueRight.BorderStyle = BorderStyle.Fixed3D;
			_txtValueRight.Name = "_txtValueRight";
			_txtPortRight.AutoSize = false;
			_txtPortRight.TextAlign = HorizontalAlignment.Right;
			_txtPortRight.Size = new Size(38, 21);
			_txtPortRight.Location = new Point(212, 89);
			_txtPortRight.TabIndex = 22;
			_txtPortRight.Text = "65535";
			_txtPortRight.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtPortRight.AcceptsReturn = true;
			_txtPortRight.BackColor = SystemColors.Window;
			_txtPortRight.CausesValidation = true;
			_txtPortRight.Enabled = true;
			_txtPortRight.ForeColor = SystemColors.WindowText;
			_txtPortRight.HideSelection = true;
			_txtPortRight.ReadOnly = false;
			_txtPortRight.MaxLength = 0;
			_txtPortRight.Cursor = Cursors.IBeam;
			_txtPortRight.Multiline = false;
			_txtPortRight.RightToLeft = RightToLeft.No;
			_txtPortRight.ScrollBars = ScrollBars.None;
			_txtPortRight.TabStop = true;
			_txtPortRight.Visible = true;
			_txtPortRight.BorderStyle = BorderStyle.Fixed3D;
			_txtPortRight.Name = "_txtPortRight";
			_txtValueUp.AutoSize = false;
			_txtValueUp.TextAlign = HorizontalAlignment.Right;
			_txtValueUp.Size = new Size(26, 21);
			_txtValueUp.Location = new Point(258, 17);
			_txtValueUp.TabIndex = 6;
			_txtValueUp.Text = "255";
			_txtValueUp.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtValueUp.AcceptsReturn = true;
			_txtValueUp.BackColor = SystemColors.Window;
			_txtValueUp.CausesValidation = true;
			_txtValueUp.Enabled = true;
			_txtValueUp.ForeColor = SystemColors.WindowText;
			_txtValueUp.HideSelection = true;
			_txtValueUp.ReadOnly = false;
			_txtValueUp.MaxLength = 0;
			_txtValueUp.Cursor = Cursors.IBeam;
			_txtValueUp.Multiline = false;
			_txtValueUp.RightToLeft = RightToLeft.No;
			_txtValueUp.ScrollBars = ScrollBars.None;
			_txtValueUp.TabStop = true;
			_txtValueUp.Visible = true;
			_txtValueUp.BorderStyle = BorderStyle.Fixed3D;
			_txtValueUp.Name = "_txtValueUp";
			_txtPortUp.AutoSize = false;
			_txtPortUp.TextAlign = HorizontalAlignment.Right;
			_txtPortUp.Size = new Size(38, 21);
			_txtPortUp.Location = new Point(212, 17);
			_txtPortUp.TabIndex = 4;
			_txtPortUp.Text = "65535";
			_txtPortUp.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtPortUp.AcceptsReturn = true;
			_txtPortUp.BackColor = SystemColors.Window;
			_txtPortUp.CausesValidation = true;
			_txtPortUp.Enabled = true;
			_txtPortUp.ForeColor = SystemColors.WindowText;
			_txtPortUp.HideSelection = true;
			_txtPortUp.ReadOnly = false;
			_txtPortUp.MaxLength = 0;
			_txtPortUp.Cursor = Cursors.IBeam;
			_txtPortUp.Multiline = false;
			_txtPortUp.RightToLeft = RightToLeft.No;
			_txtPortUp.ScrollBars = ScrollBars.None;
			_txtPortUp.TabStop = true;
			_txtPortUp.Visible = true;
			_txtPortUp.BorderStyle = BorderStyle.Fixed3D;
			_txtPortUp.Name = "_txtPortUp";
			_txtRight.AutoSize = false;
			_txtRight.Size = new Size(29, 21);
			_txtRight.Location = new Point(164, 89);
			_txtRight.TabIndex = 20;
			_txtRight.Text = "XXX";
			_txtRight.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtRight.AcceptsReturn = true;
			_txtRight.TextAlign = HorizontalAlignment.Left;
			_txtRight.BackColor = SystemColors.Window;
			_txtRight.CausesValidation = true;
			_txtRight.Enabled = true;
			_txtRight.ForeColor = SystemColors.WindowText;
			_txtRight.HideSelection = true;
			_txtRight.ReadOnly = false;
			_txtRight.MaxLength = 0;
			_txtRight.Cursor = Cursors.IBeam;
			_txtRight.Multiline = false;
			_txtRight.RightToLeft = RightToLeft.No;
			_txtRight.ScrollBars = ScrollBars.None;
			_txtRight.TabStop = true;
			_txtRight.Visible = true;
			_txtRight.BorderStyle = BorderStyle.Fixed3D;
			_txtRight.Name = "_txtRight";
			_txtLeft.AutoSize = false;
			_txtLeft.Size = new Size(29, 21);
			_txtLeft.Location = new Point(164, 65);
			_txtLeft.TabIndex = 14;
			_txtLeft.Text = "X";
			_txtLeft.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtLeft.AcceptsReturn = true;
			_txtLeft.TextAlign = HorizontalAlignment.Left;
			_txtLeft.BackColor = SystemColors.Window;
			_txtLeft.CausesValidation = true;
			_txtLeft.Enabled = true;
			_txtLeft.ForeColor = SystemColors.WindowText;
			_txtLeft.HideSelection = true;
			_txtLeft.ReadOnly = false;
			_txtLeft.MaxLength = 0;
			_txtLeft.Cursor = Cursors.IBeam;
			_txtLeft.Multiline = false;
			_txtLeft.RightToLeft = RightToLeft.No;
			_txtLeft.ScrollBars = ScrollBars.None;
			_txtLeft.TabStop = true;
			_txtLeft.Visible = true;
			_txtLeft.BorderStyle = BorderStyle.Fixed3D;
			_txtLeft.Name = "_txtLeft";
			_txtDown.AutoSize = false;
			_txtDown.Size = new Size(29, 21);
			_txtDown.Location = new Point(164, 41);
			_txtDown.TabIndex = 8;
			_txtDown.Text = "X";
			_txtDown.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtDown.AcceptsReturn = true;
			_txtDown.TextAlign = HorizontalAlignment.Left;
			_txtDown.BackColor = SystemColors.Window;
			_txtDown.CausesValidation = true;
			_txtDown.Enabled = true;
			_txtDown.ForeColor = SystemColors.WindowText;
			_txtDown.HideSelection = true;
			_txtDown.ReadOnly = false;
			_txtDown.MaxLength = 0;
			_txtDown.Cursor = Cursors.IBeam;
			_txtDown.Multiline = false;
			_txtDown.RightToLeft = RightToLeft.No;
			_txtDown.ScrollBars = ScrollBars.None;
			_txtDown.TabStop = true;
			_txtDown.Visible = true;
			_txtDown.BorderStyle = BorderStyle.Fixed3D;
			_txtDown.Name = "_txtDown";
			_txtUp.AutoSize = false;
			_txtUp.Size = new Size(29, 21);
			_txtUp.Location = new Point(164, 17);
			_txtUp.TabIndex = 2;
			_txtUp.Text = "X";
			_txtUp.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtUp.AcceptsReturn = true;
			_txtUp.TextAlign = HorizontalAlignment.Left;
			_txtUp.BackColor = SystemColors.Window;
			_txtUp.CausesValidation = true;
			_txtUp.Enabled = true;
			_txtUp.ForeColor = SystemColors.WindowText;
			_txtUp.HideSelection = true;
			_txtUp.ReadOnly = false;
			_txtUp.MaxLength = 0;
			_txtUp.Cursor = Cursors.IBeam;
			_txtUp.Multiline = false;
			_txtUp.RightToLeft = RightToLeft.No;
			_txtUp.ScrollBars = ScrollBars.None;
			_txtUp.TabStop = true;
			_txtUp.Visible = true;
			_txtUp.BorderStyle = BorderStyle.Fixed3D;
			_txtUp.Name = "_txtUp";
			_lblOr_3.Text = "or";
			_lblOr_3.Size = new Size(11, 14);
			_lblOr_3.Location = new Point(197, 93);
			_lblOr_3.TabIndex = 21;
			_lblOr_3.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblOr_3.TextAlign = ContentAlignment.TopLeft;
			_lblOr_3.BackColor = SystemColors.Control;
			_lblOr_3.Enabled = true;
			_lblOr_3.ForeColor = SystemColors.ControlText;
			_lblOr_3.Cursor = Cursors.Default;
			_lblOr_3.RightToLeft = RightToLeft.No;
			_lblOr_3.UseMnemonic = true;
			_lblOr_3.Visible = true;
			_lblOr_3.AutoSize = false;
			_lblOr_3.BorderStyle = BorderStyle.None;
			_lblOr_3.Name = "_lblOr_3";
			_lblOr_2.Text = "or";
			_lblOr_2.Size = new Size(11, 14);
			_lblOr_2.Location = new Point(197, 69);
			_lblOr_2.TabIndex = 15;
			_lblOr_2.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblOr_2.TextAlign = ContentAlignment.TopLeft;
			_lblOr_2.BackColor = SystemColors.Control;
			_lblOr_2.Enabled = true;
			_lblOr_2.ForeColor = SystemColors.ControlText;
			_lblOr_2.Cursor = Cursors.Default;
			_lblOr_2.RightToLeft = RightToLeft.No;
			_lblOr_2.UseMnemonic = true;
			_lblOr_2.Visible = true;
			_lblOr_2.AutoSize = false;
			_lblOr_2.BorderStyle = BorderStyle.None;
			_lblOr_2.Name = "_lblOr_2";
			_lblOr_1.Text = "or";
			_lblOr_1.Size = new Size(11, 14);
			_lblOr_1.Location = new Point(197, 45);
			_lblOr_1.TabIndex = 9;
			_lblOr_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblOr_1.TextAlign = ContentAlignment.TopLeft;
			_lblOr_1.BackColor = SystemColors.Control;
			_lblOr_1.Enabled = true;
			_lblOr_1.ForeColor = SystemColors.ControlText;
			_lblOr_1.Cursor = Cursors.Default;
			_lblOr_1.RightToLeft = RightToLeft.No;
			_lblOr_1.UseMnemonic = true;
			_lblOr_1.Visible = true;
			_lblOr_1.AutoSize = false;
			_lblOr_1.BorderStyle = BorderStyle.None;
			_lblOr_1.Name = "_lblOr_1";
			_lblOr_0.Text = "or";
			_lblOr_0.Size = new Size(11, 14);
			_lblOr_0.Location = new Point(197, 21);
			_lblOr_0.TabIndex = 3;
			_lblOr_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblOr_0.TextAlign = ContentAlignment.TopLeft;
			_lblOr_0.BackColor = SystemColors.Control;
			_lblOr_0.Enabled = true;
			_lblOr_0.ForeColor = SystemColors.ControlText;
			_lblOr_0.Cursor = Cursors.Default;
			_lblOr_0.RightToLeft = RightToLeft.No;
			_lblOr_0.UseMnemonic = true;
			_lblOr_0.Visible = true;
			_lblOr_0.AutoSize = false;
			_lblOr_0.BorderStyle = BorderStyle.None;
			_lblOr_0.Name = "_lblOr_0";
			_lblKomma_3.Text = ",";
			_lblKomma_3.Size = new Size(5, 14);
			_lblKomma_3.Location = new Point(252, 45);
			_lblKomma_3.TabIndex = 11;
			_lblKomma_3.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblKomma_3.TextAlign = ContentAlignment.TopLeft;
			_lblKomma_3.BackColor = SystemColors.Control;
			_lblKomma_3.Enabled = true;
			_lblKomma_3.ForeColor = SystemColors.ControlText;
			_lblKomma_3.Cursor = Cursors.Default;
			_lblKomma_3.RightToLeft = RightToLeft.No;
			_lblKomma_3.UseMnemonic = true;
			_lblKomma_3.Visible = true;
			_lblKomma_3.AutoSize = false;
			_lblKomma_3.BorderStyle = BorderStyle.None;
			_lblKomma_3.Name = "_lblKomma_3";
			_lblKomma_2.Text = ",";
			_lblKomma_2.Size = new Size(5, 14);
			_lblKomma_2.Location = new Point(252, 69);
			_lblKomma_2.TabIndex = 17;
			_lblKomma_2.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblKomma_2.TextAlign = ContentAlignment.TopLeft;
			_lblKomma_2.BackColor = SystemColors.Control;
			_lblKomma_2.Enabled = true;
			_lblKomma_2.ForeColor = SystemColors.ControlText;
			_lblKomma_2.Cursor = Cursors.Default;
			_lblKomma_2.RightToLeft = RightToLeft.No;
			_lblKomma_2.UseMnemonic = true;
			_lblKomma_2.Visible = true;
			_lblKomma_2.AutoSize = false;
			_lblKomma_2.BorderStyle = BorderStyle.None;
			_lblKomma_2.Name = "_lblKomma_2";
			_lblKomma_1.Text = ",";
			_lblKomma_1.Size = new Size(5, 14);
			_lblKomma_1.Location = new Point(252, 93);
			_lblKomma_1.TabIndex = 23;
			_lblKomma_1.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblKomma_1.TextAlign = ContentAlignment.TopLeft;
			_lblKomma_1.BackColor = SystemColors.Control;
			_lblKomma_1.Enabled = true;
			_lblKomma_1.ForeColor = SystemColors.ControlText;
			_lblKomma_1.Cursor = Cursors.Default;
			_lblKomma_1.RightToLeft = RightToLeft.No;
			_lblKomma_1.UseMnemonic = true;
			_lblKomma_1.Visible = true;
			_lblKomma_1.AutoSize = false;
			_lblKomma_1.BorderStyle = BorderStyle.None;
			_lblKomma_1.Name = "_lblKomma_1";
			_lblKomma_0.Text = ",";
			_lblKomma_0.Size = new Size(5, 14);
			_lblKomma_0.Location = new Point(252, 21);
			_lblKomma_0.TabIndex = 5;
			_lblKomma_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblKomma_0.TextAlign = ContentAlignment.TopLeft;
			_lblKomma_0.BackColor = SystemColors.Control;
			_lblKomma_0.Enabled = true;
			_lblKomma_0.ForeColor = SystemColors.ControlText;
			_lblKomma_0.Cursor = Cursors.Default;
			_lblKomma_0.RightToLeft = RightToLeft.No;
			_lblKomma_0.UseMnemonic = true;
			_lblKomma_0.Visible = true;
			_lblKomma_0.AutoSize = false;
			_lblKomma_0.BorderStyle = BorderStyle.None;
			_lblKomma_0.Name = "_lblKomma_0";
			lblRight.TextAlign = ContentAlignment.TopRight;
			lblRight.Text = "Joystick right translated into:";
			lblRight.Size = new Size(148, 14);
			lblRight.Location = new Point(13, 93);
			lblRight.TabIndex = 19;
			lblRight.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblRight.BackColor = SystemColors.Control;
			lblRight.Enabled = true;
			lblRight.ForeColor = SystemColors.ControlText;
			lblRight.Cursor = Cursors.Default;
			lblRight.RightToLeft = RightToLeft.No;
			lblRight.UseMnemonic = true;
			lblRight.Visible = true;
			lblRight.AutoSize = false;
			lblRight.BorderStyle = BorderStyle.None;
			lblRight.Name = "lblRight";
			lblLeft.TextAlign = ContentAlignment.TopRight;
			lblLeft.Text = "Joystick left translated into:";
			lblLeft.Size = new Size(148, 14);
			lblLeft.Location = new Point(13, 69);
			lblLeft.TabIndex = 13;
			lblLeft.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblLeft.BackColor = SystemColors.Control;
			lblLeft.Enabled = true;
			lblLeft.ForeColor = SystemColors.ControlText;
			lblLeft.Cursor = Cursors.Default;
			lblLeft.RightToLeft = RightToLeft.No;
			lblLeft.UseMnemonic = true;
			lblLeft.Visible = true;
			lblLeft.AutoSize = false;
			lblLeft.BorderStyle = BorderStyle.None;
			lblLeft.Name = "lblLeft";
			lblDown.TextAlign = ContentAlignment.TopRight;
			lblDown.Text = "Joystick down translated into:";
			lblDown.Size = new Size(148, 14);
			lblDown.Location = new Point(13, 45);
			lblDown.TabIndex = 7;
			lblDown.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblDown.BackColor = SystemColors.Control;
			lblDown.Enabled = true;
			lblDown.ForeColor = SystemColors.ControlText;
			lblDown.Cursor = Cursors.Default;
			lblDown.RightToLeft = RightToLeft.No;
			lblDown.UseMnemonic = true;
			lblDown.Visible = true;
			lblDown.AutoSize = false;
			lblDown.BorderStyle = BorderStyle.None;
			lblDown.Name = "lblDown";
			lblUp.TextAlign = ContentAlignment.TopRight;
			lblUp.Text = "Joystick up translated into:";
			lblUp.Size = new Size(148, 14);
			lblUp.Location = new Point(13, 21);
			lblUp.TabIndex = 1;
			lblUp.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblUp.BackColor = SystemColors.Control;
			lblUp.Enabled = true;
			lblUp.ForeColor = SystemColors.ControlText;
			lblUp.Cursor = Cursors.Default;
			lblUp.RightToLeft = RightToLeft.No;
			lblUp.UseMnemonic = true;
			lblUp.Visible = true;
			lblUp.AutoSize = false;
			lblUp.BorderStyle = BorderStyle.None;
			lblUp.Name = "lblUp";
			Controls.Add(_cmdLoad);
			Controls.Add(_cmdSave);
			Controls.Add(_CancelButton_Renamed);
			Controls.Add(_OKButton);
			Controls.Add(fraButtonDefintions);
			Controls.Add(fraDirectionDefintions);
			fraButtonDefintions.Controls.Add(lvButtons);
			fraButtonDefintions.Controls.Add(_cmdUndefine);
			fraButtonDefintions.Controls.Add(_cmdDefine);
			fraDirectionDefintions.Controls.Add(_txtValueDown);
			fraDirectionDefintions.Controls.Add(_txtPortDown);
			fraDirectionDefintions.Controls.Add(_txtValueLeft);
			fraDirectionDefintions.Controls.Add(_txtPortLeft);
			fraDirectionDefintions.Controls.Add(_txtValueRight);
			fraDirectionDefintions.Controls.Add(_txtPortRight);
			fraDirectionDefintions.Controls.Add(_txtValueUp);
			fraDirectionDefintions.Controls.Add(_txtPortUp);
			fraDirectionDefintions.Controls.Add(_txtRight);
			fraDirectionDefintions.Controls.Add(_txtLeft);
			fraDirectionDefintions.Controls.Add(_txtDown);
			fraDirectionDefintions.Controls.Add(_txtUp);
			fraDirectionDefintions.Controls.Add(_lblOr_3);
			fraDirectionDefintions.Controls.Add(_lblOr_2);
			fraDirectionDefintions.Controls.Add(_lblOr_1);
			fraDirectionDefintions.Controls.Add(_lblOr_0);
			fraDirectionDefintions.Controls.Add(_lblKomma_3);
			fraDirectionDefintions.Controls.Add(_lblKomma_2);
			fraDirectionDefintions.Controls.Add(_lblKomma_1);
			fraDirectionDefintions.Controls.Add(_lblKomma_0);
			fraDirectionDefintions.Controls.Add(lblRight);
			fraDirectionDefintions.Controls.Add(lblLeft);
			fraDirectionDefintions.Controls.Add(lblDown);
			fraDirectionDefintions.Controls.Add(lblUp);
			lblKomma.SetIndex(_lblKomma_3, Conversions.ToShort(3));
			lblKomma.SetIndex(_lblKomma_2, Conversions.ToShort(2));
			lblKomma.SetIndex(_lblKomma_1, Conversions.ToShort(1));
			lblKomma.SetIndex(_lblKomma_0, Conversions.ToShort(0));
			lblOr.SetIndex(_lblOr_3, Conversions.ToShort(3));
			lblOr.SetIndex(_lblOr_2, Conversions.ToShort(2));
			lblOr.SetIndex(_lblOr_1, Conversions.ToShort(1));
			lblOr.SetIndex(_lblOr_0, Conversions.ToShort(0));
			((System.ComponentModel.ISupportInitialize)lblOr).EndInit();
			((System.ComponentModel.ISupportInitialize)lblKomma).EndInit();
			fraButtonDefintions.ResumeLayout(false);
			fraDirectionDefintions.ResumeLayout(false);
			Load += new EventHandler(frmDefJoystick_Load);
			Activated += new EventHandler(frmDefJoystick_Activated);
			FormClosed += new FormClosedEventHandler(frmDefJoystick_FormClosed);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}