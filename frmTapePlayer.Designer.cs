﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmTapePlayer
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmTapePlayer() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_lstBlocks.Name = "lstBlocks";
			_cmdNext.Name = "cmdNext";
			_cmdPrev.Name = "cmdPrev";
			_cmdRewind.Name = "cmdRewind";
			_cmdStop.Name = "cmdStop";
			_cmdPlay.Name = "cmdPlay";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private ListBox _lstBlocks;

		public ListBox lstBlocks
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _lstBlocks;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_lstBlocks != null)
				{
					_lstBlocks.SelectedIndexChanged -= lstBlocks_SelectedIndexChanged;
				}

				_lstBlocks = value;
				if (_lstBlocks != null)
				{
					_lstBlocks.SelectedIndexChanged += lstBlocks_SelectedIndexChanged;
				}
			}
		}

		private Button _cmdNext;

		public Button cmdNext
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdNext;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdNext != null)
				{
					_cmdNext.Click -= cmdNext_Click;
				}

				_cmdNext = value;
				if (_cmdNext != null)
				{
					_cmdNext.Click += cmdNext_Click;
				}
			}
		}

		private Button _cmdPrev;

		public Button cmdPrev
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdPrev;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdPrev != null)
				{
					_cmdPrev.Click -= cmdPrev_Click;
				}

				_cmdPrev = value;
				if (_cmdPrev != null)
				{
					_cmdPrev.Click += cmdPrev_Click;
				}
			}
		}

		private Button _cmdRewind;

		public Button cmdRewind
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdRewind;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdRewind != null)
				{
					_cmdRewind.Click -= cmdRewind_Click;
				}

				_cmdRewind = value;
				if (_cmdRewind != null)
				{
					_cmdRewind.Click += cmdRewind_Click;
				}
			}
		}

		private Button _cmdStop;

		public Button cmdStop
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdStop;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdStop != null)
				{
					_cmdStop.Click -= cmdStop_Click;
				}

				_cmdStop = value;
				if (_cmdStop != null)
				{
					_cmdStop.Click += cmdStop_Click;
				}
			}
		}

		private Button _cmdPlay;

		public Button cmdPlay
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdPlay;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdPlay != null)
				{
					_cmdPlay.Click -= cmdPlay_Click;
				}

				_cmdPlay = value;
				if (_cmdPlay != null)
				{
					_cmdPlay.Click += cmdPlay_Click;
				}
			}
		}
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmTapePlayer));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_lstBlocks = new ListBox();
			_lstBlocks.SelectedIndexChanged += new EventHandler(lstBlocks_SelectedIndexChanged);
			_cmdNext = new Button();
			_cmdNext.Click += new EventHandler(cmdNext_Click);
			_cmdPrev = new Button();
			_cmdPrev.Click += new EventHandler(cmdPrev_Click);
			_cmdRewind = new Button();
			_cmdRewind.Click += new EventHandler(cmdRewind_Click);
			_cmdStop = new Button();
			_cmdStop.Click += new EventHandler(cmdStop_Click);
			_cmdPlay = new Button();
			_cmdPlay.Click += new EventHandler(cmdPlay_Click);
			SuspendLayout();
			ToolTip1.Active = true;
			FormBorderStyle = FormBorderStyle.FixedToolWindow;
			Text = "vbSpec TZX Tape Controls";
			ClientSize = new Size(254, 207);
			Location = new Point(3, 19);
			KeyPreview = true;
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.WindowsDefaultLocation;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmTapePlayer";
			_lstBlocks.Size = new Size(253, 176);
			_lstBlocks.Location = new Point(0, 32);
			_lstBlocks.TabIndex = 5;
			_lstBlocks.TabStop = false;
			_lstBlocks.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lstBlocks.BorderStyle = BorderStyle.Fixed3D;
			_lstBlocks.BackColor = SystemColors.Window;
			_lstBlocks.CausesValidation = true;
			_lstBlocks.Enabled = true;
			_lstBlocks.ForeColor = SystemColors.WindowText;
			_lstBlocks.IntegralHeight = true;
			_lstBlocks.Cursor = Cursors.Default;
			_lstBlocks.SelectionMode = SelectionMode.One;
			_lstBlocks.RightToLeft = RightToLeft.No;
			_lstBlocks.Sorted = false;
			_lstBlocks.Visible = true;
			_lstBlocks.MultiColumn = false;
			_lstBlocks.Name = "_lstBlocks";
			_cmdNext.TextAlign = ContentAlignment.BottomCenter;
			_cmdNext.Size = new Size(49, 25);
			_cmdNext.Location = new Point(204, 4);
			_cmdNext.Image = (Image)resources.GetObject("cmdNext.Image");
			_cmdNext.TabIndex = 4;
			ToolTip1.SetToolTip(_cmdNext, "Next Block");
			_cmdNext.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdNext.BackColor = SystemColors.Control;
			_cmdNext.CausesValidation = true;
			_cmdNext.Enabled = true;
			_cmdNext.ForeColor = SystemColors.ControlText;
			_cmdNext.Cursor = Cursors.Default;
			_cmdNext.RightToLeft = RightToLeft.No;
			_cmdNext.TabStop = true;
			_cmdNext.Name = "_cmdNext";
			_cmdPrev.TextAlign = ContentAlignment.BottomCenter;
			_cmdPrev.Size = new Size(49, 25);
			_cmdPrev.Location = new Point(152, 4);
			_cmdPrev.Image = (Image)resources.GetObject("cmdPrev.Image");
			_cmdPrev.TabIndex = 3;
			ToolTip1.SetToolTip(_cmdPrev, "Previous Block");
			_cmdPrev.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdPrev.BackColor = SystemColors.Control;
			_cmdPrev.CausesValidation = true;
			_cmdPrev.Enabled = true;
			_cmdPrev.ForeColor = SystemColors.ControlText;
			_cmdPrev.Cursor = Cursors.Default;
			_cmdPrev.RightToLeft = RightToLeft.No;
			_cmdPrev.TabStop = true;
			_cmdPrev.Name = "_cmdPrev";
			_cmdRewind.TextAlign = ContentAlignment.BottomCenter;
			_cmdRewind.Size = new Size(53, 25);
			_cmdRewind.Location = new Point(96, 4);
			_cmdRewind.Image = (Image)resources.GetObject("cmdRewind.Image");
			_cmdRewind.TabIndex = 2;
			ToolTip1.SetToolTip(_cmdRewind, "Rewind to start");
			_cmdRewind.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdRewind.BackColor = SystemColors.Control;
			_cmdRewind.CausesValidation = true;
			_cmdRewind.Enabled = true;
			_cmdRewind.ForeColor = SystemColors.ControlText;
			_cmdRewind.Cursor = Cursors.Default;
			_cmdRewind.RightToLeft = RightToLeft.No;
			_cmdRewind.TabStop = true;
			_cmdRewind.Name = "_cmdRewind";
			_cmdStop.TextAlign = ContentAlignment.BottomCenter;
			_cmdStop.Size = new Size(45, 25);
			_cmdStop.Location = new Point(48, 4);
			_cmdStop.Image = (Image)resources.GetObject("cmdStop.Image");
			_cmdStop.TabIndex = 1;
			ToolTip1.SetToolTip(_cmdStop, "Stop/Pause");
			_cmdStop.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdStop.BackColor = SystemColors.Control;
			_cmdStop.CausesValidation = true;
			_cmdStop.Enabled = true;
			_cmdStop.ForeColor = SystemColors.ControlText;
			_cmdStop.Cursor = Cursors.Default;
			_cmdStop.RightToLeft = RightToLeft.No;
			_cmdStop.TabStop = true;
			_cmdStop.Name = "_cmdStop";
			_cmdPlay.TextAlign = ContentAlignment.BottomCenter;
			_cmdPlay.Size = new Size(45, 25);
			_cmdPlay.Location = new Point(0, 4);
			_cmdPlay.Image = (Image)resources.GetObject("cmdPlay.Image");
			_cmdPlay.TabIndex = 0;
			ToolTip1.SetToolTip(_cmdPlay, "Play");
			_cmdPlay.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdPlay.BackColor = SystemColors.Control;
			_cmdPlay.CausesValidation = true;
			_cmdPlay.Enabled = true;
			_cmdPlay.ForeColor = SystemColors.ControlText;
			_cmdPlay.Cursor = Cursors.Default;
			_cmdPlay.RightToLeft = RightToLeft.No;
			_cmdPlay.TabStop = true;
			_cmdPlay.Name = "_cmdPlay";
			Controls.Add(_lstBlocks);
			Controls.Add(_cmdNext);
			Controls.Add(_cmdPrev);
			Controls.Add(_cmdRewind);
			Controls.Add(_cmdStop);
			Controls.Add(_cmdPlay);
			Activated += new EventHandler(frmTapePlayer_Activated);
			KeyDown += new KeyEventHandler(frmTapePlayer_KeyDown);
			KeyUp += new KeyEventHandler(frmTapePlayer_KeyUp);
			Load += new EventHandler(frmTapePlayer_Load);
			FormClosed += new FormClosedEventHandler(frmTapePlayer_FormClosed);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}