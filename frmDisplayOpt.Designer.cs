﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmDisplayOpt
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmDisplayOpt() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdCancel.Name = "cmdCancel";
			_cmdOK.Name = "cmdOK";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _cmdCancel;

		public Button cmdCancel
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdCancel;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdCancel != null)
				{
					_cmdCancel.Click -= cmdCancel_Click;
				}

				_cmdCancel = value;
				if (_cmdCancel != null)
				{
					_cmdCancel.Click += cmdCancel_Click;
				}
			}
		}

		private Button _cmdOK;

		public Button cmdOK
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOK;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOK != null)
				{
					_cmdOK.Click -= cmdOK_Click;
				}

				_cmdOK = value;
				if (_cmdOK != null)
				{
					_cmdOK.Click += cmdOK_Click;
				}
			}
		}

		public RadioButton optFullScreen;
		public RadioButton optTriple;
		public RadioButton optDouble;
		public RadioButton optDoubleHeight;
		public RadioButton optDoubleWidth;
		public RadioButton optNormal;
		public GroupBox fraDisplaySize;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmDisplayOpt));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_cmdCancel = new Button();
			_cmdCancel.Click += new EventHandler(cmdCancel_Click);
			_cmdOK = new Button();
			_cmdOK.Click += new EventHandler(cmdOK_Click);
			fraDisplaySize = new GroupBox();
			optFullScreen = new RadioButton();
			optTriple = new RadioButton();
			optDouble = new RadioButton();
			optDoubleHeight = new RadioButton();
			optDoubleWidth = new RadioButton();
			optNormal = new RadioButton();
			fraDisplaySize.SuspendLayout();
			SuspendLayout();
			ToolTip1.Active = true;
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "Display Options";
			ClientSize = new Size(272, 153);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmDisplayOpt";
			_cmdCancel.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _cmdCancel;
			_cmdCancel.Text = "Cancel";
			_cmdCancel.Size = new Size(73, 25);
			_cmdCancel.Location = new Point(192, 44);
			_cmdCancel.TabIndex = 7;
			_cmdCancel.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdCancel.BackColor = SystemColors.Control;
			_cmdCancel.CausesValidation = true;
			_cmdCancel.Enabled = true;
			_cmdCancel.ForeColor = SystemColors.ControlText;
			_cmdCancel.Cursor = Cursors.Default;
			_cmdCancel.RightToLeft = RightToLeft.No;
			_cmdCancel.TabStop = true;
			_cmdCancel.Name = "_cmdCancel";
			_cmdOK.TextAlign = ContentAlignment.MiddleCenter;
			_cmdOK.Text = "OK";
			AcceptButton = _cmdOK;
			_cmdOK.Size = new Size(73, 25);
			_cmdOK.Location = new Point(192, 11);
			_cmdOK.TabIndex = 6;
			_cmdOK.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOK.BackColor = SystemColors.Control;
			_cmdOK.CausesValidation = true;
			_cmdOK.Enabled = true;
			_cmdOK.ForeColor = SystemColors.ControlText;
			_cmdOK.Cursor = Cursors.Default;
			_cmdOK.RightToLeft = RightToLeft.No;
			_cmdOK.TabStop = true;
			_cmdOK.Name = "_cmdOK";
			fraDisplaySize.Text = "&Display Size";
			fraDisplaySize.Size = new Size(173, 143);
			fraDisplaySize.Location = new Point(8, 4);
			fraDisplaySize.TabIndex = 0;
			fraDisplaySize.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			fraDisplaySize.BackColor = SystemColors.Control;
			fraDisplaySize.Enabled = true;
			fraDisplaySize.ForeColor = SystemColors.ControlText;
			fraDisplaySize.RightToLeft = RightToLeft.No;
			fraDisplaySize.Visible = true;
			fraDisplaySize.Padding = new Padding(0);
			fraDisplaySize.Name = "fraDisplaySize";
			optFullScreen.TextAlign = ContentAlignment.MiddleLeft;
			optFullScreen.Text = "Full screen (auto size)";
			optFullScreen.Size = new Size(153, 19);
			optFullScreen.Location = new Point(12, 115);
			optFullScreen.TabIndex = 8;
			optFullScreen.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optFullScreen.CheckAlign = ContentAlignment.MiddleLeft;
			optFullScreen.BackColor = SystemColors.Control;
			optFullScreen.CausesValidation = true;
			optFullScreen.Enabled = true;
			optFullScreen.ForeColor = SystemColors.ControlText;
			optFullScreen.Cursor = Cursors.Default;
			optFullScreen.RightToLeft = RightToLeft.No;
			optFullScreen.Appearance = Appearance.Normal;
			optFullScreen.TabStop = true;
			optFullScreen.Checked = false;
			optFullScreen.Visible = true;
			optFullScreen.Name = "optFullScreen";
			optTriple.TextAlign = ContentAlignment.MiddleLeft;
			optTriple.Text = "&Triple size (768 x 576)";
			optTriple.Size = new Size(153, 17);
			optTriple.Location = new Point(12, 96);
			optTriple.TabIndex = 5;
			optTriple.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optTriple.CheckAlign = ContentAlignment.MiddleLeft;
			optTriple.BackColor = SystemColors.Control;
			optTriple.CausesValidation = true;
			optTriple.Enabled = true;
			optTriple.ForeColor = SystemColors.ControlText;
			optTriple.Cursor = Cursors.Default;
			optTriple.RightToLeft = RightToLeft.No;
			optTriple.Appearance = Appearance.Normal;
			optTriple.TabStop = true;
			optTriple.Checked = false;
			optTriple.Visible = true;
			optTriple.Name = "optTriple";
			optDouble.TextAlign = ContentAlignment.MiddleLeft;
			optDouble.Text = "&Double size (512 x 384)";
			optDouble.Size = new Size(145, 17);
			optDouble.Location = new Point(12, 77);
			optDouble.TabIndex = 4;
			optDouble.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optDouble.CheckAlign = ContentAlignment.MiddleLeft;
			optDouble.BackColor = SystemColors.Control;
			optDouble.CausesValidation = true;
			optDouble.Enabled = true;
			optDouble.ForeColor = SystemColors.ControlText;
			optDouble.Cursor = Cursors.Default;
			optDouble.RightToLeft = RightToLeft.No;
			optDouble.Appearance = Appearance.Normal;
			optDouble.TabStop = true;
			optDouble.Checked = false;
			optDouble.Visible = true;
			optDouble.Name = "optDouble";
			optDoubleHeight.TextAlign = ContentAlignment.MiddleLeft;
			optDoubleHeight.Text = "Double &height (256 x 384)";
			optDoubleHeight.Size = new Size(153, 17);
			optDoubleHeight.Location = new Point(12, 58);
			optDoubleHeight.TabIndex = 3;
			optDoubleHeight.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optDoubleHeight.CheckAlign = ContentAlignment.MiddleLeft;
			optDoubleHeight.BackColor = SystemColors.Control;
			optDoubleHeight.CausesValidation = true;
			optDoubleHeight.Enabled = true;
			optDoubleHeight.ForeColor = SystemColors.ControlText;
			optDoubleHeight.Cursor = Cursors.Default;
			optDoubleHeight.RightToLeft = RightToLeft.No;
			optDoubleHeight.Appearance = Appearance.Normal;
			optDoubleHeight.TabStop = true;
			optDoubleHeight.Checked = false;
			optDoubleHeight.Visible = true;
			optDoubleHeight.Name = "optDoubleHeight";
			optDoubleWidth.TextAlign = ContentAlignment.MiddleLeft;
			optDoubleWidth.Text = "Double &width (512 x 192)";
			optDoubleWidth.Size = new Size(149, 17);
			optDoubleWidth.Location = new Point(12, 39);
			optDoubleWidth.TabIndex = 2;
			optDoubleWidth.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optDoubleWidth.CheckAlign = ContentAlignment.MiddleLeft;
			optDoubleWidth.BackColor = SystemColors.Control;
			optDoubleWidth.CausesValidation = true;
			optDoubleWidth.Enabled = true;
			optDoubleWidth.ForeColor = SystemColors.ControlText;
			optDoubleWidth.Cursor = Cursors.Default;
			optDoubleWidth.RightToLeft = RightToLeft.No;
			optDoubleWidth.Appearance = Appearance.Normal;
			optDoubleWidth.TabStop = true;
			optDoubleWidth.Checked = false;
			optDoubleWidth.Visible = true;
			optDoubleWidth.Name = "optDoubleWidth";
			optNormal.TextAlign = ContentAlignment.MiddleLeft;
			optNormal.Text = "&Normal size (256 x 192)";
			optNormal.Size = new Size(149, 17);
			optNormal.Location = new Point(12, 20);
			optNormal.TabIndex = 1;
			optNormal.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			optNormal.CheckAlign = ContentAlignment.MiddleLeft;
			optNormal.BackColor = SystemColors.Control;
			optNormal.CausesValidation = true;
			optNormal.Enabled = true;
			optNormal.ForeColor = SystemColors.ControlText;
			optNormal.Cursor = Cursors.Default;
			optNormal.RightToLeft = RightToLeft.No;
			optNormal.Appearance = Appearance.Normal;
			optNormal.TabStop = true;
			optNormal.Checked = false;
			optNormal.Visible = true;
			optNormal.Name = "optNormal";
			Controls.Add(_cmdCancel);
			Controls.Add(_cmdOK);
			Controls.Add(fraDisplaySize);
			fraDisplaySize.Controls.Add(optFullScreen);
			fraDisplaySize.Controls.Add(optTriple);
			fraDisplaySize.Controls.Add(optDouble);
			fraDisplaySize.Controls.Add(optDoubleHeight);
			fraDisplaySize.Controls.Add(optDoubleWidth);
			fraDisplaySize.Controls.Add(optNormal);
			fraDisplaySize.ResumeLayout(false);
			Load += new EventHandler(frmDisplayOpt_Load);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}