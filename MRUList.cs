﻿using Microsoft.VisualBasic;

namespace vbSpec
{
	internal class MRUList
	{
		// /*******************************************************************************
		// MRUList.cls within vbSpec.vbp
		// 
		// Most Recently Used file list handling
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2002 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		private const short MaxMRUFiles = 5;
		private int MRUCount;
		private string[] MRU = new string[(MaxMRUFiles + 1)];

		public int AddMRUFile(ref string sFile)
		{
			int AddMRUFileRet = default;
			int l;
			string s;

			// // Empty filename, ignore
			if (string.IsNullOrEmpty(sFile))
				return AddMRUFileRet;

			// // If file is already in MRU, bring it to the top of the list
			for (l = 1; l <= MaxMRUFiles; l++)
			{
				if ((Strings.LCase(sFile) ?? "") == (Strings.LCase(MRU[l]) ?? ""))
				{
					s = MRU[1];
					MRU[1] = sFile;
					MRU[l] = s;
					return AddMRUFileRet;
				}
			}

			// // Move all the MRU files down one and insert the new file
			// // into position 1
			for (l = MaxMRUFiles; l >= 2; l -= 1)
				MRU[l] = MRU[l - 1];
			MRU[1] = sFile;

			// // Set the MRUCount appropriately
			MRUCount = 0;
			for (l = 1; l <= MaxMRUFiles; l++)
			{
				if (!string.IsNullOrEmpty(MRU[l]))
					MRUCount = MRUCount + 1;
			}

			AddMRUFileRet = MRUCount;
			return AddMRUFileRet;
		}

		public int GetMRUCount()
		{
			int GetMRUCountRet = default;
			GetMRUCountRet = MRUCount;
			return GetMRUCountRet;
		}

		public string GetMRUFile(ref int l)
		{
			string GetMRUFileRet = default;
			if (l > 0 & l <= MRUCount)
			{
				GetMRUFileRet = MRU[l];
			}

			return GetMRUFileRet;
		}

		public int GetMRUMax()
		{
			int GetMRUMaxRet = default;
			GetMRUMaxRet = MaxMRUFiles;
			return GetMRUMaxRet;
		}

		// UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		private void Class_Initialize_Renamed()
		{
			int l;
			string s;
			MRUCount = 0;
			// // Read files in from registry
			for (l = 1; l <= MaxMRUFiles; l++)
			{
				s = Interaction.GetSetting("Grok", "vbSpec", "MRU" + l.ToString(), "");
				if (!string.IsNullOrEmpty(s))
				{
					MRUCount = MRUCount + 1;
					MRU[MRUCount] = s;
				}
			}
		}

		public MRUList() : base()
		{
			Class_Initialize_Renamed();
		}


		// UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		private void Class_Terminate_Renamed()
		{
			int l, c;
			c = 0;
			// // Read files in from registry
			for (l = 1; l <= MaxMRUFiles; l++)
			{
				if (!string.IsNullOrEmpty(MRU[l]))
				{
					c = c + 1;
					Interaction.SaveSetting("Grok", "vbSpec", "MRU" + c.ToString(), MRU[l]);
				}
			}
		}

		~MRUList()
		{
			Class_Terminate_Renamed();
		}
	}
}