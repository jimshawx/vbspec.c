﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmAbout
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmAbout() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdOK.Name = "cmdOK";
			_lblWebSite.Name = "lblWebSite";
			_lblVerInfo.Name = "lblVerInfo";
			_Image1.Name = "Image1";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _cmdOK;

		public Button cmdOK
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdOK;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdOK != null)
				{
					_cmdOK.Click -= cmdOK_Click;
				}

				_cmdOK = value;
				if (_cmdOK != null)
				{
					_cmdOK.Click += cmdOK_Click;
				}
			}
		}

		private Label _lblWebSite;

		public Label lblWebSite
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _lblWebSite;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_lblWebSite != null)
				{
					_lblWebSite.Click -= lblWebSite_Click;
					_lblWebSite.MouseMove -= lblWebSite_MouseMove;
				}

				_lblWebSite = value;
				if (_lblWebSite != null)
				{
					_lblWebSite.Click += lblWebSite_Click;
					_lblWebSite.MouseMove += lblWebSite_MouseMove;
				}
			}
		}

		public Label _lblStatic_2;
		public Microsoft.VisualBasic.PowerPacks.LineShape _Line1_0;
		public Microsoft.VisualBasic.PowerPacks.LineShape _Line1_1;
		private Label _lblVerInfo;

		public Label lblVerInfo
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _lblVerInfo;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_lblVerInfo != null)
				{
					_lblVerInfo.MouseMove -= lblVerInfo_MouseMove;
				}

				_lblVerInfo = value;
				if (_lblVerInfo != null)
				{
					_lblVerInfo.MouseMove += lblVerInfo_MouseMove;
				}
			}
		}

		public Label _lblStatic_0;
		private PictureBox _Image1;

		public PictureBox Image1
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _Image1;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_Image1 != null)
				{
					_Image1.MouseMove -= Image1_MouseMove;
				}

				_Image1 = value;
				if (_Image1 != null)
				{
					_Image1.MouseMove += Image1_MouseMove;
				}
			}
		}
		// Public WithEvents Line1 As LineShapeArray
		private Microsoft.VisualBasic.Compatibility.VB6.LabelArray _lblStatic;

		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblStatic
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _lblStatic;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_lblStatic != null)
				{
					_lblStatic.MouseMove -= lblStatic_MouseMove;
				}

				_lblStatic = value;
				if (_lblStatic != null)
				{
					_lblStatic.MouseMove += lblStatic_MouseMove;
				}
			}
		}

		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmAbout));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			_cmdOK = new Button();
			_cmdOK.Click += new EventHandler(cmdOK_Click);
			_lblWebSite = new Label();
			_lblWebSite.Click += new EventHandler(lblWebSite_Click);
			_lblWebSite.MouseMove += new MouseEventHandler(lblWebSite_MouseMove);
			_lblStatic_2 = new Label();
			_Line1_0 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			_Line1_1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			_lblVerInfo = new Label();
			_lblVerInfo.MouseMove += new MouseEventHandler(lblVerInfo_MouseMove);
			_lblStatic_0 = new Label();
			_Image1 = new PictureBox();
			_Image1.MouseMove += new MouseEventHandler(Image1_MouseMove);
			// Me.Line1 = New LineShapeArray(components)
			_lblStatic = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			_lblStatic.MouseMove += new MouseEventHandler(lblStatic_MouseMove);
			SuspendLayout();
			ToolTip1.Active = true;
			// CType(Me.Line1, System.ComponentModel.ISupportInitialize).BeginInit()
			((System.ComponentModel.ISupportInitialize)_lblStatic).BeginInit();
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "About vbSpec";
			ClientSize = new Size(316, 188);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmAbout";
			_cmdOK.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _cmdOK;
			_cmdOK.Text = "OK";
			AcceptButton = _cmdOK;
			_cmdOK.Size = new Size(73, 25);
			_cmdOK.Location = new Point(237, 156);
			_cmdOK.TabIndex = 4;
			_cmdOK.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdOK.BackColor = SystemColors.Control;
			_cmdOK.CausesValidation = true;
			_cmdOK.Enabled = true;
			_cmdOK.ForeColor = SystemColors.ControlText;
			_cmdOK.Cursor = Cursors.Default;
			_cmdOK.RightToLeft = RightToLeft.No;
			_cmdOK.TabStop = true;
			_cmdOK.Name = "_cmdOK";
			_lblWebSite.Text = "http://www.muhi.org/vbspec/";
			_lblWebSite.ForeColor = SystemColors.Highlight;
			_lblWebSite.Size = new Size(151, 17);
			_lblWebSite.Location = new Point(96, 52);
			_lblWebSite.TabIndex = 3;
			_lblWebSite.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblWebSite.TextAlign = ContentAlignment.TopLeft;
			_lblWebSite.BackColor = SystemColors.Control;
			_lblWebSite.Enabled = true;
			_lblWebSite.Cursor = Cursors.Default;
			_lblWebSite.RightToLeft = RightToLeft.No;
			_lblWebSite.UseMnemonic = true;
			_lblWebSite.Visible = true;
			_lblWebSite.AutoSize = false;
			_lblWebSite.BorderStyle = BorderStyle.None;
			_lblWebSite.Name = "_lblWebSite";
			_lblStatic_2.Text = "vbSpec comes with ABSOLUTELY NO WARRANTY. This is free software and you are welcome to redistribute it under certain conditions; see the README.TXT file which accompanies this distribution for details.";
			_lblStatic_2.Size = new Size(261, 73);
			_lblStatic_2.Location = new Point(52, 76);
			_lblStatic_2.TabIndex = 2;
			_lblStatic_2.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblStatic_2.TextAlign = ContentAlignment.TopLeft;
			_lblStatic_2.BackColor = SystemColors.Control;
			_lblStatic_2.Enabled = true;
			_lblStatic_2.ForeColor = SystemColors.ControlText;
			_lblStatic_2.Cursor = Cursors.Default;
			_lblStatic_2.RightToLeft = RightToLeft.No;
			_lblStatic_2.UseMnemonic = true;
			_lblStatic_2.Visible = true;
			_lblStatic_2.AutoSize = false;
			_lblStatic_2.BorderStyle = BorderStyle.None;
			_lblStatic_2.Name = "_lblStatic_2";
			_Line1_0.BorderColor = SystemColors.ControlDark;
			_Line1_0.X1 = 48;
			_Line1_0.X2 = 616;
			_Line1_0.Y1 = 44;
			_Line1_0.Y2 = 44;
			_Line1_0.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			_Line1_0.BorderWidth = 1;
			_Line1_0.Visible = true;
			_Line1_0.Name = "_Line1_0";
			_Line1_1.BorderColor = SystemColors.ControlLight;
			_Line1_1.X1 = 48;
			_Line1_1.X2 = 620;
			_Line1_1.Y1 = 45;
			_Line1_1.Y2 = 45;
			_Line1_1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			_Line1_1.BorderWidth = 1;
			_Line1_1.Visible = true;
			_Line1_1.Name = "_Line1_1";
			_lblVerInfo.Text = "Version 0.00.0000";
			_lblVerInfo.Size = new Size(253, 17);
			_lblVerInfo.Location = new Point(52, 24);
			_lblVerInfo.TabIndex = 1;
			_lblVerInfo.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblVerInfo.TextAlign = ContentAlignment.TopLeft;
			_lblVerInfo.BackColor = SystemColors.Control;
			_lblVerInfo.Enabled = true;
			_lblVerInfo.ForeColor = SystemColors.ControlText;
			_lblVerInfo.Cursor = Cursors.Default;
			_lblVerInfo.RightToLeft = RightToLeft.No;
			_lblVerInfo.UseMnemonic = true;
			_lblVerInfo.Visible = true;
			_lblVerInfo.AutoSize = false;
			_lblVerInfo.BorderStyle = BorderStyle.None;
			_lblVerInfo.Name = "_lblVerInfo";
			_lblStatic_0.Text = "vbSpec - Sinclair ZX Spectrum Emulator";
			_lblStatic_0.Font = new Font("Arial", 8.25f, FontStyle.Bold | FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblStatic_0.Size = new Size(253, 17);
			_lblStatic_0.Location = new Point(52, 8);
			_lblStatic_0.TabIndex = 0;
			_lblStatic_0.TextAlign = ContentAlignment.TopLeft;
			_lblStatic_0.BackColor = SystemColors.Control;
			_lblStatic_0.Enabled = true;
			_lblStatic_0.ForeColor = SystemColors.ControlText;
			_lblStatic_0.Cursor = Cursors.Default;
			_lblStatic_0.RightToLeft = RightToLeft.No;
			_lblStatic_0.UseMnemonic = true;
			_lblStatic_0.Visible = true;
			_lblStatic_0.AutoSize = false;
			_lblStatic_0.BorderStyle = BorderStyle.None;
			_lblStatic_0.Name = "_lblStatic_0";
			_Image1.Size = new Size(32, 32);
			_Image1.Location = new Point(8, 8);
			_Image1.Image = (Image)resources.GetObject("Image1.Image");
			_Image1.Enabled = true;
			_Image1.Cursor = Cursors.Default;
			_Image1.SizeMode = PictureBoxSizeMode.Normal;
			_Image1.Visible = true;
			_Image1.BorderStyle = BorderStyle.None;
			_Image1.Name = "_Image1";
			Controls.Add(_cmdOK);
			Controls.Add(_lblWebSite);
			Controls.Add(_lblStatic_2);
			ShapeContainer1.Shapes.Add(_Line1_0);
			ShapeContainer1.Shapes.Add(_Line1_1);
			Controls.Add(_lblVerInfo);
			Controls.Add(_lblStatic_0);
			Controls.Add(_Image1);
			Controls.Add(ShapeContainer1);
			// Me.Line1.SetIndex(_Line1_0, CType(0, Short))
			// Me.Line1.SetIndex(_Line1_1, CType(1, Short))
			_lblStatic.SetIndex(_lblStatic_2, Conversions.ToShort(2));
			_lblStatic.SetIndex(_lblStatic_0, Conversions.ToShort(0));
			((System.ComponentModel.ISupportInitialize)_lblStatic).EndInit();
			Load += new EventHandler(frmAbout_Load);
			MouseMove += new MouseEventHandler(frmAbout_MouseMove);
			// CType(Me.Line1, System.ComponentModel.ISupportInitialize).EndInit()
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}