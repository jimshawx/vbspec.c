﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmZXPrinter : Form
	{
		// /*******************************************************************************
		// frmZXPrinter.frm within vbSpec.vbp
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2002 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		private int iWidth;

		private void InitZXPrinterBitmap()
		{
			modMain.bmiZXPrinter.bmiHeader.biSize = Strings.Len(modMain.bmiZXPrinter.bmiHeader);
			modMain.bmiZXPrinter.bmiHeader.biWidth = 256;
			modMain.bmiZXPrinter.bmiHeader.biHeight = 1152;
			modMain.bmiZXPrinter.bmiHeader.biPlanes = 1;
			modMain.bmiZXPrinter.bmiHeader.biBitCount = 1;
			modMain.bmiZXPrinter.bmiHeader.biCompression = modMain.BI_RGB;
			modMain.bmiZXPrinter.bmiHeader.biSizeImage = 0;
			modMain.bmiZXPrinter.bmiHeader.biXPelsPerMeter = 200;
			modMain.bmiZXPrinter.bmiHeader.biYPelsPerMeter = 200;
			modMain.bmiZXPrinter.bmiHeader.biClrUsed = 2;
			modMain.bmiZXPrinter.bmiHeader.biClrImportant = 2;
			if (optZX.Checked == true)
			{
				modMain.bmiZXPrinter.bmiColors[0].rgbRed = 192;
				modMain.bmiZXPrinter.bmiColors[0].rgbGreen = 192;
				modMain.bmiZXPrinter.bmiColors[0].rgbBlue = 192;
				modMain.bmiZXPrinter.bmiColors[1].rgbRed = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbGreen = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbBlue = 0;
			}
			else
			{
				modMain.bmiZXPrinter.bmiColors[0].rgbRed = 255;
				modMain.bmiZXPrinter.bmiColors[0].rgbGreen = 255;
				modMain.bmiZXPrinter.bmiColors[0].rgbBlue = 255;
				modMain.bmiZXPrinter.bmiColors[1].rgbRed = 64;
				modMain.bmiZXPrinter.bmiColors[1].rgbGreen = 64;
				modMain.bmiZXPrinter.bmiColors[1].rgbBlue = 192;
			}

			modMain.gcZXPrinterBits = new byte[36865]; // // 1152 * 32
			modMain.glZXPrinterBMPHeight = 1152;
		}

		private void SaveMonoBitmap(ref string sFile)
		{
			int X, lSize, h, l, d;
			byte[] b;
			h = FileSystem.FreeFile();
			FileSystem.FileOpen(h, sFile, OpenMode.Binary, OpenAccess.Write);
			lSize = 14 + Strings.Len(modMain.bmiZXPrinter.bmiHeader) + 8 + modSpectrum.lZXPrinterY * 32;

			// // BITMAPFILEHEADER - 14

			modMain.bmiZXPrinter.bmiHeader.biHeight = modSpectrum.lZXPrinterY;
			modMain.bmiZXPrinter.bmiHeader.biSizeImage = modSpectrum.lZXPrinterY * 32;

			// // Flip the stored image as RGB bitmaps are stored bottom-left to top-right
			d = modSpectrum.lZXPrinterY - 1;
			b = new byte[modSpectrum.lZXPrinterY * 32 + 1];
			var loopTo = modSpectrum.lZXPrinterY - 1;
			for (l = 0; l <= loopTo; l++)
			{
				for (X = 0; X <= 31; X++)
					b[X + l * 32] = modMain.gcZXPrinterBits[X + d * 32];
				d = d - 1;
			}

			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, "BM"); // // bitmap ident
										 // UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, lSize); // // size
										  // UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, 0);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, 62);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiHeader);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[0].rgbBlue);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[0].rgbGreen);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[0].rgbRed);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[0].rgbReserved);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[1].rgbBlue);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[1].rgbGreen);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[1].rgbRed);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, modMain.bmiZXPrinter.bmiColors[1].rgbReserved);
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(h, b);
			FileSystem.FileClose(h);
		}

		private void cmdClear_Click(object eventSender, EventArgs eventArgs)
		{
			modSpectrum.lZXPrinterY = 0;
			vs.Minimum = 0;
			vs.Maximum = 0 + vs.LargeChange - 1;
			// UPGRADE_ISSUE: PictureBox method picView.Cls was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// picView.Cls()
			InitZXPrinterBitmap();
		}

		private void cmdFF_MouseDown(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			tmrFormFeed.Interval = 50;
			tmrFormFeed.Enabled = true;
		}

		private void cmdFF_MouseUp(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			tmrFormFeed.Enabled = false;
		}

		private void cmdSave_Click(object eventSender, EventArgs eventArgs)
		{
			Information.Err().Clear();

			// On Error Resume Next

			// dlgSaveOpen.DefaultExt = "bmp"
			// dlgSaveSave.DefaultExt = "bmp"
			// dlgSaveOpen.FileName = ""
			// dlgSaveSave.FileName = ""
			// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// dlgSaveOpen.CheckPathExists = True
			// dlgSaveSave.CheckPathExists = True
			// 'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgSave.Flags was upgraded to dlgSaveSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// dlgSaveSave.OverwritePrompt = True
			// 'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgSave.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			// 'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgSave.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			// 'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgSave.Flags was upgraded to dlgSaveOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			// dlgSaveOpen.ShowReadOnly = False
			// 'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgSave.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
			// 'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// dlgSave.CancelError = True
			// dlgSaveSave.ShowDialog()
			// dlgSaveOpen.FileName = dlgSaveSave.FileName
			// 'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// If Err.Number = DialogResult.Cancel Then
			// Exit Sub
			// End If

			// If dlgSaveOpen.FileName <> "" Then
			// SaveMonoBitmap((dlgSaveOpen.FileName))

			// cmdClear_Click(cmdClear, New System.EventArgs())
			// End If
		}

		private void frmZXPrinter_Load(object eventSender, EventArgs eventArgs)
		{
			int y, X, h;
			InitZXPrinterBitmap();
			iWidth = Width;
			X = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "ZXPrnWndX", "-1"));
			y = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "ZXPrnWndY", "-1"));
			h = (int)Conversion.Val(Interaction.GetSetting("Grok", "vbSpec", "ZXPrnWndHeight", "-1"));
			if (X >= 0 & X <= Screen.PrimaryScreen.Bounds.Width - 16)
			{
				Left = X;
			}

			if (y >= 0 & y <= Screen.PrimaryScreen.Bounds.Height - 16)
			{
				Top = y;
			}

			if (h >= 0 & h <= Screen.PrimaryScreen.Bounds.Height - Top - 16)
			{
				Height = h;
			}
		}

		// UPGRADE_WARNING: Event frmZXPrinter.Resize may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void frmZXPrinter_Resize(object eventSender, EventArgs eventArgs)
		{
			int l;
			Width = iWidth;
			l = ClientRectangle.Height - picView.Top - picLogo.Height;
			if (l > 8)
			{
				picView.Height = l;
			}
			else
			{
				picView.Height = 8;
			}

			vs.Height = picView.Height;
			// UPGRADE_ISSUE: PictureBox method picView.Cls was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// picView.Cls()
			vs_Change(0);
		}

		private void frmZXPrinter_FormClosed(object eventSender, FormClosedEventArgs eventArgs)
		{
			Interaction.SaveSetting("Grok", "vbSpec", "ZXPrnWndX", Left.ToString());
			Interaction.SaveSetting("Grok", "vbSpec", "ZXPrnWndY", Top.ToString());
			Interaction.SaveSetting("Grok", "vbSpec", "ZXPrnWndHeight", Height.ToString());
			My.MyProject.Forms.frmMainWnd.mnuOptions[5].Checked = false;
		}

		// UPGRADE_WARNING: Event optAlphacom.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void optAlphacom_CheckedChanged(object eventSenderX, EventArgs eventArgs)
		{
			var eventSender = eventSenderX as RadioButton;
			if (Conversions.ToBoolean(eventSender.Checked))
			{
				imgAlpha.Visible = true;
				imgSinclair.Visible = false;
				picView.BackColor = ColorTranslator.FromOle(Information.RGB(255, 255, 255));
				modMain.bmiZXPrinter.Initialize();
				modMain.bmiZXPrinter.bmiColors[0].rgbRed = 255;
				modMain.bmiZXPrinter.bmiColors[0].rgbGreen = 255;
				modMain.bmiZXPrinter.bmiColors[0].rgbBlue = 255;
				modMain.bmiZXPrinter.bmiColors[0].rgbReserved = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbRed = 64;
				modMain.bmiZXPrinter.bmiColors[1].rgbGreen = 64;
				modMain.bmiZXPrinter.bmiColors[1].rgbBlue = 192;
				modMain.bmiZXPrinter.bmiColors[1].rgbReserved = 0;
				vs_Change(0);
			}
		}

		// UPGRADE_WARNING: Event optZX.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void optZX_CheckedChanged(object eventSenderX, EventArgs eventArgs)
		{
			var eventSender = eventSenderX as RadioButton;
			if (Conversions.ToBoolean(eventSender.Checked))
			{
				imgSinclair.Visible = true;
				imgAlpha.Visible = false;
				picView.BackColor = ColorTranslator.FromOle(Information.RGB(192, 192, 192));
				modMain.bmiZXPrinter.Initialize();
				modMain.bmiZXPrinter.bmiColors[0].rgbRed = 192;
				modMain.bmiZXPrinter.bmiColors[0].rgbGreen = 192;
				modMain.bmiZXPrinter.bmiColors[0].rgbBlue = 192;
				modMain.bmiZXPrinter.bmiColors[0].rgbReserved = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbRed = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbGreen = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbBlue = 0;
				modMain.bmiZXPrinter.bmiColors[1].rgbReserved = 0;
				vs_Change(0);
			}
		}

		private void picView_Resize(object eventSender, EventArgs eventArgs)
		{
			if (modSpectrum.lZXPrinterY > picView.Height)
			{
				vs.Minimum = picView.Height / 8;
				vs.Maximum = modSpectrum.lZXPrinterY / 8 + vs.LargeChange - 1;
				vs.Value = modSpectrum.lZXPrinterY / 8;
			}
			else
			{
				vs.Minimum = 0;
				vs.Maximum = 0 + vs.LargeChange - 1;
			}
		}

		private void tmrFormFeed_Tick(object eventSender, EventArgs eventArgs)
		{
			modSpectrum.lZXPrinterEncoder = 0;
			modSpectrum.lZXPrinterX = 0;
			modSpectrum.lZXPrinterY = modSpectrum.lZXPrinterY + 1;
			if (modSpectrum.lZXPrinterY >= modMain.glZXPrinterBMPHeight)
			{
				modMain.glZXPrinterBMPHeight = modSpectrum.lZXPrinterY + 32;
				Array.Resize(ref modMain.gcZXPrinterBits, modMain.glZXPrinterBMPHeight * 32 + 1);
				modMain.bmiZXPrinter.bmiHeader.biHeight = modMain.glZXPrinterBMPHeight;
			}

			if (picView.Height > modSpectrum.lZXPrinterY)
			{
			}
			// UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// StretchDIBitsMono(picView.hdc, 0, picView.Height, 256, -lZXPrinterY - 1, 0, 0, 256, lZXPrinterY + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
			else
			{
				// UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// StretchDIBitsMono(picView.hdc, 0, picView.Height, 256, -picView.Height - 1, 0, lZXPrinterY - picView.Height, 256, picView.Height + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
			}

			picView.Refresh();

			// // Set up the scroll bar properties for the visible display
			// // to allow scrolling back over the material printed so far
			if (modSpectrum.lZXPrinterY > picView.Height)
			{
				vs.Minimum = picView.Height / 8;
				vs.Maximum = modSpectrum.lZXPrinterY / 8 + vs.LargeChange - 1;
				vs.Value = modSpectrum.lZXPrinterY / 8;
			}
			else
			{
				vs.Minimum = 0;
				vs.Maximum = 0 + vs.LargeChange - 1;
			}
		}

		// UPGRADE_NOTE: vs.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
		// UPGRADE_WARNING: VScrollBar event vs.Change has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		private void vs_Change(int newScrollValue)
		{
			if (picView.Height > modSpectrum.lZXPrinterY)
			{
			}
			// UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// StretchDIBitsMono(Me.picView.hdc, 0, Me.picView.Height, 256, -lZXPrinterY - 1, 0, 0, 256, lZXPrinterY + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
			else
			{
				// UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// StretchDIBitsMono(Me.picView.hdc, 0, Me.picView.Height, 256, -Me.picView.Height - 1, 0, newScrollValue * 8 - Me.picView.Height, 256, Me.picView.Height + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
			}

			// If frmZXPrinter.picView.Height > lZXPrinterY Then
			// BitBlt frmZXPrinter.picView.hdc, 0, frmZXPrinter.picView.Height - lZXPrinterY, 256, frmZXPrinter.picView.Height, frmZXPrinter.picPrn.hdc, 0, 0, SRCCOPY
			// Else
			// BitBlt frmZXPrinter.picView.hdc, 0, 0, 256, frmZXPrinter.picView.Height, frmZXPrinter.picPrn.hdc, 0, vs.Value * 8, SRCCOPY
			// End If

			picView.Refresh();
		}

		// UPGRADE_NOTE: vs.Scroll was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
		private void vs_Scroll_Renamed(int newScrollValue)
		{
			vs_Change(0);
		}

		private void vs_Scroll(object eventSender, ScrollEventArgs eventArgs)
		{
			switch (eventArgs.Type)
			{
				case ScrollEventType.ThumbTrack:
					{
						vs_Scroll_Renamed(eventArgs.NewValue);
						break;
					}

				case ScrollEventType.EndScroll:
					{
						vs_Change(eventArgs.NewValue);
						break;
					}
			}
		}
	}
}