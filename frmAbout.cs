﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace vbSpec
{
	internal partial class frmAbout : Form
	{
		// /*******************************************************************************
		// frmAbout.frm within vbSpec.vbp
		// 
		// "About..." dialog for vbSpec
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2000 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		private void cmdOK_Click(object eventSender, EventArgs eventArgs)
		{
			Close();
		}

		private void frmAbout_Load(object eventSender, EventArgs eventArgs)
		{
			lblVerInfo.Text = "Version " + My.MyProject.Application.Info.Version.Major + "." + Microsoft.VisualBasic.Compatibility.VB6.Support.Format(My.MyProject.Application.Info.Version.Minor, "00") + "." + Microsoft.VisualBasic.Compatibility.VB6.Support.Format(My.MyProject.Application.Info.Version.Revision, "0000");
		}

		private void Image1_MouseMove(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			lblWebSite.Font = Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblWebSite.Font, false);
		}

		private void lblStatic_MouseMove(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			short Index = lblStatic.GetIndex((Label)eventSender);
			lblWebSite.Font = Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblWebSite.Font, false);
		}

		private void lblVerInfo_MouseMove(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			lblWebSite.Font = Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblWebSite.Font, false);
		}

		private void lblWebSite_Click(object eventSender, EventArgs eventArgs)
		{
			string arglpOperation = "open";
			string arglpFile = "http://www.muhi.org/vbspec/";
			modMain.ShellExecute(0, arglpOperation, arglpFile, Constants.vbNullString, Constants.vbNullString, 0);
		}

		private void lblWebSite_MouseMove(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			lblWebSite.Font = Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblWebSite.Font, true);
		}

		private void frmAbout_MouseMove(object eventSender, MouseEventArgs eventArgs)
		{
			short Button = (short)((int)eventArgs.Button / 0x100000);
			short Shift = (short)((int)ModifierKeys / 0x10000);
			float X = eventArgs.X;
			float y = eventArgs.Y;
			lblWebSite.Font = Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblWebSite.Font, false);
		}
	}
}