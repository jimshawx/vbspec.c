﻿using System;
using System.Diagnostics;
using Microsoft.VisualBasic;

namespace vbSpec
{
	static class modTAP
	{
		// /*******************************************************************************
		// modTAP.bas within vbSpec.vbp
		// 
		// Handles loading of ".TAP" files (Spectrum tape images)
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)2001-2002 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		private static int m_lChecksum;

		public static void CloseTAPFile()
		{
			if (modMain.ghTAPFile > 0)
			{
				FileSystem.FileClose(modMain.ghTAPFile);
				modMain.ghTAPFile = 0;
			}
		}

		public static bool SaveTAP(ref int lID, ref int lStart, ref int lLength)
		{
			bool SaveTAPRet = default;
			int n, lChecksum;
			;
			// // Move to end of existing TAP data, if there is any
			if (FileSystem.LOF(modMain.ghTAPFile) > 0L)
				FileSystem.Seek(modMain.ghTAPFile, FileSystem.LOF(modMain.ghTAPFile) + 1L);
			Debug.Print(lID + "  " + lStart + "  " + lLength);

			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(modMain.ghTAPFile, (short)(lLength + 2));
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(modMain.ghTAPFile, (byte)lID);
			lChecksum = lID;
			var loopTo = lStart + lLength - 1;
			for (n = lStart; n <= loopTo; n++)
			{
				// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
				FileSystem.FilePut(modMain.ghTAPFile, modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[n]], n & 16383]);
				lChecksum = lChecksum ^ modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[n]], n & 16383];
			}
			// UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileSystem.FilePut(modMain.ghTAPFile, (byte)lChecksum);
			SaveTAPRet = true;
			return SaveTAPRet;
		}

		public static bool LoadTAP(ref int lID, ref int lStart, ref int lLength)
		{
			bool LoadTAPRet = default;
			int lBlockID, lBlockLen, lBlockChecksum;
			var s = new byte[2];
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 3769


			Input:

					On Error Resume Next

			 */
			if (string.IsNullOrEmpty(modMain.gsTAPFileName))
			{
				LoadTAPRet = false;
			}
			else
			{
				if (FileSystem.Seek(modMain.ghTAPFile) > FileSystem.LOF(modMain.ghTAPFile))
					FileSystem.Seek(modMain.ghTAPFile, 1L);
				if (FileSystem.EOF(modMain.ghTAPFile))
					FileSystem.Seek(modMain.ghTAPFile, 1L);
				// UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
				Array argValue = (Array)s;
				FileSystem.FileGet(modMain.ghTAPFile, ref argValue);
				lBlockLen = (int)s[1] * 256 + (int)s[0] - 2;
				lBlockID = modMain.Asc(modMain.InputString16(modMain.ghTAPFile, 1));
				m_lChecksum = lBlockID; // // Initialize the checksum
				if (lBlockID == lID)
				{
					// // This block type is the same as the requested block type
					if (lLength <= lBlockLen)
					{
						// // There are enough bytes in the block to cover this request
						ReadTAPBlock(ref lStart, ref lLength);
						if (lLength < lBlockLen)
						{
							// // Skip the rest of the bytes up to the end of the block
							int arglLen = lBlockLen - lLength;
							SkipTAPBytes(ref arglLen);
						}

						lBlockChecksum = modMain.Asc(modMain.InputString16(modMain.ghTAPFile, 1));
						modMain.regIX = modMain.regIX + lLength & 0xFFFF;
						modMain.regDE = 0;
						if (m_lChecksum == lBlockChecksum)
						{
							LoadTAPRet = true;
						}
						else
						{
							LoadTAPRet = false;
						}
					}
					else
					{
						// // More bytes requested than there are in the block
						ReadTAPBlock(ref lStart, ref lBlockLen);
						lBlockChecksum = modMain.Asc(modMain.InputString16(modMain.ghTAPFile, 1));
						modMain.regIX = modMain.regIX + lBlockLen & 0xFFFF;
						modMain.regDE = modMain.regDE - lBlockLen;
						LoadTAPRet = false;
					}
				}
				else
				{
					// // Wrong block type -- skip this block
					SkipTAPBytes(ref lBlockLen);
					lBlockChecksum = modMain.Asc(modMain.InputString16(modMain.ghTAPFile, 1));
					LoadTAPRet = false;
				}
			}

			modMain.initscreen();
			modSpectrum.screenPaint();
			return LoadTAPRet;
		}

		public static void OpenTAPFile(ref string sName)
		{
			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (string.IsNullOrEmpty(FileSystem.Dir(sName)))
				return;
			if (modMain.ghTAPFile > 0)
				FileSystem.FileClose(modMain.ghTAPFile);

			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (string.IsNullOrEmpty(FileSystem.Dir(sName)))
				return;
			modTZX.StopTape(); // // Stop the TZX tape player
			modMain.ghTAPFile = FileSystem.FreeFile();
			FileSystem.FileOpen(modMain.ghTAPFile, sName, OpenMode.Binary);
			if (FileSystem.LOF(modMain.ghTAPFile) == 0L)
			{
				FileSystem.FileClose(modMain.ghTAPFile);
				return;
			}

			modMain.gsTAPFileName = sName;
			My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName + " - " + modMain.GetFilePart(sName);
		}

		private static void ReadTAPBlock(ref int lStart, ref int lLen)
		{
			byte[] s;
			int lCounter, a;
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 7991


			Input:

					On Error Resume Next

			 */
			s = new byte[lLen - 1 + 1];
			// UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			Array argValue = s;
			FileSystem.FileGet(modMain.ghTAPFile, ref argValue);
			var loopTo = lLen - 1;
			for (lCounter = 0; lCounter <= loopTo; lCounter++)
			{
				a = lStart + lCounter;
				modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[a]], a & 16383] = s[lCounter];
				m_lChecksum = m_lChecksum ^ s[lCounter];
			}
		}

		public static void SaveTAPFileDlg()
		{
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 8663


			Input:
					On Error Resume Next

			 */
			if (modMain.ghTAPFile < 1)
			{
				Information.Err().Clear();
				// UPGRADE_WARNING: CommonDialog variable was not upgraded Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="671167DC-EA81-475D-B690-7A40C7BF4A23"'
				// With frmMainWnd.dlgCommon
				// .Title = "Select TAP file for saving"
				// .DefaultExt = ".tap"
				// .FileName = "*.tap"
				// 'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
				// .Filter = "Tape files (*.tap)|*.tap|All Files (*.*)|*.*"
				// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
				// 'UPGRADE_WARNING: MSComDlg.CommonDialog property frmMainWnd.dlgCommon.Flags was upgraded to frmMainWnd.dlgCommonOpen.CheckPathExists which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
				// .CheckPathExists = True
				// 'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
				// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// .Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
				// 'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
				// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// .Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
				// 'UPGRADE_WARNING: MSComDlg.CommonDialog property frmMainWnd.dlgCommon.Flags was upgraded to frmMainWnd.dlgCommonOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
				// 'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
				// .ShowReadOnly = False
				// 'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
				// 'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
				// .Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
				// 'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
				// .CancelError = True
				// .ShowDialog()
				// resetKeyboard()
				// 'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
				// If Err.Number = DialogResult.Cancel Then
				// Exit Sub
				// End If
				// End With
				if (!string.IsNullOrEmpty(My.MyProject.Forms.frmMainWnd.dlgCommonOpen.FileName))
				{
					modMain.ghTAPFile = FileSystem.FreeFile();
					FileSystem.FileOpen(modMain.ghTAPFile, My.MyProject.Forms.frmMainWnd.dlgCommonOpen.FileName, OpenMode.Binary);
					modMain.gsTAPFileName = My.MyProject.Forms.frmMainWnd.dlgCommonOpen.FileName;
					modMain.gMRU.AddMRUFile(ref modMain.gsTAPFileName);
					My.MyProject.Forms.frmMainWnd.NewCaption = My.MyProject.Application.Info.ProductName + " - " + modMain.GetFilePart(modMain.gsTAPFileName);
				}
			}

			modTZX.StopTape(); // // Stop the TZX tape player
			if (SaveTAP(ref modMain.glMemAddrDiv256[modMain.regAF_], ref modMain.regIX, ref modMain.regDE))
			{
				modMain.regIX = modMain.regIX + modMain.regDE;
				modMain.regDE = 0;
			}

			modMain.resetKeyboard();
			modMain.regPC = 1342; // RET
		}

		private static void SkipTAPBytes(ref int lLen)
		{
			string s;
			int lCounter;
			s = modMain.InputString16(modMain.ghTAPFile, lLen);
			var loopTo = Strings.Len(s);
			for (lCounter = 1; lCounter <= loopTo; lCounter++)
				m_lChecksum = m_lChecksum ^ modMain.Asc(Strings.Mid(s, lCounter, 1));
		}
	}
}