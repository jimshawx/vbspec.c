﻿using System.Reflection;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.


// TODO: Review the values of the assembly attributes


[assembly: AssemblyTitle("Sinclair Spectrum Emulator")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("Miklos Muhi <vbspec@muhi.org>")]
[assembly: AssemblyProduct("vbSpec")]
[assembly: AssemblyCopyright("Copyright ©2003 Miklos Muhi, Copyright ©1999-2003 Grok Developments Ltd, with portions copyrighted by their respective authors.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:

// Major version
// Minor Version
// Build Number
// Revision

// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.80.*")]
