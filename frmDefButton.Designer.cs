﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmDefButton
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmDefButton() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_CancelButton_Renamed.Name = "CancelButton_Renamed";
			_OKButton.Name = "OKButton";
			_txtValueFire.Name = "txtValueFire";
			_txtPortFire.Name = "txtPortFire";
			_txtFire.Name = "txtFire";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _CancelButton_Renamed;

		public Button CancelButton_Renamed
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _CancelButton_Renamed;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_CancelButton_Renamed != null)
				{
					_CancelButton_Renamed.Click -= CancelButton_Renamed_Click;
				}

				_CancelButton_Renamed = value;
				if (_CancelButton_Renamed != null)
				{
					_CancelButton_Renamed.Click += CancelButton_Renamed_Click;
				}
			}
		}

		private Button _OKButton;

		public Button OKButton
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _OKButton;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_OKButton != null)
				{
					_OKButton.Click -= OKButton_Click;
				}

				_OKButton = value;
				if (_OKButton != null)
				{
					_OKButton.Click += OKButton_Click;
				}
			}
		}

		private TextBox _txtValueFire;

		public TextBox txtValueFire
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtValueFire;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtValueFire != null)
				{
					_txtValueFire.TextChanged -= txtValueFire_TextChanged;
					_txtValueFire.Enter -= txtValueFire_Enter;
				}

				_txtValueFire = value;
				if (_txtValueFire != null)
				{
					_txtValueFire.TextChanged += txtValueFire_TextChanged;
					_txtValueFire.Enter += txtValueFire_Enter;
				}
			}
		}

		private TextBox _txtPortFire;

		public TextBox txtPortFire
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtPortFire;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtPortFire != null)
				{
					_txtPortFire.TextChanged -= txtPortFire_TextChanged;
					_txtPortFire.Enter -= txtPortFire_Enter;
				}

				_txtPortFire = value;
				if (_txtPortFire != null)
				{
					_txtPortFire.TextChanged += txtPortFire_TextChanged;
					_txtPortFire.Enter += txtPortFire_Enter;
				}
			}
		}

		private TextBox _txtFire;

		public TextBox txtFire
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _txtFire;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_txtFire != null)
				{
					_txtFire.KeyDown -= txtFire_KeyDown;
					_txtFire.TextChanged -= txtFire_TextChanged;
				}

				_txtFire = value;
				if (_txtFire != null)
				{
					_txtFire.KeyDown += txtFire_KeyDown;
					_txtFire.TextChanged += txtFire_TextChanged;
				}
			}
		}

		public Label _lblOr_0;
		public Label _lblKomma_0;
		public Label lblButton;
		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblKomma;
		public Microsoft.VisualBasic.Compatibility.VB6.LabelArray lblOr;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmDefButton));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_CancelButton_Renamed = new Button();
			_CancelButton_Renamed.Click += new EventHandler(CancelButton_Renamed_Click);
			_OKButton = new Button();
			_OKButton.Click += new EventHandler(OKButton_Click);
			_txtValueFire = new TextBox();
			_txtValueFire.TextChanged += new EventHandler(txtValueFire_TextChanged);
			_txtValueFire.Enter += new EventHandler(txtValueFire_Enter);
			_txtPortFire = new TextBox();
			_txtPortFire.TextChanged += new EventHandler(txtPortFire_TextChanged);
			_txtPortFire.Enter += new EventHandler(txtPortFire_Enter);
			_txtFire = new TextBox();
			_txtFire.KeyDown += new KeyEventHandler(txtFire_KeyDown);
			_txtFire.TextChanged += new EventHandler(txtFire_TextChanged);
			_lblOr_0 = new Label();
			_lblKomma_0 = new Label();
			lblButton = new Label();
			lblKomma = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			lblOr = new Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components);
			SuspendLayout();
			ToolTip1.Active = true;
			((System.ComponentModel.ISupportInitialize)lblKomma).BeginInit();
			((System.ComponentModel.ISupportInitialize)lblOr).BeginInit();
			FormBorderStyle = FormBorderStyle.FixedDialog;
			Text = "Joystick button definition";
			ClientSize = new Size(255, 71);
			Location = new Point(3, 22);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmDefButton";
			_CancelButton_Renamed.TextAlign = ContentAlignment.MiddleCenter;
			CancelButton = _CancelButton_Renamed;
			_CancelButton_Renamed.Text = "Cancel";
			_CancelButton_Renamed.Size = new Size(73, 25);
			_CancelButton_Renamed.Location = new Point(128, 39);
			_CancelButton_Renamed.TabIndex = 7;
			_CancelButton_Renamed.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_CancelButton_Renamed.BackColor = SystemColors.Control;
			_CancelButton_Renamed.CausesValidation = true;
			_CancelButton_Renamed.Enabled = true;
			_CancelButton_Renamed.ForeColor = SystemColors.ControlText;
			_CancelButton_Renamed.Cursor = Cursors.Default;
			_CancelButton_Renamed.RightToLeft = RightToLeft.No;
			_CancelButton_Renamed.TabStop = true;
			_CancelButton_Renamed.Name = "_CancelButton_Renamed";
			_OKButton.TextAlign = ContentAlignment.MiddleCenter;
			_OKButton.Text = "OK";
			AcceptButton = _OKButton;
			_OKButton.Size = new Size(73, 25);
			_OKButton.Location = new Point(51, 39);
			_OKButton.TabIndex = 6;
			_OKButton.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_OKButton.BackColor = SystemColors.Control;
			_OKButton.CausesValidation = true;
			_OKButton.Enabled = true;
			_OKButton.ForeColor = SystemColors.ControlText;
			_OKButton.Cursor = Cursors.Default;
			_OKButton.RightToLeft = RightToLeft.No;
			_OKButton.TabStop = true;
			_OKButton.Name = "_OKButton";
			_txtValueFire.AutoSize = false;
			_txtValueFire.TextAlign = HorizontalAlignment.Right;
			_txtValueFire.Size = new Size(26, 21);
			_txtValueFire.Location = new Point(224, 6);
			_txtValueFire.TabIndex = 4;
			_txtValueFire.Text = "255";
			_txtValueFire.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtValueFire.AcceptsReturn = true;
			_txtValueFire.BackColor = SystemColors.Window;
			_txtValueFire.CausesValidation = true;
			_txtValueFire.Enabled = true;
			_txtValueFire.ForeColor = SystemColors.WindowText;
			_txtValueFire.HideSelection = true;
			_txtValueFire.ReadOnly = false;
			_txtValueFire.MaxLength = 0;
			_txtValueFire.Cursor = Cursors.IBeam;
			_txtValueFire.Multiline = false;
			_txtValueFire.RightToLeft = RightToLeft.No;
			_txtValueFire.ScrollBars = ScrollBars.None;
			_txtValueFire.TabStop = true;
			_txtValueFire.Visible = true;
			_txtValueFire.BorderStyle = BorderStyle.Fixed3D;
			_txtValueFire.Name = "_txtValueFire";
			_txtPortFire.AutoSize = false;
			_txtPortFire.TextAlign = HorizontalAlignment.Right;
			_txtPortFire.Size = new Size(38, 21);
			_txtPortFire.Location = new Point(178, 6);
			_txtPortFire.TabIndex = 2;
			_txtPortFire.Text = "65535";
			_txtPortFire.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtPortFire.AcceptsReturn = true;
			_txtPortFire.BackColor = SystemColors.Window;
			_txtPortFire.CausesValidation = true;
			_txtPortFire.Enabled = true;
			_txtPortFire.ForeColor = SystemColors.WindowText;
			_txtPortFire.HideSelection = true;
			_txtPortFire.ReadOnly = false;
			_txtPortFire.MaxLength = 0;
			_txtPortFire.Cursor = Cursors.IBeam;
			_txtPortFire.Multiline = false;
			_txtPortFire.RightToLeft = RightToLeft.No;
			_txtPortFire.ScrollBars = ScrollBars.None;
			_txtPortFire.TabStop = true;
			_txtPortFire.Visible = true;
			_txtPortFire.BorderStyle = BorderStyle.Fixed3D;
			_txtPortFire.Name = "_txtPortFire";
			_txtFire.AutoSize = false;
			_txtFire.Size = new Size(29, 21);
			_txtFire.Location = new Point(128, 6);
			_txtFire.TabIndex = 1;
			_txtFire.Text = "X";
			_txtFire.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_txtFire.AcceptsReturn = true;
			_txtFire.TextAlign = HorizontalAlignment.Left;
			_txtFire.BackColor = SystemColors.Window;
			_txtFire.CausesValidation = true;
			_txtFire.Enabled = true;
			_txtFire.ForeColor = SystemColors.WindowText;
			_txtFire.HideSelection = true;
			_txtFire.ReadOnly = false;
			_txtFire.MaxLength = 0;
			_txtFire.Cursor = Cursors.IBeam;
			_txtFire.Multiline = false;
			_txtFire.RightToLeft = RightToLeft.No;
			_txtFire.ScrollBars = ScrollBars.None;
			_txtFire.TabStop = true;
			_txtFire.Visible = true;
			_txtFire.BorderStyle = BorderStyle.Fixed3D;
			_txtFire.Name = "_txtFire";
			_lblOr_0.Text = "or";
			_lblOr_0.Size = new Size(11, 14);
			_lblOr_0.Location = new Point(162, 10);
			_lblOr_0.TabIndex = 5;
			_lblOr_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblOr_0.TextAlign = ContentAlignment.TopLeft;
			_lblOr_0.BackColor = SystemColors.Control;
			_lblOr_0.Enabled = true;
			_lblOr_0.ForeColor = SystemColors.ControlText;
			_lblOr_0.Cursor = Cursors.Default;
			_lblOr_0.RightToLeft = RightToLeft.No;
			_lblOr_0.UseMnemonic = true;
			_lblOr_0.Visible = true;
			_lblOr_0.AutoSize = false;
			_lblOr_0.BorderStyle = BorderStyle.None;
			_lblOr_0.Name = "_lblOr_0";
			_lblKomma_0.Text = ",";
			_lblKomma_0.Size = new Size(5, 14);
			_lblKomma_0.Location = new Point(218, 10);
			_lblKomma_0.TabIndex = 3;
			_lblKomma_0.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_lblKomma_0.TextAlign = ContentAlignment.TopLeft;
			_lblKomma_0.BackColor = SystemColors.Control;
			_lblKomma_0.Enabled = true;
			_lblKomma_0.ForeColor = SystemColors.ControlText;
			_lblKomma_0.Cursor = Cursors.Default;
			_lblKomma_0.RightToLeft = RightToLeft.No;
			_lblKomma_0.UseMnemonic = true;
			_lblKomma_0.Visible = true;
			_lblKomma_0.AutoSize = false;
			_lblKomma_0.BorderStyle = BorderStyle.None;
			_lblKomma_0.Name = "_lblKomma_0";
			lblButton.TextAlign = ContentAlignment.TopRight;
			lblButton.Text = "Button X translated into:";
			lblButton.Size = new Size(114, 15);
			lblButton.Location = new Point(4, 10);
			lblButton.TabIndex = 0;
			lblButton.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			lblButton.BackColor = SystemColors.Control;
			lblButton.Enabled = true;
			lblButton.ForeColor = SystemColors.ControlText;
			lblButton.Cursor = Cursors.Default;
			lblButton.RightToLeft = RightToLeft.No;
			lblButton.UseMnemonic = true;
			lblButton.Visible = true;
			lblButton.AutoSize = false;
			lblButton.BorderStyle = BorderStyle.None;
			lblButton.Name = "lblButton";
			Controls.Add(_CancelButton_Renamed);
			Controls.Add(_OKButton);
			Controls.Add(_txtValueFire);
			Controls.Add(_txtPortFire);
			Controls.Add(_txtFire);
			Controls.Add(_lblOr_0);
			Controls.Add(_lblKomma_0);
			Controls.Add(lblButton);
			lblKomma.SetIndex(_lblKomma_0, Conversions.ToShort(0));
			lblOr.SetIndex(_lblOr_0, Conversions.ToShort(0));
			((System.ComponentModel.ISupportInitialize)lblOr).EndInit();
			((System.ComponentModel.ISupportInitialize)lblKomma).EndInit();
			Load += new EventHandler(frmDefButton_Load);
			Activated += new EventHandler(frmDefButton_Activated);
			FormClosed += new FormClosedEventHandler(frmDefButton_FormClosed);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}