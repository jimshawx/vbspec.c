﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	[DesignerGenerated()]
	internal partial class frmZXPrinter
	{
		/* TODO ERROR: Skipped RegionDirectiveTrivia */
		[DebuggerNonUserCode()]
		public frmZXPrinter() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			_cmdClear.Name = "cmdClear";
			_optAlphacom.Name = "optAlphacom";
			_optZX.Name = "optZX";
			_cmdFF.Name = "cmdFF";
			_cmdSave.Name = "cmdSave";
			_vs.Name = "vs";
			_picView.Name = "picView";
		}
		// Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool Disposing)
		{
			if (Disposing)
			{
				if (components is object)
				{
					components.Dispose();
				}
			}

			base.Dispose(Disposing);
		}
		// Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public ToolTip ToolTip1;
		private Button _cmdClear;

		public Button cmdClear
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdClear;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdClear != null)
				{
					_cmdClear.Click -= cmdClear_Click;
				}

				_cmdClear = value;
				if (_cmdClear != null)
				{
					_cmdClear.Click += cmdClear_Click;
				}
			}
		}

		private RadioButton _optAlphacom;

		public RadioButton optAlphacom
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _optAlphacom;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_optAlphacom != null)
				{
					_optAlphacom.CheckedChanged -= optAlphacom_CheckedChanged;
				}

				_optAlphacom = value;
				if (_optAlphacom != null)
				{
					_optAlphacom.CheckedChanged += optAlphacom_CheckedChanged;
				}
			}
		}

		private RadioButton _optZX;

		public RadioButton optZX
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _optZX;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_optZX != null)
				{
					_optZX.CheckedChanged -= optZX_CheckedChanged;
				}

				_optZX = value;
				if (_optZX != null)
				{
					_optZX.CheckedChanged += optZX_CheckedChanged;
				}
			}
		}

		private Timer _tmrFormFeed;

		public Timer tmrFormFeed
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _tmrFormFeed;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_tmrFormFeed != null)
				{
					_tmrFormFeed.Tick -= tmrFormFeed_Tick;
				}

				_tmrFormFeed = value;
				if (_tmrFormFeed != null)
				{
					_tmrFormFeed.Tick += tmrFormFeed_Tick;
				}
			}
		}

		public PictureBox imgAlpha;
		public PictureBox imgSinclair;
		public Panel picLogo;
		private Button _cmdFF;

		public Button cmdFF
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdFF;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdFF != null)
				{
					_cmdFF.MouseDown -= cmdFF_MouseDown;
					_cmdFF.MouseUp -= cmdFF_MouseUp;
				}

				_cmdFF = value;
				if (_cmdFF != null)
				{
					_cmdFF.MouseDown += cmdFF_MouseDown;
					_cmdFF.MouseUp += cmdFF_MouseUp;
				}
			}
		}

		private Button _cmdSave;

		public Button cmdSave
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _cmdSave;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_cmdSave != null)
				{
					_cmdSave.Click -= cmdSave_Click;
				}

				_cmdSave = value;
				if (_cmdSave != null)
				{
					_cmdSave.Click += cmdSave_Click;
				}
			}
		}

		private VScrollBar _vs;

		public VScrollBar vs
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _vs;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_vs != null)
				{
					_vs.Scroll -= vs_Scroll;
				}

				_vs = value;
				if (_vs != null)
				{
					_vs.Scroll += vs_Scroll;
				}
			}
		}

		public OpenFileDialog dlgSaveOpen;
		public SaveFileDialog dlgSaveSave;
		private Panel _picView;

		public Panel picView
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			get
			{
				return _picView;
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (_picView != null)
				{
					_picView.Resize -= picView_Resize;
				}

				_picView = value;
				if (_picView != null)
				{
					_picView.Resize += picView_Resize;
				}
			}
		}
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.
		// Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
			var resources = new System.Resources.ResourceManager(typeof(frmZXPrinter));
			components = new System.ComponentModel.Container();
			ToolTip1 = new ToolTip(components);
			_cmdClear = new Button();
			_cmdClear.Click += new EventHandler(cmdClear_Click);
			_optAlphacom = new RadioButton();
			_optAlphacom.CheckedChanged += new EventHandler(optAlphacom_CheckedChanged);
			_optZX = new RadioButton();
			_optZX.CheckedChanged += new EventHandler(optZX_CheckedChanged);
			_tmrFormFeed = new Timer(components);
			_tmrFormFeed.Tick += new EventHandler(tmrFormFeed_Tick);
			picLogo = new Panel();
			imgAlpha = new PictureBox();
			imgSinclair = new PictureBox();
			_cmdFF = new Button();
			_cmdFF.MouseDown += new MouseEventHandler(cmdFF_MouseDown);
			_cmdFF.MouseUp += new MouseEventHandler(cmdFF_MouseUp);
			_cmdSave = new Button();
			_cmdSave.Click += new EventHandler(cmdSave_Click);
			_vs = new VScrollBar();
			_vs.Scroll += new ScrollEventHandler(vs_Scroll);
			_picView = new Panel();
			_picView.Resize += new EventHandler(picView_Resize);
			dlgSaveOpen = new OpenFileDialog();
			dlgSaveSave = new SaveFileDialog();
			picLogo.SuspendLayout();
			_picView.SuspendLayout();
			SuspendLayout();
			ToolTip1.Active = true;
			FormBorderStyle = FormBorderStyle.SizableToolWindow;
			Text = "ZX Printer";
			ClientSize = new Size(273, 270);
			Location = new Point(4, 20);
			MaximizeBox = false;
			MinimizeBox = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.WindowsDefaultLocation;
			Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.Control;
			ControlBox = true;
			Enabled = true;
			KeyPreview = false;
			Cursor = Cursors.Default;
			RightToLeft = RightToLeft.No;
			HelpButton = false;
			WindowState = FormWindowState.Normal;
			Name = "frmZXPrinter";
			_cmdClear.TextAlign = ContentAlignment.BottomCenter;
			_cmdClear.Size = new Size(25, 23);
			_cmdClear.Location = new Point(60, 4);
			_cmdClear.Image = (Image)resources.GetObject("cmdClear.Image");
			_cmdClear.TabIndex = 7;
			ToolTip1.SetToolTip(_cmdClear, "Clear Output");
			_cmdClear.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdClear.BackColor = SystemColors.Control;
			_cmdClear.CausesValidation = true;
			_cmdClear.Enabled = true;
			_cmdClear.ForeColor = SystemColors.ControlText;
			_cmdClear.Cursor = Cursors.Default;
			_cmdClear.RightToLeft = RightToLeft.No;
			_cmdClear.TabStop = true;
			_cmdClear.Name = "_cmdClear";
			_optAlphacom.TextAlign = ContentAlignment.BottomCenter;
			_optAlphacom.Size = new Size(81, 20);
			_optAlphacom.Location = new Point(188, 4);
			_optAlphacom.Image = (Image)resources.GetObject("optAlphacom.Image");
			_optAlphacom.Appearance = Appearance.Button;
			_optAlphacom.TabIndex = 6;
			_optAlphacom.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_optAlphacom.CheckAlign = ContentAlignment.MiddleLeft;
			_optAlphacom.BackColor = SystemColors.Control;
			_optAlphacom.CausesValidation = true;
			_optAlphacom.Enabled = true;
			_optAlphacom.ForeColor = SystemColors.ControlText;
			_optAlphacom.Cursor = Cursors.Default;
			_optAlphacom.RightToLeft = RightToLeft.No;
			_optAlphacom.TabStop = true;
			_optAlphacom.Checked = false;
			_optAlphacom.Visible = true;
			_optAlphacom.Name = "_optAlphacom";
			_optZX.TextAlign = ContentAlignment.BottomCenter;
			_optZX.Size = new Size(81, 20);
			_optZX.Location = new Point(100, 4);
			_optZX.Image = (Image)resources.GetObject("optZX.Image");
			_optZX.Appearance = Appearance.Button;
			_optZX.TabIndex = 5;
			ToolTip1.SetToolTip(_optZX, "ZX Printer");
			_optZX.Checked = true;
			_optZX.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_optZX.CheckAlign = ContentAlignment.MiddleLeft;
			_optZX.BackColor = SystemColors.Control;
			_optZX.CausesValidation = true;
			_optZX.Enabled = true;
			_optZX.ForeColor = SystemColors.ControlText;
			_optZX.Cursor = Cursors.Default;
			_optZX.RightToLeft = RightToLeft.No;
			_optZX.TabStop = true;
			_optZX.Visible = true;
			_optZX.Name = "_optZX";
			_tmrFormFeed.Enabled = false;
			_tmrFormFeed.Interval = 2000;
			picLogo.Dock = DockStyle.Bottom;
			picLogo.BackColor = Color.Black;
			picLogo.Size = new Size(273, 38);
			picLogo.Location = new Point(0, 232);
			picLogo.TabIndex = 4;
			picLogo.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			picLogo.CausesValidation = true;
			picLogo.Enabled = true;
			picLogo.ForeColor = SystemColors.ControlText;
			picLogo.Cursor = Cursors.Default;
			picLogo.RightToLeft = RightToLeft.No;
			picLogo.TabStop = true;
			picLogo.Visible = true;
			picLogo.BorderStyle = BorderStyle.None;
			picLogo.Name = "picLogo";
			imgAlpha.Size = new Size(206, 26);
			imgAlpha.Location = new Point(4, 8);
			imgAlpha.Image = (Image)resources.GetObject("imgAlpha.Image");
			imgAlpha.Visible = false;
			imgAlpha.Enabled = true;
			imgAlpha.Cursor = Cursors.Default;
			imgAlpha.SizeMode = PictureBoxSizeMode.Normal;
			imgAlpha.BorderStyle = BorderStyle.None;
			imgAlpha.Name = "imgAlpha";
			imgSinclair.Size = new Size(206, 26);
			imgSinclair.Location = new Point(12, 4);
			imgSinclair.Image = (Image)resources.GetObject("imgSinclair.Image");
			imgSinclair.Enabled = true;
			imgSinclair.Cursor = Cursors.Default;
			imgSinclair.SizeMode = PictureBoxSizeMode.Normal;
			imgSinclair.Visible = true;
			imgSinclair.BorderStyle = BorderStyle.None;
			imgSinclair.Name = "imgSinclair";
			_cmdFF.TextAlign = ContentAlignment.BottomCenter;
			_cmdFF.Size = new Size(25, 23);
			_cmdFF.Location = new Point(32, 4);
			_cmdFF.Image = (Image)resources.GetObject("cmdFF.Image");
			_cmdFF.TabIndex = 3;
			ToolTip1.SetToolTip(_cmdFF, "Form Feed");
			_cmdFF.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdFF.BackColor = SystemColors.Control;
			_cmdFF.CausesValidation = true;
			_cmdFF.Enabled = true;
			_cmdFF.ForeColor = SystemColors.ControlText;
			_cmdFF.Cursor = Cursors.Default;
			_cmdFF.RightToLeft = RightToLeft.No;
			_cmdFF.TabStop = true;
			_cmdFF.Name = "_cmdFF";
			_cmdSave.TextAlign = ContentAlignment.BottomCenter;
			_cmdSave.Size = new Size(25, 23);
			_cmdSave.Location = new Point(4, 4);
			_cmdSave.Image = (Image)resources.GetObject("cmdSave.Image");
			_cmdSave.TabIndex = 2;
			ToolTip1.SetToolTip(_cmdSave, "Save Output");
			_cmdSave.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_cmdSave.BackColor = SystemColors.Control;
			_cmdSave.CausesValidation = true;
			_cmdSave.Enabled = true;
			_cmdSave.ForeColor = SystemColors.ControlText;
			_cmdSave.Cursor = Cursors.Default;
			_cmdSave.RightToLeft = RightToLeft.No;
			_cmdSave.TabStop = true;
			_cmdSave.Name = "_cmdSave";
			_vs.Size = new Size(17, 177);
			_vs.LargeChange = 8;
			_vs.Location = new Point(256, 32);
			_vs.SmallChange = 8;
			_vs.TabIndex = 1;
			_vs.TabStop = false;
			_vs.CausesValidation = true;
			_vs.Enabled = true;
			_vs.Maximum = 32774;
			_vs.Minimum = 0;
			_vs.Cursor = Cursors.Default;
			_vs.RightToLeft = RightToLeft.No;
			_vs.Value = 0;
			_vs.Visible = true;
			_vs.Name = "_vs";
			_picView.BackColor = Color.FromArgb(192, 192, 192);
			_picView.ForeColor = Color.Black;
			_picView.Size = new Size(256, 177);
			_picView.Location = new Point(0, 32);
			_picView.TabIndex = 0;
			_picView.Font = new Font("Arial", 8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
			_picView.Dock = DockStyle.None;
			_picView.CausesValidation = true;
			_picView.Enabled = true;
			_picView.Cursor = Cursors.Default;
			_picView.RightToLeft = RightToLeft.No;
			_picView.TabStop = true;
			_picView.Visible = true;
			_picView.BorderStyle = BorderStyle.None;
			_picView.Name = "_picView";
			dlgSaveOpen.DefaultExt = "bmp";
			dlgSaveSave.DefaultExt = "bmp";
			dlgSaveOpen.Title = "Save Output";
			dlgSaveSave.Title = "Save Output";
			dlgSaveOpen.FileName = "untitled";
			dlgSaveSave.FileName = "untitled";
			dlgSaveOpen.Filter = "Windows Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*";
			dlgSaveSave.Filter = "Windows Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*";
			Controls.Add(_cmdClear);
			Controls.Add(_optAlphacom);
			Controls.Add(_optZX);
			Controls.Add(picLogo);
			Controls.Add(_cmdFF);
			Controls.Add(_cmdSave);
			Controls.Add(_vs);
			Controls.Add(_picView);
			picLogo.Controls.Add(imgAlpha);
			picLogo.Controls.Add(imgSinclair);
			picLogo.ResumeLayout(false);
			_picView.ResumeLayout(false);
			Load += new EventHandler(frmZXPrinter_Load);
			Resize += new EventHandler(frmZXPrinter_Resize);
			FormClosed += new FormClosedEventHandler(frmZXPrinter_FormClosed);
			ResumeLayout(false);
			PerformLayout();
		}
		/* TODO ERROR: Skipped EndRegionDirectiveTrivia */
	}
}