﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	internal partial class frmOptions : Form
	{
		// /*******************************************************************************
		// frmOptions.frm within vbSpec.vbp
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// Joystick support code: Miklos Muhi <vbspec@muhi.org>
		// 
		// Copyright (C)1999-2000 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		// // Identifies the emulated model at the time
		// // the dialog is displayed. We need to reboot
		private int lOriginalModel; // // the Spectrum if this changes.

		// MM 12.03.2003 - Joystick support code
		// =================================================================================
		// The first activation flag
		private bool bIsFirstActivate;
		// Property value -- wich PC-joytick will be set up
		private PCJOYSTICKS lPCJoystick;
		// So many Buttons will be supported
		private int lSupportedButtons;
		// Supported PC-Joysticks
		public enum PCJOYSTICKS
		{
			pcjInvalid = -1,
			pcjJoystick1 = 0,
			pcjJoystick2 = 1
		}
		// =================================================================================

		private void cmdCancel_Click(object eventSender, EventArgs eventArgs)
		{
			Close();
		}

		private void cmdOK_Click(object eventSender, EventArgs eventArgs)
		{

			// Variables
			modSpectrum.JOYSTICK_DEFINITION jdTMP;
			if (optSpeed50.Checked)
			{
				modMain.glInterruptDelay = 40;
			}
			else if (optSpeed100.Checked)
			{
				modMain.glInterruptDelay = 20;
			}
			else if (optSpeed200.Checked)
			{
				modMain.glInterruptDelay = 10;
			}
			else if (optSpeedFastest.Checked)
			{
				modMain.glInterruptDelay = 0;
			}

			Interaction.SaveSetting("Grok", "vbSpec", "InterruptDelay", modMain.glInterruptDelay.ToString());
			modMain.glEmulatedModel = Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cboModel, cboModel.SelectedIndex);
			if (modMain.glEmulatedModel != lOriginalModel | Conversions.ToInteger((int)chkSEBasic.CheckState == 1) != modMain.gbSEBasicROM)
			{
				// // Model or ROM has been changed - reboot the Spectrum
				Interaction.SaveSetting("Grok", "vbSpec", "SEBasicROM", ((int)chkSEBasic.CheckState).ToString());
				Interaction.SaveSetting("Grok", "vbSpec", "EmulatedModel", modMain.glEmulatedModel.ToString());
				modZ80.Z80Reset();
			}

			Interaction.SaveSetting("Grok", "vbSpec", "MouseType", cboMouseType.SelectedIndex.ToString());
			Interaction.SaveSetting("Grok", "vbSpec", "SoundEnabled", ((int)chkSound.CheckState).ToString());
			modMain.gbSEBasicROM = -(int)chkSEBasic.CheckState;
			modMain.glMouseType = cboMouseType.SelectedIndex;
			if (optMouseGlobal.Checked)
			{
				modMain.gbMouseGlobal = Conversions.ToInteger(true);
			}
			else
			{
				modMain.gbMouseGlobal = Conversions.ToInteger(false);
			}

			Interaction.SaveSetting("Grok", "vbSpec", "MouseGlobal", modMain.gbMouseGlobal.ToString());
			if (modMain.glMouseType == modMain.MOUSE_NONE)
			{
				My.MyProject.Forms.frmMainWnd.picDisplay.Cursor = Cursors.Default;
			}
			else
			{
				// UPGRADE_ISSUE: PictureBox property picDisplay.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
				// frmMainWnd.picDisplay.Cursor = vbCustom
			}

			if (Conversions.ToBoolean(modMain.gbSoundEnabled))
			{
				if (chkSound.CheckState == 0)
				{
					modMain.CloseWaveOut();
				}
			}
			else if ((int)chkSound.CheckState == 1)
			{
				modMain.gbSoundEnabled = Conversions.ToInteger(modMain.InitializeWaveOut());
			}

			// MM 12.03.2003 - Joystick support code
			// =================================================================================
			// Save values
			if (modSpectrum.bJoystick1Valid)
			{
				modSpectrum.lPCJoystick1Is = (modSpectrum.ZXJOYSTICKS)Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[0], cmbZXJoystick[0].SelectedIndex);
				if (modSpectrum.lPCJoystick1Is != modSpectrum.ZXJOYSTICKS.zxjInvalid & modSpectrum.lPCJoystick1Is != modSpectrum.ZXJOYSTICKS.zxjUserDefined)
				{
					// UPGRADE_WARNING: Couldn't resolve default property of object jdTMP. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdTMP = modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.lPCJoystick1Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick1Fire + 1];
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.lPCJoystick1Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick1Fire + 1].sKey = Constants.vbNullString;
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.lPCJoystick1Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick1Fire + 1].lPort = 0;
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.lPCJoystick1Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick1Fire + 1].lValue = 0;
					modSpectrum.lPCJoystick1Fire = (modSpectrum.PCJOYBUTTONS)Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbFire[0], cmbFire[0].SelectedIndex);
					// UPGRADE_WARNING: Couldn't resolve default property of object aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lPCJoystick1Fire + 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK1, (int)modSpectrum.lPCJoystick1Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick1Fire + 1] = jdTMP;
				}
			}

			if (modSpectrum.bJoystick2Valid)
			{
				modSpectrum.lPCJoystick2Is = (modSpectrum.ZXJOYSTICKS)Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[1], cmbZXJoystick[1].SelectedIndex);
				if (modSpectrum.lPCJoystick2Is != modSpectrum.ZXJOYSTICKS.zxjInvalid & modSpectrum.lPCJoystick2Is != modSpectrum.ZXJOYSTICKS.zxjUserDefined)
				{
					// UPGRADE_WARNING: Couldn't resolve default property of object jdTMP. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdTMP = modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK2, (int)modSpectrum.lPCJoystick2Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick2Fire + 1];
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK2, (int)modSpectrum.lPCJoystick2Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick2Fire + 1].sKey = Constants.vbNullString;
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK2, (int)modSpectrum.lPCJoystick2Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick2Fire + 1].lPort = 0;
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK2, (int)modSpectrum.lPCJoystick2Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick2Fire + 1].lValue = 0;
					modSpectrum.lPCJoystick2Fire = (modSpectrum.PCJOYBUTTONS)Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbFire[1], cmbFire[1].SelectedIndex);
					// UPGRADE_WARNING: Couldn't resolve default property of object aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lPCJoystick2Fire + 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					modSpectrum.aJoystikDefinitionTable[modSpectrum.JDT_JOYSTICK2, (int)modSpectrum.lPCJoystick2Is, modSpectrum.JDT_BUTTON_BASE + (int)modSpectrum.lPCJoystick2Fire + 1] = jdTMP;
				}
			}
			// =================================================================================

			Close();
		}

		private void frmOptions_Load(object eventSender, EventArgs eventArgs)
		{
			switch (modMain.glInterruptDelay)
			{
				case 40:
					{
						// // 40ms delay = 50% Spectrum speed
						optSpeed50.Checked = true;
						break;
					}

				case 20:
					{
						// // 20ms delay = 100% Spectrum speed
						optSpeed100.Checked = true;
						break;
					}

				case 10:
					{
						// // 10ms delay = 200% Spectrum speed
						optSpeed200.Checked = true;
						break;
					}

				case 0:
					{
						// // 0 delay = run as fast as possible
						optSpeedFastest.Checked = true;
						break;
					}
			}

			cboModel.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("ZX Spectrum 48K", 0));
			cboModel.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("ZX Spectrum 128", 1));
			cboModel.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("ZX Spectrum +2", 2));
			cboModel.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Timex TC2048", 5));
			lOriginalModel = modMain.glEmulatedModel;
			switch (modMain.glEmulatedModel)
			{
				case 0: // // 48K
					{
						cboModel.SelectedIndex = 0;
						break;
					}

				case 1: // // 128K
					{
						cboModel.SelectedIndex = 1;
						break;
					}

				case 2: // // +2
					{
						cboModel.SelectedIndex = 2;
						break;
					}

				case 5: // // TC2048
					{
						cboModel.SelectedIndex = 3;
						break;
					}
			}

			if (Conversions.ToBoolean(modMain.gbSoundEnabled))
				chkSound.CheckState = CheckState.Checked;
			else
				chkSound.CheckState = CheckState.Unchecked;
			if (Conversions.ToBoolean(modMain.gbSEBasicROM))
				chkSEBasic.CheckState = CheckState.Checked;
			else
				chkSEBasic.CheckState = CheckState.Unchecked;

			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (string.IsNullOrEmpty(FileSystem.Dir(My.MyProject.Application.Info.DirectoryPath + @"\sebasic.rom")))
				chkSEBasic.Enabled = false;
			if (modMain.glMouseType == modMain.MOUSE_KEMPSTON)
			{
				cboMouseType.SelectedIndex = 1;
			}
			else if (modMain.glMouseType == modMain.MOUSE_AMIGA)
			{
				cboMouseType.SelectedIndex = 2;
			}
			else
			{
				cboMouseType.SelectedIndex = 0;
			}

			if (Conversions.ToBoolean(modMain.gbMouseGlobal))
				optMouseGlobal.Checked = true;
			else
				optMouseVB.Checked = true;

			// MM 12.03.2003 - Joystick support code
			// =================================================================================
			// Set up the controles
			{
				var withBlock = cmbZXJoystick[0];
				withBlock.Items.Clear();
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("(Invalid)", (int)modSpectrum.ZXJOYSTICKS.zxjInvalid));
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Kempston joystick", (int)modSpectrum.ZXJOYSTICKS.zxjKempston));
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Cursor joystick", (int)modSpectrum.ZXJOYSTICKS.zxjCursor));
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Sinclair 1", (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1));
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Sinclair 2", (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2));
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Fuller Box", (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox));
				// MM JD
				withBlock.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("User defined", (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined));
			}

			{
				var withBlock1 = cmbZXJoystick[0];
				withBlock1.Items.Clear();
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("(Invalid)", (int)modSpectrum.ZXJOYSTICKS.zxjInvalid));
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Kempston joystick", (int)modSpectrum.ZXJOYSTICKS.zxjKempston));
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Cursor joystick", (int)modSpectrum.ZXJOYSTICKS.zxjCursor));
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Sinclair 1", (int)modSpectrum.ZXJOYSTICKS.zxjSinclair1));
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Sinclair 2", (int)modSpectrum.ZXJOYSTICKS.zxjSinclair2));
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("Fuller Box", (int)modSpectrum.ZXJOYSTICKS.zxjFullerBox));
				// MM JD
				withBlock1.Items.Add(new Microsoft.VisualBasic.Compatibility.VB6.ListBoxItem("User defined", (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined));
			}
			// The first activation starts
			bIsFirstActivate = true;
			// =================================================================================
		}

		// MM 12.03.2003 - Joystick support code
		// =================================================================================
		// This code executes every time the window becomes visible
		// UPGRADE_WARNING: Form event frmOptions.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		private void frmOptions_Activated(object eventSender, EventArgs eventArgs)
		{

			// Help-Variable
			short iCounter;

			// By the first activate only
			if (bIsFirstActivate)
			{
				// Validity check
				lblZXJoystick[0].Enabled = modSpectrum.bJoystick1Valid;
				lblFire[0].Enabled = modSpectrum.bJoystick1Valid;
				cmbZXJoystick[0].Enabled = modSpectrum.bJoystick1Valid;
				cmbFire[0].Enabled = modSpectrum.bJoystick1Valid;
				cmdDefine[0].Enabled = modSpectrum.bJoystick1Valid;
				lblZXJoystick[1].Enabled = modSpectrum.bJoystick2Valid;
				lblFire[1].Enabled = modSpectrum.bJoystick2Valid;
				cmbZXJoystick[1].Enabled = modSpectrum.bJoystick2Valid;
				cmbFire[1].Enabled = modSpectrum.bJoystick2Valid;
				cmdDefine[1].Enabled = modSpectrum.bJoystick2Valid;
				// If joystick1 is valid
				if (modSpectrum.bJoystick1Valid)
				{
					// Get supported buttons
					lSupportedButtons = modSpectrum.lPCJoystick1Buttons;
					// Initialise Combos
					{
						var withBlock = cmbFire[0];
						// Add invalid button
						withBlock.Items.Add("(Invalid)");
						// Initialise ItemData
						// UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
						// VB6.SetItemData(cmbFire(0), cmbFire(0).NewIndex, -1)
						// Add valid buttons
						var loopTo = (short)lSupportedButtons;
						for (iCounter = 1; iCounter <= loopTo; iCounter++)
							// Add button
							// Initialise ItemData
							// UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
							// VB6.SetItemData(cmbFire(0), cmbFire(0).NewIndex, iCounter - 1)
							withBlock.Items.Add("Button " + Strings.Trim(iCounter.ToString()));
					}
					// Refresh the form
					Refresh();
					// Joystick1 values
					if (modSpectrum.lPCJoystick1Is == modSpectrum.ZXJOYSTICKS.zxjInvalid)
					{
						cmbZXJoystick[0].SelectedIndex = 0;
					}
					else
					{
						cmbZXJoystick[0].SelectedIndex = (int)modSpectrum.lPCJoystick1Is + 1;
					}

					if (modSpectrum.lPCJoystick1Fire == modSpectrum.PCJOYBUTTONS.pcjbInvalid)
					{
						cmbFire[0].SelectedIndex = 0;
					}
					else
					{
						cmbFire[0].SelectedIndex = (int)modSpectrum.lPCJoystick1Fire + 1;
					}
				}
				// If joystick2 is valid
				if (modSpectrum.bJoystick2Valid)
				{
					// Get supported buttons
					lSupportedButtons = modSpectrum.lPCJoystick1Buttons;
					// Initialise Combos
					{
						var withBlock1 = cmbFire[1];
						// Add invalid button
						withBlock1.Items.Add("(Invalid)");
						// Initialise ItemData
						// UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
						// VB6.SetItemData(cmbFire(1), cmbFire(1).NewIndex, -1)
						// Add valid buttons
						var loopTo1 = (short)lSupportedButtons;
						for (iCounter = 1; iCounter <= loopTo1; iCounter++)
							// Add button
							// Initialise ItemData
							// UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
							// VB6.SetItemData(cmbFire(1), cmbFire(1).NewIndex, iCounter - 1)
							withBlock1.Items.Add("Button " + Strings.Trim(iCounter.ToString()));
					}
					// Refresh the form
					Refresh();
					// Joystick2 Values
					if (modSpectrum.lPCJoystick2Is == modSpectrum.ZXJOYSTICKS.zxjInvalid)
					{
						cmbZXJoystick[1].SelectedIndex = 0;
					}
					else
					{
						cmbZXJoystick[1].SelectedIndex = (int)modSpectrum.lPCJoystick2Is + 1;
					}

					if (modSpectrum.lPCJoystick2Fire == modSpectrum.PCJOYBUTTONS.pcjbInvalid)
					{
						cmbFire[1].SelectedIndex = 0;
					}
					else
					{
						cmbFire[1].SelectedIndex = (int)modSpectrum.lPCJoystick2Fire + 1;
					}
				}
				// The first activate ends here
				bIsFirstActivate = false;
			}
		}
		// =================================================================================

		// MM JD
		// User Joystick definition support
		private void cmdDefine_Click(object eventSender, EventArgs eventArgs)
		{
			short Index = cmdDefine.GetIndex((Button)eventSender);
			// Variables
			frmDefJoystick frmDefJoystickWnd;
			int lCounter;
			// You cannot redefine an invalid joystick
			if (Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex) == (int)modSpectrum.ZXJOYSTICKS.zxjInvalid)
			{
				return;
				// Error handel
			};
			// Initialise
			frmDefJoystickWnd = new frmDefJoystick();
			// UPGRADE_ISSUE: Load statement is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
			// Load(frmDefJoystickWnd)
			frmDefJoystickWnd.JoystickNo = Index + 1;
			frmDefJoystickWnd.JoystickSetting = Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex);
			frmDefJoystickWnd.FireButton = cmbFire[Index].SelectedIndex;
			// Show
			frmDefJoystickWnd.ShowDialog();
			// If the changes where taken over
			if (frmDefJoystickWnd.OK)
			{
				// That is user defined
				var loopTo = cmbZXJoystick[Index].Items.Count - 1;
				for (lCounter = 0; lCounter <= loopTo; lCounter++)
				{
					if (Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], lCounter) == (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined)
					{
						cmbZXJoystick[Index].SelectedIndex = lCounter;
					}
				}
			}
			// Set to Nothing
			if (frmDefJoystickWnd is object)
			{
				// UPGRADE_NOTE: Object frmDefJoystickWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
				frmDefJoystickWnd = null;
			}
			// The End
			return;
		CMDDEFINECLICK_ERROR:
			;

			// Report error
			Interaction.MsgBox(Information.Err().Description, (MsgBoxStyle)((int)MsgBoxStyle.Information + (int)MsgBoxStyle.OkOnly));
			// Set to Nothing
			if (frmDefJoystickWnd is object)
			{
				// UPGRADE_NOTE: Object frmDefJoystickWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
				frmDefJoystickWnd = null;
			}
		}

		// MM JD
		// You cannot redefine an invalid joystick
		// UPGRADE_WARNING: Event cmbZXJoystick.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
		private void cmbZXJoystick_SelectedIndexChanged(object eventSender, EventArgs eventArgs)
		{
			short Index = cmbZXJoystick.GetIndex((ComboBox)eventSender);
			cmdDefine[Index].Enabled = Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex) != (int)modSpectrum.ZXJOYSTICKS.zxjInvalid;
			cmbFire[Index].Enabled = Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex) != (int)modSpectrum.ZXJOYSTICKS.zxjInvalid & Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex) != (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined;
			lblFire[Index].Enabled = Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex) != (int)modSpectrum.ZXJOYSTICKS.zxjInvalid & Microsoft.VisualBasic.Compatibility.VB6.Support.GetItemData(cmbZXJoystick[Index], cmbZXJoystick[Index].SelectedIndex) != (int)modSpectrum.ZXJOYSTICKS.zxjUserDefined;
		}






		// Private Sub ts1_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ts1.ClickEvent
		// Dim l As Integer

		// For l = 1 To ts1.Tabs.count
		// picFrame(l).Visible = ts1.Tabs(l).Selected
		// Next l
		// End Sub


		// Private Sub ts1_MouseDownEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxMSComctlLib.ITabStripEvents_MouseDownEvent) Handles ts1.MouseDownEvent
		// ts1_ClickEvent(ts1, New System.EventArgs())
		// End Sub
	}
}