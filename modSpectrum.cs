﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace vbSpec
{
	static class modSpectrum
	{
		// /*******************************************************************************
		// modSpectrum.bas within vbSpec.vbp
		// 
		// Routines for emulating the spectrum hardware; displaying the
		// video memory (0x4000 - 0x5AFF), reading the keyboard (port
		// 0xFE), and displaying the border colour (out (xxFE),x)
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2003 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/


		// MM JD

		// MM 03.02.2003 - BEGIN
		// Remarks:
		// 1. this code supports analog joysticks only
		// 2. the user must have a joytick driver under Windows
		// 3. the Y-axes represents the up and down
		// 4. the X-axes represents the right and left
		// 5. only one PC-joystick button is as fire supported
		// 6. the user can theoretically assign two PC-joysticks to one ZX-joystick, but I
		// don't think that't a good idea...
		// JoyStick API
		public const short MAXPNAMELEN = 32;
		// MM 03.02.2003 - BEGIN
		public const short MAX_JOYSTICKOEMVXDNAME = 260;
		// MM 03.02.2003 - END
		// The JOYINFOEX user-defined type contains extended information about the joystick position,
		// point-of-view position, and button state.
		public struct JOYINFOEX
		{
			public int dwSize; // size of structure
			public int dwFlags; // flags to indicate what to return
			public int dwXpos; // x position
			public int dwYpos; // y position
			public int dwZpos; // z position
			public int dwRpos; // rudder/4th axis position
			public int dwUpos; // 5th axis position
			public int dwVpos; // 6th axis position
			public int dwButtons; // button states
			public int dwButtonNumber; // current button number pressed
			public int dwPOV; // point of view state
			public int dwReserved1; // reserved for communication between winmm driver
			public int dwReserved2; // reserved for future expansion
		}
		// The JOYCAPS user-defined type contains information about the joystick capabilities
		public struct JOYCAPS
		{
			public short wMid; // Manufacturer identifier of the device driver for the MIDI output device
							   // For a list of identifiers, see the Manufacturer Indentifier topic in the
							   // Multimedia Reference of the Platform SDK.
			public short wPid; // Product Identifier Product of the MIDI output device. For a list of
							   // product identifiers, see the Product Identifiers topic in the Multimedia
							   // Reference of the Platform SDK.
							   // UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
			[VBFixedString(MAXPNAMELEN)]
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXPNAMELEN)]
			public char[] szPname; // Null-terminated string containing the joystick product name
			public int wXmin; // Minimum X-coordinate.
			public int wXmax; // Maximum X-coordinate.
			public int wYmin; // Minimum Y-coordinate
			public int wYmax; // Maximum Y-coordinate
			public int wZmin; // Minimum Z-coordinate
			public int wZmax; // Maximum Z-coordinate
			public int wNumButtons; // Number of joystick buttons
			public int wPeriodMin; // Smallest polling frequency supported when captured by the joySetCapture function.
			public int wPeriodMax; // Largest polling frequency supported when captured by the joySetCapture function.
			public int wRmin; // Minimum rudder value. The rudder is a fourth axis of movement.
			public int wRmax; // Maximum rudder value. The rudder is a fourth axis of movement.
			public int wUmin; // Minimum u-coordinate (fifth axis) values.
			public int wUmax; // Maximum u-coordinate (fifth axis) values.
			public int wVmin; // Minimum v-coordinate (sixth axis) values.
			public int wVmax; // Maximum v-coordinate (sixth axis) values.
			public int wCaps; // Joystick capabilities as defined by the following flags
							  // JOYCAPS_HASZ-     Joystick has z-coordinate information.
							  // JOYCAPS_HASR-     Joystick has rudder (fourth axis) information.
							  // JOYCAPS_HASU-     Joystick has u-coordinate (fifth axis) information.
							  // JOYCAPS_HASV-     Joystick has v-coordinate (sixth axis) information.
							  // JOYCAPS_HASPOV-   Joystick has point-of-view information.
							  // JOYCAPS_POV4DIR-  Joystick point-of-view supports discrete values (centered, forward, backward, left, and right).
							  // JOYCAPS_POVCTS Joystick point-of-view supports continuous degree bearings.
			public int wMaxAxes; // Maximum number of axes supported by the joystick.
			public int wNumAxes; // Number of axes currently in use by the joystick.
			public int wMaxButtons; // Maximum number of buttons supported by the joystick.
									// UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
			[VBFixedString(MAXPNAMELEN)]
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXPNAMELEN)]
			public char[] szRegKey; // String containing the registry key for the joystick.
									// MM 03.02.2003 - BEGIN
									// UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
			[VBFixedString(MAX_JOYSTICKOEMVXDNAME)]
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_JOYSTICKOEMVXDNAME)]
			public char[] szOEMVxD; // Null-terminated string identifying the joystick driver OEM.
									// MM 03.02.2003 - END
		}
		// UPGRADE_WARNING: Structure JOYINFOEX may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("winmm.dll")]
		static extern int joyGetPosEx(int uJoyID, ref JOYINFOEX pji);
		// This function queries a joystick for its position and button status. The function
		// requires the following parameters;
		// uJoyID-  integer identifying the joystick to be queried. Use the constants
		// JOYSTICKID1 or JOYSTICKID2 for this value.
		// pji-     user-defined type variable that stores extended position information
		// and button status of the joystick. The information returned from
		// this function depends on the flags you specify in dwFlags member of
		// the user-defined type variable.
		// 
		// The function returns the constant JOYERR_NOERROR if successful or one of the
		// following error values:
		// MMSYSERR_NODRIVER-      The joystick driver is not present.
		// MMSYSERR_INVALPARAM-    An invalid parameter was passed.
		// MMSYSERR_BADDEVICEID-   The specified joystick identifier is invalid.
		// JOYERR_UNPLUGGED-       The specified joystick is not connected to the system.
		// UPGRADE_WARNING: Structure JOYCAPS may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
		[DllImport("winmm.dll", EntryPoint = "joyGetDevCapsA")]
		public static extern int joyGetDevCaps(int id, ref JOYCAPS lpCaps, int uSize);
		// This function queries a joystick to determine its capabilities. The function requires
		// the following parameters:
		// uJoyID-  integer identifying the joystick to be queried. Use the contstants
		// JOYSTICKID1 or JOYSTICKID2 for this value.
		// pjc-     user-defined type variable that stores the capabilities of the joystick.
		// cbjc-    Size, in bytes, of the pjc variable. Use the Len function for this value.
		// The function returns the constant JOYERR_NOERROR if a joystick is present or one of
		// the following error values:
		// MMSYSERR_NODRIVER-   The joystick driver is not present.
		// MMSYSERR_INVALPARAM- An invalid parameter was passed.
		public const short JOYERR_OK = 0;
		public const short JOYSTICKID1 = 0;
		public const short JOYSTICKID2 = 1;
		public const int JOY_RETURNBUTTONS = 0x80;
		public const int JOY_RETURNCENTERED = 0x400;
		public const int JOY_RETURNPOV = 0x40;
		public const int JOY_RETURNR = 0x8;
		public const int JOY_RETURNU = 0x10;
		public const int JOY_RETURNV = 0x20;
		public const int JOY_RETURNX = 0x1;
		public const int JOY_RETURNY = 0x2;
		public const int JOY_RETURNZ = 0x4;
		public const bool JOY_RETURNALL = true;
		public const int JOYCAPS_HASZ = 0x1;
		public const int JOYCAPS_HASR = 0x2;
		public const int JOYCAPS_HASU = 0x4;
		public const int JOYCAPS_HASV = 0x8;
		public const int JOYCAPS_HASPOV = 0x10;
		public const int JOYCAPS_POV4DIR = 0x20;
		public const int JOYCAPS_POVCTS = 0x40;
		public const short JOYERR_BASE = 160;
		public const int JOYERR_UNPLUGGED = JOYERR_BASE + 7;
		// Supported ZX-Joysticks
		public enum ZXJOYSTICKS
		{
			zxjInvalid = -1,
			zxjKempston = 0,
			zxjCursor = 1,
			zxjSinclair1 = 2,
			zxjSinclair2 = 3,
			zxjFullerBox = 4,
			// MM JD
			zxjUserDefined = 5
		}
		// Supported PC-joystick buttons
		public enum PCJOYBUTTONS
		{
			pcjbInvalid = -1,
			pcjbButton1 = 0,
			pcjbButton2 = 1,
			pcjbButton3 = 2,
			pcjbButton4 = 3,
			pcjbButton5 = 4,
			pcjbButton6 = 5,
			pcjbButton7 = 6,
			pcjbButton8 = 7
		}
		// Settings for PC-joystick nr. 1
		// The ZX-joystick type to be emulated by PC-joystick nr. 1
		public static ZXJOYSTICKS lPCJoystick1Is;
		// The Fire button of the ZX-joystick played by the button nr. ... of the
		// PC-joystick  nr. 1
		public static PCJOYBUTTONS lPCJoystick1Fire;
		// Number of the buttons by PC-joystick nr. 1
		// Remarks: up to 8 PC-joystick buttons supported
		public static int lPCJoystick1Buttons;
		// Settings for PC-joystick nr. 2
		// The ZX-joystick type to be emulated by PC-joystick nr. 2
		public static ZXJOYSTICKS lPCJoystick2Is;
		// The Fire button of the ZX-joystick played by the button nr. ... of the
		// PC-joystick  nr. 2
		public static PCJOYBUTTONS lPCJoystick2Fire;
		// Number of the buttons by PC-joystick nr. 2
		// Remarks: up to 8 PC-joystick buttons supported
		public static int lPCJoystick2Buttons;
		// Supported joystick directions
		public const int zxjdUp = 1;
		public const int zxjdDown = 2;
		public const int zxjdLeft = 4;
		public const int zxjdRight = 8;
		public const int zxjdFire = 16;
		// MM JD
		public const int zxjdButtonBase = 32;
		// Kempston joystick directions
		public const int KEMPSTON_UP = 8;
		public const int KEMPSTON_PUP = 31;
		public const int KEMPSTON_DOWN = 4;
		public const int KEMPSTON_PDOWN = 31;
		public const int KEMPSTON_LEFT = 2;
		public const int KEMPSTON_PLEFT = 31;
		public const int KEMPSTON_RIGHT = 1;
		public const int KEMPSTON_PRIGHT = 31;
		public const int KEMPSTON_FIRE = 16;
		public const int KEMPSTON_PFIRE = 31;
		// Cursor joystick directions
		public const int CURSOR_UP = 183;
		public const int CURSOR_PUP = 61438;
		public const int CURSOR_DOWN = 175;
		public const int CURSOR_PDOWN = 61438;
		public const int CURSOR_LEFT = 175;
		public const int CURSOR_PLEFT = 63486;
		public const int CURSOR_RIGHT = 187;
		public const int CURSOR_PRIGHT = 61438;
		public const int CURSOR_FIRE = 190;
		public const int CURSOR_PFIRE = 61438;
		// Sinclair1 joystick directions
		public const int SINCLAIR1_UP = 189;
		public const int SINCLAIR1_PUP = 61438;
		public const int SINCLAIR1_DOWN = 187;
		public const int SINCLAIR1_PDOWN = 61438;
		public const int SINCLAIR1_LEFT = 175;
		public const int SINCLAIR1_PLEFT = 61438;
		public const int SINCLAIR1_RIGHT = 183;
		public const int SINCLAIR1_PRIGHT = 61438;
		public const int SINCLAIR1_FIRE = 190;
		public const int SINCLAIR1_PFIRE = 61438;
		// Sinclair2 joystick directions
		public const int SINCLAIR2_UP = 183;
		public const int SINCLAIR2_PUP = 63486;
		public const int SINCLAIR2_DOWN = 187;
		public const int SINCLAIR2_PDOWN = 63486;
		public const int SINCLAIR2_LEFT = 190;
		public const int SINCLAIR2_PLEFT = 63486;
		public const int SINCLAIR2_RIGHT = 189;
		public const int SINCLAIR2_PRIGHT = 63486;
		public const int SINCLAIR2_FIRE = 175;
		public const int SINCLAIR2_PFIRE = 63486;
		// Fuller Box directions
		public const int FULLER_UP = 1;
		public const int FULLER_PUP = 127;
		public const int FULLER_DOWN = 2;
		public const int FULLER_PDOWN = 127;
		public const int FULLER_LEFT = 4;
		public const int FULLER_PLEFT = 127;
		public const int FULLER_RIGHT = 8;
		public const int FULLER_PRIGHT = 127;
		public const int FULLER_FIRE = 128;
		public const int FULLER_PFIRE = 127;
		// Last key
		private static int lKey1;
		private static int lKey2;
		// MM 12.03.2003 - Joystick support code
		// =================================================================================
		// Joystick validity Flags
		public static bool bJoystick1Valid;
		public static bool bJoystick2Valid;
		// =================================================================================
		// MM 03.02.2003 - END

		// MM JD
		// Maximal number of buttons
		public const int JDT_MAXBUTTONS = 8;
		// Joystick actions
		public const int JDT_UP = 0;
		public const int JDT_DOWN = 1;
		public const int JDT_LEFT = 2;
		public const int JDT_RIGHT = 3;
		public const int JDT_BUTTON1 = 4;
		public const int JDT_BUTTON2 = 5;
		public const int JDT_BUTTON3 = 6;
		public const int JDT_BUTTON4 = 7;
		public const int JDT_BUTTON5 = 8;
		public const int JDT_BUTTON6 = 9;
		public const int JDT_BUTTON7 = 10;
		public const int JDT_BUTTON8 = 11;
		public const int JDT_BUTTON_BASE = 3;
		// Joystick numbers
		public const int JDT_JOYSTICK1 = 0;
		public const int JDT_JOYSTICK2 = 1;
		// Joystick definition type
		public struct JOYSTICK_DEFINITION
		{
			public string sKey;
			public int lPort;
			public int lValue;
			public int lCumulate;
		}
		// Joystick definition table
		// That is a 3D table, wich describes the joystick actions, even for standart joysticks
		// - first dimension 1..0, the number of the joystick
		// - the second dimension 0..5, the joystick type (Enum ZXJOYSTICKS)
		// - the third dimension 0..11, the joystick actions (JDT_...)
		public static JOYSTICK_DEFINITION[,,] aJoystikDefinitionTable = new JOYSTICK_DEFINITION[2, 6, 12];

		// MM 16.04.2003
		// True if Speccy in Full-Screen-Modus
		public static bool bFullScreen;
		public static int glNewBorder;
		public static int glLastBorder;
		public static int lZXPrinterX;
		public static int lZXPrinterY;
		public static int lZXPrinterEncoder;
		private static int lZXPrinterMotorOn;
		private static int lZXPrinterStylusOn;
		private static int[] lRevBitValues = new int[8];
		public static int[] glAmigaMouseY = new int[4];
		public static int[] glAmigaMouseX = new int[4];
		private static int lOldAmigaX;
		private static int lOldAmigaY;
		private static int lAmigaXPtr;
		private static int lAmigaYPtr;

		public static bool doKey(ref bool down, ref short ascii, ref short mods)
		{
			bool doKeyRet = default;
			bool CAPS, SYMB;
			CAPS = Conversions.ToBoolean(mods & 1);
			SYMB = Conversions.ToBoolean(mods & 2);

			// // Change control versions of keys to lower case
			if (ascii >= 1 & ascii <= 0x27 & SYMB)
			{
				ascii = (short)(ascii + modMain.Asc("a") - 1);
			}

			if (CAPS)
				modMain.keyCAPS_V = modMain.keyCAPS_V & ~1;
			else
				modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
			if (SYMB)
				modMain.keyB_SPC = modMain.keyB_SPC & ~2;
			else
				modMain.keyB_SPC = modMain.keyB_SPC | 2;
			switch (ascii)
			{
				case 8: // Backspace
					{
						if (down)
						{
							modMain.key6_0 = modMain.key6_0 & ~1;
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~1;
						}
						else
						{
							modMain.key6_0 = modMain.key6_0 | 1;
							if (!CAPS)
							{
								modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
							}
						}

						break;
					}

				case 65: // A
					{
						if (down)
							modMain.keyA_G = modMain.keyA_G & ~1;
						else
							modMain.keyA_G = modMain.keyA_G | 1;
						break;
					}

				case 66: // B
					{
						if (down)
							modMain.keyB_SPC = modMain.keyB_SPC & ~16;
						else
							modMain.keyB_SPC = modMain.keyB_SPC | 16;
						break;
					}

				case 67: // C
					{
						if (down)
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~8;
						else
							modMain.keyCAPS_V = modMain.keyCAPS_V | 8;
						break;
					}

				case 68: // D
					{
						if (down)
							modMain.keyA_G = modMain.keyA_G & ~4;
						else
							modMain.keyA_G = modMain.keyA_G | 4;
						break;
					}

				case 69: // E
					{
						if (down)
							modMain.keyQ_T = modMain.keyQ_T & ~4;
						else
							modMain.keyQ_T = modMain.keyQ_T | 4;
						break;
					}

				case 70: // F
					{
						if (down)
							modMain.keyA_G = modMain.keyA_G & ~8;
						else
							modMain.keyA_G = modMain.keyA_G | 8;
						break;
					}

				case 71: // G
					{
						if (down)
							modMain.keyA_G = modMain.keyA_G & ~16;
						else
							modMain.keyA_G = modMain.keyA_G | 16;
						break;
					}

				case 72: // H
					{
						if (down)
							modMain.keyH_ENT = modMain.keyH_ENT & ~16;
						else
							modMain.keyH_ENT = modMain.keyH_ENT | 16;
						break;
					}

				case 73: // I
					{
						if (down)
							modMain.keyY_P = modMain.keyY_P & ~4;
						else
							modMain.keyY_P = modMain.keyH_ENT | 4;
						break;
					}

				case 74: // J
					{
						if (down)
							modMain.keyH_ENT = modMain.keyH_ENT & ~8;
						else
							modMain.keyH_ENT = modMain.keyH_ENT | 8;
						break;
					}

				case 75: // K
					{
						if (down)
							modMain.keyH_ENT = modMain.keyH_ENT & ~4;
						else
							modMain.keyH_ENT = modMain.keyH_ENT | 4;
						break;
					}

				case 76: // L
					{
						if (down)
							modMain.keyH_ENT = modMain.keyH_ENT & ~2;
						else
							modMain.keyH_ENT = modMain.keyH_ENT | 2;
						break;
					}

				case 77: // M
					{
						if (down)
							modMain.keyB_SPC = modMain.keyB_SPC & ~4;
						else
							modMain.keyB_SPC = modMain.keyB_SPC | 4;
						break;
					}

				case 78: // N
					{
						if (down)
							modMain.keyB_SPC = modMain.keyB_SPC & ~8;
						else
							modMain.keyB_SPC = modMain.keyB_SPC | 8;
						break;
					}

				case 79: // O
					{
						if (down)
							modMain.keyY_P = modMain.keyY_P & ~2;
						else
							modMain.keyY_P = modMain.keyY_P | 2;
						break;
					}

				case 80: // P
					{
						if (down)
							modMain.keyY_P = modMain.keyY_P & ~1;
						else
							modMain.keyY_P = modMain.keyY_P | 1;
						break;
					}

				case 81: // Q
					{
						if (down)
							modMain.keyQ_T = modMain.keyQ_T & ~1;
						else
							modMain.keyQ_T = modMain.keyQ_T | 1;
						break;
					}

				case 82: // R
					{
						if (down)
							modMain.keyQ_T = modMain.keyQ_T & ~8;
						else
							modMain.keyQ_T = modMain.keyQ_T | 8;
						break;
					}

				case 83: // S
					{
						if (down)
							modMain.keyA_G = modMain.keyA_G & ~2;
						else
							modMain.keyA_G = modMain.keyA_G | 2;
						break;
					}

				case 84: // T
					{
						if (down)
							modMain.keyQ_T = modMain.keyQ_T & ~16;
						else
							modMain.keyQ_T = modMain.keyQ_T | 16;
						break;
					}

				case 85: // U
					{
						if (down)
							modMain.keyY_P = modMain.keyY_P & ~8;
						else
							modMain.keyY_P = modMain.keyY_P | 8;
						break;
					}

				case 86: // V
					{
						if (down)
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~16;
						else
							modMain.keyCAPS_V = modMain.keyCAPS_V | 16;
						break;
					}

				case 87: // W
					{
						if (down)
							modMain.keyQ_T = modMain.keyQ_T & ~2;
						else
							modMain.keyQ_T = modMain.keyQ_T | 2;
						break;
					}

				case 88: // X
					{
						if (down)
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~4;
						else
							modMain.keyCAPS_V = modMain.keyCAPS_V | 4;
						break;
					}

				case 89: // Y
					{
						// MM 03.02.2003 - BEGIN
						if (!My.MyProject.Forms.frmMainWnd.mnuOptions[7].Checked)
						{
							if (down)
								modMain.keyY_P = modMain.keyY_P & ~16;
							else
								modMain.keyY_P = modMain.keyY_P | 16;
						}
						else if (down)
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~2;
						else
							modMain.keyCAPS_V = modMain.keyCAPS_V | 2;
						break;
					}
				// MM 03.02.2003 - END
				case 90: // Z
					{
						// MM 03.02.2003 - BEGIN
						if (!My.MyProject.Forms.frmMainWnd.mnuOptions[7].Checked)
						{
							if (down)
								modMain.keyCAPS_V = modMain.keyCAPS_V & ~2;
							else
								modMain.keyCAPS_V = modMain.keyCAPS_V | 2;
						}
						else if (down)
							modMain.keyY_P = modMain.keyY_P & ~16;
						else
							modMain.keyY_P = modMain.keyY_P | 16;
						break;
					}
				// MM 03.02.2003 - END
				case 48: // 0
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~1;
						else
							modMain.key6_0 = modMain.key6_0 | 1;
						break;
					}

				case 49: // 1
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~1;
						else
							modMain.key1_5 = modMain.key1_5 | 1;
						break;
					}

				case 50: // 2
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~2;
						else
							modMain.key1_5 = modMain.key1_5 | 2;
						break;
					}

				case 51: // 3
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~4;
						else
							modMain.key1_5 = modMain.key1_5 | 4;
						break;
					}

				case 52: // 4
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~8;
						else
							modMain.key1_5 = modMain.key1_5 | 8;
						break;
					}

				case 53: // 5
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~16;
						else
							modMain.key1_5 = modMain.key1_5 | 16;
						break;
					}

				case 54: // 6
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~16;
						else
							modMain.key6_0 = modMain.key6_0 | 16;
						break;
					}

				case 55: // 7
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~8;
						else
							modMain.key6_0 = modMain.key6_0 | 8;
						break;
					}

				case 56: // 8
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~4;
						else
							modMain.key6_0 = modMain.key6_0 | 4;
						break;
					}

				case 57: // 9
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~2;
						else
							modMain.key6_0 = modMain.key6_0 | 2;
						break;
					}

				case 96: // Keypad 0
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~1;
						else
							modMain.key6_0 = modMain.key6_0 | 1;
						break;
					}

				case 97: // Keypad 1
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~1;
						else
							modMain.key1_5 = modMain.key1_5 | 1;
						break;
					}

				case 98: // Keypad 2
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~2;
						else
							modMain.key1_5 = modMain.key1_5 | 2;
						break;
					}

				case 99: // Keypad 3
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~4;
						else
							modMain.key1_5 = modMain.key1_5 | 4;
						break;
					}

				case 100: // Keypad 4
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~8;
						else
							modMain.key1_5 = modMain.key1_5 | 8;
						break;
					}

				case 101: // Keypad 5
					{
						if (down)
							modMain.key1_5 = modMain.key1_5 & ~16;
						else
							modMain.key1_5 = modMain.key1_5 | 16;
						break;
					}

				case 102: // Keypad 6
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~16;
						else
							modMain.key6_0 = modMain.key6_0 | 16;
						break;
					}

				case 103: // Keypad 7
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~8;
						else
							modMain.key6_0 = modMain.key6_0 | 8;
						break;
					}

				case 104: // Keypad 8
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~4;
						else
							modMain.key6_0 = modMain.key6_0 | 4;
						break;
					}

				case 105: // Keypad 9
					{
						if (down)
							modMain.key6_0 = modMain.key6_0 & ~2;
						else
							modMain.key6_0 = modMain.key6_0 | 2;
						break;
					}

				case 106: // Keypad *
					{
						if (down)
						{
							modMain.keyB_SPC = modMain.keyB_SPC & ~18;
						}
						else if (SYMB)
						{
							modMain.keyB_SPC = modMain.keyB_SPC | 16;
						}
						else
						{
							modMain.keyB_SPC = modMain.keyB_SPC | 18;
						}

						break;
					}

				case 107: // Keypad +
					{
						if (down)
						{
							modMain.keyH_ENT = modMain.keyH_ENT & ~4;
							modMain.keyB_SPC = modMain.keyB_SPC & ~2;
						}
						else
						{
							modMain.keyH_ENT = modMain.keyH_ENT | 4;
							if (!SYMB)
							{
								modMain.keyB_SPC = modMain.keyB_SPC | 2;
							}
						}

						break;
					}

				case 109: // Keypad -
					{
						if (down)
						{
							modMain.keyH_ENT = modMain.keyH_ENT & ~8;
							modMain.keyB_SPC = modMain.keyB_SPC & ~2;
						}
						else
						{
							modMain.keyH_ENT = modMain.keyH_ENT | 8;
							if (!SYMB)
							{
								modMain.keyB_SPC = modMain.keyB_SPC | 2;
							}
						}

						break;
					}

				case 110: // Keypad .
					{
						if (down)
						{
							modMain.keyB_SPC = modMain.keyB_SPC & ~6;
						}
						else if (SYMB)
						{
							modMain.keyB_SPC = modMain.keyB_SPC | 4;
						}
						else
						{
							modMain.keyB_SPC = modMain.keyB_SPC | 6;
						}

						break;
					}

				case 111: // Keypad /
					{
						if (down)
						{
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~16;
							modMain.keyB_SPC = modMain.keyB_SPC & ~2;
						}
						else
						{
							modMain.keyCAPS_V = modMain.keyCAPS_V | 16;
							if (!SYMB)
							{
								modMain.keyB_SPC = modMain.keyB_SPC | 2;
							}
						}

						break;
					}

				case 37: // Left
					{
						if (down)
						{
							modMain.key1_5 = modMain.key1_5 & ~16;
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~1;
						}
						else
						{
							modMain.key1_5 = modMain.key1_5 | 16;
							if (!SYMB)
							{
								modMain.keyB_SPC = modMain.keyB_SPC | 2;
							}
						}

						break;
					}

				case 38: // Up
					{
						if (down)
						{
							modMain.key6_0 = modMain.key6_0 & ~8;
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~1;
						}
						else
						{
							modMain.key6_0 = modMain.key6_0 | 8;
							if (!CAPS)
							{
								modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
							}
						}

						break;
					}

				case 39: // Right
					{
						if (down)
						{
							modMain.key6_0 = modMain.key6_0 & ~4;
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~1;
						}
						else
						{
							modMain.key6_0 = modMain.key6_0 | 4;
							if (!CAPS)
							{
								modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
							}
						}

						break;
					}

				case 40: // Down
					{
						if (down)
						{
							modMain.key6_0 = modMain.key6_0 & ~16;
							modMain.keyCAPS_V = modMain.keyCAPS_V & ~1;
						}
						else
						{
							modMain.key6_0 = modMain.key6_0 | 16;
							if (!CAPS)
							{
								modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
							}
						}

						break;
					}

				case 13: // RETURN
					{
						if (down)
							modMain.keyH_ENT = modMain.keyH_ENT & ~1;
						else
							modMain.keyH_ENT = modMain.keyH_ENT | 1;
						break;
					}

				case 32: // SPACE BAR
					{
						if (down)
							modMain.keyB_SPC = modMain.keyB_SPC & ~1;
						else
							modMain.keyB_SPC = modMain.keyB_SPC | 1;
						break;
					}

				case 187: // =/+ key
					{
						if (down)
						{
							if (CAPS)
							{
								modMain.keyH_ENT = modMain.keyH_ENT & ~4;
							}
							else
							{
								modMain.keyH_ENT = modMain.keyH_ENT & ~2;
							}

							modMain.keyB_SPC = modMain.keyB_SPC & ~2;
							modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
						}
						else
						{
							modMain.keyH_ENT = modMain.keyH_ENT | 4;
							modMain.keyH_ENT = modMain.keyH_ENT | 2;
							modMain.keyB_SPC = modMain.keyB_SPC | 2;
						}

						break;
					}

				case 189: // -/_ key
					{
						if (down)
						{
							if (CAPS)
							{
								modMain.key6_0 = modMain.key6_0 & ~1;
							}
							else
							{
								modMain.keyH_ENT = modMain.keyH_ENT & ~8;
							}

							modMain.keyB_SPC = modMain.keyB_SPC & ~2;
							modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
						}
						else
						{
							modMain.key6_0 = modMain.key6_0 | 1; // // Release the Spectrum's '0' key
							modMain.keyH_ENT = modMain.keyH_ENT | 8; // // Release the Spectrum's 'J' key
							modMain.keyB_SPC = modMain.keyB_SPC | 2;
						} // // Release the Symbol Shift key

						break;
					}

				case 186: // ;/: keys
					{
						if (down)
						{
							if (CAPS)
							{
								modMain.keyCAPS_V = modMain.keyCAPS_V & ~2;
							}
							else
							{
								modMain.keyY_P = modMain.keyY_P & ~2;
							}

							modMain.keyB_SPC = modMain.keyB_SPC & ~2;
							modMain.keyCAPS_V = modMain.keyCAPS_V | 1;
						}
						else
						{
							modMain.keyCAPS_V = modMain.keyCAPS_V | 2;
							modMain.keyY_P = modMain.keyY_P | 2;
							modMain.keyB_SPC = modMain.keyB_SPC | 2;
						}

						break;
					}

				default:
					{
						doKeyRet = false;
						break;
					}
			}

			doKeyRet = true;
			return doKeyRet;
		}

		public static void Hook_LDBYTES()
		{
			int l;
			if (modTAP.LoadTAP(ref modMain.glMemAddrDiv256[modMain.regAF_], ref modMain.regIX, ref modMain.regDE))
			{
				modMain.regAF_ = modMain.regAF_ | 64; // // Congraturation Load Sucsess!
			}
			else
			{
				modMain.regAF_ = modMain.regAF_ & 190;
			} // // Load failed

			l = modZ80.getAF();
			modZ80.setAF(modMain.regAF_);
			modMain.regAF_ = l;
			modMain.regPC = 1506;
		}

		public static void Hook_SABYTES()
		{
			modTAP.SaveTAPFileDlg();
		}

		public static void InitReverseBitValues()
		{
			lRevBitValues[0] = 128;
			lRevBitValues[1] = 64;
			lRevBitValues[2] = 32;
			lRevBitValues[3] = 16;
			lRevBitValues[4] = 8;
			lRevBitValues[5] = 4;
			lRevBitValues[6] = 2;
			lRevBitValues[7] = 1;
		}

		public static void plot(int addr)
		{
			int i, lne, X;
			if (addr < 22528)
			{
				// // Alter a pixel
				lne = modMain.glMemAddrDiv256[addr] & 0x7 | modMain.glMemAddrDiv4[addr] & 0x38 | modMain.glMemAddrDiv32[addr] & 0xC0;
				modMain.ScrnLines[lne, 32] = Conversions.ToInteger(true);
				modMain.ScrnLines[lne, addr & 31] = Conversions.ToInteger(true);
			}
			else
			{
				// // Alter an attribute
				lne = modMain.glMemAddrDiv32[addr - 22528];
				X = addr % 32;
				var loopTo = lne * 8 + 7;
				for (i = lne * 8; i <= loopTo; i++)
				{
					modMain.ScrnLines[i, 32] = Conversions.ToInteger(true);
					modMain.ScrnLines[i, X] = Conversions.ToInteger(true);
				}
			}

			if (modMain.glUseScreen >= 1000)
				modMain.ScrnNeedRepaint = Conversions.ToInteger(true);
		}

		public static int inb(int port)
		{
			int inbRet = default;
			var p = default(modMain.POINTAPI);
			bool bPortDefined;
			int lCounter;

			// MM JD
			// Init inb with the joystick values
			inbRet = JoystickInitIN(port);
			int res;
			if ((port & 0xFF) == 254)
			{
				res = 0xFF;
				if ((port & 0x8000) == 0)
				{
					res = res & modMain.keyB_SPC;
				}

				if ((port & 0x4000) == 0)
				{
					res = res & modMain.keyH_ENT;
				}

				if ((port & 0x2000) == 0)
				{
					res = res & modMain.keyY_P;
				}

				if ((port & 0x1000) == 0)
				{
					res = res & modMain.key6_0;
				}

				if ((port & 0x800) == 0)
				{
					res = res & modMain.key1_5;
				}

				if ((port & 0x400) == 0)
				{
					res = res & modMain.keyQ_T;
				}

				if ((port & 0x200) == 0)
				{
					res = res & modMain.keyA_G;
				}

				if ((port & 0x100) == 0)
				{
					res = res & modMain.keyCAPS_V;
				}

				if (inbRet != 0)
				{
					inbRet = inbRet & res & modMain.glKeyPortMask | modTZX.glEarBit; // glEarBit holds tape state (0 or 64 only)
				}
				else
				{
					inbRet = res & modMain.glKeyPortMask | modTZX.glEarBit;
				} // glEarBit holds tape state (0 or 64 only)

				modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
			}
			else if (port == 0xFFFD)
			{
				if ((modMain.glEmulatedModel & 3) != 0)
				{
					inbRet = modAY8912.AYPSG.Regs[modMain.glSoundRegister];
				}
			}
			else if ((port & 0xFF) == 0xFF)
			{
				if (modMain.glEmulatedModel == 5)
				{
					// // TC2048
					inbRet = modMain.glTC2048LastFFOut;
				}
				else if (modMain.glTStates >= modMain.glTStatesAtTop & modMain.glTStates <= modMain.glTStatesAtBottom)
				{
					inbRet = 0;
				}
				else
				{
					inbRet = 255;
				}
			}
			else if ((port & 0xFF) == 31)
			{
				if (modMain.glMouseType == modMain.MOUSE_AMIGA)
				{
					modMain.GetCursorPos(out p);
					if (p.X > lOldAmigaX)
					{
						lAmigaXPtr = lAmigaXPtr + 1;
						if (lAmigaXPtr == 4)
							lAmigaXPtr = 0;
					}
					else if (p.X < lOldAmigaX)
					{
						lAmigaXPtr = lAmigaXPtr - 1;
						if (lAmigaXPtr == -1)
							lAmigaXPtr = 3;
					}

					if (p.y < lOldAmigaY)
					{
						lAmigaYPtr = lAmigaYPtr + 1;
						if (lAmigaYPtr == 4)
							lAmigaYPtr = 0;
					}
					else if (p.y > lOldAmigaY)
					{
						lAmigaYPtr = lAmigaYPtr - 1;
						if (lAmigaYPtr == -1)
							lAmigaYPtr = 3;
					}

					lOldAmigaX = p.X;
					lOldAmigaY = p.y;
					inbRet = glAmigaMouseX[lAmigaXPtr] | glAmigaMouseY[lAmigaYPtr];
					if (Conversions.ToBoolean(modMain.gbMouseGlobal))
					{
						inbRet = inbRet | -Conversions.ToShort((modMain.GetKeyState(modMain.VK_LBUTTON) & 256) == 256) * 16 | -Conversions.ToShort((modMain.GetKeyState(modMain.VK_RBUTTON) & 256) == 256) * 32;
					}
					else
					{
						inbRet = inbRet | modMain.glMouseBtn * 16;
					}
				}
				else
				{
					// MM JD
					// The emulator does not need this line anymore
					// inb = 0&
				}
			}
			else if ((port & 4) == 0)
			{
				inbRet = ZXPrinterIn();
			}
			// // Kempston Mouse Interface
			else if (modMain.glMouseType == modMain.MOUSE_KEMPSTON)
			{
				modMain.GetCursorPos(out p);
				if (port == 64479)
				{
					inbRet = p.X % 256;
				}
				else if (port == 65503)
				{
					inbRet = (4000 - p.y) % 256;
				}
				else if (port == 64223)
				{
					if (Conversions.ToBoolean(modMain.gbMouseGlobal))
					{
						inbRet = 255 + Conversions.ToShort((modMain.GetKeyState(modMain.VK_RBUTTON) & 256) == 256);
						inbRet = inbRet + Conversions.ToShort((modMain.GetKeyState(modMain.VK_LBUTTON) & 256) == 256) * 2;
					}
					else
					{
						inbRet = 255 - (short)(modMain.glMouseBtn & 1) * 2;
						inbRet = inbRet - (modMain.glMouseBtn & 2) / 2;
					}
				}
			}
			// // Unconnected port
			else if (modMain.glTStates >= modMain.glTStatesAtTop & modMain.glTStates <= modMain.glTStatesAtBottom & modMain.glEmulatedModel != 5)
			{
			}
			// If inb is not 0 here, this means, that that is a joystick port an this port
			// is connected. Othervise is inb alread 0, so the Speccy does not need this
			// line anymore
			// inb = 0& '// This should return a floating bus value, but zero suffices
			// '// for commericial games that depend on the floating bus such
			// '// as Cobra and Arkanoid
			// Only if the port ist'n the actual joystick port
			// IF the actual port is a valid joystick port and there is some move on the
			// joystick, the inb is not 0.
			else if (inbRet == 0)
			{
				inbRet = 255;
			}

			return inbRet;
		}

		public static void outb(int port, int outbyte)
		{
			if ((port & 1) == 0)
			{
				modMain.glLastFEOut = outbyte & 0xFF;
				if (modMain.glUseScreen != 1006)
				{
					glNewBorder = modMain.glNormalColor[outbyte & 0x7];
				}

				if (Conversions.ToBoolean(outbyte & 16))
				{
					modMain.glBeeperVal = 159;
				}
				else
				{
					modMain.glBeeperVal = 128;
				}

				modMain.glTStates = modMain.glTStates + modMain.glContentionTable[-modMain.glTStates + 30];
				return;
			}
			else if (modMain.glMemPagingType != 0)
			{
				// // 128/+2 memory page operation
				if ((port & 32770) == 0)
				{
					// // RAM page
					modMain.glPageAt[3] = outbyte & 7;
					// // Screen page
					if (Conversions.ToBoolean(outbyte & 8))
					{
						if (modMain.glUseScreen == 5)
						{
							modMain.glUseScreen = 7;
							modMain.initscreen();
						}
					}
					else if (modMain.glUseScreen == 7)
					{
						modMain.glUseScreen = 5;
						modMain.initscreen();
					}
					// // ROM
					if (Conversions.ToBoolean(outbyte & 16))
					{
						modMain.glPageAt[0] = 9;
						modMain.glPageAt[4] = 9;
					}
					else
					{
						modMain.glPageAt[0] = 8;
						modMain.glPageAt[4] = 8;
					}

					if (Conversions.ToBoolean(outbyte & 32))
						modMain.glMemPagingType = 0;
					modMain.glLastOut7FFD = outbyte;
				}
				else if ((port & 0xC002) == 0xC000)
				{
					modMain.glSoundRegister = outbyte & 0xF;
					return;
				}
				else if ((port & 0xC002) == 0x8000)
				{
					modAY8912.AYWriteReg(ref modMain.glSoundRegister, ref outbyte);
					return;
					// ElseIf port = &HBEFD& Then
					// AYWriteReg glSoundRegister, outbyte
					// ElseIf port = &H1FFD& Then
					// // +2A/+3 special paging mode
				}
			}
			else if ((port & 0x4) == 0)
			{
				ZXPrinterOut(ref outbyte);
			}
			else if (modMain.glEmulatedModel == 5)
			{
				// // TC2048=May slow things down :(
				if ((port & 0xFF) == 0xFF)
				{
					modMain.glTC2048LastFFOut = outbyte & 0xFF;
					if ((outbyte & 7) == 0)
					{
						// // screen 0
						modMain.glUseScreen = 5;
						modMain.bmiBuffer.bmiHeader.biWidth = 256;
						glNewBorder = modMain.glNormalColor[modMain.glLastFEOut & 7];
					}
					else if ((outbyte & 7) == 1)
					{
						// // screen 1
						modMain.glUseScreen = 1001;
						modMain.bmiBuffer.bmiHeader.biWidth = 256;
						glNewBorder = modMain.glNormalColor[modMain.glLastFEOut & 7];
					}
					else if ((outbyte & 7) == 2)
					{
						// // hi-colour
						modMain.glUseScreen = 1002;
						modMain.bmiBuffer.bmiHeader.biWidth = 256;
						glNewBorder = modMain.glNormalColor[modMain.glLastFEOut & 7];
					}
					else if ((outbyte & 7) == 6)
					{
						// // hi-res
						modMain.glUseScreen = 1006;
						modMain.bmiBuffer.bmiHeader.biWidth = 512;
						if ((outbyte & 56) == 0)
						{
							// // Black on white
							modMain.glTC2048HiResColour = 120;
							glNewBorder = modMain.glBrightColor[7];
						}
						else if ((outbyte & 56) == 8)
						{
							// // Blue on yellow
							modMain.glTC2048HiResColour = 113;
							glNewBorder = modMain.glBrightColor[6];
						}
						else if ((outbyte & 56) == 16)
						{
							// // Red on cyan
							modMain.glTC2048HiResColour = 106;
							glNewBorder = modMain.glBrightColor[5];
						}
						else if ((outbyte & 56) == 24)
						{
							// // Magenta on green
							modMain.glTC2048HiResColour = 99;
							glNewBorder = modMain.glBrightColor[4];
						}
						else if ((outbyte & 56) == 32)
						{
							// // Green on magenta
							modMain.glTC2048HiResColour = 92;
							glNewBorder = modMain.glBrightColor[3];
						}
						else if ((outbyte & 56) == 40)
						{
							// // Cyan on red
							modMain.glTC2048HiResColour = 85;
							glNewBorder = modMain.glBrightColor[2];
						}
						else if ((outbyte & 56) == 48)
						{
							// // Yellow on blue
							modMain.glTC2048HiResColour = 78;
							glNewBorder = modMain.glBrightColor[1];
						}
						else if ((outbyte & 56) == 56)
						{
							// // White on black
							modMain.glTC2048HiResColour = 71;
							glNewBorder = modMain.glBrightColor[0];
						}
					}

					modMain.initscreen();
				}
			}
		}

		public static void plotTC2048HiResHiArea(int addr)
		{
			int lne;

			// // Alter a pixel in the higher screen (odd columns)
			lne = modMain.glMemAddrDiv256[addr] & 0x7 | modMain.glMemAddrDiv4[addr] & 0x38 | modMain.glMemAddrDiv32[addr] & 0xC0;
			modMain.ScrnLines[lne, 64] = Conversions.ToInteger(true);
			modMain.ScrnLines[lne, (short)(addr & 31) * 2 + 1] = Conversions.ToInteger(true);
			modMain.ScrnNeedRepaint = Conversions.ToInteger(true);
		}

		public static void plotTC2048HiResLowArea(int addr)
		{
			int lne;

			// // Alter a pixel in the lower screen (even columns)
			lne = modMain.glMemAddrDiv256[addr] & 0x7 | modMain.glMemAddrDiv4[addr] & 0x38 | modMain.glMemAddrDiv32[addr] & 0xC0;
			modMain.ScrnLines[lne, 64] = Conversions.ToInteger(true);
			modMain.ScrnLines[lne, (short)(addr & 31) * 2] = Conversions.ToInteger(true);
			modMain.ScrnNeedRepaint = Conversions.ToInteger(true);
		}

		public static void refreshFlashChars()
		{
			if (modMain.glUseScreen > 8)
			{
				TC2048refreshFlashChars();
				return;
			}

			int lne, addr, i;
			modMain.bFlashInverse = ~modMain.bFlashInverse;
			for (addr = 6144; addr <= 6911; addr++)
			{
				if (Conversions.ToBoolean(modMain.gRAMPage[modMain.glUseScreen, addr] & 128))
				{
					lne = modMain.glMemAddrDiv32[addr - 6144];
					var loopTo = lne * 8 + 7;
					for (i = lne * 8; i <= loopTo; i++)
					{
						modMain.ScrnLines[i, 32] = Conversions.ToInteger(true);
						modMain.ScrnLines[i, addr & 31] = Conversions.ToInteger(true);
					}
				}
			}
		}

		public static void ScanlinePaint(int lne)
		{
			if (modMain.glUseScreen >= 1000)
				return;

			// UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			int abyte, X, lLneIndex, lColIndex, sbyte_Renamed, lIndex;
			if (modMain.ScrnLines[lne, 32] == Conversions.ToInteger(true))
			{
				if (lne < modMain.glTopMost)
					modMain.glTopMost = lne;
				if (lne > modMain.glBottomMost)
					modMain.glBottomMost = lne;
				lLneIndex = modMain.glRowIndex[lne];
				lColIndex = modMain.glColIndex[lne];
				for (X = 0; X <= 31; X++)
				{
					if (modMain.ScrnLines[lne, X] == Conversions.ToInteger(true))
					{
						if (X < modMain.glLeftMost)
							modMain.glLeftMost = X;
						if (X > modMain.glRightMost)
							modMain.glRightMost = X;
						sbyte_Renamed = modMain.gRAMPage[modMain.glUseScreen, modMain.glScreenMem[lne, X]];
						abyte = modMain.gRAMPage[modMain.glUseScreen, lLneIndex + X];
						if (Conversions.ToBoolean(abyte & 128 & modMain.bFlashInverse))
						{
							// // Swap fore- and back-colours
							abyte = abyte ^ 128;
						}

						lIndex = lColIndex + X + X;
						modMain.glBufferBits[lIndex] = modMain.gtBitTable[sbyte_Renamed, abyte].dw0;
						modMain.glBufferBits[lIndex + 1] = modMain.gtBitTable[sbyte_Renamed, abyte].dw1;
						modMain.ScrnLines[lne, X] = Conversions.ToInteger(false);
					}
				}

				modMain.ScrnLines[lne, 32] = Conversions.ToInteger(false); // // Flag indicates this line has been rendered on the bitmap
				modMain.ScrnNeedRepaint = Conversions.ToInteger(true);
			}
		}

		public static void screenPaint()
		{
			// // Only update screen if necessary
			if (modMain.ScrnNeedRepaint == Conversions.ToInteger(false))
				return;

			// // TC2048=May slow things down :(
			if (modMain.glUseScreen >= 1000)
			{
				TC2048screenPaint();
				return;
			}

			modMain.glLeftMost = modMain.glLeftMost * 8;
			modMain.glRightMost = modMain.glRightMost * 8;
			modMain.gpicDisplay.Hide();
			int i;
			i = Marshal.SizeOf(modMain.glBufferBits[0]) * modMain.glBufferBits.Length;
			IntPtr p;
			p = Marshal.AllocHGlobal(i);
			Marshal.Copy(modMain.glBufferBits, 0, p, modMain.glBufferBits.Length);
			modMain.gpicDisplay.Size = new Size(modMain.bmiBuffer.bmiHeader.biWidth, modMain.bmiBuffer.bmiHeader.biHeight);

			// gpicGraphics = gpicDisplay.CreateGraphics()
			// gpicDC = gpicGraphics.GetHdc()

			// StretchDIBits(gpicDC,
			// glLeftMost * glDisplayXMultiplier,
			// (glBottomMost + 1) * glDisplayYMultiplier - 1,
			// (glRightMost - glLeftMost + 8) * glDisplayXMultiplier,
			// -(glBottomMost - glTopMost + 1) * glDisplayYMultiplier,
			// glLeftMost, glTopMost,
			// (glRightMost - glLeftMost) + 8,
			// glBottomMost - glTopMost + 1,
			// glBufferBits,
			// bmiBuffer,
			// DIB_RGB_COLORS, SRCCOPY)

			// 'StretchDIBits(gpicDC,
			// '			  glLeftMost * glDisplayXMultiplier,
			// '			  (glBottomMost + 1) * glDisplayYMultiplier - 1,
			// '			  (glRightMost - glLeftMost + 8) * glDisplayXMultiplier,
			// '			  -(glBottomMost - glTopMost + 1) * glDisplayYMultiplier,
			// '			  glLeftMost, glTopMost,
			// '			  (glRightMost - glLeftMost) + 8,
			// '			  glBottomMost - glTopMost + 1,
			// 'p,
			// 'bmiBuffer,
			// 'DIB_RGB_COLORS, SRCCOPY)

			// gpicGraphics.ReleaseHdc(gpicDC)

			// Dim flag As Bitmap
			// Dim flagGraphics As Graphics
			// flag = New Bitmap(200, 100)
			// Dim red As Integer
			// Dim white As Integer
			// flagGraphics = Graphics.FromImage(flag)
			// red = 0
			// white = 11
			// While white <= 100
			// flagGraphics.FillRectangle(Brushes.Red, 0, red, 200, 10)
			// flagGraphics.FillRectangle(Brushes.White, 0, white, 200, 10)
			// red += 20
			// white += 20
			// End While
			// gpicDisplay.Image = flag

			// Dim b(glBufferBits.Length * 4) As Byte
			// For i = 0 To glBufferBits.Length - 1
			// b(i * 4 + 0) = glBufferBits(i) Mod 256
			// b(i * 4 + 1) = glBufferBits(i) / 256 Mod 256
			// b(i * 4 + 2) = glBufferBits(i) / 256 / 256 Mod 256
			// b(i * 4 + 3) = glBufferBits(i) / 256 / 256 / 256
			// Next
			// Dim pb As New System.IO.MemoryStream(b)
			// Dim gfx As Graphics
			// Dim bmp As Bitmap
			// gfx = Graphics.FromImage(Image.FromStream(pb))
			// bmp = New Bitmap(pb)
			// gpicDisplay.Image = bmp

			var bmp = new Bitmap(modMain.bmiBuffer.bmiHeader.biWidth, modMain.bmiBuffer.bmiHeader.biHeight, modMain.bmiBuffer.bmiHeader.biWidth, PixelFormat.Format8bppIndexed, p);
			int pp;
			ColorPalette cp;
			cp = bmp.Palette;
			for (pp = 0; pp <= 15; pp++)
				cp.Entries[pp] = Color.FromArgb(modMain.bmiBuffer.bmiColors[pp + 1].rgbReserved, modMain.bmiBuffer.bmiColors[pp].rgbRed, modMain.bmiBuffer.bmiColors[pp].rgbGreen, modMain.bmiBuffer.bmiColors[pp].rgbBlue);
			bmp.Palette = cp;
			modMain.gpicDisplay.Image = bmp;

			// Dim converter As New ImageConverter()
			// Dim img As Image
			// Converter.ConvertFrom(b)
			// img = DirectCast(converter.ConvertFrom(b), Image)
			// gpicDisplay.Image = img

			// Marshal.FreeHGlobal(p)

			modMain.gpicDisplay.Refresh();
			modMain.gpicDisplay.Show();
			modMain.glTopMost = 191;
			modMain.glBottomMost = 0;
			modMain.glLeftMost = 31;
			modMain.glRightMost = 0;
			modMain.ScrnNeedRepaint = Conversions.ToInteger(false);
		}

		private static void SetZXPrinterPixel(ref int X, ref int y)
		{
			int lElement;
			if (X == -1)
				X = 256;
			lElement = y * 32 + X / 8;
			modMain.gcZXPrinterBits[lElement] = (byte)(modMain.gcZXPrinterBits[lElement] | lRevBitValues[X % 8]);
		}

		public static void TC2048PaintHiRes()
		{
			int lne, X;
			// UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			int sbyte_Renamed;
			int lTopMost, lLeftMost, lRightMost, lBottomMost;

			// // Bob Woodring's (RGW) improvements to display speed (lookup table of colour values)
			int lIndex;
			int lLneIndex;
			int lColIndex;

			// gpicDisplay.Visible = False

			lTopMost = 191;
			lBottomMost = 0;
			lLeftMost = 63;
			lRightMost = 0;
			for (lne = 0; lne <= 191; lne++)
			{
				if (modMain.ScrnLines[lne, 64] == Conversions.ToInteger(true))
				{
					if (lne < lTopMost)
						lTopMost = lne;
					if (lne > lBottomMost)
						lBottomMost = lne;
					// // RGW: Get line and column indexes from a lookup table for speed
					lLneIndex = modMain.glRowIndex[lne];
					lColIndex = modMain.glColIndex[lne] * 2;
					for (X = 0; X <= 63; X++)
					{
						if (modMain.ScrnLines[lne, X] == Conversions.ToInteger(true))
						{
							if (X < lLeftMost)
								lLeftMost = X;
							if (X > lRightMost)
								lRightMost = X;
							sbyte_Renamed = modMain.gRAMPage[5, modMain.glScreenMemTC2048HiRes[lne, X]];
							lIndex = lColIndex + X + X;
							modMain.glBufferBits[lIndex] = modMain.gtBitTable[sbyte_Renamed, modMain.glTC2048HiResColour].dw0;
							modMain.glBufferBits[lIndex + 1] = modMain.gtBitTable[sbyte_Renamed, modMain.glTC2048HiResColour].dw1;
							modMain.ScrnLines[lne, X] = Conversions.ToInteger(false);
						}
					}

					modMain.ScrnLines[lne, 64] = Conversions.ToInteger(false);
				}
			}

			lLeftMost = lLeftMost * 8;
			lRightMost = lRightMost * 8;
			var tmp = modMain.glBufferBits;
			object arglpBits = tmp[0];
			modMain.StretchDIBits(modMain.gpicDC, (int)(lLeftMost * (modMain.glDisplayXMultiplier / 2d)), (lBottomMost + 1) * modMain.glDisplayYMultiplier - 1, (int)((lRightMost - lLeftMost + 8) * (modMain.glDisplayXMultiplier / 2d)), -(lBottomMost - lTopMost + 1) * modMain.glDisplayYMultiplier, lLeftMost, lTopMost, lRightMost - lLeftMost + 8, lBottomMost - lTopMost + 1, arglpBits, ref modMain.bmiBuffer, modMain.DIB_RGB_COLORS, modMain.SRCCOPY);
			modMain.gpicDisplay.Refresh();
			// gpicDisplay.Visible = True
			modMain.ScrnNeedRepaint = Conversions.ToInteger(false);
		}

		public static void TC2048refreshFlashChars()
		{
			int lScrn, lne, addr, i, lOffset = default;
			if (modMain.glUseScreen == 1006)
				return;
			if (modMain.glUseScreen == 1001)
			{
				lOffset = 8192;
			}
			else if (modMain.glUseScreen == 1002)
			{
				// // HiColour
				modMain.bFlashInverse = ~modMain.bFlashInverse;
				for (addr = 8192; addr <= 14335; addr++)
				{
					if (Conversions.ToBoolean(modMain.gRAMPage[5, addr] & 128))
					{
						lne = modMain.glMemAddrDiv32[addr - 8192];
						var loopTo = lne * 8 + 7;
						for (i = lne * 8; i <= loopTo; i++)
						{
							modMain.ScrnLines[i, 32] = Conversions.ToInteger(true);
							modMain.ScrnLines[i, addr & 31] = Conversions.ToInteger(true);
						}

						modMain.ScrnNeedRepaint = Conversions.ToInteger(true);
					}
				}

				return;
			}

			modMain.bFlashInverse = ~modMain.bFlashInverse;
			for (addr = 6144; addr <= 6911; addr++)
			{
				if (Conversions.ToBoolean(modMain.gRAMPage[5, addr + lOffset] & 128))
				{
					lne = modMain.glMemAddrDiv32[addr - 6144];
					var loopTo1 = lne * 8 + 7;
					for (i = lne * 8; i <= loopTo1; i++)
					{
						modMain.ScrnLines[i, 32] = Conversions.ToInteger(true);
						modMain.ScrnLines[i, addr & 31] = Conversions.ToInteger(true);
					}

					modMain.ScrnNeedRepaint = Conversions.ToInteger(true);
				}
			}
		}

		public static void TC2048screenPaint()
		{
			if (modMain.glUseScreen == 1001)
			{
				TC2048ScreenPaintScrn1();
			}
			else if (modMain.glUseScreen == 1002)
			{
				TC2048PaintHiColour();
			}
			else if (modMain.glUseScreen == 1006)
			{
				TC2048PaintHiRes();
			}
		}

		public static void TC2048PaintHiColour()
		{
			int lne, X;
			// UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			int sbyte_Renamed, abyte;
			int lTopMost, lLeftMost, lRightMost, lBottomMost;

			// // Bob Woodring's (RGW) improvements to display speed (lookup table of colour values)
			int lIndex;
			int lLneIndex;
			int lColIndex;

			// gpicDisplay.Visible = False

			lTopMost = 191;
			lBottomMost = 0;
			lLeftMost = 31;
			lRightMost = 0;
			for (lne = 0; lne <= 191; lne++)
			{
				if (modMain.ScrnLines[lne, 32] == Conversions.ToInteger(true))
				{
					if (lne < lTopMost)
						lTopMost = lne;
					if (lne > lBottomMost)
						lBottomMost = lne;
					// // RGW: Get line and column indexes from a lookup table for speed
					lLneIndex = modMain.glRowIndex[lne];
					lColIndex = modMain.glColIndex[lne];
					for (X = 0; X <= 31; X++)
					{
						if (modMain.ScrnLines[lne, X] == Conversions.ToInteger(true))
						{
							if (X < lLeftMost)
								lLeftMost = X;
							if (X > lRightMost)
								lRightMost = X;

							// // All screen memory is in the bottom 16K of RAM (page 5)
							sbyte_Renamed = modMain.gRAMPage[5, modMain.glScreenMem[lne, X]];
							abyte = modMain.gRAMPage[5, modMain.glScreenMem[lne, X] + 8192];
							if (Conversions.ToBoolean(abyte & 128 & modMain.bFlashInverse))
							{
								// // Swap fore- and back-colours
								abyte = abyte ^ 128;
							}

							lIndex = lColIndex + X + X;
							modMain.glBufferBits[lIndex] = modMain.gtBitTable[sbyte_Renamed, abyte].dw0;
							modMain.glBufferBits[lIndex + 1] = modMain.gtBitTable[sbyte_Renamed, abyte].dw1;
							modMain.ScrnLines[lne, X] = Conversions.ToInteger(false);
						}
					}

					modMain.ScrnLines[lne, 32] = Conversions.ToInteger(false);
				}
			}

			lLeftMost = lLeftMost * 8;
			lRightMost = lRightMost * 8;
			var tmp = modMain.glBufferBits;
			object arglpBits = tmp[0];
			modMain.StretchDIBits(modMain.gpicDC, lLeftMost * modMain.glDisplayXMultiplier, (lBottomMost + 1) * modMain.glDisplayYMultiplier - 1, (lRightMost - lLeftMost + 8) * modMain.glDisplayXMultiplier, -(lBottomMost - lTopMost + 1) * modMain.glDisplayYMultiplier, lLeftMost, lTopMost, lRightMost - lLeftMost + 8, lBottomMost - lTopMost + 1, arglpBits, ref modMain.bmiBuffer, modMain.DIB_RGB_COLORS, modMain.SRCCOPY);
			modMain.gpicDisplay.Refresh();
			// gpicDisplay.Visible = True
			modMain.ScrnNeedRepaint = Conversions.ToInteger(false);
		}

		public static void TC2048ScreenPaintScrn1()
		{
			int lne, X;
			// UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			int sbyte_Renamed, abyte;
			int lTopMost, lLeftMost, lRightMost, lBottomMost;

			// // Bob Woodring's (RGW) improvements to display speed (lookup table of colour values)
			int lIndex;
			int lLneIndex;
			int lColIndex;
			lTopMost = 191;
			lBottomMost = 0;
			lLeftMost = 31;
			lRightMost = 0;
			for (lne = 0; lne <= 191; lne++)
			{
				if (modMain.ScrnLines[lne, 32] == Conversions.ToInteger(true))
				{
					if (lne < lTopMost)
						lTopMost = lne;
					if (lne > lBottomMost)
						lBottomMost = lne;
					// // RGW: Get line and column indexes from a lookup table for speed
					lLneIndex = modMain.glRowIndex[lne];
					lColIndex = modMain.glColIndex[lne];
					for (X = 0; X <= 31; X++)
					{
						if (modMain.ScrnLines[lne, X] == Conversions.ToInteger(true))
						{
							if (X < lLeftMost)
								lLeftMost = X;
							if (X > lRightMost)
								lRightMost = X;

							// // All screen memory is in the bottom 16K of RAM (page 5)
							sbyte_Renamed = modMain.gRAMPage[5, modMain.glScreenMem[lne, X] + 8192];
							abyte = modMain.gRAMPage[5, lLneIndex + X + 8192];
							if (Conversions.ToBoolean(abyte & 128 & modMain.bFlashInverse))
							{
								// // Swap fore- and back-colours
								abyte = abyte ^ 128;
							}

							lIndex = lColIndex + X + X;
							modMain.glBufferBits[lIndex] = modMain.gtBitTable[sbyte_Renamed, abyte].dw0;
							modMain.glBufferBits[lIndex + 1] = modMain.gtBitTable[sbyte_Renamed, abyte].dw1;
							modMain.ScrnLines[lne, X] = Conversions.ToInteger(false);
						}
					}

					modMain.ScrnLines[lne, 32] = Conversions.ToInteger(false);
				}
			}

			lLeftMost = lLeftMost * 8;
			lRightMost = lRightMost * 8;
			var tmp = modMain.glBufferBits;
			object arglpBits = tmp[0];
			modMain.StretchDIBits(modMain.gpicDC, lLeftMost * modMain.glDisplayXMultiplier, (lBottomMost + 1) * modMain.glDisplayYMultiplier - 1, (lRightMost - lLeftMost + 8) * modMain.glDisplayXMultiplier, -(lBottomMost - lTopMost + 1) * modMain.glDisplayYMultiplier, lLeftMost, lTopMost, lRightMost - lLeftMost + 8, lBottomMost - lTopMost + 1, arglpBits, ref modMain.bmiBuffer, modMain.DIB_RGB_COLORS, modMain.SRCCOPY);
			modMain.gpicDisplay.Refresh();
			modMain.ScrnNeedRepaint = Conversions.ToInteger(false);
		}

		private static int ZXPrinterIn()
		{
			int ZXPrinterInRet = default;
			// //  (64) D6 = 0 if ZXPrinter is present, else 1
			// // (128) D7 = 1 if the stylus in on the paper
			// //   (1) D0 = 0/1 toggle from the encoder disk

			if (My.MyProject.Forms.frmZXPrinter.Visible == false)
			{
				// // Unconnected port
				ZXPrinterInRet = 64;
				return ZXPrinterInRet;
			}

			if (Conversions.ToBoolean(lZXPrinterMotorOn))
			{
				// // Flip the encoder disk bit
				if (lZXPrinterEncoder == 1)
					lZXPrinterEncoder = 0;
				else
					lZXPrinterEncoder = 1;
				// // For every 0>1 cycle of the encoder disk, draw a pixel if the stylus is on
				if (lZXPrinterEncoder == 0 | lZXPrinterX >= 257)
				{
					if (Conversions.ToBoolean(lZXPrinterStylusOn))
					{
						if (lZXPrinterX > 128)
						{
							int argX = lZXPrinterX - 2;
							SetZXPrinterPixel(ref argX, ref lZXPrinterY);
						}
						else
						{
							int argX1 = lZXPrinterX - 1;
							SetZXPrinterPixel(ref argX1, ref lZXPrinterY);
						}
					}

					lZXPrinterX = lZXPrinterX + 1;
				}
			}

			if (lZXPrinterX >= 0 & lZXPrinterX < 128)
			{
				// // Stylus 1 is over paper
				ZXPrinterInRet = 128 | lZXPrinterEncoder;
			}
			else if (lZXPrinterX == 128)
			{
				// // Stylus 1 has left the paper
				ZXPrinterInRet = lZXPrinterEncoder;
				lZXPrinterX = 129;
			}
			else if (lZXPrinterX > 128 & lZXPrinterX <= 256)
			{
				// // Stylus 2 is over paper
				ZXPrinterInRet = 128 | lZXPrinterEncoder;
			}
			else
			{
				// // Stylus 2 has left the paper, advance the paper one pixel row
				// // and set the position of Stylus 1 to the start of the next row
				lZXPrinterEncoder = 0;
				ZXPrinterInRet = lZXPrinterEncoder;
				lZXPrinterX = 0;
				lZXPrinterY = lZXPrinterY + 1;
				if (lZXPrinterY >= modMain.glZXPrinterBMPHeight)
				{
					modMain.glZXPrinterBMPHeight = lZXPrinterY + 32;
					Array.Resize(ref modMain.gcZXPrinterBits, modMain.glZXPrinterBMPHeight * 32 + 1);
					modMain.bmiZXPrinter.bmiHeader.biHeight = modMain.glZXPrinterBMPHeight;
				}

				if (lZXPrinterY % 8 == 0)
				{
					// // Every 8 rows, update the visible display
					if (My.MyProject.Forms.frmZXPrinter.picView.Height > lZXPrinterY)
					{
					}
					// UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
					// StretchDIBitsMono(frmZXPrinter.picView.hdc, 0, frmZXPrinter.picView.Height, 256, -lZXPrinterY - 1, 0, 0, 256, lZXPrinterY + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
					else
					{
						// UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
						// StretchDIBitsMono(frmZXPrinter.picView.hdc, 0, frmZXPrinter.picView.Height, 256, -frmZXPrinter.picView.Height - 1, 0, lZXPrinterY - frmZXPrinter.picView.Height, 256, frmZXPrinter.picView.Height + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
					}

					My.MyProject.Forms.frmZXPrinter.picView.Refresh();

					// // Set up the scroll bar properties for the visible display
					// // to allow scrolling back over the material printed so far
					if (lZXPrinterY > My.MyProject.Forms.frmZXPrinter.picView.Height)
					{
						My.MyProject.Forms.frmZXPrinter.vs.Minimum = My.MyProject.Forms.frmZXPrinter.picView.Height / 8;
						My.MyProject.Forms.frmZXPrinter.vs.Maximum = lZXPrinterY / 8 + My.MyProject.Forms.frmZXPrinter.vs.LargeChange - 1;
						My.MyProject.Forms.frmZXPrinter.vs.Value = lZXPrinterY / 8;
					}
					else
					{
						My.MyProject.Forms.frmZXPrinter.vs.Minimum = 0;
						My.MyProject.Forms.frmZXPrinter.vs.Maximum = 0 + My.MyProject.Forms.frmZXPrinter.vs.LargeChange - 1;
					}
				}
			}

			return ZXPrinterInRet;
		}

		private static void ZXPrinterOut(ref int b)
		{
			if (Conversions.ToBoolean(b & 4))
			{
				lZXPrinterMotorOn = Conversions.ToInteger(false);
			}
			else
			{
				lZXPrinterMotorOn = Conversions.ToInteger(true);
			}

			if (Conversions.ToBoolean(b & 128))
			{
				lZXPrinterStylusOn = Conversions.ToInteger(true);
			}
			else
			{
				lZXPrinterStylusOn = Conversions.ToInteger(false);
			}
		}


		// MM 03.02.2003 - BEGIN
		private static int PCJoystickToZXJoystick(int lPCJoystick, int lPCJoystickFire)
		{
			int PCJoystickToZXJoystickRet = default;

			// API return value
			int lResult;
			// Joystick capablities
			var uJoyCaps = default(JOYCAPS);
			// Extended joystick-info
			var uJoyInfoEx = default(JOYINFOEX);
			// Minimal Up-Value
			int lMinimalUp;
			// Minimal Down-Value
			int lMinimalDown;
			// Minimal Left-Value
			int lMinimalLeft;
			// Minimal Right-Value
			int lMinimalRight;
			// Middle X
			int lMiddleX;
			// Middle Y
			int lMiddleY;
			// Joystick-Value
			int lJoyRes;

			// Init
			PCJoystickToZXJoystickRet = 0;

			// vbSpec reads the joystick capabilities -- the user could theoretically
			// unplugg a joystick and plug another with another capabilities. This
			// code supports analog joysticks only -- I didn't have any digital :)
			lResult = joyGetDevCaps(lPCJoystick, ref uJoyCaps, Strings.Len(uJoyCaps));
			// The uJoyInfoEx will be preared to get the joystick informations
			// The lenght of the structure muss be transfered
			uJoyInfoEx.dwSize = Strings.Len(uJoyInfoEx);
			// All joystick information should be got
			uJoyInfoEx.dwFlags = Conversions.ToInteger(JOY_RETURNALL);
			// Joystickinfos will be loaded into uJoyInfoEx
			// Remarks: the joystick must be pluggen as Joystick1
			lResult = joyGetPosEx(lPCJoystick, ref uJoyInfoEx);
			// Calculate the middle X and y
			lMiddleX = (int)Conversion.Fix((uJoyCaps.wXmax - uJoyCaps.wXmin) / 2d);
			lMiddleY = (int)Conversion.Fix((uJoyCaps.wYmax - uJoyCaps.wYmin) / 2d);
			// Calculate minimal values for the directions
			lMinimalUp = (int)(lMiddleY - Conversion.Fix((uJoyCaps.wYmax - uJoyCaps.wYmin) / 4d));
			lMinimalDown = (int)(lMiddleY + Conversion.Fix((uJoyCaps.wYmax - uJoyCaps.wYmin) / 4d));
			lMinimalLeft = (int)(lMiddleX - Conversion.Fix((uJoyCaps.wXmax - uJoyCaps.wXmin) / 4d));
			lMinimalRight = (int)(lMiddleX + Conversion.Fix((uJoyCaps.wXmax - uJoyCaps.wXmin) / 4d));
			// Init joystick value
			lJoyRes = 0;
			// Up
			if (uJoyInfoEx.dwYpos < lMinimalUp)
			{
				lJoyRes = lJoyRes | zxjdUp;
			}
			// Down
			if (uJoyInfoEx.dwYpos > lMinimalDown)
			{
				lJoyRes = lJoyRes | zxjdDown;
			}
			// Left
			if (uJoyInfoEx.dwXpos < lMinimalLeft)
			{
				lJoyRes = lJoyRes | zxjdLeft;
			}
			// Right
			if (uJoyInfoEx.dwXpos > lMinimalRight)
			{
				lJoyRes = lJoyRes | zxjdRight;
			}
			// Fire
			if ((uJoyInfoEx.dwButtons & (int)Math.Pow(2d, lPCJoystickFire)) > 0)
			{
				lJoyRes = lJoyRes | zxjdFire;
			}
			// Button
			if (uJoyInfoEx.dwButtons != 0)
			{
				lJoyRes = lJoyRes | uJoyInfoEx.dwButtons * zxjdButtonBase;
			}
			// Return value
			PCJoystickToZXJoystickRet = lJoyRes;
			return PCJoystickToZXJoystickRet;
		}
		// MM 03.02.2003 - END

		// MM JD
		private static int JoystickInitIN(int port)
		{
			int JoystickInitINRet = default;

			// ZX-joystick position
			int lZXJoystickPosition;
			// Joystick value (as the result of IN)
			int lJoyRes;
			// Joystick definition
			JOYSTICK_DEFINITION jdAction;
			// Counter
			int lCounter;
			// TMP-Value
			int lTMP;
			string sTMP;
			// Result
			int lRes;

			// Initialise
			lRes = 0;

			// If Joystick 1 is valid then
			if (bJoystick1Valid)
			{
				// Get joystick position
				lZXJoystickPosition = PCJoystickToZXJoystick(JOYSTICKID1, (int)lPCJoystick1Fire);
				// Up
				if ((lZXJoystickPosition & zxjdUp) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_UP].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Down
				if ((lZXJoystickPosition & zxjdDown) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_DOWN].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Left
				if ((lZXJoystickPosition & zxjdLeft) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_LEFT].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Right
				if ((lZXJoystickPosition & zxjdRight) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_RIGHT].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Buttons
				if ((lZXJoystickPosition & 0xFFFFFFE0) > 0)
				{
					// Get button number
					lCounter = (int)((short)(lZXJoystickPosition & 0xFFFFFFE0) / 32d);
					lCounter = (int)(Math.Log(lCounter) / Math.Log(2d)) + 1;
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK1, (int)lPCJoystick1Is, JDT_BUTTON_BASE + lCounter].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Finish
				if (lZXJoystickPosition == 0)
				{
					if (lKey1 > 0)
					{
						lRes = 0;
						lKey1 = 0;
					}
				}
			}

			// If Joystick 2 is valid then
			if (bJoystick2Valid)
			{
				// Get joystick position
				lZXJoystickPosition = PCJoystickToZXJoystick(JOYSTICKID2, (int)lPCJoystick2Fire);
				// Up
				if ((lZXJoystickPosition & zxjdUp) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_UP].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Down
				if ((lZXJoystickPosition & zxjdDown) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_DOWN].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Left
				if ((lZXJoystickPosition & zxjdLeft) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_LEFT].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Right
				if ((lZXJoystickPosition & zxjdRight) > 0)
				{
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_RIGHT].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Buttons
				if ((lZXJoystickPosition & 0xFFFFFFE0) > 0)
				{
					// Get button number
					lCounter = (int)((short)(lZXJoystickPosition & 0xFFFFFFE0) / 32d);
					lCounter = (int)(Math.Log(lCounter) / Math.Log(2d)) + 1;
					// Get Joystick definition
					// UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					jdAction = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter];
					// If the joystick had been
					if (ActionDefined(ref jdAction))
					{
						// If the actual port is the up-port of the joystikc
						if ((port & 0xFF) == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter].lPort & string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter].sKey) | port == aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter].lPort & !string.IsNullOrEmpty(aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter].sKey))
						{
							// Set value
							lTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter].lValue;
							sTMP = aJoystikDefinitionTable[JDT_JOYSTICK2, (int)lPCJoystick2Is, JDT_BUTTON_BASE + lCounter].sKey;
							if (!string.IsNullOrEmpty(sTMP))
							{
								if (lRes == 0)
								{
									lRes = lTMP;
								}
								else
								{
									lRes = lRes & lTMP;
								}
							}
							else
							{
								lRes = lRes | lTMP;
							}

							lKey1 = 1;
						}
					}
				}
				// Finish
				if (lZXJoystickPosition == 0)
				{
					if (lKey1 > 0)
					{
						lRes = 0;
						lKey1 = 0;
					}
				}
			}

			// Return value
			JoystickInitINRet = lRes;
			return JoystickInitINRet;
		}

		// MM JD
		// Get if the actual action has been definet
		private static bool ActionDefined(ref JOYSTICK_DEFINITION jdAction)
		{
			bool ActionDefinedRet = default;
			// An action is defined if you have port-and-value-pair or a key
			ActionDefinedRet = jdAction.lPort != 0 & jdAction.lValue != 0;
			return ActionDefinedRet;
		}

		// MM JD
		// Translate key strokes in port and in values
		public static void KeyStrokeToPortValue(int lKeyCode, int lShift, ref string sKey, ref int lPort, ref int lValue)
		{

			// Initialise
			sKey = Constants.vbNullString;
			lPort = 0;
			lValue = 0;

			// Delete current definition
			if (lKeyCode == 46 & lShift == 0)
			{
				sKey = Constants.vbNullString;
				lPort = 0;
				lValue = 0;
				return;
			}
			// Symbol shift
			if (lKeyCode == 17 & lShift == 2)
			{
				sKey = "SYM";
				lPort = 32766;
				lValue = 189;
				return;
			}
			// Caps shift
			if (lKeyCode == 16 & lShift == 1)
			{
				sKey = "CAP";
				lPort = 65278;
				lValue = 190;
				return;
			}
			// Space
			if (lKeyCode == 32 & lShift == 0)
			{
				sKey = "SPA";
				lPort = 32766;
				lValue = 190;
				return;
			}
			// The normal keys
			sKey = Conversions.ToString((char)lKeyCode);
			// First row
			if (sKey == "1")
			{
				lPort = 63486;
				lValue = 190;
				return;
			}

			if (sKey == "2")
			{
				lPort = 63486;
				lValue = 189;
				return;
			}

			if (sKey == "3")
			{
				lPort = 63486;
				lValue = 187;
				return;
			}

			if (sKey == "4")
			{
				lPort = 63486;
				lValue = 183;
				return;
			}

			if (sKey == "5")
			{
				lPort = 63486;
				lValue = 175;
				return;
			}

			if (sKey == "6")
			{
				lPort = 61438;
				lValue = 175;
				return;
			}

			if (sKey == "7")
			{
				lPort = 61438;
				lValue = 183;
				return;
			}

			if (sKey == "8")
			{
				lPort = 61438;
				lValue = 187;
				return;
			}

			if (sKey == "9")
			{
				lPort = 61438;
				lValue = 189;
				return;
			}

			if (sKey == "0")
			{
				lPort = 61438;
				lValue = 190;
				return;
			}
			// Second row
			if (sKey == "Q")
			{
				lPort = 64510;
				lValue = 190;
				return;
			}

			if (sKey == "W")
			{
				lPort = 64510;
				lValue = 189;
				return;
			}

			if (sKey == "E")
			{
				lPort = 64510;
				lValue = 187;
				return;
			}

			if (sKey == "R")
			{
				lPort = 64510;
				lValue = 183;
				return;
			}

			if (sKey == "T")
			{
				lPort = 64510;
				lValue = 175;
				return;
			}

			if (sKey == "Y")
			{
				lPort = 57342;
				lValue = 175;
				return;
			}

			if (sKey == "U")
			{
				lPort = 57342;
				lValue = 183;
				return;
			}

			if (sKey == "I")
			{
				lPort = 57342;
				lValue = 187;
				return;
			}

			if (sKey == "O")
			{
				lPort = 57342;
				lValue = 189;
				return;
			}

			if (sKey == "P")
			{
				lPort = 57342;
				lValue = 190;
				return;
			}
			// Third row
			if (sKey == "A")
			{
				lPort = 65022;
				lValue = 190;
				return;
			}

			if (sKey == "S")
			{
				lPort = 65022;
				lValue = 189;
				return;
			}

			if (sKey == "D")
			{
				lPort = 65022;
				lValue = 187;
				return;
			}

			if (sKey == "F")
			{
				lPort = 65022;
				lValue = 183;
				return;
			}

			if (sKey == "G")
			{
				lPort = 65022;
				lValue = 175;
				return;
			}

			if (sKey == "H")
			{
				lPort = 49150;
				lValue = 175;
				return;
			}

			if (sKey == "J")
			{
				lPort = 49150;
				lValue = 183;
				return;
			}

			if (sKey == "K")
			{
				lPort = 49150;
				lValue = 187;
				return;
			}

			if (sKey == "L")
			{
				lPort = 49150;
				lValue = 189;
				return;
			}
			// Fourth row
			if (sKey == "Z")
			{
				lPort = 65278;
				lValue = 189;
				return;
			}

			if (sKey == "X")
			{
				lPort = 65278;
				lValue = 187;
				return;
			}

			if (sKey == "C")
			{
				lPort = 65278;
				lValue = 183;
				return;
			}

			if (sKey == "V")
			{
				lPort = 65278;
				lValue = 175;
				return;
			}

			if (sKey == "B")
			{
				lPort = 32766;
				lValue = 175;
				return;
			}

			if (sKey == "N")
			{
				lPort = 32766;
				lValue = 183;
				return;
			}

			if (sKey == "M")
			{
				lPort = 32766;
				lValue = 187;
				return;
			}
			// This key is not a Speccy-key
			sKey = Constants.vbNullString;
			lPort = 0;
			lValue = 0;
		}

		// MM JD
		// Translate ports and values in keystrokes
		public static void PortValueToKeyStroke(int lPort, int lValue, ref string sKey)
		{

			// Initialise
			sKey = Constants.vbNullString;

			// First row, left
			if (lPort == 63486)
			{
				if (lValue == 190)
				{
					sKey = "1";
					return;
				}

				if (lValue == 189)
				{
					sKey = "2";
					return;
				}

				if (lValue == 187)
				{
					sKey = "3";
					return;
				}

				if (lValue == 183)
				{
					sKey = "4";
					return;
				}

				if (lValue == 175)
				{
					sKey = "5";
					return;
				}
			}
			// First row, right
			if (lPort == 61438)
			{
				if (lValue == 190)
				{
					sKey = "0";
					return;
				}

				if (lValue == 189)
				{
					sKey = "9";
					return;
				}

				if (lValue == 187)
				{
					sKey = "8";
					return;
				}

				if (lValue == 183)
				{
					sKey = "7";
					return;
				}

				if (lValue == 175)
				{
					sKey = "6";
					return;
				}
			}
			// Second row, left
			if (lPort == 64510)
			{
				if (lValue == 190)
				{
					sKey = "Q";
					return;
				}

				if (lValue == 189)
				{
					sKey = "W";
					return;
				}

				if (lValue == 187)
				{
					sKey = "E";
					return;
				}

				if (lValue == 183)
				{
					sKey = "R";
					return;
				}

				if (lValue == 175)
				{
					sKey = "T";
					return;
				}
			}
			// Second row, right
			if (lPort == 57342)
			{
				if (lValue == 190)
				{
					sKey = "P";
					return;
				}

				if (lValue == 189)
				{
					sKey = "O";
					return;
				}

				if (lValue == 187)
				{
					sKey = "I";
					return;
				}

				if (lValue == 183)
				{
					sKey = "U";
					return;
				}

				if (lValue == 175)
				{
					sKey = "Y";
					return;
				}
			}
			// Third row, left
			if (lPort == 65022)
			{
				if (lValue == 190)
				{
					sKey = "A";
					return;
				}

				if (lValue == 189)
				{
					sKey = "S";
					return;
				}

				if (lValue == 187)
				{
					sKey = "D";
					return;
				}

				if (lValue == 183)
				{
					sKey = "F";
					return;
				}

				if (lValue == 175)
				{
					sKey = "G";
					return;
				}
			}
			// Third row, right
			if (lPort == 49150)
			{
				if (lValue == 190)
				{
					sKey = "RET";
					return;
				}

				if (lValue == 189)
				{
					sKey = "L";
					return;
				}

				if (lValue == 187)
				{
					sKey = "K";
					return;
				}

				if (lValue == 183)
				{
					sKey = "J";
					return;
				}

				if (lValue == 175)
				{
					sKey = "H";
					return;
				}
			}
			// Fourth row, left
			if (lPort == 65278)
			{
				if (lValue == 190)
				{
					sKey = "CAP";
					return;
				}

				if (lValue == 189)
				{
					sKey = "Z";
					return;
				}

				if (lValue == 187)
				{
					sKey = "X";
					return;
				}

				if (lValue == 183)
				{
					sKey = "C";
					return;
				}

				if (lValue == 175)
				{
					sKey = "V";
					return;
				}
			}
			// Fourth row, right
			if (lPort == 32766)
			{
				if (lValue == 190)
				{
					sKey = "SPA";
					return;
				}

				if (lValue == 189)
				{
					sKey = "SYM";
					return;
				}

				if (lValue == 187)
				{
					sKey = "M";
					return;
				}

				if (lValue == 183)
				{
					sKey = "N";
					return;
				}

				if (lValue == 175)
				{
					sKey = "B";
					return;
				}
			}
			// That is not a key
			sKey = Constants.vbNullString;
		}
	}
}