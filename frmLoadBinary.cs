﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace vbSpec
{
	internal partial class frmLoadBinary : Form
	{
		// /*******************************************************************************
		// frmLoadBinary.frm within vbSpec.vbp
		// 
		// Author: Chris Cowley <ccowley@grok.co.uk>
		// 
		// Copyright (C)1999-2000 Grok Developments Ltd.
		// http://www.grok.co.uk/
		// 
		// This program is free software; you can redistribute it and/or
		// modify it under the terms of the GNU General Public License
		// as published by the Free Software Foundation; either version 2
		// of the License, or (at your option) any later version.
		// This program is distributed in the hope that it will be useful,
		// but WITHOUT ANY WARRANTY; without even the implied warranty of
		// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		// GNU General Public License for more details.
		// 
		// You should have received a copy of the GNU General Public License
		// along with this program; if not, write to the Free Software
		// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
		// 
		// *******************************************************************************/

		private void cmdCancel_Click(object eventSender, EventArgs eventArgs)
		{
			Hide();
		}

		private void cmdOK_Click(object eventSender, EventArgs eventArgs)
		{
			int lAddr, h;
			string s;
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 1616


			Input:

					On Error Resume Next

			 */
			if (Strings.Left(txtAddr.Text, 1) == "$")
			{
				lAddr = (int)Conversion.Val("&H" + Strings.Mid(txtAddr.Text, 2) + "&");
			}
			else if (cboBase.Text == "Hex")
			{
				lAddr = (int)Conversion.Val("&H" + txtAddr.Text + "&");
			}
			else
			{
				lAddr = (int)Conversion.Val(txtAddr.Text);
			}

			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (string.IsNullOrEmpty(FileSystem.Dir(txtFile.Text)))
			{
				Interaction.MsgBox(txtFile.Text + Constants.vbCrLf + Constants.vbCrLf + "File not found.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "vbSpec");
				return;
			}
			else
			{
				Information.Err().Clear();
				h = FileSystem.FreeFile();
				FileSystem.FileOpen(h, txtFile.Text, OpenMode.Binary);
				if (Information.Err().Number == 76)
				{
					FileSystem.FileClose(h);
					Interaction.MsgBox(txtFile.Text + Constants.vbCrLf + Constants.vbCrLf + "File not found.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "vbSpec");
					return;
				}

				if (FileSystem.LOF(h) + lAddr > 65536L)
				{
					if (Interaction.MsgBox(txtFile.Text + Constants.vbCrLf + Constants.vbCrLf + "File will overrun the 64K upper boundary." + Constants.vbCrLf + "Do you want to load the first " + (65536 - lAddr).ToString() + " bytes?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "vbSpec") == MsgBoxResult.No)
					{
						FileSystem.FileClose(h);
						return;
					}
				}

				// // Load as many bytes as will fit into the Z80 memory space
				if (FileSystem.LOF(h) > 65536L)
					s = modMain.InputString16(h, 65536);
				else
					s = modMain.InputString16(h, (int)FileSystem.LOF(h));
				if (lAddr + Strings.Len(s) > 65536)
					s = Strings.Left(s, 65536 - lAddr);
				FileSystem.FileClose(h);
				h = 1;
				do
				{
					modMain.gRAMPage[modMain.glPageAt[modMain.glMemAddrDiv16384[lAddr]], lAddr & 16383] = (byte)modMain.Asc(Strings.Mid(s, h, 1));
					lAddr = lAddr + 1;
					h = h + 1;
				}
				while (h <= Strings.Len(s));
			}

			Hide();

			// MM 23.04.2003
			// Bugfix - you will see instantly if you binary load a screen
			modMain.initscreen();
			modSpectrum.screenPaint();
		}

		private void cmdOpen_Click(object eventSender, EventArgs eventArgs)
		{
			;
//#error Cannot convert OnErrorResumeNextStatementSyntax - see comment for details
			/* Cannot convert OnErrorResumeNextStatementSyntax, CONVERSION ERROR: Conversion for OnErrorResumeNextStatement not implemented, please report this issue in 'On Error Resume Next' at character 5180


			Input:
					On Error Resume Next

			 */
			Information.Err().Clear();
			dlgCommonOpen.Title = "Open Binary File";
			dlgCommonOpen.DefaultExt = ".bin";
			dlgCommonOpen.FileName = "";
			// UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			dlgCommonOpen.Filter = "All Files|*.*";
			// UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.CheckFileExists which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			dlgCommonOpen.CheckFileExists = true;
			dlgCommonOpen.CheckPathExists = true;
			// UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			// UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			// UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			// dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			// dlgCommon.CancelError = True
			// UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			if (!string.IsNullOrEmpty(FileSystem.Dir(txtFile.Text)))
			{
				dlgCommonOpen.InitialDirectory = txtFile.Text;
			}

			dlgCommonOpen.ShowDialog();
			// UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			if (Information.Err().Number == (int)DialogResult.Cancel)
			{
				return;
			}

			if (!string.IsNullOrEmpty(dlgCommonOpen.FileName))
			{
				txtFile.Text = dlgCommonOpen.FileName;
			}
		}

		private void frmLoadBinary_Load(object eventSender, EventArgs eventArgs)
		{
			cboBase.Items.Add("Decimal");
			cboBase.Items.Add("Hex");
			cboBase.SelectedIndex = 0;
		}

		private void txtAddr_KeyPress(object eventSender, KeyPressEventArgs eventArgs)
		{
			short KeyAscii = (short)modMain.Asc(eventArgs.KeyChar);
			if (KeyAscii >= 97 & KeyAscii <= 102)
				KeyAscii = (short)(KeyAscii - 32);
			if (KeyAscii > 70)
				KeyAscii = 0;
			if (KeyAscii > 57 & KeyAscii < 65)
				KeyAscii = 0;
			if (KeyAscii > 64)
				cboBase.SelectedIndex = 1;
			eventArgs.KeyChar = (char)KeyAscii;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
		}
	}
}